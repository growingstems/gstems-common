# growingSTEMS Common Library
main: [![pipeline status](https://gitlab.com/growingstems/gstems-common/badges/main/pipeline.svg)](https://gitlab.com/growingstems/gstems-common/-/commits/main)<br>
[JavaDoc Here!](https://growingstems.gitlab.io/gstems-common/)<br><br>
Java library with general purpose functionality for robotics.<br>
Uses [Spotless](https://github.com/diffplug/spotless) for code formatting. See the [Spotless Gradle](vscode:extension/richardwillis.vscode-spotless-gradle) extension for auto-formatting in VS Code!<br>
For usage, see [this wiki page on GitLab](https://gitlab.com/growingstems/gstems-common/-/wikis/Normal%20Usage)
