/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.timer;

import org.growingstems.measurements.Measurements.Time;

/**
 * {@link TimeSource} implementation using Java's built-in {@link java.lang.System#nanoTime()}.
 * Precision: 1 nanosecond.
 */
public class PreciseJavaTimeSource implements TimeSource {
    /**
     * Gets the time using {@link java.lang.System#nanoTime()}
     *
     * @return System Time in seconds.
     */
    @Override
    public Time clockTime() {
        return Time.nanoseconds(System.nanoTime());
    }
}
