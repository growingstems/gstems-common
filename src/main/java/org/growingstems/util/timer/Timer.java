/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.timer;

import org.growingstems.measurements.Measurements.Time;

/**
 * Timer that can be used as a stopwatch, countdown timer, or periodic timer.<br>
 * <br>
 * Once constructed, the timer must be started to begin accumulating time.
 */
public class Timer {
    /** The source of time used for all timing calculations. */
    protected final TimeSource m_timeSource;

    /**
     * Last value reported by {@link TimeSource#clockTime()} when {@link #start()} or {@link #reset()}
     * was last called.
     */
    protected Time m_lastStartTime;

    /**
     * Total time accumulated used to calculate {@link #get()} correctly. Can be negative if
     * {@link #hasPeriodPassed(Time)} is called and {@link #stop} was never called.
     */
    protected Time m_accumulatedTime;

    /**
     * True if {@link #start()} was called more recently than {@link #stop()}.<br>
     * <br>
     * {@link #reset()} does not change this value.
     */
    protected boolean m_isRunning = false;

    /**
     * Construct a new Timer with no accumulated time and a specified time source.
     *
     * @param timeSource time source used to create the timer
     */
    protected Timer(TimeSource timeSource) {
        m_timeSource = timeSource;
        reset();
    }

    /**
     * Gets the current value of the timer.
     *
     * @return current time
     */
    public Time get() {
        if (m_isRunning) {
            Time currentTime = m_timeSource.clockTime();
            return m_accumulatedTime.add(currentTime.sub(m_lastStartTime));
        }

        return m_accumulatedTime;
    }

    /**
     * Resets the timer such that the value of the timer is 0.0 seconds.<br>
     * <br>
     * This method will not start or stop the timer!
     *
     * @return current time
     */
    public Time reset() {
        Time time = get();
        m_accumulatedTime = Time.ZERO;
        m_lastStartTime = m_timeSource.clockTime();
        return time;
    }

    /**
     * Starts the timer. The timer's value will begin increasing.<br>
     * <br>
     * No-op if timer is already running.
     *
     * @return this timer for chaining calls
     */
    public Timer start() {
        // No-op if already started
        if (m_isRunning) {
            return this;
        }

        m_lastStartTime = m_timeSource.clockTime();
        m_isRunning = true;
        return this;
    }

    /**
     * Stops the timer. The timer's value will stop increasing, but it will not be reset to 0.0
     * seconds.<br>
     * <br>
     * No-op if timer is already stopped.
     *
     * @return this timer for chaining calls
     */
    public Timer stop() {
        // No-op if already stopped
        if (!m_isRunning) {
            return this;
        }

        // Store new value for accumulated time until start is called again.
        m_accumulatedTime = get();
        m_isRunning = false;
        return this;
    }

    /**
     * Test if the timer is currently running.
     *
     * @return true if the timer is currently running
     */
    public boolean isRunning() {
        return m_isRunning;
    }

    /**
     * Test if the timer's value has surpassed a specific value.
     *
     * @param time the value that will be compared to the timer's value
     * @return true if the timer's value is greater than or equal to the provided value. Otherwise,
     *     false
     */
    public boolean hasElapsed(Time time) {
        return get().ge(time);
    }

    /**
     * Test if the timer's value has surpassed a specific value. <br>
     * As opposed to {@link #hasElapsed(org.growingstems.measurements.Measurements.Time)
     * hasElapsed(Time)}, if this method returns true, the value of the timer will decrease by the
     * input parameter's value.
     *
     * @param time the value that will be compared to the timer's value
     * @return true if the timer's value is greater than or equal to the provided value. Otherwise,
     *     false
     */
    public boolean hasPeriodPassed(Time time) {
        if (hasElapsed(time)) {
            m_accumulatedTime = m_accumulatedTime.sub(time);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Test if the timer's value has surpassed a specific value.<br>
     * <br>
     * As opposed to {@link #hasElapsed(org.growingstems.measurements.Measurements.Time)
     * hasElapsed(Time)} and {@link #hasPeriodPassed(org.growingstems.measurements.Measurements.Time)
     * hasPeriodPassed(Time)}, if the method returns true the timer stops.
     *
     * @param time the value being compared to the timer's value
     * @return true if the timer's value is greater than or equal to the provided value, or is already
     *     paused (successful stop). Otherwise, false
     */
    public boolean stopWhenElapsed(Time time) {
        // No-op if already stopped
        if (!m_isRunning) {
            return true;
        }

        if (hasElapsed(time)) {
            m_isRunning = false;
        }

        return !m_isRunning;
    }
}
