/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.timer;

import org.growingstems.measurements.Measurements.Time;

/**
 * Interface that offers {@link #clockTime()} as a way to get a time source for other classes that
 * require a concept of time.
 */
public interface TimeSource {
    /**
     * Get implementation-dependent time.
     *
     * @return The current time
     */
    Time clockTime();

    /**
     * Create a timer using this TimeSource
     *
     * @return a new Timer
     */
    default Timer createTimer() {
        return new Timer(this);
    }
}
