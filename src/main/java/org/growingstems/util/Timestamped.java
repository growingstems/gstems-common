/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;

/**
 * Generic container class used to store various data alongside a timestamp representing when the
 * data was collected.
 */
public class Timestamped<T> {
    private final T m_data;
    private final Time m_timestamp;

    /**
     * Stores the given data with an already generated timestamp.
     *
     * @param data the data to store.
     * @param timestamp the timestamp meant to represent when the data was collected.
     */
    public Timestamped(T data, Time timestamp) {
        this.m_data = data;
        this.m_timestamp = timestamp;
    }

    /**
     * Stores the given data while generating a timestamp for the data based on a given time source.
     *
     * @param data the data to store.
     * @param timeSource the time source to be used to generate the timestamp with.
     */
    public Timestamped(T data, TimeSource timeSource) {
        this.m_data = data;
        this.m_timestamp = timeSource.clockTime();
    }

    /**
     * Gets the stored data on construction.
     *
     * @return the data stored within.
     */
    public T getData() {
        return m_data;
    }

    /**
     * Gets the timestamp of the data.
     *
     * @return the timestamp of the data.
     */
    public Time getTimestamp() {
        return m_timestamp;
    }
}
