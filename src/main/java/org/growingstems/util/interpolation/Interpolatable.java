/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.interpolation;

/**
 * Interpolatable is an interface that defines how an object can perform a linear interpolation
 * operation and a liner extrapolation operation. This allows a user to be able to create a new data
 * point that lies on a line created by two known data points.
 *
 * <p>Depending on how the extrapolation is implemented, this can give varying results of usability
 * as the newly created data is nothing more than a prediction.
 */
public interface Interpolatable<T> {

    /**
     * The linear interpolate operation. Given an 'a' point (this) and a 'b' point, this method
     * returns a new data point where the new point lies on a line between the 'a' point and 'b' point
     * based on the interpoalation parameter (x).
     *
     * <ul>
     *   <li>An interpolation parameter of 0.0 will return a data point identical to the 'a' point.
     *   <li>An interpolation parameter of 0.5 will return a data point half-way between the 'a' point
     *       and 'b' point.
     *   <li>An interpolation parameter of 1.0 will return a data point identical to the 'b' point.
     * </ul>
     *
     * <p>'a' and 'b' are not ordered.
     *
     * @param b The B point.
     * @param x The interpolation parameter.
     * @return A new data point between the a point and b point based on the interpolation paramter.
     */
    public T linearInterpolate(T b, double x);

    /**
     * The linear extrapolate operation. Given an 'a' point (this) and a 'b' point, this method
     * returns a data point that lies on a line created by the 'a' point and 'b' point, and is outside
     * of these two points. Where this new point is created along this line is defined by the
     * expolation paramater 'x'.
     *
     * <p>An 'x' value equal to 0.0 will return the 'a' point and an 'x' value equal 1.0 will return
     * the b point. An 'x' value less than 0.0 means that the data point is beyond the 'a' point,
     * while a value greater than 1.0 means that the data point is beyond the 'b' point. 'a' and 'b'
     * are not ordered.
     *
     * @param b The B point.
     * @param x Extrapolation paramater. (-inf, 0.0] | [1.0, inf)
     * @return A new data point based on the a point (this) and the b point.
     */
    public T linearExtrapolate(T b, double x);
}
