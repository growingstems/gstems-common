/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.interpolation;

/**
 * InverseInterpolatable is an interface that defines how an object can perform an inverse linear
 * interpolation operation with itself. This allows a user to be able to "reverse-lookup" an
 * object's interpolation parameter, commonly refered to as 'x', based on an a point and a b point.
 * <br>
 * <br>
 * An example use for this interface is when given a key value between two known key values, this
 * method can return the interpolation paramater to be used to interpolate two values related to the
 * given known key values.
 */
public interface InverseInterpolatable<T> {

    /**
     * The inverse linear interpolate operation. Given an 'a' point (this) and a 'b' point, this
     * method returns where the 'other' point lies between the 'a' point and 'b' point. A returned
     * value of 0.0 means other lies on top of the 'a' point and a value of 1.0 means other lies on
     * top of the 'b' point.
     *
     * @param b The B point.
     * @param other Other point.
     * @return The interpolation parameter (x) that represents where the other point lies between the
     *     a and b points.
     */
    public double linearInverseInterpolate(T b, T other);
}
