/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.interpolation;

/**
 * InterpolatableLong is a container class for a long value that allows for it to be linearly
 * interpolated, linearly extrapolated and inverse linearly interpolated.
 */
public class InterpolatableLong extends InterpolatableValue<InterpolatableLong, Long>
        implements InverseInterpolatable<InterpolatableLong> {

    public InterpolatableLong(long value) {
        super(Long.valueOf(value));
    }

    @Override
    public double linearInverseInterpolate(InterpolatableLong upper, InterpolatableLong other) {
        double range = upper.m_value - this.m_value;
        double otherShifted = other.m_value - this.m_value;

        return otherShifted / range;
    }

    @Override
    public InterpolatableLong linearInterpolate(InterpolatableLong upper, double x) {
        long range = upper.m_value - this.m_value;
        long mid = Math.round(x * range);

        return new InterpolatableLong(mid + this.m_value);
    }

    @Override
    public InterpolatableLong linearExtrapolate(InterpolatableLong upper, double x) {
        /**
         * Extrapolate uses the interpolate method because the interpolate implementation can handle
         * extrapolation for this class.
         */
        return linearInterpolate(upper, x);
    }

    @Override
    public int compareTo(InterpolatableLong other) {
        if (this.m_value > other.m_value) {
            return 1;
        } else if (this.m_value < other.m_value) {
            return -1;
        } else {
            return 0;
        }
    }
}
