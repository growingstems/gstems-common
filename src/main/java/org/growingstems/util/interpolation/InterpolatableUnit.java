/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.interpolation;

import org.growingstems.measurements.Unit;

/**
 * InterpolatabeUnit is a class that is used to represent a generic measurements unit class that
 * allows for it to be linearly interpolated, linearly extrapolated and inverse linearly
 * interpolated.
 */
public class InterpolatableUnit<U extends Unit<U>>
        extends InterpolatableValue<InterpolatableUnit<U>, U>
        implements InverseInterpolatable<InterpolatableUnit<U>> {

    public InterpolatableUnit(U value) {
        super(value);
    }

    @Override
    public double linearInverseInterpolate(InterpolatableUnit<U> upper, InterpolatableUnit<U> other) {
        double range = upper.m_value.sub(this.m_value).getBaseValue();
        U otherShifted = other.m_value.sub(this.m_value);

        return otherShifted.div(range).getBaseValue();
    }

    @Override
    public InterpolatableUnit<U> linearInterpolate(InterpolatableUnit<U> upper, double x) {
        U range = upper.m_value.sub(this.m_value);
        return new InterpolatableUnit<U>(range.mul(x).add(this.m_value));
    }

    @Override
    public InterpolatableUnit<U> linearExtrapolate(InterpolatableUnit<U> upper, double x) {
        /**
         * Extrapolate uses the interpolate method because the interpolate implementation can handle
         * extrapolation for this class.
         */
        return linearInterpolate(upper, x);
    }

    @Override
    public int compareTo(InterpolatableUnit<U> other) {
        if (this.m_value.gt(other.m_value)) {
            return 1;
        } else if (this.m_value.lt(other.m_value)) {
            return -1;
        } else {
            return 0;
        }
    }
}
