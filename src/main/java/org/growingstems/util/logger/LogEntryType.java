/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.IOException;

/**
 * A Type to be used by a Log Entry for writing data to a sepcific DHL file.
 *
 * <p>Can represent either a Primitive Log Entry Type or a Group Log Entry Type.
 *
 * @param <T> The type of loggable data
 */
public abstract class LogEntryType<T> implements IOConsumer<T> {
    /** The Type ID, as defined by the DHL File Format. */
    protected final int typeId;
    /** A name representing this type of data. */
    protected final String name;
    /**
     * The default initial value to be used if a log entry is created with this type, and an initial
     * value is not provided.
     */
    protected final T defaultInitialValue;

    /**
     * Constructs a Log Entry Type given a known Type ID and Name.
     *
     * @param typeId The Type ID of the Log Entry Type
     * @param name The Name of the Log Entry Type
     * @param defaultInitialValue The default value that is initially logged and compared to in order
     *     to
     */
    protected LogEntryType(int typeId, String name, T defaultInitialValue) {
        this.typeId = typeId;
        this.name = name;
        this.defaultInitialValue = defaultInitialValue;
    }

    /**
     * Create a {@link LogEntryType} with a known {@link IOConsumer}.
     *
     * @param <T> The type writable by this {@link LogEntryType}
     * @param typeId The Type ID of the Log Entry Type
     * @param name The Name of the Log Entry Type
     * @param defaultInitialValue The default value that is initially logged and compared to in order
     *     to
     * @param writer A function which can write this type of data
     * @return A fully constructed {@link LogEntryType}
     */
    public static <T> LogEntryType<T> makeEntry(
            int typeId, String name, T defaultInitialValue, IOConsumer<T> writer) {
        return new LogEntryType<T>(typeId, name, defaultInitialValue) {
            @Override
            public void write(T t) throws IOException {
                writer.write(t);
            }
        };
    }

    /**
     * Gets the unique Log Entry Type ID.
     *
     * @return the Log Entry Type's ID
     */
    public int getTypeId() {
        return typeId;
    }
}
