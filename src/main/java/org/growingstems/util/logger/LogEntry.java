/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.util.function.Consumer;

/**
 * A Log Entry for a specific DHL File.<br>
 * Represents a loggable item of a specific type.
 *
 * @param <T> The type of data to be logged
 */
public abstract class LogEntry<T> implements Consumer<T> {
    /** The name of the Log Entry. */
    protected final String name;
    /** The Logger's {@link LogEntryType} which can write data to the file. */
    protected final LogEntryType<T> entryType;
    /** A handle to the Logger the entry belongs to. */
    protected DataHiveLogger logger = null;

    /**
     * Construct a Log Entry given the type required by all entries that extend this class.
     *
     * @param name The Log Entry name
     * @param entryType The type of entry
     */
    protected LogEntry(String name, LogEntryType<T> entryType) {
        this.name = name;
        this.entryType = entryType;
    }

    /**
     * Initialize the Log Entry with the data needed to write data to the file.
     *
     * @param logger The logger which owns this Log Entry
     */
    protected void init(DataHiveLogger logger) {
        this.logger = logger;
    }
}
