/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Optional;
import org.apache.commons.io.FilenameUtils;
import org.growingstems.math.RateCalculatorU;
import org.growingstems.measurements.Measurements.Data;
import org.growingstems.measurements.Measurements.DataRate;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Unit;
import org.growingstems.util.timer.TimeSource;

/**
 * File based logger built with Data Hive Logger. File logging is handled by a separate thread that
 * periodically saves logged data to a file.
 */
public class LogFileBuilder extends DataHiveLogger.Builder implements AutoCloseable {
    /** Settings used to control features of the file logger. */
    public static class Settings {
        /** The minimum size that the piped stream is allowed to be. This is not a recommended size. */
        public static final Data k_minimumPipeSize = Data.bytes(1024.0);
        /** The pipe size used when left unspecified */
        public static final Data k_defaultPipeSize = Data.bytes(65536.0);
        /** The write sleep time when left unspecified */
        public static final Time k_defaultWriteSleepTime = Time.milliseconds(250.0);
        /**
         * Max number of log files before a "_MAX" file is generated and re-used. Once the logger is at
         * its maximum count, the previous "_MAX" file will be overwritten whenever a new log file is
         * generated. If this behavior is not desired, assign this value to {@link Optional#empty()}.
         */
        public final Optional<Integer> maxFileAttempts;
        /**
         * How large the piped stream is that is used to transfer data into the log file. Generally
         * should be left as the default value.
         */
        public final Data pipeSize;
        /** How long the thread should wait before checking to dump data to the log file. */
        public final Time writeSleepTime;
        /**
         * How much data must be collected before writing all the collected data to the log file. If
         * this behavior is not desired, assign this value to {@link Optional#empty()}.
         */
        public final Optional<Data> dumpSize;

        /** LogFileBuilder default settings. */
        public Settings() {
            this(Optional.of(10), k_defaultPipeSize, k_defaultWriteSleepTime, Optional.empty());
        }

        /**
         * LogFileBuilder settings. See default constructor for default settings.
         *
         * @param maxFileAttempts Max number of log files before a "_MAX" file is generated and re-used.
         *     Once the logger is at its maximum count, the previous "_MAX" file will be overwritten
         *     whenever a new log file is generated. If this behavior is not desired, assign this value
         *     to {@link Optional#empty()}.
         * @param pipeSize How large the piped stream is that is used to transfer data into the log
         *     file. Generally should be left as the default value.
         * @param writeSleepTime How long the thread should wait before checking to dump data to the log
         *     file.
         * @param dumpSize How much data must be collected before writing all the collected data to the
         *     log file. If this behavior is not desired, assign this value to {@link Optional#empty()}.
         */
        public Settings(
                Optional<Integer> maxFileAttempts,
                Data pipeSize,
                Time writeSleepTime,
                Optional<Data> dumpSize) {
            this.maxFileAttempts = maxFileAttempts.orElse(0) > 0 ? maxFileAttempts : Optional.empty();
            this.pipeSize = Unit.max(pipeSize, k_minimumPipeSize);
            this.writeSleepTime = Unit.max(writeSleepTime, Time.ZERO);
            this.dumpSize = dumpSize.orElse(Data.ZERO).gt(Data.ZERO) ? dumpSize : Optional.empty();
        }
    }

    /** The directory where the user wishes to store their log files on the system. */
    protected final File m_logDir;
    /** Output stream provided to the {@link DataHiveLogger}. */
    protected final PipedOutputStream m_outputStream;
    /** The file handle for the log file. */
    protected File m_logFile;
    /** Settings used to control features of the file logger. */
    protected final Settings m_settings;
    /** Time source being used to keep track of log timestamps and for other functionalities. */
    protected final TimeSource m_timeSource;
    /** Thread that reads the piped data and writes it to the file. */
    protected Thread m_writeThread;
    /** Input stream used only in-thread except for closing the builder. */
    protected PipedInputStream m_inputStream;

    private volatile int m_lastTransferred = 0;

    private volatile int m_totalTransferred = 0;
    private volatile DataRate m_currentRate = DataRate.ZERO;
    private volatile boolean m_cancelLogging = false;

    /**
     * Creates a file based Data Hive Logger that will handle logging any data that is captured by the
     * Data Hive Logger system and store it on the system as file. The default settings from
     * {@link Settings#Settings()} are used to control the file logger thread.
     *
     * @param logDir The directory that the log files will be logged to
     * @param initialLogFileName The initial base log file name. The filename that actually gets
     *     logged will have extra characters appended to it based on the default settings from
     *     {@link Settings#Settings()}
     * @param description The description of the file that is stored in the log file's header
     * @param timeSource A time source to keep track of log timestamps and for other functionalities
     */
    public LogFileBuilder(
            File logDir, String initialLogFileName, String description, TimeSource timeSource) {
        this(
                logDir,
                initialLogFileName,
                description,
                new Settings(),
                timeSource,
                new PipedOutputStream());
    }

    /**
     * Creates a file based Data Hive Logger that will handle logging any data that is captured by the
     * Data Hive Logger system and store it on the system as file.
     *
     * @param logDir The directory that the log files will be logged to
     * @param initialLogFileName The initial base log file name. The filename that actually gets
     *     logged will have extra characters appended to it based on the settings that are provided
     * @param description The description of the file that is stored in the log file's header
     * @param settings The settings used to control the file logger thread
     * @param timeSource A time source to keep track of log timestamps and for other functionalities
     */
    public LogFileBuilder(
            File logDir,
            String initialLogFileName,
            String description,
            Settings settings,
            TimeSource timeSource) {
        this(logDir, initialLogFileName, description, settings, timeSource, new PipedOutputStream());
    }

    /**
     * Creates a file based Data Hive Logger that will handle logging any data that is captured by the
     * Data Hive Logger system and store it on the system as file.
     *
     * @param logDir The directory that the log files will be logged to
     * @param initialLogFileName The initial base log file name. The filename that actually gets
     *     logged will have extra characters appended to it based on the settings that are provided
     * @param description The description of the file that is stored in the log file's header
     * @param settings The settings used to control the file logger thread
     * @param timeSource A time source to keep track of log timestamps and for other functionalities
     * @param outputStream Output stream provided to the {@link DataHiveLogger}
     */
    protected LogFileBuilder(
            File logDir,
            String initialLogFileName,
            String description,
            Settings settings,
            TimeSource timeSource,
            PipedOutputStream outputStream) {
        super(outputStream, timeSource, description);

        m_logDir = logDir;
        m_settings = settings;
        m_timeSource = timeSource;

        m_outputStream = outputStream;
        m_logFile = deconflictFilename(initialLogFileName);
    }

    @Override
    public DataHiveLogger init() throws IOException {
        if (m_writeThread != null) {
            throw new IOException("Cannot initialize LogFileBuilder twice without closing in between.");
        }

        // Ensure log directory exists
        m_logDir.mkdir();

        // This stream needs to be construted fully before `super.init()` is called.
        m_inputStream =
                new PipedInputStream(m_outputStream, (int) Math.ceil(m_settings.pipeSize.asBytes()));

        // Make separate thread transfer data to writing stream
        m_writeThread = new Thread(() -> {
            RateCalculatorU<Data, DataRate> fileWriteRate =
                    new RateCalculatorU<>(m_timeSource, DataRate.ZERO);

            try (FileOutputStream fileStream = new FileOutputStream(m_logFile); ) {

                while (!m_cancelLogging) {
                    try {
                        int avail = m_inputStream.available();
                        if (Data.bytes(avail).gt(m_settings.dumpSize.orElseGet(() -> Data.ZERO))) {
                            m_lastTransferred = avail;
                            fileStream.write(m_inputStream.readNBytes(m_lastTransferred));

                            m_totalTransferred = m_totalTransferred + m_lastTransferred;
                            m_currentRate = fileWriteRate.update(Data.bytes(m_totalTransferred));
                        }
                    } catch (IOException e) {
                        System.err.println("Error occurred while writing: " + e.toString());
                    }

                    try {
                        Thread.sleep((long) m_settings.writeSleepTime.asMilliseconds());
                    } catch (InterruptedException e) {
                        System.err.println("Log thread interrupted while sleeping: " + e.toString());
                    }
                }
            } catch (IOException e) {
                System.err.println("Error occurred while logging: " + e.toString());
            }
        });
        m_writeThread.start();
        return super.init();
    }

    /**
     * Generates a file handle with the given log name. Path and file extensions are stripped from the
     * given log name if there are any. File extension is based on
     * {@link DataHiveLogger#k_fileExtension}.
     *
     * @param logName The log file name
     * @return A File object
     */
    protected File stringToFileHandle(String logName) {
        return new File(m_logDir, FilenameUtils.getBaseName(logName) + DataHiveLogger.k_fileExtension);
    }

    /**
     * Creates a File object for a new file with the given name. Path and file extensions are stripped
     * from the given log name if there are any.
     *
     * @param logName The log file name
     * @return A File object that is not already used (unless the max attempts are used)
     */
    protected File deconflictFilename(String logName) {
        int i = 0;
        File file = stringToFileHandle(logName);
        do {
            if (file.exists()) {
                file = stringToFileHandle(logName + "_" + Integer.toString(++i));
            } else {
                return file;
            }
        } while (i < m_settings.maxFileAttempts.orElseGet(() -> Integer.MAX_VALUE));
        return stringToFileHandle(logName + "_MAX");
    }

    /**
     * Renames the log file. Path and file extensions are stripped from the given log name if there
     * are any. File extension is *.dhl.
     *
     * <p>Warning: This function does not succeed on Windows if the file is currently open, and often
     * fails if the file was recently open!
     *
     * @param logName The extension log file name
     * @return True if the rename was successful
     */
    public boolean renameTo(String logName) {
        File newFile = deconflictFilename(logName);
        boolean success = m_logFile.renameTo(newFile);
        if (success) {
            m_logFile = newFile;
        }
        return success;
    }

    /**
     * The current name of the log file.
     *
     * @return The current log file name
     */
    public String getLogFilename() {
        return m_logFile.getName();
    }

    /**
     * Get how much data was last transferred by the thread.
     *
     * @return The last amount of data transferred to the log file
     */
    public Data getLastTransferred() {
        return Data.bytes(m_lastTransferred);
    }

    /**
     * The total data transferred to the file.
     *
     * @return The total data transferred to the log file
     */
    public int getTotalTransferred() {
        return m_totalTransferred;
    }

    /**
     * The current file transfer rate that was calculated by the log file writing thread.
     *
     * @return The current transfer rate
     */
    public DataRate getCurrentRate() {
        return m_currentRate;
    }

    @Override
    public void close() throws IOException {
        m_cancelLogging = true;
        try {
            m_writeThread.join();
            m_writeThread = null;
            m_inputStream.close();
            m_inputStream = null;
        } catch (InterruptedException e) {
            System.err.println("Error closing LogFileBuilder while joining thread: " + e.toString());
        }
    }
}
