/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;

/**
 * The {@link DataHiveLogger} represents a single Data Hive Log <a
 * href="https://growingstems.org/files/DataHiveLoggerV1.pdf">[link]</a> file (*.dhl), Version 1.
 *
 * <p>Since the DHL File Format requires all type data be known before the first message is logged,
 * it cannot be constructed directly. You must use the {@link Builder} class to define all Group
 * Entry Types and Log Entries. Invoking {@link Builder#init()} will then provide an initialized
 * {@link DataHiveLogger} for use. After {@link Builder#init()} is called, additional Group Entry
 * Types and Log Entries cannot be added.
 *
 * <p>Refer to {@link Builder} for more information on how to configure a {@link DataHiveLogger} and
 * on how to generate {@link LogEntry}s and custom log entry types.
 *
 * <p>After utilizing {@link Builder} to configure a {@link DataHiveLogger} and then generate said
 * logger via {@link Builder#init()}, use the {@link LogEntry} objects that were returned when
 * generating {@link LogEntry}s via {@link Builder} to log data during code execution. Updates to
 * {@link AsyncLogEntry}s are logged immediately, while updates to {@link SyncLogEntry}s will not be
 * logged until {@link DataHiveLogger#update()} is called.
 *
 * <p>It's up to the user implementing {@link DataHiveLogger} to decide how the {@code OutputStream}
 * passed into {@link Builder}, and ultimately used by {@link DataHiveLogger}, is utilized.
 *
 * @see <a href="https://growingstems.org/files/DataHiveLoggerV1.pdf">Data Hive Log File Format</a>
 *     <pre>{@code
 * // Custom class
 * public class Foo {
 *     private int m_a;
 *     private double m_b;
 *
 *     public Foo(int a, double b) {
 *         m_a = a;
 *         m_b = b;
 *     }
 *
 *     public int getA() {
 *         return m_a;
 *     }
 *
 *     public double getB() {
 *         return m_b;
 *     }
 * }
 *
 * // Custom class that utilizes another custom class
 * public class Bar {
 *     private boolean m_isTrue;
 *     private Foo m_foo;
 *
 *     public Bar(boolean isTrue, Foo foo) {
 *         m_isTrue = isTrue;
 *         m_foo = foo;
 *     }
 *
 *     public boolean isTrue() {
 *         return m_isTrue;
 *     }
 *
 *     public Foo getFoo() {
 *         return m_foo;
 *     }
 * }
 *
 * public enum ExampleEnum {
 *     A,
 *     B
 * }
 *
 * // Creating log builder
 * DataHiveLogger.Builder logBuilder = new DataHiveLogger.Builder(outStream, timeSource, fileDescription);
 *
 * // Example of registering a custom type
 * LogEntryType<Foo> fooType = logBuilder.<Foo>buildGroupType("Foo")
 *         .addMember("A", logBuilder.integerType, Foo::getA)
 *         .addMember("B", logBuilder.doubleType, Foo::getB)
 *         .register();
 *
 * // Example of registering a custom type that utilizes another custom type
 * // NOTE: The other custom type that is utilized by this custom type MUST be
 * // registered first or else an exception is thrown.
 * LogEntryType<Bar> barType = logBuilder.<Bar>buildGroupType("It's a Bar")
 *         .addMember("isTrue", logBuilder.booleanType, Bar::isTrue)
 *         .addMember("Bar's Foo", fooType, Bar::getFoo)
 *         .register();
 *
 * // Example of registering an enum
 * LogEntryType<ExampleEnum> enumType = logBuilder.registerEnum(ExampleEnum.class);
 *
 * // Example of synchronous and asynchronous log entries
 * SyncLogEntry<ExampleEnum> syncEnum = logBuilder.makeSyncLogEntry("Sync Enum", enumType, ExampleEnum.A);
 * AsyncLogEntry<Bar> asyncBar = logBuilder.makeAsyncLogEntry("Async Bar", barType);
 *
 * // Initialize the DataHiveLogger - Possibly throws IOException!
 * DataHiveLogger logger = logBuilder.init();
 *
 * // Log an asynchronous message - Logs to outStream immediately
 * asyncBar.accept(new Bar(true, new Foo(1, 2.3)));
 *
 * // Set synchronous data - Does not write to outStream
 * syncEnum.accept(ExampleEnum.B);
 *
 * // Write synchronous message - Immediately writes a synchronous log entry
 * // which contains an ExampleEnum set to B
 * logger.update();
 * }</pre>
 */
public class DataHiveLogger {
    /** The number of reserved Log Entry Type IDs. */
    protected static final int k_reservedTypeIdCount = 64;
    /** File extension that should be used when saving DHL files on a file system. */
    protected static final String k_fileExtension = ".dhl";

    /**
     * A {@link Builder} instance can be used to initialize a Data Hive Log.
     *
     * <p>Each {@link Builder} must only be used to initialize one Data Hive Log, because
     * {@link LogEntry}s and {@link LogEntryType}s are tied to a single log.
     *
     * <p>After constructing {@link Builder}, use the various methods within {@link Builder} to
     * generate both {@link SyncLogEntry}s and {@link AsyncLogEntry}s. {@link LogEntry}s are built
     * from Primitives, or Group Types that are created via {@link GroupTypeBuilder}. Primitives are
     * defined as:
     *
     * <ul>
     *   <li>{@code Byte}, {@link UnsignedByte}
     *   <li>{@code Short}, {@link UnsignedShort}
     *   <li>{@code Integer}, {@link UnsignedInt}
     *   <li>{@code Long}, {@link UnsignedLong}
     *   <li>{@code Float}
     *   <li>{@code Double}
     *   <li>{@code Boolean}
     *   <li>{@code String}
     * </ul>
     *
     * <p>Refer to {@link GroupTypeBuilder} on how to generate Group Types that can be used by Log
     * Entries.
     *
     * <p>Registering an enum should be done via {@link Builder#registerEnum(Class, Enum)} or
     * {@link Builder#registerEnum(Class, String, Enum)}.
     */
    public static class Builder {
        /**
         * A {@link GroupTypeBuilder} is used to define a new {@link GroupLogEntryType} in a Data Hive
         * Log.
         *
         * <p>Each non-Primitive Log Entry Type must be defined as a Group Type before a
         * {@link LogEntry} can use it. An exception is thrown if a class is registered as a group type
         * more than once.
         *
         * @param <T> The type being defined
         */
        public class GroupTypeBuilder<T> {
            /** The name to identify this type with. */
            protected final String name;
            /** The members that this type is composed of. */
            protected final List<GroupLogEntryType.Member<T>> members = new ArrayList<>();

            /**
             * Construct a {@link GroupTypeBuilder} which assists in defining a new
             * {@link GroupLogEntryType}.
             *
             * @param name The name of this type
             */
            protected GroupTypeBuilder(String name) {
                this.name = name;
            }

            /**
             * Defines a member of the Group Type.
             *
             * @param <U> The type of member to add
             * @param name The name of the member
             * @param memberType A {@link Class} object representing the type of member to add
             * @param getter A function which obtains an instance of the member from the Group Type
             * @return {@code this} - Useful for chaining calls
             */
            public <U> GroupTypeBuilder<T> addMember(
                    String name, LogEntryType<U> memberType, Function<T, U> getter) {
                members.add(new GroupLogEntryType.Member<>(
                        memberType.getTypeId(), name, v -> memberType.write(getter.apply(v))));
                return this;
            }

            /**
             * Defines an array-based member of the Group Type.
             *
             * @param <U> The component type of the array member
             * @param name The name of the array member
             * @param elementType A {@link Class} object representing the array type
             * @param getter A function which obtains an array Group Type
             * @return {@code this} - Useful for chaining calls
             */
            public <U> GroupTypeBuilder<T> addArrayMember(
                    String name, LogEntryType<U> elementType, Function<T, U[]> getter) {
                members.add(new GroupLogEntryType.ArrayMember<>(elementType.getTypeId(), name, t -> {
                    U[] array = getter.apply(t);
                    stream.writeShort(array.length);
                    for (U value : array) {
                        elementType.write(value);
                    }
                }));
                return this;
            }

            /**
             * Defines a list-based member of the Group Type.
             *
             * @param <U> The component type of the list member
             * @param name The name of the list member
             * @param elementType A {@link Class} object representing the list type
             * @param getter A function which obtains a list Group Type
             * @return {@code this} - Useful for chaining calls
             */
            public <U> GroupTypeBuilder<T> addListMember(
                    String name, LogEntryType<U> elementType, Function<T, List<U>> getter) {
                members.add(new GroupLogEntryType.ArrayMember<>(elementType.getTypeId(), name, t -> {
                    List<U> list = getter.apply(t);
                    stream.writeShort(list.size());
                    for (U value : list) {
                        elementType.write(value);
                    }
                }));
                return this;
            }

            /**
             * Finalizes the {@link GroupLogEntryType} and adds it to the {@link Builder}'s type map.
             *
             * <p>Once all members have been added, {@link GroupTypeBuilder#register(Object)} is used to
             * add the type to the {@link Builder}. Calling {@link GroupTypeBuilder#register(Object)}
             * twice on the same {@link GroupTypeBuilder} results in an exception.
             *
             * @param defaultInitialValue The default value that is initially logged and compared to in
             *     order to detect a change in value
             * @return The newly registered type
             */
            public GroupLogEntryType<T> register(T defaultInitialValue) {
                GroupLogEntryType<T> entryType =
                        new GroupLogEntryType<>(nextGroupTypeId++, name, defaultInitialValue, members);
                registerTypeId(entryType);
                return entryType;
            }
        }

        /**
         * List of no repeat async entries that wish to be initialized with a value when the logger is
         * created/initialized.
         */
        protected final List<Runnable> initNoRepeatAsyncEntryList = new ArrayList<>();

        /** Every Synchronous Log Entry added to the Data Hive Log. */
        protected final List<SyncLogEntry<?>> syncEntries = new ArrayList<>();
        /** Every Asynchronous Log Entry added to the Data Hive Log. */
        protected final List<AsyncLogEntry<?>> asyncEntries = new ArrayList<>();

        /** The output stream that the Data Hive Log is written to. */
        protected final DataOutputStream stream;
        /** The source of time used for timestamps. */
        protected final TimeSource timestampSource;
        /** The Fixed Timestamp Unit if that feature is enabled, or ZERO otherwise. */
        protected final Time fixedTimestampUnit;
        /** The Data Hive Log Description. */
        protected final String description;
        /** A {@link List} of {@link LogEntryType}s to use as Group Types. */
        protected final List<LogEntryType<?>> typeList = new ArrayList<>();
        /** The Asynchronous Log Entry ID to use for the next Asynchronous Log Entry. */
        protected int nextAsyncLogEntryId = 1;
        /** The Group Entry Type ID to use for the next Group Entry Type. */
        protected int nextGroupTypeId = k_reservedTypeIdCount;

        /** Represents an Unsigned Byte. The default initial value is 0. */
        public final LogEntryType<UnsignedByte> unsignedByteType;
        /** Represents an Unsigned Short. The default initial value is 0. */
        public final LogEntryType<UnsignedShort> unsignedShortType;
        /** Represents an Unsigned Integer. The default initial value is 0. */
        public final LogEntryType<UnsignedInt> unsignedIntType;
        /** Represents an Unsigned Long. The default initial value is 0. */
        public final LogEntryType<UnsignedLong> unsignedLongType;
        /** Represents a Byte. The default initial value is 0. */
        public final LogEntryType<Byte> byteType;
        /** Represents a Short. The default initial value is 0. */
        public final LogEntryType<Short> shortType;
        /** Represents an Integer. The default initial value is 0. */
        public final LogEntryType<Integer> integerType;
        /** Represents a Long. The default initial value is 0. */
        public final LogEntryType<Long> longType;
        /** Represents a Float. The default initial value is 0.0. */
        public final LogEntryType<Float> floatType;
        /** Represents a Double. The default initial value is 0.0. */
        public final LogEntryType<Double> doubleType;
        /** Represents a Boolean. The default initial value is false. */
        public final LogEntryType<Boolean> booleanType;
        /** Represents a String. The default initial value is an empty string (""). */
        public final LogEntryType<String> stringType;

        /**
         * Create a builder given a generic output stream and no Fixed Timestamp Unit.
         *
         * @param stream The output stream of the Data Hive Log
         * @param timestampSource The source of time used for timestamps
         * @param description The Data Hive Log Description
         */
        public Builder(OutputStream stream, TimeSource timestampSource, String description) {
            this(stream, timestampSource, Time.ZERO, description);
        }

        /**
         * Create a builder given a data output stream and no Fixed Timestamp Unit.
         *
         * @param stream The output stream of the Data Hive Log
         * @param timestampSource The source of time used for timestamps
         * @param description The Data Hive Log Description
         */
        public Builder(DataOutputStream stream, TimeSource timestampSource, String description) {
            this(stream, timestampSource, Time.ZERO, description);
        }

        /**
         * Create a builder given a generic output stream and a Fixed Timestamp Unit.
         *
         * @param stream The output stream of the Data Hive Log
         * @param timestampSource The source of time used for timestamps
         * @param fixedTimestampUnit The Fixed Timestamp Unit of the Data Hive Log. When log messages
         *     are generated, the message's timestamps will be in integer multiples of this amount of
         *     time
         * @param description The Data Hive Log Description
         */
        public Builder(
                OutputStream stream,
                TimeSource timestampSource,
                Time fixedTimestampUnit,
                String description) {
            this(new DataOutputStream(stream), timestampSource, fixedTimestampUnit, description);
        }

        /**
         * Create a builder given a data output stream and a Fixed Timestamp Unit.
         *
         * @param stream The output stream of the Data Hive Log
         * @param timestampSource The source of time used for timestamps
         * @param fixedTimestampUnit The Fixed Timestamp Unit of the Data Hive Log. When log messages
         *     are generated, the message's timestamps will be in integer multiples of this amount of
         *     time
         * @param description The Data Hive Log Description
         */
        public Builder(
                DataOutputStream stream,
                TimeSource timestampSource,
                Time fixedTimestampUnit,
                String description) {
            this.stream = stream;
            this.timestampSource = timestampSource;
            this.fixedTimestampUnit = fixedTimestampUnit;
            this.description = description;

            unsignedByteType = LogEntryType.makeEntry(
                    1, "Unsigned byte", new UnsignedByte((byte) 0), b -> stream.writeByte(b.asByte()));
            unsignedShortType = LogEntryType.makeEntry(
                    2, "Unsigned short", new UnsignedShort((short) 0), s -> stream.writeShort(s.asShort()));
            unsignedIntType = LogEntryType.makeEntry(
                    3, "Unsigned int", new UnsignedInt(0), i -> stream.writeInt(i.asInt()));
            unsignedLongType = LogEntryType.makeEntry(
                    4, "Unsigned long", new UnsignedLong(0L), l -> stream.writeLong(l.asLong()));
            byteType = LogEntryType.makeEntry(5, "Byte", (byte) 0, b -> stream.writeByte(b));
            shortType = LogEntryType.makeEntry(6, "Short", (short) 0, s -> stream.writeShort(s));
            integerType = LogEntryType.makeEntry(7, "Integer", 0, stream::writeInt);
            longType = LogEntryType.makeEntry(8, "Long", 0L, stream::writeLong);
            floatType = LogEntryType.makeEntry(9, "Float", 0.0f, stream::writeFloat);
            doubleType = LogEntryType.makeEntry(10, "Double", 0.0, stream::writeDouble);
            booleanType = LogEntryType.makeEntry(11, "Boolean", false, stream::writeBoolean);
            stringType =
                    LogEntryType.makeEntry(12, "String", "", s -> DataHiveLogger.writeString(stream, s));

            List.of(
                            unsignedByteType,
                            unsignedShortType,
                            unsignedIntType,
                            unsignedLongType,
                            byteType,
                            shortType,
                            integerType,
                            longType,
                            floatType,
                            doubleType,
                            booleanType,
                            stringType)
                    .forEach(this::registerTypeId);
        }

        /**
         * Calls {@link DataHiveLogger#getMaxTimestamp(Time)} using the currently configured fixed
         * timestamp unit.
         *
         * @return The max timestamp supported by {@link DataHiveLogger}
         */
        public Time getMaxTimestamp() {
            return DataHiveLogger.getMaxTimestamp(fixedTimestampUnit);
        }

        /**
         * Creates a {@link GroupTypeBuilder} for the given type
         *
         * @param <T> The type being defined
         * @param name The name of the type
         * @return A {@link GroupTypeBuilder} linked to this {@link Builder}
         */
        public <T> GroupTypeBuilder<T> buildGroupType(String name) {
            return new GroupTypeBuilder<>(name);
        }

        /**
         * Registers an enumerated type for the Data Hive Log.
         *
         * <p>The name of the enum is obtained using {@link Class#getSimpleName()}.
         *
         * @param <E> The enumerated type to register
         * @param type A {@link Class} object of the type being registered
         * @param defaultInitialValue The default initial value that a log entry using this log entry
         *     type will start with if an initial value is not provided
         * @return The newly registered type
         */
        public <E extends Enum<E>> LogEntryType<E> registerEnum(Class<E> type, E defaultInitialValue) {
            return registerEnum(type, type.getSimpleName(), defaultInitialValue);
        }

        /**
         * Registers an enumerated type for the Data Hive Log.
         *
         * @param <E> The enumerated type to register
         * @param type A {@link Class} object of the type being registered
         * @param name The name of the type
         * @param defaultInitialValue The default initial value that a log entry using this log entry
         *     type will start with if an initial value is not provided
         * @return The newly registered type
         */
        public <E extends Enum<E>> LogEntryType<E> registerEnum(
                Class<E> type, String name, E defaultInitialValue) {
            boolean useShort = type.getEnumConstants().length > 256;
            GroupLogEntryType.EnumMember<E, E> member;
            if (useShort) {
                IOConsumer<E> writer = e -> unsignedShortType.write(new UnsignedShort((short) e.ordinal()));
                member =
                        new GroupLogEntryType.EnumMember<>(type, unsignedShortType.getTypeId(), name, writer);
            } else {
                IOConsumer<E> writer = e -> unsignedByteType.write(new UnsignedByte((byte) e.ordinal()));
                member =
                        new GroupLogEntryType.EnumMember<>(type, unsignedByteType.getTypeId(), name, writer);
            }

            GroupLogEntryType<E> entryType = new GroupLogEntryType<>(
                    nextGroupTypeId++, name, defaultInitialValue, Arrays.asList(member));
            registerTypeId(entryType);
            return entryType;
        }

        /**
         * Create a Synchronous Log Entry.
         *
         * <p><strong>NOTE:</strong> Synchronous Strings are highly discouraged, as they can potentially
         * create a lot of unnecessary file bloat. It is recommended that a synchronous enum type should
         * be considered instead, or asynchronously logging the String data via
         * {@link #makeAsyncLogEntry}.
         *
         * <p>A few examples of things that would typically be logged synchronously include:
         *
         * <ul>
         *   <li>Constantly changing values (e.g. sensor readings, user joy stick positions).
         *   <li>Anything that should be logged all the time that also doesn't have to be precisely
         *       timestamped.
         * </ul>
         *
         * @param <T> The type of Log Entry
         * @param name The name of the Log Entry
         * @param entryType The type of this Log Entry
         * @param initial The value to log until a new value is provided
         * @return A Synchronous Log Entry
         */
        public <T> SyncLogEntry<T> makeSyncLogEntry(String name, LogEntryType<T> entryType, T initial) {
            SyncLogEntry<T> entry = new SyncLogEntry<>(name, entryType, initial);
            syncEntries.add(entry);
            return entry;
        }

        /**
         * Create a Synchronous Log Entry. The initial value of this log entry will be the default
         * initial value defined by the given Log Entry Type. To specify what the initial value should
         * be, see {@link #makeSyncLogEntry(String, LogEntryType, Object)}.
         *
         * <p><strong>NOTE:</strong> Synchronous Strings are highly discouraged, as they can potentially
         * create a lot of unnecessary file bloat. It is recommended that a synchronous enum type should
         * be considered instead, or asynchronously logging the String data via
         * {@link #makeAsyncLogEntry}.
         *
         * <p>A few examples of things that would typically be logged synchronously include:
         *
         * <ul>
         *   <li>Constantly changing values (e.g. sensor readings, user joy stick positions).
         *   <li>Anything that should be logged all the time that also doesn't have to be precisely
         *       timestamped.
         * </ul>
         *
         * @param <T> The type of Log Entry
         * @param name The name of the Log Entry
         * @param entryType The type of this Log Entry
         * @return A Synchronous Log Entry
         */
        public <T> SyncLogEntry<T> makeSyncLogEntry(String name, LogEntryType<T> entryType) {
            return makeSyncLogEntry(name, entryType, entryType.defaultInitialValue);
        }

        /**
         * Create an Asynchronous Log Entry.
         *
         * <p>Consider using {@link #makeNoRepeatsAsyncLogEntry} instead as it handles async entries
         * with no data loss while reducing overall log file size.
         *
         * <p>A few examples of things that would typically be logged asynchronously include:
         *
         * <ul>
         *   <li>Precisely timestamped events.
         *   <li>Periodic events.
         *   <li>State changes in a state machine when they occur (ie. only capturing the current state
         *       once on transition).
         *   <li>State machine transitions when they occur (i.e. what caused the transition of the
         *       current state).
         *   <li>Rising/Falling edge captures of infrequent change of logic level (e.g. user controller
         *       button presses/releases).
         * </ul>
         *
         * @param <T> The type of Log Entry
         * @param name The name of the Log Entry
         * @param entryType The type of this Log Entry
         * @return An Asynchronous Log Entry
         */
        public <T> AsyncLogEntry<T> makeAsyncLogEntry(String name, LogEntryType<T> entryType) {
            AsyncLogEntry<T> entry = new AsyncLogEntry<>(name, nextAsyncLogEntryId++, entryType);
            asyncEntries.add(entry);
            return entry;
        }

        /**
         * Wraps an Asynchronous Log Entry Consumer with a new consumer that will only accept a value
         * being provided when the value has changed from its previous value. The default value will be
         * logged as soon as the logger is initialized.
         *
         * @param <T> The type being defined
         * @param asyncConsumer The original Asynchronous Log Entry to wrap
         * @param defaultInitialValue The default value that is initially logged and compared to in
         *     order to detect a change in value
         * @return A consumer used to update the value being logged
         */
        public <T> Consumer<T> makeNoRepeats(AsyncLogEntry<T> asyncConsumer, T defaultInitialValue) {
            return makeNoRepeats(asyncConsumer, defaultInitialValue, true);
        }

        /**
         * Wraps an Asynchronous Log Entry Consumer with a new consumer that will only accept a value
         * being provided when the value has changed from its previous value. The default value will be
         * logged as soon as the logger is initialized. The default value will be the default initial
         * value defined by the given Log Entry Type. To specify what the initial value should be, see
         * {@link #makeNoRepeats(AsyncLogEntry, Object)}.
         *
         * @param <T> The type being defined
         * @param asyncConsumer The original Asynchronous Log Entry to wrap
         * @return A consumer used to update the value being logged
         */
        public <T> Consumer<T> makeNoRepeats(AsyncLogEntry<T> asyncConsumer) {
            return makeNoRepeats(asyncConsumer, asyncConsumer.entryType.defaultInitialValue);
        }

        /**
         * Wraps an Asynchronous Log Entry Consumer with a new consumer that will only accept a value
         * being provided when the value has changed from its previous value.
         *
         * @param <T> The type being defined
         * @param asyncConsumer The original Asynchronous Log Entry to wrap
         * @param defaultValue The default value that is initially logged and compared to in order to
         *     detect a change in value
         * @param logDefault Whether the default value should be logged when the logger is initialized.
         *     If this is set false, a value will not be reported until a value other then the
         *     defaultValue is provided.
         * @return A consumer used to update the value being logged
         */
        public <T> Consumer<T> makeNoRepeats(
                AsyncLogEntry<T> asyncConsumer, T defaultValue, boolean logDefault) {
            if (logDefault) {
                initNoRepeatAsyncEntryList.add(() -> asyncConsumer.accept(defaultValue));
            }
            return new Consumer<T>() {
                private T prev = defaultValue;

                @Override
                public void accept(T value) {
                    // Only log when error code changes
                    if (prev != null && value.equals(prev)) {
                        return;
                    }

                    asyncConsumer.accept(value);
                    prev = value;
                }
            };
        }

        /**
         * Creates a new Asynchronous Log Entry that will only update when the value has changed from
         * its previous value. The default value will be logged as soon as the logger is initialized.
         *
         * @param <T> The type being defined
         * @param name The name of the entry
         * @param entryType The Log Entry Type that matches the type being defined
         * @param defaultValue The default value that is initially logged and compared to in order to
         *     detect a change in value
         * @return A consumer used to update the value being logged
         */
        public <T> Consumer<T> makeNoRepeatsAsyncLogEntry(
                String name, LogEntryType<T> entryType, T defaultValue) {
            return makeNoRepeats(makeAsyncLogEntry(name, entryType), defaultValue);
        }

        /**
         * Creates a new Asynchronous Log Entry that will only update when the value has changed from
         * its previous value.
         *
         * @param <T> The type being defined
         * @param name The name of the entry
         * @param entryType The Log Entry Type that matches the type being defined
         * @param defaultValue The default value that is initially logged and compared to in order to
         *     detect a change in value
         * @param logDefault Whether the default value should be logged when the logger is initialized.
         *     If this is set false, a value will not be reported until a value other then the
         *     defaultValue is provided.
         * @return A consumer used to update the value being logged
         */
        public <T> Consumer<T> makeNoRepeatsAsyncLogEntry(
                String name, LogEntryType<T> entryType, T defaultValue, boolean logDefault) {
            return makeNoRepeats(makeAsyncLogEntry(name, entryType), defaultValue, logDefault);
        }

        /**
         * Creates an array type based on an existing entry type.<br>
         * Warning: This generally should not be called on the same {@link LogEntryType} twice.
         *
         * @param <T> The type of an individual element
         * @param elementEntryType The Log Entry Type of an individual element
         * @param defaultInitialValue The default initial value that a log entry using this log entry
         *     type will start with if an initial value is not provided
         * @return The array-based LogEntryType
         */
        public <T> LogEntryType<T[]> toArray(
                LogEntryType<T> elementEntryType, T[] defaultInitialValue) {
            String arrayName = elementEntryType.name + "[]";
            GroupTypeBuilder<T[]> typeBuilder = buildGroupType(arrayName);
            return typeBuilder
                    .addArrayMember(arrayName, elementEntryType, a -> a)
                    .register(defaultInitialValue);
        }

        /**
         * Creates a list type based on an existing entry type.<br>
         * Warning: This generally should not be called on the same {@link LogEntryType} twice.
         *
         * @param <T> The type of an individual element
         * @param elementEntryType The Log Entry Type of an individual element
         * @param defaultInitialValue The default initial value that a log entry using this log entry
         *     type will start with if an initial value is not provided
         * @return The list-based LogEntryType
         */
        public <T> LogEntryType<List<T>> toList(
                LogEntryType<T> elementEntryType, List<T> defaultInitialValue) {
            String arrayName = elementEntryType.name + "List";
            GroupTypeBuilder<List<T>> typeBuilder = buildGroupType(arrayName);
            return typeBuilder
                    .addListMember(arrayName, elementEntryType, a -> a)
                    .register(defaultInitialValue);
        }

        /**
         * Initializes and returns the Data Hive Log that was built up.
         *
         * <p>After init is called, this {@link Builder} should no longer be used, as the
         * {@link LogEntry}s and {@link LogEntryType}s that were generated from it are unique to a
         * single {@link DataHiveLogger}.
         *
         * @return An initialized {@link DataHiveLogger} which has already written its Initial Block
         * @throws IOException If any write operation throws an {@link IOException}
         */
        public DataHiveLogger init() throws IOException {
            List<GroupLogEntryType<?>> groupLogEntryTypes = typeList.stream()
                    .filter(t -> t instanceof GroupLogEntryType)
                    .map(t -> (GroupLogEntryType<?>) t)
                    .sorted((a, b) -> Integer.compare(a.getTypeId(), b.getTypeId()))
                    .collect(Collectors.toList());
            DataHiveLogger logger = new DataHiveLogger(
                    stream,
                    timestampSource,
                    fixedTimestampUnit,
                    description,
                    syncEntries,
                    asyncEntries,
                    groupLogEntryTypes);
            Stream.concat(syncEntries.stream(), asyncEntries.stream()).forEach(e -> {
                e.init(logger);
            });
            logger.generateInitialBlock();

            for (Runnable noRepeatAsyncEntry : initNoRepeatAsyncEntryList) {
                noRepeatAsyncEntry.run();
            }

            return logger;
        }

        /**
         * Registers a Log Entry Type which can then be used for Log Entries
         *
         * @param <T> The type of Log Entry
         * @param logEntryType The Log Entry Type to register
         */
        protected <T> void registerTypeId(LogEntryType<T> logEntryType) {
            typeList.add(logEntryType);
        }
    }

    /** The output stream that the Data Hive Log is written to */
    protected final DataOutputStream stream;
    /** The source of time used for timestamps */
    protected final TimeSource timestampSource;
    /** The Data Hive Log Description used in the Initial Header */
    protected final String description;
    /** Stores whether or not Extended Asynchronous Entry IDs are enabled */
    protected final boolean useExtendedAsync;
    /** Stores whether or not a Fixed Timestamp Unit is enabled */
    protected final boolean useFixedTimestamp;
    /** The Fixed Timestamp Unit if that feature is enabled, or ZERO otherwise */
    protected final Time fixedTimestampUnit;

    /** Every Synchronous Log Entry added to the Data Hive Log */
    protected final List<SyncLogEntry<?>> syncEntries;
    /** Every Asynchronous Log Entry added to the Data Hive Log */
    protected final List<AsyncLogEntry<?>> asyncEntries;
    /** Every Group Log Entry Type defined in the Data Hive Log */
    protected final List<GroupLogEntryType<?>> groupLogEntryTypes;

    /**
     * Creates an uninitialized {@link DataHiveLogger}
     *
     * @param stream The output stream to write to
     * @param timestampSource A source of time for timestamp generation
     * @param fixedTimestampUnit The Fixed Timestamp Unit to use, or ZERO to disable Fixed Timestamp
     *     Units
     * @param description The Data Hive Log description
     * @param syncEntries All Synchronous Entries used in this Data Hive Log
     * @param asyncEntries All Asynchronous Entries used in this Data Hive Log
     * @param groupLogEntryTypes All Group Entry Types used in this Data Hive Log
     */
    protected DataHiveLogger(
            DataOutputStream stream,
            TimeSource timestampSource,
            Time fixedTimestampUnit,
            String description,
            List<SyncLogEntry<?>> syncEntries,
            List<AsyncLogEntry<?>> asyncEntries,
            List<GroupLogEntryType<?>> groupLogEntryTypes) {
        this.stream = stream;
        this.timestampSource = timestampSource;
        this.description = description;
        this.syncEntries = syncEntries;
        this.asyncEntries = asyncEntries;
        this.groupLogEntryTypes = groupLogEntryTypes;
        this.useExtendedAsync = asyncEntries.size() > 255;
        this.fixedTimestampUnit = fixedTimestampUnit;
        this.useFixedTimestamp = fixedTimestampUnit.gt(Time.ZERO);
    }

    /**
     * Calls {@link DataHiveLogger#getMaxTimestamp(Time)} using the currently configured fixed
     * timestamp unit.
     *
     * @return The max timestamp supported by {@link DataHiveLogger}
     */
    public Time getMaxTimestamp() {
        return DataHiveLogger.getMaxTimestamp(fixedTimestampUnit);
    }

    /**
     * Calculates the maximum timestamp supported by {@link DataHiveLogger} given a fixed timestamp
     * unit.
     *
     * <p><strong>NOTE:</strong> This is not necessarily the largest timestamp supported by the DHL
     * File Format. The {@link DataHiveLogger} class may not support timestamps as large as the DHL
     * file format supports.
     *
     * @param fixedTimestampUnit The timestamp unit used to calculate the maximum timestamp
     * @return The max timestamp supported by {@link DataHiveLogger}
     */
    public static Time getMaxTimestamp(Time fixedTimestampUnit) {
        return fixedTimestampUnit.mul(Integer.MAX_VALUE);
    }

    /**
     * Calculates the smallest fixed timestamp unit that still supports the provided timestamp. This
     * is useful if you know how long a log file will be generated for, and you want to use the
     * smallest fixed timestamp unit that will overflow the fixed timestamp counter in a specified
     * amount of time since creation.
     *
     * <p><strong>NOTE:</strong> The {@link DataHiveLogger} class may not support timestamps as large
     * as the DHL file format supports. The result of this function will be the lowest fixed timestamp
     * unit that {@link DataHiveLogger} can use which supports the provided timestamp, but it may be
     * possible in the DHL file format to use a smaller fixed timestamp unit while still supporting
     * the provided max timestamp (e.g. supporting fixed timestamp counter overflows).
     *
     * @param maxTimestamp The largest timestamp you'd like to support
     * @return The smallest fixed timestamp unit that supports the provided timestamp
     */
    public static Time getFixedTimestampUnit(Time maxTimestamp) {
        return maxTimestamp.div(Integer.MAX_VALUE);
    }

    /**
     * Initializes the DHL initial block.
     *
     * @throws IOException If creating the initial block throws an {@link IOException}
     */
    protected void generateInitialBlock() throws IOException {
        generateFileHeader();
        generateGroupLogEntryTypeTable();
        generateAsyncLogEntryTable();
        generateSyncLogEntryTable();
    }

    /**
     * Writes a null-terminated {@link String} to a {@link DataOutputStream}.
     *
     * <p>Writes in the format specified by the Data Hive Log specification.
     *
     * @param stream The stream to write a {@link String} to
     * @param s The {@link String} to write
     * @throws IOException If writing the byte data throws an {@link IOException}
     */
    protected static void writeString(DataOutputStream stream, String s) throws IOException {
        stream.writeBytes(s);
        stream.writeByte(0);
    }

    /**
     * Calls {@link DataHiveLogger#writeString(DataOutputStream, String)} with this log's
     * {@link DataOutputStream}.
     *
     * @param s The {@link String} to write
     * @throws IOException If writing the byte data throws an {@link IOException}
     */
    protected void writeString(String s) throws IOException {
        DataHiveLogger.writeString(stream, s);
    }

    /**
     * Writes the Data Hive Log File Header.
     *
     * @throws IOException If any write operation throws an {@link IOException}
     */
    protected void generateFileHeader() throws IOException {
        // 1. Exactly 6 bytes, ‘D’, ‘H’, ‘L’, ‘L’, ‘O’, ‘G’ as ASCII characters.
        stream.write("DHLLOG".getBytes());

        // 2. (Unsigned Short) DataHive Logger File Version.
        stream.writeShort(1);

        // 3. (String) Log File Date/Time String.
        writeString(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date()));

        // 4. (String) Log File Description.
        writeString(description);

        // 5. (Unsigned Byte) Log File Options.
        int logFileOptions = (useFixedTimestamp ? 1 : 0) << 1 | (useExtendedAsync ? 1 : 0);
        stream.writeByte(logFileOptions);

        // 6. (Double) Fixed Timestamp Time Unit, in seconds. Set all 8 bytes to 0x00 if
        // Fixed Timestamp Time Unit Enabled (Section 5.1.1.4.2) is not set (the bit has
        // a value of 0).
        if (useFixedTimestamp) {
            stream.writeDouble(fixedTimestampUnit.asSeconds());
        } else {
            stream.writeLong(0);
        }
    }

    /**
     * Writes the Data Hive Log Group Log Entry Type Table.
     *
     * @throws IOException If any write operation throws an {@link IOException}
     */
    protected void generateGroupLogEntryTypeTable() throws IOException {
        stream.writeShort(groupLogEntryTypes.size());
        for (GroupLogEntryType<?> entry : groupLogEntryTypes) {
            writeString(entry.name);
            stream.writeShort(entry.members.size());
            for (GroupLogEntryType.Member<?> member : entry.members) {
                writeString(member.name);
                byte flags = 0;
                boolean isEnum = member instanceof GroupLogEntryType.EnumMember;
                if (isEnum) {
                    flags |= 0x02;
                }
                boolean isArray = member instanceof GroupLogEntryType.ArrayMember;
                if (isArray) {
                    flags |= 0x01;
                }
                stream.writeByte(flags);
                stream.writeShort(member.typeId);
                if (isEnum) {
                    GroupLogEntryType.EnumMember<?, ?> enumMember =
                            (GroupLogEntryType.EnumMember<?, ?>) member;
                    Enum<?>[] enumConstants = enumMember.enumType.getEnumConstants();
                    stream.writeInt(enumConstants.length);
                    for (Enum<?> e : enumConstants) {
                        if (enumConstants.length > 256) {
                            stream.writeShort(e.ordinal());
                        } else {
                            stream.writeByte(e.ordinal());
                        }
                        writeString(e.toString());
                    }
                }
            }
        }
    }

    /**
     * Writes the Data Hive Log Asynchronous Log Entry Table.
     *
     * @throws IOException If any write operation throws an {@link IOException}
     */
    protected void generateAsyncLogEntryTable() throws IOException {
        // Unsigned Short - Total Number of Async Log Entries
        stream.writeShort((short) asyncEntries.size());

        // Loop through all entries
        for (AsyncLogEntry<?> asyncEntry : asyncEntries) {
            // String - Log Entry Label
            writeString(asyncEntry.name);

            // Unisgned Short - Log Entry ID
            stream.writeShort(asyncEntry.entryType.getTypeId());
        }
    }

    /**
     * Writes the Data Hive Log Synchronous Log Entry Table.
     *
     * @throws IOException If any write operation throws an {@link IOException}
     */
    protected void generateSyncLogEntryTable() throws IOException {
        // Unsigned Short - The total number of Sync Log Entries
        stream.writeShort(syncEntries.size());

        // Loop through all entries
        for (SyncLogEntry<?> syncEntry : syncEntries) {
            // String - Log Entry Label
            writeString(syncEntry.name);

            // Unisgned Short - Log Entry ID
            stream.writeShort(syncEntry.entryType.getTypeId());
        }
    }

    /**
     * Writes the message header for a Message.
     *
     * <p>Supports Synchronous and Asynchronous Messages. Handles Extended Asynchronous Type IDs and
     * Fixed Timestamp Units when enabled.
     *
     * @param messageId The Message ID - 0 for Synchronous Messages
     * @throws IOException If any write operation throws an {@link IOException}
     */
    protected void logMessageHeader(int messageId) throws IOException {
        // 1. (Unsigned Short) If async message and extended async IDs is enabled
        // 1. (Unsigned Byte) If sync message, if extended async ID is enabled and the
        // type ID is less than or equal 127 or if extended async IDs isn't enabled
        if (messageId > 127 && useExtendedAsync) {
            // If greater than 127, and we are using extended async IDs, we should write an
            // Unsigned Short, but with the MSB set
            stream.writeShort(messageId & 0x8000);
        } else {
            stream.writeByte(messageId);
        }

        // 2. (Float) If use fixed timestamp is false
        // 2. (Unsigned Int) If use fixed timestamp is true
        if (useFixedTimestamp) {
            stream.writeInt(
                    (int) Math.round(timestampSource.clockTime().div(fixedTimestampUnit).asNone()));
        } else {
            stream.writeFloat((float) timestampSource.clockTime().asSeconds());
        }
    }

    /**
     * Logs a Synchronous Message.
     *
     * <p>Ignores any {@link IOException} thrown.
     */
    protected void logAllSync() {
        try {
            logMessageHeader(0);
            // 3. List of primitive raw data
            for (SyncLogEntry<?> logEntry : syncEntries) {
                logEntry.write();
            }
        } catch (IOException e) {
            // TODO: Handle
        }
    }

    /**
     * Logs all synchronous log entries that are registered to this Data Hive Logger. If a specific
     * synchronous log entry was not updated since the last call to {@link update()}, the same log
     * data value will be logged again for that entry.
     */
    public void update() {
        logAllSync();
    }
}
