/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.util.Objects;

/**
 * Represents an unsigned int for purposes of logging to DHL files.
 *
 * <p>This should generally not be used external to writing DHL files unless required. The value in
 * this int is still understood by Java as a signed value. This class is generally meant for
 * documentation purposes only.
 */
public class UnsignedInt {
    private final int i;

    /**
     * Construct an unsigned int given a current int's value.
     *
     * @param i Value to treat as unsigned
     */
    public UnsignedInt(int i) {
        this.i = i;
    }

    /**
     * Provides the unsigned int's value as a long.
     *
     * <p>Values that a signed int cannot store will be properly interpreted as positive. For example,
     * the value 2,147,483,648, stored in a signed int, would be interpreted as -2,147,483,648 because
     * of overflow, but an unsigned int constructed from the value -2,147,483,648 would return an int
     * of value 2,147,483,648.
     *
     * @return The positive value of the unsigned int
     */
    public long asUnsigned() {
        return Integer.toUnsignedLong(i);
    }

    /**
     * Provides the internally stored int, which is treated as signed.
     *
     * @return The internally stored int
     */
    public int asInt() {
        return i;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        UnsignedInt value = (UnsignedInt) other;
        return this.i == value.i;
    }

    @Override
    public int hashCode() {
        return Objects.hash(i);
    }
}
