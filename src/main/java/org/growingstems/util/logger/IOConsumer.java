/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.IOException;

/**
 * A consumer of data that may potentially throw an {@link IOException} when called.
 *
 * @param <T> The type of data to write to a file
 */
public interface IOConsumer<T> {
    /**
     * Write data, most likely to a file.
     *
     * @param t The data to write
     * @throws IOException If any I/O errors occurred while writing
     */
    void write(T t) throws IOException;
}
