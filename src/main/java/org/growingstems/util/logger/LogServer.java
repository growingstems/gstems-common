/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.growingstems.measurements.Measurements.Time;

/**
 * A networked server which can safely transmit files to a connected client.
 *
 * <p>The list of files to be transferred is compiled during construction. If the {@link LogServer}
 * is being used in a program which will generate another log file, it may be important to construct
 * the server first, so it does not attempt to transmit (and possibly delete) a log file that is
 * currently being written to.
 *
 * <p>The server prioritizes exception-safety to avoid interrupting of the user's code. It will
 * safely shut down if exceptions occur internally.
 *
 * <p>A successful connection follows the following sequence:
 *
 * <ol>
 *   <li>Client transmits a {@code boolean} value. If true, the connection is in "Deletion" mode.
 *   <li>Server transmits a {@code long} representing the number of files that are ready to
 *       transmit.
 *   <li>For each file:
 *       <ol>
 *         <li>Server transmits the name of the file.
 *         <li>Server transmits a {@code long} representing the size of the file.
 *         <li>Server transmits all byte data of the file. Both the Server and Client hash this
 *             data.
 *         <li>Client transmits an {@code int} representing the length of its resulting hash
 *         <li>Client transmits its recorded hash
 *         <li>Server compares its hash to the Client's hash. If the hashes don't match, the server
 *             closes the connection. If they do match, and the connection is in "Deletion" mode,
 *             the server deletes the file and removes it from its list of files so it does not
 *             attempt to transmit the file in future connections.
 *       </ol>
 *   <li>The server closes the connection.
 * </ol>
 *
 * <p>Since the hash is checked before file deletion, if the client stores the file data before
 * transmitting the hash, this sequence should never result in loss of data. If any errors occur
 * that close the connection early, the client may re-connect to restart the process. If a previous
 * connection was in "Deletion" mode, any successfully transmitted files will not be sent again. If
 * the previous connection was not in "Deletion" mode, every file will be re-sent.
 */
public class LogServer implements AutoCloseable {
    /** The server address provided to {@link InetSocketAddress#InetSocketAddress(InetAddress,int)} */
    protected InetAddress m_serverAddress = null;
    /** The port to provide to {@link InetSocketAddress#InetSocketAddress(InetAddress,int)} */
    protected int m_port = 5805;
    /** The timeout to provide to {@link ServerSocket#setSoTimeout(int)} */
    protected Time m_socketTimeout = Time.seconds(1.0);

    /** Stream to log status to */
    protected final Optional<Consumer<String>> m_logInfo;
    /** Stream to log errors to */
    protected final Optional<Consumer<String>> m_logError;

    /** A list of all managed files that have not been transferred */
    protected List<File> m_fileList;
    /** Stores the number of files that have not been transferred, or -1 if no transfer has begun. */
    protected AtomicLong m_filesToTransfer = new AtomicLong(-1);
    /** Set to true if {@link LogServer#stop()} is ever called. */
    protected AtomicBoolean m_forceStop = new AtomicBoolean(false);
    /** Set to the bound port after the socket is bound, and set -1 after unbinding it. */
    protected AtomicInteger m_boundPort = new AtomicInteger(-1);
    /** Stores the server's running thread. */
    protected Thread m_serverThread = null;

    /**
     * Creates a {@link LogServer} which manages all files in a given directory
     *
     * @param logDir Directory to scan for files
     */
    public LogServer(File logDir) {
        this(logDir, unused -> true);
    }

    /**
     * Creates a {@link LogServer} which manages all files in a given directory with a matching
     * extension.
     *
     * @param logDir Directory to scan for files with the provided extension
     * @param extension The extension to filter files
     */
    public LogServer(File logDir, String extension) {
        this(logDir, f -> f.getName().endsWith(extension));
    }

    /**
     * Creates a {@link LogServer} which manages all files in a given directory which pass the
     * provided {@link Predicate}.
     *
     * @param logDir Directory to scan for files
     * @param filter {@link Predicate} to test each file against
     */
    public LogServer(File logDir, Predicate<? super File> filter) {
        this(logDir, filter, Optional.of(System.out::println), Optional.of(System.err::println));
    }

    /**
     * Creates a {@link LogServer} which manages all files in a given directory which pass the
     * provided {@link Predicate}.
     *
     * @param logDir Directory to scan for files
     * @param filter {@link Predicate} to test each file against
     * @param logInfo Function to use for logging info
     * @param logInfo Function to use for logging errors
     */
    public LogServer(
            File logDir,
            Predicate<? super File> filter,
            Optional<Consumer<String>> logInfo,
            Optional<Consumer<String>> logError) {
        m_logInfo = logInfo;
        m_logError = logError;
        try (Stream<Path> files = Files.walk(logDir.toPath())) {
            m_fileList = files
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .filter(filter)
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            m_logError.ifPresent(s -> s.accept("IO Error during Files.walk: " + e.getMessage()));
        } catch (Exception e) {
            m_logError.ifPresent(s -> s.accept("Non-IO Error during Files.walk: " + e.getMessage()));
        }
    }

    /**
     * Starts the Log Server in its own thread given the current configuration.
     *
     * <p>No-ops if the server was already started at any point.
     *
     * <p>Changing the configuration after calling {@link LogServer#start()} invokes <a
     * href="https://en.wikipedia.org/wiki/Undefined_behavior">Undefined Behavior</a>.
     */
    public void start() {
        if (m_serverThread != null) {
            return;
        }

        m_forceStop.set(false);
        m_serverThread = new Thread(this::runServer);
        m_serverThread.start();
    }

    /**
     * Stops the {@link LogServer}.
     *
     * <p>Blocks until the server's thread has stopped.
     *
     * <p>No-ops if the server was not running.
     */
    public void stop() {
        if (m_serverThread == null) {
            return;
        }

        m_forceStop.set(true);
        try {
            m_serverThread.join();
            m_serverThread = null;
        } catch (InterruptedException e) {
            m_logError.ifPresent(
                    s -> s.accept("Interrupted while trying to stop server!" + e.getMessage()));
        }
    }

    /** Equivalent to {@link LogServer#stop()} */
    @Override
    public void close() {
        stop();
    }

    /**
     * Reports whether or not a {@link ServerSocket} is currently bound.
     *
     * <p>Note that this may return true for a brief period after the {@link ServerSocket} is closed.
     *
     * @return true if a {@link ServerSocket} is currently bound
     */
    public boolean isBound() {
        return m_boundPort.get() != -1;
    }

    /**
     * Returns a count of the number of files still remaining in the latest transfer.<br>
     * Result is {@link Optional#empty()} if no transfer has ever been initiated.
     *
     * @return The number of files if a transfer has started, otherwise {@link Optional#empty()}.
     */
    public Optional<Long> filesToTransfer() {
        long count = m_filesToTransfer.get();
        return count == -1 ? Optional.empty() : Optional.of(count);
    }

    /**
     * Sets the server bind address.
     *
     * <p>Default: null. See {@link InetSocketAddress#InetSocketAddress(InetAddress, int)} for
     * behavior.
     *
     * <p>Note: will only take effect the next time the server is started
     *
     * @param serverAddr The server's bind address
     */
    public void setServerAddress(InetAddress serverAddr) {
        m_serverAddress = serverAddr;
    }

    /**
     * Gets the server bind address
     *
     * @return The server's bind address
     */
    public InetAddress getServerAddress() {
        return m_serverAddress;
    }

    /**
     * Sets the port to host the server on.
     *
     * <p>Default: 5805. See {@link InetSocketAddress#InetSocketAddress(InetAddress, int)} for
     * behavior.
     *
     * <p>Note: will only take effect the next time the server is started
     *
     * @param port The port setting
     */
    public void setPort(int port) {
        m_port = port;
    }

    /**
     * Gets the port setting.
     *
     * @return The port setting
     */
    public int getPort() {
        return m_port;
    }

    /**
     * Gets the currently bound port.
     *
     * @return The bound port, or Empty if unbound.
     */
    public Optional<Integer> getBoundPort() {
        int port = m_boundPort.get();
        return port == -1 ? Optional.empty() : Optional.of(port);
    }

    /**
     * Sets the socket timeout.
     *
     * <p>Default: 1 second
     *
     * @param socketTimeout The timeout to use in the server's socket
     */
    public void setSocketTimeout(Time socketTimeout) {
        m_socketTimeout = socketTimeout;
    }

    /**
     * Gets the socket timeout.
     *
     * @return The timeout to use in the server's socket
     */
    public Time getSocketTimeout() {
        return m_socketTimeout;
    }

    /**
     * Creates a socket and begins the server. This call is blocking. See
     * {@link LogServer#listen(ServerSocket)} for details.
     */
    protected void runServer() {
        try (ServerSocket serverSocket = new ServerSocket()) {
            serverSocket.setSoTimeout((int) m_socketTimeout.asMilliseconds());
            serverSocket.setReuseAddress(true);
            // InetSocketAddress's constructor accepts null addresses
            serverSocket.bind(new InetSocketAddress(m_serverAddress, m_port));
            m_boundPort.set(serverSocket.getLocalPort());
            listen(serverSocket);
        } catch (Exception e) {
            m_logError.ifPresent(s -> s.accept(
                    "Failed to create or configure server socket. " + e.toString() + e.getMessage()));
            return;
        } finally {
            m_boundPort.set(-1);
        }
    }

    /**
     * Runs the log server on the provided socket, blocking the current thread until
     * {@link LogServer#stop()} is called or an exception is thrown. The server will then shutdown.
     *
     * @param serverSocket The socket to listen for connections on
     */
    protected void listen(ServerSocket serverSocket) {
        while (!m_forceStop.get()) {
            try (Socket clientSocket = serverSocket.accept()) {
                m_filesToTransfer.set(m_fileList.size());
                sendFiles(clientSocket);
            } catch (SocketTimeoutException e) {
                // NOP
            } catch (Exception e) {
                m_logError.ifPresent(s -> s.accept("Error occurred in Log Server" + e.getMessage()));
                return;
            }
        }
    }

    /**
     * Handles sending all files to a given client.
     *
     * @param clientSocket The client to send all file data to
     */
    protected void sendFiles(Socket clientSocket) {
        MessageDigest digest;
        String defaultAlgorithm = "SHA-256";
        try {
            digest = MessageDigest.getInstance(defaultAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            m_logError.ifPresent(s -> s.accept("Unknown algorithm: " + e.getMessage()));
            return;
        }

        try (DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                DigestOutputStream digestStream =
                        new DigestOutputStream(clientSocket.getOutputStream(), digest);
                DataOutputStream dataOutputStream = new DataOutputStream(digestStream)) {

            // Read algorithm list and report first available algorithm
            String[] algorithmOptions = dataInputStream.readUTF().split(",");
            Optional<MessageDigest> selectedAlgorithm = selectAlgorithm(algorithmOptions);
            if (!selectedAlgorithm.isPresent()) {
                m_logError.ifPresent(
                        s -> s.accept("No algorithms matched!" + String.join(",", algorithmOptions)));
                dataOutputStream.writeUTF("");
                return;
            }
            digest = selectedAlgorithm.get();
            digestStream.setMessageDigest(digest);
            dataOutputStream.writeUTF(digest.getAlgorithm());

            // Read if in file deletion mode
            boolean deleteFiles = dataInputStream.readBoolean();
            dataOutputStream.writeLong(m_fileList.size());

            Iterator<File> fileIt = m_fileList.iterator();
            while (fileIt.hasNext()) {
                File file = fileIt.next();
                Optional<byte[]> optHash = sendFile(file, dataOutputStream, digest);
                if (optHash.isEmpty()) {
                    // Error reported internal to sendFile
                    return;
                }

                byte[] hash = optHash.get();
                int hashRecvLength = dataInputStream.readInt();
                byte[] hashRecv = dataInputStream.readNBytes(hashRecvLength);

                if (Arrays.equals(hash, hashRecv)) {
                    m_logInfo.ifPresent(s -> s.accept("Hash Validated for " + file.getName()));
                    if (deleteFiles) {
                        if (file.delete()) {
                            m_logInfo.ifPresent(s -> s.accept("Deleted file!"));
                            fileIt.remove();
                            m_filesToTransfer.set(m_fileList.size());
                        } else {
                            m_logError.ifPresent(s -> s.accept("Failed to delete " + file.getName()));
                        }
                    }
                } else {
                    m_logError.ifPresent(s -> s.accept("Hashes don't match! Ending connection."));
                    return;
                }
            }
        } catch (Exception e) {
            m_logError.ifPresent(s -> s.accept(
                    "Exception occurred while transmitting file data. Ending connection." + e.getMessage()));
        }
    }

    /**
     * Returns the first {@link MessageDigest} successfully constructed from the provided options.
     *
     * @param options List of algorithms to choose from
     * @return A MessageDigest if successful, otherwise {@link Optional#empty()}
     */
    protected Optional<MessageDigest> selectAlgorithm(String[] options) {
        for (String option : options) {
            try {
                return Optional.of(MessageDigest.getInstance(option));
            } catch (NoSuchAlgorithmException e) {
                // NOP
            }
        }
        return Optional.empty();
    }

    /**
     * Handles entering a single file's data to a stream.
     *
     * @param file The file to transmit
     * @param dataOutputStream The stream to send all data through
     * @param digest A pre-configured {@link MessageDigest} to use for hashing the transmitted file
     * @return The hash of the file if successfully transmitted, otherwise {@link Optional#empty()}
     */
    protected Optional<byte[]> sendFile(
            File file, DataOutputStream dataOutputStream, MessageDigest digest) {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            m_logInfo.ifPresent(s -> s.accept("Sending " + file.getName() + "..."));

            // Send filename and file length
            dataOutputStream.writeUTF(file.getName());
            dataOutputStream.writeLong(file.length());

            digest.reset();

            // Send file data
            int bytes = 0;
            byte[] buffer = new byte[4 * 1024];
            while ((bytes = fileInputStream.read(buffer)) != -1) {
                dataOutputStream.write(buffer, 0, bytes);
                dataOutputStream.flush();
            }

            m_logInfo.ifPresent(s -> s.accept("Finished sending " + file.getName()));
            return Optional.of(digest.digest());
        } catch (IOException e) {
            m_logError.ifPresent(s -> s.accept(
                    "Failed to send file \"" + file.getName() + "\". Ending connection." + e.getMessage()));
            return Optional.empty();
        }
    }
}
