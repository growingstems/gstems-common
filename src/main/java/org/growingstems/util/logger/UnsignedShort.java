/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.util.Objects;

/**
 * Represents an unsigned short for purposes of logging to DHL files.
 *
 * <p>This should generally not be used external to writing DHL files unless required. The value in
 * this short is still understood by Java as a signed value. This class is generally meant for
 * documentation purposes only.
 */
public class UnsignedShort {
    private final short s;

    /**
     * Construct an unsigned short given a current short's value.
     *
     * @param s Value to treat as unsigned
     */
    public UnsignedShort(short s) {
        this.s = s;
    }

    /**
     * Provides the unsigned short's value as an int.
     *
     * <p>Values that a signed short cannot store will be properly interpreted as positive. For
     * example, the value 40,000, stored in a signed short, would be interpreted as -25,536 because of
     * overflow, but an unsigned short constructed from the value -25,536 would return an int of value
     * 40,000.
     *
     * @return The positive value of the unsigned short
     */
    public int asUnsigned() {
        return Short.toUnsignedInt(s);
    }

    /**
     * Provides the internally stored short, which is treated as signed.
     *
     * @return The internally stored short
     */
    public short asShort() {
        return s;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        UnsignedShort value = (UnsignedShort) other;
        return this.s == value.s;
    }

    @Override
    public int hashCode() {
        return Objects.hash(s);
    }
}
