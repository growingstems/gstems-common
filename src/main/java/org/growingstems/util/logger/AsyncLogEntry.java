/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.IOException;

/**
 * An Asynchronous Log Entry for DHL Files. This class is not meant to be constructed by the user
 * directly. See {@link DataHiveLogger.Builder#makeAsyncLogEntry(String, LogEntryType)}.
 *
 * <p>An asynchronous log entry is a log entry that is generally not meant to be logged every single
 * cycle of the main user program loop. Every time data is logged to an asynchronous log entry via
 * {@link #accept(Object)}, the data is immediately queued to be logged to a file and a unique
 * timestamp is generated when {@link #accept(Object)} is called.
 *
 * <p>A few examples of things that would typically be logged asynchronously include:
 *
 * <ul>
 *   <li>Precisely timestamped events.
 *   <li>Periodic events.
 *   <li>State changes in a state machine when they occur (ie. only capturing the current state once
 *       on transition).
 *   <li>State machine transitions when they occur (i.e. what caused the transition of the current
 *       state).
 *   <li>Rising/Falling edge captures of infrequent change of logic level (e.g. user controller
 *       button presses/releases).
 * </ul>
 *
 * @param <T> The type of data to be logged
 */
public class AsyncLogEntry<T> extends LogEntry<T> {
    /** The Asynchronous Log Entry ID. */
    protected final int asyncId;

    /**
     * Constructs a new Asynchronous Log Entry
     *
     * @param name The Log Entry name
     * @param asyncId The Asynchronous Log Entry ID
     * @param entryType The type of this Log Entry
     */
    protected AsyncLogEntry(String name, int asyncId, LogEntryType<T> entryType) {
        super(name, entryType);
        this.asyncId = asyncId;
    }

    /**
     * Writes the provided data to the file queue immediately. During the process of writing to the
     * file queue, the data is also timestamped immediately.
     *
     * <p>Ignores any IOExceptions. No-ops if called before {@link LogEntry#init(DataHiveLogger)}.
     *
     * @param data The data to write immediately
     */
    @Override
    public void accept(T data) {
        if (logger == null) {
            return;
        }
        try {
            logger.logMessageHeader(asyncId);
            entryType.write(data);
        } catch (IOException e) {
            // TODO: Handle
        }
    }
}
