/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.IOException;

/**
 * An Synchronous Log Entry for DHL Files. This class is not meant to be constructed by the user
 * directly. See {@link DataHiveLogger.Builder#makeSyncLogEntry(String, LogEntryType, Object)}.
 *
 * <p><strong>NOTE:</strong> Due to a design limitation of Data Hive Logger files, all synchronous
 * log entries registered to a single file are logged under a single message update to the file and
 * as such, only a single timestamp is attributed to every registered synchronous log entry. Due to
 * this limitation, the timestamp attributed to any given log entry will be logged when
 * {@link DataHiveLogger#update()} is called and not when the data was collected for the log entry.
 *
 * <p>{@link #accept(Object)} can be called multiple times, but only the last call to
 * {@link #accept(Object)} will be logged to the file when {@link DataHiveLogger#update()} is
 * called.
 *
 * <p>A synchronous log entry is a log entry that will be logged every single cycle of the main user
 * program loop via {@link DataHiveLogger#update()}.
 *
 * <p>A few examples of things that would typically be logged synchronously include:
 *
 * <ul>
 *   <li>Constantly changing values (e.g. sensor readings, user joy stick positions).
 *   <li>Anything that should be logged all the time that also doesn't have to be precisely
 *       timestamped.
 * </ul>
 *
 * @param <T> The type of data to be logged
 */
public class SyncLogEntry<T> extends LogEntry<T> {
    /** The most recently provided data. */
    protected T data;

    /**
     * Constructs a new Synchronous Log Entry.
     *
     * @param name The Log Entry name
     * @param initial The value to log until a new value is provided
     */
    protected SyncLogEntry(String name, LogEntryType<T> entryType, T initial) {
        super(name, entryType);
        data = initial;
    }

    /**
     * Stores the provided data to be written to during the next Synchronous write. This occurs
     * whenever {@link DataHiveLogger#update()} is called.
     *
     * @param data The data to store for writing later
     */
    @Override
    public void accept(T data) {
        this.data = data;
    }

    /**
     * Writes the most recently provided data to the file.
     *
     * <p>No-ops if called before {@link LogEntry#init(DataHiveLogger)}.
     *
     * @throws IOException When writing to the file causes an {@link IOException} to be thrown
     */
    protected void write() throws IOException {
        if (logger == null) {
            return;
        }
        entryType.write(data);
    }
}
