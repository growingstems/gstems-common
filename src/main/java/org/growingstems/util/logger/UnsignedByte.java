/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.util.Objects;

/**
 * Represents an unsigned byte for purposes of logging to DHL files.
 *
 * <p>This should generally not be used external to writing DHL files unless required. The value in
 * this byte is still understood by Java as a signed value. This class is generally meant for
 * documentation purposes only.
 */
public class UnsignedByte {
    private final byte b;

    /**
     * Construct an unsigned byte given a current byte's value.
     *
     * @param b Value to treat as unsigned
     */
    public UnsignedByte(byte b) {
        this.b = b;
    }

    /**
     * Provides the unsigned byte's value as an int.
     *
     * <p>Values that a signed byte cannot store will be properly be interpreted as positive. For
     * example, the value 200, stored in a signed byte, would be interpreted as -56 because of
     * overflow, but an unsigned byte constructed from the value -56 would return an byte of value
     * 200.
     *
     * @return The positive value of the unsigned byte
     */
    public int asUnsigned() {
        return Byte.toUnsignedInt(b);
    }

    /**
     * Provides the internally stored byte, which is treated as signed.
     *
     * @return The internally stored byte
     */
    public byte asByte() {
        return b;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        UnsignedByte value = (UnsignedByte) other;
        return this.b == value.b;
    }

    @Override
    public int hashCode() {
        return Objects.hash(b);
    }
}
