/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.math.BigInteger;
import java.util.Objects;

/**
 * Represents an unsigned long for purposes of logging to DHL files.
 *
 * <p>This should generally not be used external to writing DHL files unless required. The value in
 * this long is still understood by Java as a signed value. This class is generally meant for
 * documentation purposes only.
 */
public class UnsignedLong {
    private static final BigInteger UNSIGNED_LONG_MASK =
            BigInteger.ONE.shiftLeft(Long.SIZE).subtract(BigInteger.ONE);

    private final long l;

    /**
     * Construct an unsigned long given a current long's value.
     *
     * @param l Value to treat as unsigned
     */
    public UnsignedLong(long l) {
        this.l = l;
    }

    /**
     * Provides the unsigned long's value as a {@link BigInteger}.
     *
     * <p>Values that a signed int cannot store will be properly interpreted as positive. For example,
     * the value 1000000000000000000l, stored in a signed long, would be interpreted as
     * -1,000,000,000,000,000,000 because of overflow, but an unsigned long constructed from the value
     * -1,000,000,000,000,000,000 would return a long of value 17,446,744,073,709,551,616.
     *
     * @return The positive value of the unsigned int
     */
    public BigInteger asUnsigned() {
        return BigInteger.valueOf(l).and(UNSIGNED_LONG_MASK);
    }

    /**
     * Provides the internally stored long, which is treated as signed.
     *
     * @return The internally stored long
     */
    public long asLong() {
        return l;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        UnsignedLong value = (UnsignedLong) other;
        return this.l == value.l;
    }

    @Override
    public int hashCode() {
        return Objects.hash(l);
    }
}
