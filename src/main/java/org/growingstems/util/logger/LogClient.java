/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * A networked client meant to communicate with a {@link LogServer}.
 *
 * <p>See {@link LogServer}'s documentation for details.
 */
public class LogClient {
    /** The directory to store log files in. */
    protected final File m_outputDirectory;
    /** Supported hash algorithms, in priority order */
    protected List<String> m_supportedAlgorithms = List.of("SHA-256", "SHA-1", "MD5");
    /** The port to provide to {@link Socket#Socket(InetAddress, int)} */
    protected int m_port = 5805;

    /** Stream to log status to */
    protected final Optional<Consumer<String>> m_logInfo;

    /**
     * Creates a {@link LogClient}, and makes the output directory if it does not exist.
     *
     * @param outputDirectory The directory in which to store log files
     * @throws IOException if the output directory already exists, and is not a directory.
     */
    public LogClient(File outputDirectory) throws IOException {
        this(outputDirectory, Optional.of(System.out::println));
    }

    /**
     * Creates a {@link LogClient}, and makes the output directory if it does not exist.
     *
     * @param outputDirectory The directory in which to store log files
     * @param logInfo Function to use for logging info
     * @throws IOException if the output directory already exists, and is not a directory.
     */
    public LogClient(File outputDirectory, Optional<Consumer<String>> logInfo) throws IOException {
        m_logInfo = logInfo;
        if (outputDirectory.exists()) {
            if (!outputDirectory.isDirectory()) {
                throw new IOException(
                        '"' + outputDirectory.getName() + "\" exists, and is not a directory!");
            }
        } else {
            outputDirectory.mkdir();
        }
        m_outputDirectory = outputDirectory;
    }

    /**
     * Handles reading all of the files' data from a stream.
     *
     * @param address The address of the server
     * @param deleteFiles true if the connection should be in "Deletion" mode
     * @throws IOException if the {@link Socket}, {@link DataInputStream}, or {@link DataOutputStream}
     *     throw this exception at any point
     * @throws NoSuchAlgorithmException if {@link MessageDigest#getInstance(String)} throws this
     *     exception
     */
    public void readLogs(InetAddress address, boolean deleteFiles)
            throws IOException, NoSuchAlgorithmException {
        String preferredAlgorithm = m_supportedAlgorithms.get(0);
        MessageDigest digest = MessageDigest.getInstance(preferredAlgorithm);
        try (Socket socket = new Socket(address, m_port);
                DigestInputStream digestStream = new DigestInputStream(socket.getInputStream(), digest);
                DataInputStream dataInputStream = new DataInputStream(digestStream);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {
            String algorithmOptions = String.join(",", m_supportedAlgorithms);
            dataOutputStream.writeUTF(algorithmOptions);
            String selectedAlgorithm = dataInputStream.readUTF();
            if (selectedAlgorithm.isEmpty()) {
                throw new RuntimeException("No matching hash algorithm provided!");
            } else if (!selectedAlgorithm.equals(preferredAlgorithm)) {
                // Algorithm has changed!
                digest = MessageDigest.getInstance(selectedAlgorithm);
                digestStream.setMessageDigest(digest);
            }

            dataOutputStream.writeBoolean(deleteFiles);
            long fileCount = dataInputStream.readLong();
            for (int i = 0; i < fileCount; ++i) {
                String fileName = dataInputStream.readUTF();
                m_logInfo.ifPresent(s -> s.accept("Reading data into file \"" + fileName + "\"..."));
                Optional<byte[]> optHash =
                        readToFile(new File(m_outputDirectory, fileName), dataInputStream, digest);
                if (optHash.isEmpty()) {
                    throw new RuntimeException("Error reading \"" + fileName + "\". Closing connection.");
                }
                m_logInfo.ifPresent(s -> s.accept("Successfully read \"" + fileName + "\"!"));

                byte[] hash = optHash.get();
                dataOutputStream.writeInt(hash.length);
                dataOutputStream.write(hash);
                dataOutputStream.flush();
            }
        }
    }

    /**
     * Sets the supported hash algorithms. The server will ultimately decide which gets used.
     *
     * <p>Default: ["SHA-256", "SHA-1", "MD5"]
     *
     * <p>The string is provided to {@link MessageDigest#getInstance(String)}. See its documentation
     * for details.
     *
     * <p>Warning: if a provided algorithm string is not actually supported,
     * {@link #readLogs(InetAddress, boolean)} may throw a {@link NoSuchAlgorithmException}
     *
     * @param supportedAlgorithms The hash algorithms to use, in priority order
     */
    public void setSupportedAlgorithms(List<String> supportedAlgorithms) {
        m_supportedAlgorithms = supportedAlgorithms;
    }

    /**
     * Gets the supported hash algorithms. The server will ultimately decide which gets used.
     *
     * <p>Default: ["SHA-256", "SHA-1", "MD5"]
     *
     * <p>The selected string is provided to {@link MessageDigest#getInstance(String)}. See its
     * documentation for details.
     *
     * @return The list of all supported algorithms
     */
    public List<String> getSupportedAlgorithms() {
        return m_supportedAlgorithms;
    }

    /**
     * Sets the server port to connect to.
     *
     * <p>Default: 5805.
     *
     * @param port The server's port
     */
    public void setPort(int port) {
        m_port = port;
    }

    /**
     * Gets the server port.
     *
     * @return The server's port
     */
    public int getPort() {
        return m_port;
    }

    /**
     * Handles reading a single file's data from a stream.
     *
     * @param file The file to store the results
     * @param dataInputStream The stream to read all data from
     * @param digest A pre-configured {@link MessageDigest} to use for hashing the received file
     * @return The hash of the file if successfully received, otherwise {@link Optional#empty()}
     * @throws FileNotFoundException if {@link FileOutputStream#FileOutputStream(File)} throws this
     *     exception
     * @throws IOException if {@link FileOutputStream#close()}, {@link FileOutputStream#write(byte[],
     *     int, int)}, {@link DataInputStream#readLong()}, or {@link DataInputStream#read(byte[], int,
     *     int)} throws this exception
     */
    protected static Optional<byte[]> readToFile(
            File file, DataInputStream dataInputStream, MessageDigest digest)
            throws FileNotFoundException, IOException {
        int bytes = 0;
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            long size = dataInputStream.readLong(); // read file size

            digest.reset();

            byte[] buffer = new byte[4 * 1024];
            while (size > 0
                    && (bytes = dataInputStream.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
                // Here we write the file using write method
                fileOutputStream.write(buffer, 0, bytes);
                size -= bytes; // read upto file size
            }
            return Optional.of(digest.digest());
        }
    }
}
