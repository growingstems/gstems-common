/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import java.io.IOException;
import java.util.List;

/**
 * A {@link LogEntryType} which specifically represents a Group Log Entry Type. This class is not
 * meant to be constructed by the user directly. See {@link DataHiveLogger}.
 *
 * <p>A group log entry type is typically analogous to a Java Class and is built up such that it
 * represents a specific class that is expected to be logged.
 *
 * @param <T> The type of loggable data
 */
public class GroupLogEntryType<T> extends LogEntryType<T> {
    /**
     * A member included in a {@link GroupLogEntryType}. Can be another {@link GroupLogEntryType}.
     *
     * @param <T> The type of Member
     */
    protected static class Member<T> {
        /** The Log Entry Type ID of the member. */
        protected final int typeId;
        /** The member name. */
        protected final String name;
        /** A writer capable of writing the member. */
        protected final IOConsumer<T> writer;

        /**
         * Constructs a {@link Member} given an ID, name, and writer.
         *
         * @param id The Log Entry Type ID of the member
         * @param name The member name
         * @param writer A writer capable of writing the member
         */
        protected Member(int id, String name, IOConsumer<T> writer) {
            this.typeId = id;
            this.name = name;
            this.writer = writer;
        }
    }

    /**
     * A {@link Member} which uses the DHL File Format's Enum feature.
     *
     * @param <T> The type of data to write to the DHL file to represent this Enum
     * @param <E> The enumerated type
     */
    protected static class EnumMember<T, E extends Enum<E>> extends Member<T> {
        /** A {@link Class} handle for the enumerated type */
        protected final Class<E> enumType;

        /**
         * Constructs an {@link EnumMember}
         *
         * @param enumType A {@link Class} handle for the enumerated type
         * @param id The Log Entry Type ID of the enumerated member
         * @param name The enumerated member name
         * @param writer A writer capable of writing the enumerated member
         */
        protected EnumMember(Class<E> enumType, int id, String name, IOConsumer<T> writer) {
            super(id, name, writer);
            this.enumType = enumType;
        }
    }

    /**
     * An array-based member included in a {@link GroupLogEntryType}.
     *
     * <p>It is up to the user to ensure this is only used for Array Types.
     *
     * @param <T> The Array Type of the Member
     */
    protected static class ArrayMember<T> extends Member<T> {
        /**
         * Constructs an {@link ArrayMember}<br>
         * It is up to the user to ensure this is only used for Array Types
         *
         * @param id The Log Entry Type ID of the enumerated member
         * @param name The enumerated member name
         * @param writer A writer capable of writing the enumerated member
         */
        protected ArrayMember(int id, String name, IOConsumer<T> writer) {
            super(id, name, writer);
        }
    }

    /** A list containing all {@link Member}s of the {@link GroupLogEntryType}. */
    protected final List<Member<T>> members;

    /**
     * Constructs a {@link GroupLogEntryType} given a list of {@link Member}s.
     *
     * @param typeId The Group Log Entry Type ID, according to the DHL File Format
     * @param name The name of the Group Log Entry Type
     * @param defaultInitialValue The default value that is initially logged and compared to in order
     *     to
     * @param members All members of the Group Log Entry Type
     */
    protected GroupLogEntryType(
            int typeId, String name, T defaultInitialValue, List<Member<T>> members) {
        super(typeId, name, defaultInitialValue);
        this.members = members;
    }

    /**
     * Writes the group type to the DHL File by writing each member.
     *
     * @param t The group type to write
     * @throws IOException If any members' {@link Member#writer} throws an {@link IOException} when
     *     writing the member.
     */
    @Override
    public void write(T t) throws IOException {
        for (Member<T> member : members) {
            member.writer.write(t);
        }
    }
}
