/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

/** Contains static methods that access and test arrays for various criteria. */
public class ArrayUtils {
    /**
     * Returns the number of entries in an array that are equal to a passed value.
     *
     * @param <E> the type of array to compare
     * @param array the array to check
     * @param compared the value to check for in the array
     * @return the number of equivalent entries
     */
    public static <E> int countEqual(E[] array, E compared) {
        int i = 0;
        for (E entry : array) {
            if (compared.equals(entry)) {
                i++;
            }
        }
        return i;
    }
}
