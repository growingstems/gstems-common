/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

import java.util.AbstractQueue;
import java.util.ArrayDeque;
import java.util.Iterator;

/**
 * This is an implementation of the circular buffer, a data structure with a static size that
 * overwrites previously added elements when that size is exceeded. Adding and Offering elements to
 * the Queue will always succeed, even if the size matches the max size, because it will
 * automatically delete the oldest element of the queue if needed to make room for the newest
 * element.
 */
public class CircularBuffer<E> extends AbstractQueue<E> {
    private final int m_bufferSize;
    private final ArrayDeque<E> m_internalQueue;

    /**
     * Creates a new Circular Buffer
     *
     * @param bufferSize the maximum number of elements the buffer can store
     */
    public CircularBuffer(int bufferSize) {
        if (bufferSize < 0) {
            throw new NegativeArraySizeException(
                    "Circular Buffers must have a non-negative buffer size.");
        }
        m_bufferSize = bufferSize;
        m_internalQueue = new ArrayDeque<>(bufferSize);
    }

    /**
     * Returns the maximum size the buffer can have before being overwritten
     *
     * @return maximum size
     */
    public int getMaxSize() {
        return m_bufferSize;
    }

    private void shrink() {
        while (size() > m_bufferSize) {
            remove();
        }
    }

    @Override
    public boolean offer(E e) {
        boolean result = m_internalQueue.offer(e);
        shrink();
        return result;
    }

    @Override
    public E peek() {
        return m_internalQueue.peek();
    }

    @Override
    public E poll() {
        return m_internalQueue.poll();
    }

    @Override
    public Iterator<E> iterator() {
        return m_internalQueue.iterator();
    }

    @Override
    public int size() {
        return m_internalQueue.size();
    }
}
