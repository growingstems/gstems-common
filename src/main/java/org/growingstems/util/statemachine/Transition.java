/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/** Represents a transition from one state to another. */
public interface Transition {
    /**
     * Get whether the criteria to trigger this transition have occurred.
     *
     * @return true if the transition should occur
     */
    boolean triggered();

    /**
     * Get the state to transition to.
     *
     * @return state to transition to as a result of this transition
     */
    State getTarget();

    /**
     * Returns the name of the transition.
     *
     * @return the name of the transition.
     */
    String getName();
}
;
