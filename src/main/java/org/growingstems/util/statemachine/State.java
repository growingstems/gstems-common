/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/**
 * Interface representing a state, generally used within a state machine. States are a collection of
 * functions that can be executed by a state machine at given points of time. States also contain
 * transitions to other states that a state machine can use to determine when to move from one state
 * to another.
 */
public interface State {
    /** Function that will be executed once before {@link #exec()} is called. */
    void init();

    /**
     * Function that will be executed each tick while this is the active state.<br>
     * <br>
     * <strong>Note:</strong> This function will never be executed before {@link #init()} is executed.
     */
    void exec();

    /**
     * Function that will be executed once whenever this state is being deactivated.<br>
     * <br>
     * <strong>Note:</strong> This function will never be executed until {@link #exec()} is executed
     * at least once.
     */
    void exit();

    /**
     * Add a transition from this state to another.
     *
     * @param t the transition to add to this state.
     */
    void addTransition(Transition t);

    /**
     * Get the next state that should be transitioned to.
     *
     * @return if a transition occurred, the state to transition to. null otherwise.
     */
    State getNextState();

    /**
     * Returns whichever transition that is triggered if there is one.
     *
     * @return the transition that was triggered. null if none was found.
     */
    Transition getTriggeredTransition();

    /**
     * Get the user provided name of this state.
     *
     * @return the name of this state.
     */
    String getStateName();

    /**
     * Clones the StateBase object without transitions.
     *
     * @param name Name of new cloned state.
     * @return cloned StateBase object without transitions.
     */
    State cloneWithoutTransitions(String name);
}
;
