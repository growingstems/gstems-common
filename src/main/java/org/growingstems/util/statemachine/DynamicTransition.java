/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import java.util.function.Supplier;

/**
 * Represents a transition from one state to another (possibly the same) state. The target state for
 * this transition is "dynamic", meaning that it can change based on conditions outside of the
 * control of the transition. It is valid to represent a transition from state A to state A using
 * this class
 */
public class DynamicTransition implements Transition {
    private final Supplier<Boolean> m_trigger;
    private final Supplier<State> m_targetSupplier;
    private final String m_name;

    /**
     * Construct a transition from one state to another state
     *
     * @param targetSupplier supplier that will return the correct state to transition to once the
     *     transition has been triggered
     * @param trigger supplier that will return true when this transition should occur
     */
    public DynamicTransition(Supplier<State> targetSupplier, Supplier<Boolean> trigger) {
        this("", targetSupplier, trigger);
    }

    /**
     * Construct a transition from one state to another state
     *
     * @param name name of transition
     * @param targetSupplier supplier that will return the correct state to transition to once the
     *     transition has been triggered
     * @param trigger supplier that will return true when this transition should occur
     */
    public DynamicTransition(String name, Supplier<State> targetSupplier, Supplier<Boolean> trigger) {
        m_name = name;
        m_trigger = trigger;
        m_targetSupplier = targetSupplier;
    }

    @Override
    public boolean triggered() {
        return m_trigger.get();
    }

    /**
     * Get the state to transition to <br>
     * Note: the supplier for target state will be evaluated every call to this function. As a result
     * of this, it is invalid to store the result of this function call to transition to later.
     *
     * @return state to transition to as a result of this transition
     */
    @Override
    public State getTarget() {
        return m_targetSupplier.get();
    }

    /**
     * Returns the name of the transition. Returns an empty string if none was provided on
     * construction.
     *
     * @return the name of the transition.
     */
    @Override
    public String getName() {
        return m_name;
    }
}
;
