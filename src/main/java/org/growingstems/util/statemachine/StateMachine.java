/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/**
 * Class that can be used to configure and run Finite State Machines.<br>
 * <br>
 * For details on what a FSM is, please refer to the following link:
 * https://en.wikipedia.org/wiki/Finite-state_machine.<br>
 * <br>
 * To configure the class for use, the user must simply define what the initial state to run is
 * during construction. Due to the referential nature of the State and Transition objects, it is not
 * required to register every state with the state machine instance.<br>
 * <br>
 * Calling {@link #step()} for the first time, or after {@link #stop()} has been called, will begin
 * normal operation. Continuing to call {@link #step()} iterates the state machine. See
 * {@link #step()} for more details on the operation of this class.<br>
 * <br>
 * The state machine can be stopped using {@link #stop()}. Calling {@link #stop()} will always
 * result in the current state exiting via {@link State#exit()}.
 */
public class StateMachine {
    protected final State m_initialState;
    protected State m_currentState = null;
    protected Transition m_triggeredTransition = null;
    protected boolean m_hasInitCurrentState = false;

    /**
     * Construct a state machine.
     *
     * @param initialState the initial state of the machine.
     */
    public StateMachine(State initialState) {
        m_initialState = initialState;
        m_currentState = m_initialState;
    }

    /**
     * Iterates the state machine.<br>
     * <br>
     * The first call to {@link #step()} when the state machine is starting from the initial state
     * will always result in running the initial state's {@link State#init()} and its
     * {@link State#exec()}. If the initial state provides a state to transition to (via
     * {@link State#getNextState()}), the initial state's {@link State#exit()} will also run, followed
     * by running the {@link State#init()} and {@link State#exec()} of the next state. This is the
     * only time that {@link State#init()} or {@link State#exec()} may be called twice in a single
     * {@link #step()}. After this, {@link #step()} has the same behavior as every consecutive call to
     * step.<br>
     * <br>
     * Each call to {@link #step()}, except for the first time (see above), will first result in a
     * check for the current state's {@link State#getNextState()} to see if a transition should occur.
     * If a transition should occur, the current state's {@link State#exit()} is first ran, then the
     * next state is staged as the current state and its {@link State#init()} and {@link State#exec()}
     * are ran.<br>
     * <br>
     * If none of the current state's transitions triggered, only the current state's
     * {@link State#exec()} function is ran.<br>
     * <br>
     * If the state machine is paused or stopped, {@link State#init()}, {@link State#exec()}, and
     * {@link State#exit()} will never be called.
     */
    public void step() {
        boolean initializedFirst = initCurrentState();
        if (initializedFirst) {
            execCurrentState();
        }

        // check for transition
        m_triggeredTransition = m_currentState.getTriggeredTransition();
        State nextState = null;
        if (m_triggeredTransition != null) {
            exitCurrentState();
            nextState = m_triggeredTransition.getTarget();
            m_currentState = nextState;
            initCurrentState();
        }

        if (!initializedFirst || nextState != null) {
            execCurrentState();
        }
    }

    /** Stop running the state machine, forcing the state machine to start from the initial state. */
    public void stop() {
        // run exit of current state
        exitCurrentState();
        m_currentState = m_initialState;
    }

    protected void pause() {
        exitCurrentState();
    }

    /**
     * Get the configured initial state provided at construction.
     *
     * @return configured initial state for the state machine
     */
    public State getInitialState() {
        return m_initialState;
    }

    /**
     * Get the current state. If stopped, null is returned.
     *
     * @return the current state
     */
    public State getCurrentState() {
        return m_currentState;
    }

    /**
     * Returns which transition was triggered to progress the state machine.
     *
     * @return the transition that triggered. null if none were found
     */
    public Transition getTriggeredTransition() {
        return m_triggeredTransition;
    }

    /**
     * Returns the state machine as a runnable state. <br>
     * <br>
     *
     * <table>
     * <caption> State function mapping </caption>
     * <tr>
     * <th>State Function</th>
     * <th>State Machine Function</th>
     * </tr>
     * <tr>
     * <td>init</td>
     * <td>N/A</td>
     * </tr>
     * <tr>
     * <td>exec</td>
     * <td>step</td>
     * </tr>
     * <tr>
     * <td>exit</td>
     * <td>stop OR pause</td>
     * </tr>
     * </table>
     *
     * @param stateName The name of the returned state.
     * @param pauseOnExit If true, runs the pause function on exit. Otherwise, runs stop.
     * @return the state machine as a runnable state.
     */
    public State getAsState(String stateName, boolean pauseOnExit) {
        return new ExecExitState(stateName, this::step, pauseOnExit ? this::pause : this::stop) {
            /**
             * Gets the name of the State Machine state with the name of the currently executing state
             * appended to the end.
             *
             * @return fully qualified state name
             */
            @Override
            public String getStateName() {
                if (m_currentState != null) {
                    return m_name + "." + m_currentState.getStateName();
                }
                return m_name;
            }
        };
    }

    /**
     * Initializes the current state if not yet initialized.<br>
     * Runs current state's {@link State#init()} if it has not yet been initialized. No-ops if current
     * state is already initialized.
     *
     * @return true if initialization occurs (false if already initialized)
     */
    protected boolean initCurrentState() {
        // No-op if already initialized
        if (m_hasInitCurrentState) {
            return false;
        }

        m_currentState.init();
        m_hasInitCurrentState = true;
        return true;
    }

    /**
     * Executes the current state if initialized.<br>
     * Runs current state's {@link State#exec()} if it has been initialized. No-ops if current state
     * isn't initialized.
     *
     * @return true if execution occurs (false if uninitialized)
     */
    protected boolean execCurrentState() {
        // No-op if not initialized
        if (!m_hasInitCurrentState) {
            return false;
        }

        m_currentState.exec();
        return true;
    }

    /**
     * Exits the current state if initialized.<br>
     * Runs current state's {@link State#exit()} if it has been initialized. No-ops if current state
     * isn't initialized.
     *
     * @return true if exit occurs (false if uninitialized)
     */
    protected boolean exitCurrentState() {
        // No-op if not initialized
        if (!m_hasInitCurrentState) {
            return false;
        }

        m_currentState.exit();
        m_hasInitCurrentState = false;
        return true;
    }
}
;
