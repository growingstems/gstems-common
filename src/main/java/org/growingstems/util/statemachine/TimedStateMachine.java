/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import java.util.function.Supplier;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/**
 * Class that can be used to configure and run Finite State Machines.<br>
 * <br>
 * For details on what a FSM is, please refer to the following link:
 * https://en.wikipedia.org/wiki/Finite-state_machine, but with a timer that is provided on
 * construction in order to keep track of the time since a transition last occurred.<br>
 * <br>
 * To configure the class for use, the user must simply define what the initial state to run is
 * during construction. Due to the referential nature of the State and Transition objects, it is not
 * required to register every state with the state machine instance.<br>
 * <br>
 * Calling {@link #step()} for the first time, or after {@link #stop()} has been called, will begin
 * normal operation. Continuing to call {@link #step()} iterates the state machine. See
 * {@link #step()} for more details on the operation of this class.<br>
 * <br>
 * The state machine can be stopped using {@link #stop()}. Calling {@link #stop()} will always
 * result in the current state exiting via {@link State#exit()}.
 */
public class TimedStateMachine extends StateMachine {
    private final Timer m_transitionTimer;
    private boolean m_paused = false;

    /**
     * Construct a stopped state machine.
     *
     * @param initialState the initial state of the machine.
     * @param timeSource source of time
     */
    public TimedStateMachine(State initialState, TimeSource timeSource) {
        super(initialState);

        m_transitionTimer = timeSource.createTimer();
    }

    /**
     * Stop running the state machine, forcing the state machine to start from the initial state. The
     * transition timer is also stopped and resetted.
     */
    @Override
    public void stop() {
        super.stop();
        m_transitionTimer.reset();
    }

    @Override
    protected void pause() {
        super.pause();
        m_paused = true;
    }

    /**
     * Create a transition to destinationState that will trigger after transitionTime has elapsed
     * since the state it is attached to initialized.
     *
     * @param destinationState the state to transition to after transitionTime has elapsed
     * @param transitionTime the amount of time after which the transition will trigger
     * @return transition pointing to destinationState that will trigger after transitionTime
     */
    public Transition getTimedTransition(State destinationState, Time transitionTime) {
        return new StaticTransition(
                destinationState, () -> m_transitionTimer.hasElapsed(transitionTime));
    }

    /**
     * Create a dynamic transition which will transition to which ever state is supplied by the
     * destinationStateSupplier after transitionTime has elapsed since the state it is attached to
     * initialized.
     *
     * @param destinationStateSupplier supplier that will return the correct state to transition to
     *     once the transition has been triggered
     * @param transitionTime the amount of time after which the transition will trigger
     * @return dynamic transition pointing to whichever state is supplied by destinationStateSupplier
     *     that will trigger after transitionTime
     */
    public Transition getTimedTransition(
            Supplier<State> destinationStateSupplier, Time transitionTime) {
        return new DynamicTransition(
                destinationStateSupplier, () -> m_transitionTimer.hasElapsed(transitionTime));
    }

    /**
     * Returns how long it has been since the last transition occurred.
     *
     * <p>A transition has not yet "occurred" until the new state starts, <i>ie. initializes</i>. This
     * means that after restarting the state machine, the time since transition is the time since the
     * first {@link #step()}. <br>
     * NOTE: If paused using {@link #pause()} or {@link #getAsState(String, boolean)} with
     * `pauseOnExit` set to `true`, the timer will be paused as well, and will resume after the next
     * {@link #step()}.
     *
     * @return time since the last transition occurred.
     */
    public Time getTimeSinceTransition() {
        return m_transitionTimer.get();
    }

    @Override
    protected boolean initCurrentState() {
        if (super.initCurrentState()) {
            if (!m_transitionTimer.isRunning()) {
                m_transitionTimer.start();
            }

            if (!m_paused) {
                m_transitionTimer.reset();
            } else {
                m_paused = false;
            }

            return true;
        }

        return false;
    }

    @Override
    protected boolean exitCurrentState() {
        m_transitionTimer.stop();
        return super.exitCurrentState();
    }
}
;
