/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/** State that only contains an exit function */
public class ExitState extends StateBase {
    private final Runnable m_exitFunc;

    /**
     * @param name name of the state
     * @param exitFunc Function to run when the state is transitioned out of
     */
    public ExitState(String name, Runnable exitFunc) {
        super(name);

        m_exitFunc = exitFunc;
    }

    @Override
    public void init() {
        // do nothing
    }

    @Override
    public void exec() {
        // do nothing
    }

    @Override
    public void exit() {
        m_exitFunc.run();
    }

    /**
     * Clones the StateBase object without transitions. The Exit runnable is copied by reference to
     * the cloned State object.
     *
     * @param name name of the state
     * @return Cloned StateBase object without transitions.
     */
    @Override
    public ExitState cloneWithoutTransitions(String name) {
        return new ExitState(name, m_exitFunc);
    }
}
;
