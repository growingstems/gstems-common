/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/** State with an init and exit function */
public class InitExitState extends StateBase {
    private final Runnable m_initFunc;
    private final Runnable m_exitFunc;

    /**
     * Construct a state with an init and exit function
     *
     * @param name name of the state
     * @param initFunc Function to run when the state is entered
     * @param exitFunc Function to run when the state is transitioned out of
     */
    public InitExitState(String name, Runnable initFunc, Runnable exitFunc) {
        super(name);

        m_initFunc = initFunc;
        m_exitFunc = exitFunc;
    }

    @Override
    public void init() {
        m_initFunc.run();
    }

    @Override
    public void exec() {
        // do nothing
    }

    @Override
    public void exit() {
        m_exitFunc.run();
    }

    /**
     * Clones the StateBase object without transitions. The Init and Exit runnables are copied by
     * reference to the cloned State object.
     *
     * @param name name of the state
     * @return Cloned StateBase object without transitions.
     */
    @Override
    public InitExitState cloneWithoutTransitions(String name) {
        return new InitExitState(name, m_initFunc, m_exitFunc);
    }
}
;
