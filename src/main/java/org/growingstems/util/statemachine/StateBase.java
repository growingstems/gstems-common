/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class that handles storing and executing of transitions. This is a good place to start
 * when making states. This class handles the addition and storage of transitions, as well
 * determining which state to transition to next.
 */
public abstract class StateBase implements State {
    /** Name of the state. */
    protected final String m_name;

    /** List of all possible transitions out of this state. */
    protected List<Transition> m_transitions = new ArrayList<>();

    /**
     * Construct a StateBase.
     *
     * @param name name of the state
     */
    protected StateBase(String name) {
        m_name = name;
    }

    @Override
    public abstract void init();

    @Override
    public abstract void exec();

    @Override
    public abstract void exit();

    @Override
    public void addTransition(Transition t) {
        m_transitions.add(t);
    }

    @Override
    public State getNextState() {
        Transition triggeredTransition = getTriggeredTransition();
        return triggeredTransition == null ? null : triggeredTransition.getTarget();
    }

    /**
     * Returns the first transition that is triggered. Transitions are checked based on the order they
     * were added with {@link #addTransition(Transition t)}.
     *
     * @return the transition that was triggered. null if none was found.
     */
    @Override
    public Transition getTriggeredTransition() {
        return m_transitions.stream().filter(Transition::triggered).findFirst().orElse(null);
    }

    @Override
    public String getStateName() {
        return m_name;
    }
}
;
