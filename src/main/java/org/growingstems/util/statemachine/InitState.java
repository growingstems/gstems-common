/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/** State that only has an init function */
public class InitState extends StateBase {
    private final Runnable m_initFunc;

    /**
     * @param name name for this state
     * @param initFunc Function to run when the state is entered
     */
    public InitState(String name, Runnable initFunc) {
        super(name);
        m_initFunc = initFunc;
    }

    @Override
    public void init() {
        m_initFunc.run();
    }

    @Override
    public void exec() {
        // do nothing
    }

    @Override
    public void exit() {
        // do nothing
    }

    /**
     * Clones the StateBase object without transitions. The Init runnable is copied by reference to
     * the cloned State object.
     *
     * @param name name of the state
     * @return Cloned StateBase object without transitions.
     */
    @Override
    public InitState cloneWithoutTransitions(String name) {
        return new InitState(name, m_initFunc);
    }
}
;
