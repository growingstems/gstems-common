/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import java.util.function.Supplier;

/**
 * Represents a transition from one state to another (possibly the same) state. The target state for
 * this transition will not change once the transition is constructed. <br>
 * It is valid to represent a transition from state A to state A using this class
 */
public class StaticTransition implements Transition {
    private final Supplier<Boolean> m_trigger;
    private final State m_target;
    private final String m_name;

    /**
     * Construct a transition from one state to another state
     *
     * @param target the state to move to once the transition occurs
     * @param trigger supplier that will return true when this transition should occur
     */
    public StaticTransition(State target, Supplier<Boolean> trigger) {
        this("", target, trigger);
    }

    /**
     * Construct a transition from one state to another state
     *
     * @param name name of transition
     * @param target the state to move to once the transition occurs
     * @param trigger supplier that will return true when this transition should occur
     */
    public StaticTransition(String name, State target, Supplier<Boolean> trigger) {
        m_name = name;
        m_trigger = trigger;
        m_target = target;
    }

    @Override
    public boolean triggered() {
        return m_trigger.get();
    }

    @Override
    public State getTarget() {
        return m_target;
    }

    /**
     * Returns the name of the transition. Returns an empty string if none was provided on
     * construction.
     *
     * @return the name of the transition.
     */
    @Override
    public String getName() {
        return m_name;
    }
}
;
