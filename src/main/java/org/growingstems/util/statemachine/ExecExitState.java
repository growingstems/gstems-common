/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/** State with an exec and exit function */
public class ExecExitState extends StateBase {
    private final Runnable m_execFunc;
    private final Runnable m_exitFunc;

    /**
     * Construct a state with an exec and exit function.
     *
     * @param name name of the state
     * @param execFunc Function to run when the state is stepped
     * @param exitFunc Function to run when the state is transitioned out of
     */
    public ExecExitState(String name, Runnable execFunc, Runnable exitFunc) {
        super(name);

        m_execFunc = execFunc;
        m_exitFunc = exitFunc;
    }

    @Override
    public void init() {
        // do nothing
    }

    @Override
    public void exec() {
        m_execFunc.run();
    }

    @Override
    public void exit() {
        m_exitFunc.run();
    }

    /**
     * Clones the StateBase object without transitions. The Exec and Exit runnables are copied by
     * reference to the cloned State object.
     *
     * @param name name of the state
     * @return Cloned StateBase object without transitions.
     */
    @Override
    public ExecExitState cloneWithoutTransitions(String name) {
        return new ExecExitState(name, m_execFunc, m_exitFunc);
    }
}
;
