/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/** State that only contains an exec function */
public class ExecState extends StateBase {
    private final Runnable m_execFunc;

    /**
     * Construct a state that will run the provided function as the exec function for the state.
     *
     * @param name name of the state
     * @param execFunc function to run when the state is stepped
     */
    public ExecState(String name, Runnable execFunc) {
        super(name);

        m_execFunc = execFunc;
    }

    @Override
    public void init() {
        // do nothing
    }

    @Override
    public void exec() {
        m_execFunc.run();
    }

    @Override
    public void exit() {
        // do nothing
    }

    /**
     * Clones the StateBase object without transitions. The Exec runnable is copied by reference to
     * the cloned State object.
     *
     * @param name name of the state
     * @return Cloned StateBase object without transitions.
     */
    @Override
    public ExecState cloneWithoutTransitions(String name) {
        return new ExecState(name, m_execFunc);
    }
}
;
