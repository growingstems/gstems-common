/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

/** State with an init and exec function */
public class InitExecState extends StateBase {
    private final Runnable m_initFunc;
    private final Runnable m_execFunc;

    /**
     * Construct a state with an init and exec function.
     *
     * @param name name of the state
     * @param initFunc Function to run when the state is entered
     * @param execFunc Function to run when the state is stepped
     */
    public InitExecState(String name, Runnable initFunc, Runnable execFunc) {
        super(name);

        m_initFunc = initFunc;
        m_execFunc = execFunc;
    }

    @Override
    public void init() {
        m_initFunc.run();
    }

    @Override
    public void exec() {
        m_execFunc.run();
    }

    @Override
    public void exit() {
        // do nothing
    }

    /**
     * Clones the StateBase object without transitions. The Init and Exec runnables are copied by
     * reference to the cloned State object.
     *
     * @param name name of the state
     * @return Cloned StateBase object without transitions.
     */
    @Override
    public InitExecState cloneWithoutTransitions(String name) {
        return new InitExecState(name, m_initFunc, m_execFunc);
    }
}
;
