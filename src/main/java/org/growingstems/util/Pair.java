/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

import java.util.Objects; // used for Objects.hash()

/**
 * Generic Pair class. Stores two objects
 *
 * @param <X> First object type stored
 * @param <Y> Second object type stored
 */
public class Pair<X, Y> {

    private X m_x;
    private Y m_y;

    /**
     * Create a Pair with the given values
     *
     * @param x First value
     * @param y Second value
     */
    public Pair(X x, Y y) {
        m_x = x;
        m_y = y;
    }

    /**
     * Get the first value stored in this Pair
     *
     * @return first value
     */
    public X getX() {
        return m_x;
    }

    /**
     * Get the second value stored in this Pair
     *
     * @return second value
     */
    public Y getY() {
        return m_y;
    }

    /**
     * Set the first value
     *
     * @param x new value
     */
    public void setX(X x) {
        m_x = x;
    }

    /**
     * Set the second value
     *
     * @param y new value
     */
    public void setY(Y y) {
        m_y = y;
    }

    /**
     * Test this Pair for equality with another Object.
     *
     * @param o the Object to test for equality with this Pair
     * @return true if the given Object is equal to this Pair else false
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Pair<?, ?>) {
            Pair<?, ?> rhs = (Pair<?, ?>) o;
            return rhs.m_x.equals(m_x) && rhs.m_y.equals(m_y);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(m_x, m_y);
    }

    /**
     * String representation of this Pair. String returned is of format "(x,y)"
     *
     * @return String representation of this Pair
     */
    @Override
    public String toString() {
        return "(" + m_x.toString() + "," + m_y.toString() + ")";
    }
}
