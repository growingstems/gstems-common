/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.drive;

import org.growingstems.util.Pair;

/**
 * Stores sens values and performs calculations that take in wheel and throttle to output motor
 * power.
 */
public class CheesyDrive {

    /** Indicates whether low sens values or high sens values will be used. */
    public enum GearState {
        LOW,
        HIGH
    }

    private double m_speedTurnSensLowMax;
    private double m_speedTurnSensLow;
    private double m_speedTurnSensHighMax;
    private double m_speedTurnSensHigh;
    private double m_quickTurnSensLow;
    private double m_quickTurnSensHigh;

    /**
     * Default constructor. Makes a new instance of CheesyDrive with all speedturn sens values set to
     * 0.6 and all quickturn sens values set to 1.
     */
    public CheesyDrive() {
        this(0.6, 0.6, 0.6, 0.6, 1, 1);
    }

    /**
     * Proper constructor, allows all 6 sens values to be configured.
     *
     * @param speedTurnSensLowMax Used when gear == low and quickturn == false
     * @param speedTurnSensLow Used when gear == low and quickturn == false
     * @param speedTurnSensHighMax Used when gear == high and quickturn == false
     * @param speedTurnSensHigh Used when gear == high and quickturn == false
     * @param quickTurnSensLow Used when gear == low and quickturn == true
     * @param quickTurnSensHigh Used when gear == high and quickturn == true
     */
    public CheesyDrive(
            double speedTurnSensLowMax,
            double speedTurnSensLow,
            double speedTurnSensHighMax,
            double speedTurnSensHigh,
            double quickTurnSensLow,
            double quickTurnSensHigh) {
        m_speedTurnSensLowMax = speedTurnSensLowMax;
        m_speedTurnSensLow = speedTurnSensLow;
        m_speedTurnSensHighMax = speedTurnSensHighMax;
        m_speedTurnSensHigh = speedTurnSensHigh;
        m_quickTurnSensHigh = quickTurnSensHigh;
        m_quickTurnSensLow = quickTurnSensLow;
    }

    public void setSpeedTurnSensLowMax(double val) {
        m_speedTurnSensLowMax = val;
    }

    public void setSpeedTurnSensLow(double val) {
        m_speedTurnSensLow = val;
    }

    public void setSpeedTurnSensHighMax(double val) {
        m_speedTurnSensHighMax = val;
    }

    public void setSpeedTurnSensHigh(double val) {
        m_speedTurnSensHigh = val;
    }

    public void setQuickTurnSensLow(double val) {
        m_quickTurnSensLow = val;
    }

    public void setQuickTurnSensHigh(double val) {
        m_quickTurnSensHigh = val;
    }

    public double getSpeedTurnSensLowMax() {
        return m_speedTurnSensLowMax;
    }

    public double getSpeedTurnSensLow() {
        return m_speedTurnSensLow;
    }

    public double getSpeedTurnSensHighMax() {
        return m_speedTurnSensHighMax;
    }

    public double getSpeedTurnSensHigh() {
        return m_speedTurnSensHigh;
    }

    public double getQuickTurnSensLow() {
        return m_quickTurnSensLow;
    }

    public double getQuickTurnSensHigh() {
        return m_quickTurnSensHigh;
    }

    /**
     * Performs the actions of hmi_cheesy_drive.vi, defaulting gear to LOW
     *
     * @param wheel influcences the power of wheels.
     * @param throttle influcences the direction of the robot.
     * @param quickTurn if true, uses quickTurn calculations. If false, uses speedTurn calculations
     * @return a {@code Pair<Double, Double>}, of the form (Left, Right) that represent motor powers.
     */
    public Pair<Double, Double> cheesyDrive(double wheel, double throttle, boolean quickTurn) {
        return cheesyDrive(wheel, throttle, GearState.LOW, quickTurn);
    }

    /**
     * Performs the actions of hmi_cheesy_drive.vi
     *
     * @param wheel influcences the power of wheels.
     * @param throttle influcences the direction of the robot.
     * @param gear determines whether high or low sens values are used in calculations.
     * @param quickTurn if true, uses quickTurn calculations. If false, uses speedTurn calculations
     * @return a {@code Pair<Double, Double>}, of the form (Left, Right) that represent motor powers.
     */
    public Pair<Double, Double> cheesyDrive(
            double wheel, double throttle, GearState gear, boolean quickTurn) {
        wheel = Math.max(-1, Math.min(1, wheel));
        throttle = Math.max(-1, Math.min(1, throttle));

        if (quickTurn) {
            return this.quickTurn(wheel, throttle, gear);
        } else {
            return this.speedTurn(wheel, throttle, gear);
        }
    }

    private Pair<Double, Double> quickTurn(double wheel, double throttle, GearState gear) {
        double left;
        double right;
        double tempVal;
        if (gear == GearState.LOW) {
            tempVal = wheel * m_quickTurnSensLow;
        } else {
            tempVal = wheel * m_quickTurnSensHigh;
        }

        left = tempVal + throttle;
        right = tempVal - throttle;
        left = Math.max(-1, Math.min(1, left));
        right = Math.max(-1, Math.min(1, right));

        return new Pair<Double, Double>(left, right);
    }

    private Pair<Double, Double> speedTurn(double wheel, double throttle, GearState gear) {
        double left;
        double right;
        double tempVal;

        if (gear == GearState.LOW) {
            tempVal = m_speedTurnSensLowMax - m_speedTurnSensLow;
            tempVal *= Math.abs(throttle);
            tempVal = m_speedTurnSensLowMax - tempVal;
        } else {
            tempVal = m_speedTurnSensHighMax - m_speedTurnSensHigh;
            tempVal *= Math.abs(throttle);
            tempVal = m_speedTurnSensHighMax - tempVal;
        }

        tempVal = tempVal * wheel * throttle;

        left = tempVal + throttle;
        right = tempVal - throttle;
        left = Math.max(-1, Math.min(1, left));
        right = Math.max(-1, Math.min(1, right));

        return new Pair<Double, Double>(left, right);
    }
}
