/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.geometry;

import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;
import org.growingstems.measurements.Angle;

/**
 * This is a class constructing a circular sector.
 *
 * @see <a href=
 *     "//https://en.wikipedia.org/wiki/Circular_sector">//https://en.wikipedia.org/wiki/Circular_sector</a>
 */
public class Cone2d {

    /*
     * Final member variables used to get values defined in the constructor
     */

    private final Angle m_fov;
    private final double m_radius;
    private final Pose2d m_pose;

    /**
     * Class constructor
     *
     * @param fov The field of view of the circular sector centered where the sector is pointing
     * @param radius The radius of the circular sector
     * @param pose The coordinate position and orientation of the sector
     */
    public Cone2d(Angle fov, double radius, Pose2d pose) {
        m_fov = fov;
        m_radius = radius;
        m_pose = pose;
    }

    /**
     * Given a defined Cone2d, this method takes in a given point and returns whether that point is in
     * the given cone.
     *
     * @param checkPoint The point that is checked to see if it is within the sector
     * @return boolean result of the parameter's position in the sector, true if the point is in the
     *     sector, false if the point is not in the sector
     */
    public boolean isInCone(Vector2d checkPoint) {
        Vector2d checkVector = checkPoint.sub(m_pose.getVector());
        return checkVector.getMagnitude() <= m_radius
                && checkVector.getAngle().difference(m_pose.getRotation()).abs().le(m_fov.div(2.0));
    }

    /*
     * Getter methods
     */

    public Angle getFov() {
        return m_fov;
    }

    public double getRadius() {
        return m_radius;
    }

    public Pose2d getPose() {
        return m_pose;
    }
}
