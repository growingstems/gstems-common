/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.EdgeDetector;

/**
 *
 *
 * <pre>
 * Performs the actions of a JK latch.
 * Has 2 booleans, J and K, that are associated with an edge, either Rising or Falling.
 * Based on J and K, it outputs a boolean.
 *  - if the J input recieves its designated edge, the output becomes True.
 *  - if the K input recieves its designated edge, the output becomes False.
 *  - if both inputs receive their designated edge at the same time, the output inverts.
 * </pre>
 */
public class JKLatch {
    private boolean m_output;
    private final EdgeDetector m_jEdge;
    private final EdgeDetector m_kEdge;

    /**
     * Takes in an initial output and edges for both J and K, individually.
     *
     * @param initialOut the starting output
     * @param jEdge the edge on which J is triggered
     * @param kEdge the edge on which K is triggered
     */
    public JKLatch(boolean initialOut, Edge jEdge, Edge kEdge) {
        m_output = initialOut;
        m_jEdge = new EdgeDetector(jEdge);
        m_kEdge = new EdgeDetector(kEdge);
    }

    /**
     * Takes in an initial output and one edge direction to use for both J and K.
     *
     * @param initialOut the starting output
     * @param bothEdges the edge on which J and K are triggered.
     */
    public JKLatch(boolean initialOut, Edge bothEdges) {
        this(initialOut, bothEdges, bothEdges);
    }

    /**
     * Takes in an initial output and sets both edges to rising.
     *
     * @param initialOut the starting output
     */
    public JKLatch(boolean initialOut) {
        this(initialOut, Edge.RISING, Edge.RISING);
    }

    /**
     * Takes in one edge direction to use for both J and K, and set the initial output to false
     *
     * @param bothEdges the edge on which J and K are triggered.
     */
    public JKLatch(Edge bothEdges) {
        this(false, bothEdges, bothEdges);
    }

    /**
     * Takes in edges for both J and K, individually, and set the initial output to false.
     *
     * @param jEdge the edge on which J is triggered
     * @param kEdge the edge on which K is triggered
     */
    public JKLatch(Edge jEdge, Edge kEdge) {
        this(false, jEdge, kEdge);
    }

    /** Sets initial output to false and sets both edges to rising. */
    public JKLatch() {
        this(false, Edge.RISING, Edge.RISING);
    }

    /**
     * Performs the action of a JK latch
     *
     * @param j on the triggered edge, either makes the output true or inverts it
     * @param k on the triggered edge, either makes the output false or inverts it
     * @return the output
     */
    public boolean step(boolean j, boolean k) {
        boolean jEdged = m_jEdge.update(j);
        boolean kEdged = m_kEdge.update(k);

        if (jEdged && kEdged) {
            m_output = !m_output;
        } else if (jEdged) {
            m_output = true;
        } else if (kEdged) {
            m_output = false;
        }
        return m_output;
    }
}
