/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.EdgeDetector;

/**
 * Tracks a value, and increments or decrements said value by 1. <br>
 * Whether the value changes on a rising or falling edge is configurable. <br>
 * Also allows for resetting the value to a configurable number.
 */
public class UpDownCounter {
    private int m_inital;
    private int m_count;
    private final EdgeDetector m_upEdge;
    private final EdgeDetector m_downEdge;

    /** Sets the inital value to 0 and edges to Rising. */
    public UpDownCounter() {
        this(0, Edge.RISING, Edge.RISING);
    }

    /**
     *
     *
     * <pre>
     * Sets the inital value to 0.
     * Allows configuration of the edge to trigger both up and down counts on.
     * </pre>
     *
     * @param edge The edge that, when detected, causes the counter to change.
     */
    public UpDownCounter(Edge edge) {
        this(0, edge, edge);
    }

    /**
     *
     *
     * <pre>
     * Sets the inital value to 0.
     * Allows configuration of the edge to trigger on for up and down counts seperately.
     * </pre>
     *
     * @param upCountEdge The edge that, when detected, causes the counter to increment.
     * @param downCountEdge The edge that, when detected, causes the counter to decrement.
     */
    public UpDownCounter(Edge upCountEdge, Edge downCountEdge) {
        this(0, upCountEdge, downCountEdge);
    }

    /**
     *
     *
     * <pre>
     * Sets the inital value to the supplied parameter.
     * The edges are set to rising.
     * </pre>
     *
     * @param initalVal The value to set the inital value to.
     */
    public UpDownCounter(int initalVal) {
        this(initalVal, Edge.RISING, Edge.RISING);
    }

    /**
     *
     *
     * <pre>
     * Sets the inital value to the supplied parameter.
     * Allows configuration of the edge to trigger both up and down counts on.
     * </pre>
     *
     * @param initalVal The value to set the inital value to.
     * @param edge The edge that, when detected, causes the counter to change. If {@code edge} is
     *     neither {@link Edge#RISING} nor {@link Edge#FALLING}, and error will be reported and the
     *     set edge will fall back to {@link Edge#RISING}.
     */
    public UpDownCounter(int initalVal, Edge edge) {
        this(initalVal, edge, edge);
    }

    /**
     *
     *
     * <pre>
     * Sets the inital value to the supplied parameter.
     * Allows configuration of the edge to trigger on for up and down counts seperately.
     * </pre>
     *
     * @param initalVal The value to set the inital value to.
     * @param upCountEdge The edge that, when detected, causes the counter to increment. If
     *     {@code edge} is neither {@link Edge#RISING} nor {@link Edge#FALLING}, an error will be
     *     reported and the set edge will fall back to {@link Edge#RISING}.
     * @param downCountEdge The edge that, when detected, causes the counter to decrement. If
     *     {@code edge} is neither {@link Edge#RISING} nor {@link Edge#FALLING}, an error will be
     *     reported and the set edge will fall back to {@link Edge#RISING}.
     */
    public UpDownCounter(int initalVal, Edge upCountEdge, Edge downCountEdge) {
        m_inital = initalVal;
        m_count = initalVal;

        if (verifyEdgeValidity(upCountEdge)) {
            m_upEdge = new EdgeDetector(upCountEdge);
        } else {
            m_upEdge = new EdgeDetector(Edge.RISING);
        }

        if (verifyEdgeValidity(downCountEdge)) {
            m_downEdge = new EdgeDetector(downCountEdge);
        } else {
            m_downEdge = new EdgeDetector(Edge.RISING);
        }
    }

    /**
     * Sets the inital value to the supplied parameter after calling {@link #reset()}.<br>
     * The current count will remain unchanged.
     *
     * @param value The value to set the inital value to.
     */
    public void setInitial(int value) {
        m_inital = value;
    }

    private boolean verifyEdgeValidity(Edge edge) {
        if (!edge.isEdge()) {
            System.err.println("Unsupported input: UpDownCounter does not accept " + edge);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns the inital value.
     *
     * @return The current inital value
     */
    public int getInitial() {
        return m_inital;
    }

    /**
     * Returns the {@link Edge} that must be detected in order to count up.
     *
     * @return The {@link Edge} that must be detected in order to count up
     */
    public Edge getUpEdge() {
        return m_upEdge.getTrackedEdge();
    }

    /**
     * Returns the {@link Edge} that must be detected in order to count down.
     *
     * @return The {@link Edge} that must be detected in order to count down
     */
    public Edge getDownEdge() {
        return m_downEdge.getTrackedEdge();
    }

    /** Sets the current count to the inital value. */
    public void reset() {
        m_count = m_inital;
    }

    /**
     * Depending on boolean inputs, either increments or decrements the counter by 1, or does nothing.
     * <br>
     * If both signals become true at the same time, the count is unchanged.
     *
     * @param upSignal If true when previously false and downSignal is not, increments the counter by
     *     1
     * @param downSignal If true when previously false and upSignal is not, decrements the counter by
     *     1
     * @return The current count
     */
    public int count(boolean upSignal, boolean downSignal) {
        boolean up = m_upEdge.update(upSignal);
        boolean down = m_downEdge.update(downSignal);
        if (up != down) {
            if (up) {
                m_count++;
            }
            if (down) {
                m_count--;
            }
        }

        return m_count;
    }

    /**
     * Returns the currently held count without sending a value to the edge detector.
     *
     * @return The current count
     */
    public int getCount() {
        return m_count;
    }
}
