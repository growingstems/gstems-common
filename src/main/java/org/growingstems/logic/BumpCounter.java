/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

/**
 * Tracks a value, incrementing or decrementing by 1. Also can reset and output the count and the
 * count multiplied by a supplied quanta.
 */
public class BumpCounter {
    private boolean m_firstRun = true;
    private final UpDownCounter udc;

    /** Bundle class that stores an int and a double. */
    public class OutClass {
        public int m_bumps;
        public double m_out;

        public OutClass(int bumps, double out) {
            this.m_bumps = bumps;
            this.m_out = out;
        }
    }

    /** Creates a new BumpCounter. */
    public BumpCounter() {
        udc = new UpDownCounter();
    }

    /**
     * Returns the current bumps and the quanta'd bumps.
     *
     * @param cntrl The value that is checked to determine if the counter goes up or down by 1.
     * @param ref The value that is added to the quanta'd output.
     * @param reset if true, the counter resets to 0.
     * @param quanta the value the bumps are multipled by to get an output.
     * @return The integer bumps and the double quanta multipled by the bumps, added to the ref.
     */
    public OutClass getBumps(double cntrl, double ref, boolean reset, double quanta) {
        return this.getBumps(cntrl, ref, reset, quanta, 0.5);
    }

    /**
     * Returns the current bumps and the quanta'd bumps.
     *
     * @param cntrl The value that is compared to activationThreshold to determine if the counter goes
     *     up or down by 1.
     * @param ref The value that is added to the quanta'd output.
     * @param reset if true, the counter resets to 0.
     * @param quanta the value the bumps are multipled by to get an output.
     * @param activationThreshhold the value that determines the minimum absolute value to cntrl to
     *     update the counter.
     * @return The integer bumps and the double quanta multipled by the bumps, added to the ref.
     */
    public OutClass getBumps(
            double cntrl, double ref, boolean reset, double quanta, double activationThreshhold) {
        udc.count(cntrl > activationThreshhold, cntrl < -activationThreshhold);

        if (m_firstRun) {
            udc.reset();
            m_firstRun = false;
        }

        if (reset) {
            udc.reset();
        }

        return new OutClass(udc.getCount(), (udc.getCount() * quanta) + ref);
    }

    public int getBumpCount() {
        return udc.getCount();
    }
}
