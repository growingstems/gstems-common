/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.signals.api.SignalModifier;

/** Aquires the last boolean of an array that became true, even if the boolean is no longer true. */
public class SingleButtonDetect implements SignalModifier<List<Boolean>, Integer> {
    private List<EdgeDetector> m_edgeDetectors = null;
    private int m_lastPressed = -1;

    /**
     * Provide the index of the last pressed button.
     *
     * <p>Inital value is -1. If the same button becomes true twice in a row, the value deselects.
     *
     * @throws DimensionMismatchException the size of buttonValues changes
     * @param buttonValues the values to check
     * @return the index of the last value to have a rising edge, or -1 when deselected.
     */
    public Integer update(List<Boolean> buttonValues) {
        if (m_edgeDetectors == null) {
            // First run: Make EdgeDetector List
            m_edgeDetectors = new ArrayList<>();
            for (int i = 0; i < buttonValues.size(); i++) {
                m_edgeDetectors.add(new EdgeDetector(Edge.RISING));
            }
        } else if (buttonValues.size() != m_edgeDetectors.size()) {
            // Received different number of buttonValues from first iteration
            throw new DimensionMismatchException(buttonValues.size(), m_edgeDetectors.size());
        }
        // Update edge detectors
        for (int i = 0; i < m_edgeDetectors.size(); i++) {
            if (m_edgeDetectors.get(i).update(buttonValues.get(i))) {
                if (m_lastPressed == i) {
                    // Button pressed twice in a row
                    m_lastPressed = -1;
                } else {
                    // New button pressed
                    m_lastPressed = i;
                }
            }
        }
        return m_lastPressed;
    }
}
