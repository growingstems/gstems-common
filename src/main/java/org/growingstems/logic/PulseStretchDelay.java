/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/**
 * This class is used to stretch pulses using a timer. Can be configured to detect rising or falling
 * edges independently.
 */
public class PulseStretchDelay implements SignalModifier<Boolean, Boolean> {
    private Time m_width;
    private Time m_delay;
    private final EdgeDetector m_edge;
    private final Timer m_timer;
    private boolean m_currentState;

    /**
     * Constructs a new pulse stretch delay
     *
     * @param width How long the output is high
     * @param delay How long after edge is detected before setting the output high
     * @param edge Which edge to detect. If {@code edge} is neither {@link Edge#RISING} nor
     *     {@link Edge#FALLING}, an error will be reported and the set edge will fall back to
     *     {@code RISING}
     * @param timeSource Source of time
     * @param initialState Value of the pulse stretch delay before any updates
     */
    public PulseStretchDelay(
            Time width, Time delay, Edge edge, TimeSource timeSource, boolean initialState) {
        m_width = width;
        m_delay = delay;
        m_timer = timeSource.createTimer();
        m_currentState = initialState;

        if (verifyEdgeValidity(edge)) {
            m_edge = new EdgeDetector(edge);
        } else {
            m_edge = new EdgeDetector(Edge.RISING);
        }
    }

    public void setDelay(Time delay) {
        m_delay = delay;
    }

    public void setWidth(Time width) {
        m_width = width;
    }

    public Time getDelay() {
        return m_delay;
    }

    public Time getWidth() {
        return m_width;
    }

    private boolean verifyEdgeValidity(Edge edge) {
        if (!edge.isEdge()) {
            System.err.println("Unsupported input: PulseStretchDelay does not support " + edge);
            return false;
        } else {
            return true;
        }
    }

    public Edge getEdge() {
        return m_edge.getTrackedEdge();
    }

    /**
     * Sets the output to a given boolean and resets the edge and timer
     *
     * @param newOut The new output
     */
    public void reset(boolean newOut) {
        m_currentState = newOut;
        m_timer.stop();
        m_timer.reset();
        m_edge.reset();
    }

    /**
     * Performs pulse stretch delay action When the timer is running, edge detector is ignored and
     * output is set based on if the delay/pulse width has elapsed If the timer is not running,
     * utilizes the edge detector to start the timer Width refers to pulse width
     *
     * @param in Input signal
     * @return Current state of the pulse stretch delay
     */
    @Override
    public Boolean update(Boolean in) {
        if (m_edge.update(in)) {
            m_timer.start();
        }
        if (m_timer.isRunning()) {
            if (m_timer.hasElapsed(m_delay.add(m_width))) {
                m_currentState = false;
                m_timer.stop();
                m_timer.reset();
            } else if (m_timer.hasElapsed(m_delay)) {
                m_currentState = true;
            } else {
                m_currentState = false;
            }
        }
        return m_currentState;
    }
}
