/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

/**
 * Enumerations, constants, and other utilties that are commonly used in reference to logic fields.
 */
public class LogicCore {
    /**
     * Represents a logic level transition (also known as an {@code edge}) or options related to a
     * logic level transition. A logic level transition is any time a logic level signal goes from
     * high(true) to low(false), or low(false) to high(true).
     *
     * <p>Refer to <a href=
     * "https://www.ni.com/docs/en-US/bundle/ni-hsdio/page/hsdio/fedge_trigger.html">Edge Trigger</a>
     * for a picture of what both edges look like visually.
     */
    public static enum Edge {
        /**
         * A false signal (LOW) followed by a true signal (HIGH), visualized as a signal rising from a
         * lower state to a higher state.
         */
        RISING,
        /**
         * A true signal (HIGH) followed by a false signal (LOW), visualized as a signal falling from a
         * higher state to a lower state.
         */
        FALLING,
        /** No edge. */
        NONE,
        /** Either a RISING or FALLING edge. */
        ANY;

        /**
         * Returns the opposite {@link Edge}. If the given {@link Edge} is {@code RISING},
         * {@code FALLING} is returned. If the given {@link Edge} is {@code FALLING}, {@code RISING} is
         * returned. If the given {@link Edge} is neither {@code RISING} nor {@code FALLING},
         * {@code null} is returned.
         *
         * @return returns the opposite edge. {@code null} if the given {@link Edge} is neither
         *     {@code RISING} nor {@code FALLING}
         */
        public Edge getOppositeEdge() {
            if (this == RISING) {
                return FALLING;
            } else if (this == FALLING) {
                return RISING;
            } else {
                return null;
            }
        }

        /**
         * Returns true if the {@link Edge} is {@link Edge#RISING} or {@link Edge#FALLING}.
         *
         * @return true if {@link Edge} is {@link Edge#RISING} or {@link Edge#FALLING}, false otherwise.
         */
        public boolean isEdge() {
            return (this == RISING || this == FALLING);
        }
    };
}
