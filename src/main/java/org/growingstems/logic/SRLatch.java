/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

/**
 * {@link SRLatch} is a latch based on the similarly named Flip-Flop based latch. <a href=
 * "https://en.wikipedia.org/wiki/Flip-flop_(electronics)#Flip-flop_types">Link to Flip-Flop SR
 * Latch wiki</a>. Supplying a {@code SRLatchType} allows the user to decide the behavior when both
 * S (set) and R (reset) are {@code true} at the same time.
 */
public class SRLatch {
    /**
     * {@link SRLatch} options that allow the user to decide which operation to prioritize when both S
     * (set) and R (reset) are {@code true} at the same time.
     */
    public enum SRLatchType {
        /**
         * This will prioritize setting the SR Latch (output {@code true}) if both the S and R inputs
         * are {@code true} at the same time when {@link SRLatch#update(boolean, boolean)} is called.
         */
        PRIORITIZE_SET,
        /**
         * This behavior is similar to the <a href=
         * "https://en.wikipedia.org/wiki/Flip-flop_(electronics)#SR_AND-OR_latch">SR AND-OR Latch</a>.
         * This will prioritize resetting the SR Latch (output {@code false}) if both the S and R inputs
         * are {@code true} at the same time when {@link SRLatch#update(boolean, boolean)} is called.
         */
        PRIORITIZE_RESET,
        /**
         * This behavior is similar to the <a href=
         * "https://en.wikipedia.org/wiki/Flip-flop_(electronics)#SR_NAND_latch">SR NAND Latch</a>. SR
         * Latch will not change the output if both the S and R inputs are {@code true} at the same time
         * when {@link SRLatch#update(boolean, boolean)} is called.
         */
        NO_CHANGE,
        /**
         * This behavior is similar to the <a
         * href="https://en.wikipedia.org/wiki/Flip-flop_(electronics)#JK_latch">JK Latch</a>. The SR
         * Latch will toggle the output ({@code true} to {@code false} or {@code false} to {@code true})
         * if both the S and R inputs are {@code true} at the same time when
         * {@link SRLatch#update(boolean, boolean)} is called.
         */
        TOGGLE
    }

    private boolean m_output;
    private SRLatchType m_type;

    /**
     * Constructs an {@link SRLatch} with an initial state and also a priority on which operation to
     * prioritize when both S (set) and R (reset) are {@code true} at the same time.
     *
     * @param initialState the initial output state of the {@link SRLatch}.
     * @param type which operation to prioritize when both S (set) and R (reset) are {@code true} at
     *     the same time.
     */
    public SRLatch(boolean initialState, SRLatchType type) {
        m_output = initialState;
        m_type = type;
    }

    /**
     * Constructs an {@link SRLatch} with an initial state. If both S (set) and R (reset) are
     * {@code true} at the same time, reset is given priority, forcing the output to {@code false}.
     * This version of the {@link SRLatch} is similar to the <a href=
     * "https://en.wikipedia.org/wiki/Flip-flop_(electronics)#SR_AND-OR_latch">SR AND-OR Latch</a>.
     *
     * @param initialState the initial output state of the {@link SRLatch}.
     */
    public SRLatch(boolean initialState) {
        this(initialState, SRLatchType.PRIORITIZE_RESET);
    }

    /**
     * Constructs an {@link SRLatch} with an initial state of {@code false}. If both S (set) and R
     * (reset) are {@code true} at the same time, reset is given priority, forcing the output to
     * {@code false}. This version of the {@link SRLatch} is similar to the <a href=
     * "https://en.wikipedia.org/wiki/Flip-flop_(electronics)#SR_AND-OR_latch">SR AND-OR Latch</a>.
     */
    public SRLatch() {
        this(false);
    }

    /**
     * Updates the {@link SRLatch}. If neither {@code s} (set) nor {@code r} (reset) is {@code true},
     * {@link SRLatch} maintains its previous output state from the last time {@link #update(boolean,
     * boolean)} was called, or {@code initialState} if {@link #update(boolean, boolean)} hasn't been
     * called yet. If both {@code s} and {@code r} are {@code true}, the behavior of
     * {@link #update(boolean, boolean)} is based on the {@code SRLatchType}, prioritizing reset if
     * none was provided on construction.
     *
     * @param s sets the {@link SRLatch} to {@code true}.
     * @param r resets the {@link SRLatch} to {@code false}.
     * @return the current state of the {@link SRLatch}.
     */
    public boolean update(boolean s, boolean r) {
        if (s && r) {
            switch (m_type) {
                case PRIORITIZE_SET:
                    m_output = true;
                    break;
                case PRIORITIZE_RESET:
                    m_output = false;
                    break;
                case TOGGLE:
                    m_output = !m_output;
                    break;
                case NO_CHANGE:
                    // Intentionally NOPing for NO_CHANGE
            }
        } else if (s) {
            m_output = true;
        } else if (r) {
            m_output = false;
        }

        return m_output;
    }
}
