/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.sensor;

/** Enumerations, constants, and other utilties that are commonly used when referencing sensors. */
public class SensorCore {
    public static enum Correlation {
        /** positive inputs cause the sensor reading to increase */
        POSITIVE_INCREASING,
        /** negative inputs cause the sensor reading to increase */
        NEGATIVE_INCREASING
    };
}
