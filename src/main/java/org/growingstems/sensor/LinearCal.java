/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.sensor;

import org.growingstems.signals.api.SignalModifier;

/**
 * LinearCal takes the inputed number then scales the number by the provided mutiplier and outputs
 * it.
 */
public class LinearCal implements SignalModifier<Double, Double> {
    private final double m_scaleFactor;

    /**
     * Constructs a LinearCal
     *
     * @param scaleFactor factored scaled input
     */
    public LinearCal(double scaleFactor) {
        m_scaleFactor = scaleFactor;
    }

    /**
     * Scales inpput by scale factor
     *
     * @param in Value to be scaled
     * @return Scale value
     */
    @Override
    public Double update(Double in) {
        return in * m_scaleFactor;
    }
}
