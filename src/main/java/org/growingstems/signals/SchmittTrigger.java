/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.signals.api.SignalModifier;

/**
 * Takes in a value and compares it to the thresholds. If the passed value is higher than the high
 * trigger, the output is true. If the passed value is lower than the low trigger, the output is
 * false. Otherwise, the value remains the same.
 */
public class SchmittTrigger implements SignalModifier<Double, Boolean> {
    private double m_highTrigger;
    private double m_lowTrigger;
    private boolean m_state;

    /**
     * Reduced constructor takes in inital values for both the high and low triggers, and the inital
     * output is false. Having a low trigger value higher than the high trigger value is undefined
     * behavior.
     *
     * @param low the value at which the output becomes false
     * @param high the value at which the output becomes true
     */
    public SchmittTrigger(double low, double high) {
        this(low, high, false);
    }

    /**
     * Full constructor takes in inital values for both the high and low triggers and the initial
     * output. Having a low trigger value higher than the high trigger value is undefined behavior.
     *
     * @param low the value at which the output becomes false
     * @param high the value at which the output becomes true
     * @param inital the inital state of the trigger
     */
    public SchmittTrigger(double low, double high, boolean inital) {
        setHigh(high);
        setLow(low);
        m_state = inital;
    }

    public void setHigh(double high) {
        m_highTrigger = high;
    }

    public void setLow(double low) {
        m_lowTrigger = low;
    }

    public double getHigh() {
        return m_highTrigger;
    }

    public double getLow() {
        return m_lowTrigger;
    }

    /**
     * Performs the function of a Schmitt trigger. Having a low trigger value higher than the high
     * trigger value is undefined behavior.
     *
     * @param senseIn The value compared to the triggers
     * @return The result of the comparison.
     */
    @Override
    public Boolean update(Double senseIn) {
        if (m_lowTrigger > m_highTrigger) {
            System.err.println("Low trigger is higher than high trigger.");
        }

        if (senseIn > m_highTrigger) {
            m_state = true;
        } else if (senseIn < m_lowTrigger) {
            m_state = false;
        }

        return m_state;
    }
}
