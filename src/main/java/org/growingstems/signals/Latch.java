/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.api.SignalModifier;

/**
 * A latch that operates as a "push-toggle" latch. The output of the latch inverts when a user
 * configurable edge is detected on the input boolean signal.
 */
public class Latch implements SignalModifier<Boolean, Boolean> {
    private final EdgeDetector m_edgeDetector;
    private boolean m_currentState;

    /**
     * Creates a latch that flips on a rising edge.
     *
     * @param initialState The value of the Latch before any updates occur
     */
    public Latch(boolean initialState) {
        this(Edge.RISING, initialState);
    }

    /**
     * Creates a latch that flips on an edge passed in by the user.
     *
     * @param edge The edge to change the output on. If {@code edge} is neither {@link Edge#RISING}
     *     nor {@link Edge#FALLING}, an error will be reported and the set edge will fall back to
     *     {@link Edge#RISING}.
     * @param initialState The value of the Latch before any updates occur
     */
    public Latch(Edge edge, boolean initialState) {
        m_currentState = initialState;

        if (verifyEdgeValidity(edge)) {
            m_edgeDetector = new EdgeDetector(edge);
        } else {
            m_edgeDetector = new EdgeDetector(Edge.RISING);
        }
    }

    private boolean verifyEdgeValidity(Edge edge) {
        if (!edge.isEdge()) {
            System.err.println("Unsupported input: Latch does not support " + edge);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns the edge being tracked to flip the output on.
     *
     * @return The tracked edge.
     */
    public Edge getEdge() {
        return m_edgeDetector.getTrackedEdge();
    }

    /**
     * Sets the output to a given boolean and resets the edge detector.
     *
     * @param newOut the new output.
     */
    public void reset(boolean newOut) {
        m_currentState = newOut;
        m_edgeDetector.reset();
    }

    /**
     * Performs the latch action. When the edge detector is tripped, the output changes from false to
     * true.
     *
     * @param input the input. causes a change when it turns to true or false, depending on the set
     *     edge.
     * @return the latch state.
     */
    @Override
    public Boolean update(Boolean input) {
        if (m_edgeDetector.update(input)) {
            m_currentState = !m_currentState;
        }
        return m_currentState;
    }
}
