/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals.vector;

import org.growingstems.math.Vector2dU;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;

/** Provides factories to create {@link Vector2dU} {@link SignalModifier}s. */
public class VectorUModifiers {
    /**
     * Given an X and Y double {@link SignalModifier}s, creates a {@link Vector2dU}
     * {@link SignalModifier}s where the X and Y components of the input {@link Vector2dU} are
     * separately modified and then recombined into the given output {@link Vector2dU}.
     *
     * @param <U> input unit
     * @param <V> output unit
     * @param x the signal modifier that will be applied to the X component of the input
     * @param y the signal modifier that will be applied to the Y component of the input
     * @return the resulting {@link Vector2dU} {@link SignalModifier}
     */
    public static <U extends Unit<U>, V extends Unit<V>>
            SignalModifier<Vector2dU<U>, Vector2dU<V>> xyModifier(
                    SignalModifier<U, V> x, SignalModifier<U, V> y) {
        return v -> new Vector2dU<V>(x.update(v.getX()), y.update(v.getY()));
    }

    /**
     * Given an X {@link SignalModifier}, creates a {@link Vector2dU} {@link SignalModifier}s where
     * the X component of the input {@link Vector2dU} is modified, but the Y component of the input
     * {@link Vector2dU} is maintained, resulting in the given output {@link Vector2dU}.
     *
     * @param <U> input/output unit
     * @param x the signal modifier that will be applied to the X component of the input
     * @return the resulting {@link Vector2dU} {@link SignalModifier}
     */
    public static <U extends Unit<U>> SignalModifier<Vector2dU<U>, Vector2dU<U>> xModifier(
            SignalModifier<U, U> x) {
        return xyModifier(x, y -> y);
    }

    /**
     * Given an Y {@link SignalModifier}, creates a {@link Vector2dU} {@link SignalModifier}s where
     * the Y component of the input {@link Vector2dU} is modified, but the X component of the input
     * {@link Vector2dU} is maintained, resulting in the given output {@link Vector2dU}.
     *
     * @param <U> input/output unit
     * @param y the signal modifier that will be applied to the Y component of the input
     * @return the resulting {@link Vector2dU} {@link SignalModifier}
     */
    public static <U extends Unit<U>> SignalModifier<Vector2dU<U>, Vector2dU<U>> yModifier(
            SignalModifier<U, U> y) {
        return xyModifier(x -> x, y);
    }

    /**
     * Given a Magnitude and Angle {@link SignalModifier}s, creates a {@link Vector2dU}
     * {@link SignalModifier}s where the Angle and Magnitude components of the input {@link Vector2dU}
     * are separately modified and then recombined into the given output {@link Vector2dU}.
     *
     * @param <U> input unit
     * @param <V> output unit
     * @param mag the signal modifier that will be applied to the Magnitude component of the input
     * @param angle the signal modifier that will be applied to the Angle component of the input
     * @return the resulting {@link Vector2dU} {@link SignalModifier}
     */
    public static <U extends Unit<U>, V extends Unit<V>>
            SignalModifier<Vector2dU<U>, Vector2dU<V>> magAngleModifier(
                    SignalModifier<U, V> mag, SignalModifier<Angle, Angle> angle) {
        return v -> Vector2dU.fromPolar(mag.update(v.getMagnitude()), angle.update(v.getAngle()));
    }

    /**
     * Given a Magnitude {@link SignalModifier}, creates a {@link Vector2dU} {@link SignalModifier}s
     * where the Magnitude component of the input {@link Vector2dU} is modified, but the Angle
     * component of the input {@link Vector2dU} is maintained, resulting in the given output
     * {@link Vector2dU}.
     *
     * @param <U> input unit
     * @param <V> output unit
     * @param mag the signal modifier that will be applied to the Magnitude component of the input
     * @return the resulting {@link Vector2dU} {@link SignalModifier}
     */
    public static <U extends Unit<U>, V extends Unit<V>>
            SignalModifier<Vector2dU<U>, Vector2dU<V>> magModifier(SignalModifier<U, V> mag) {
        return magAngleModifier(mag, a -> a);
    }

    /**
     * Given a Angle {@link SignalModifier}, creates a {@link Vector2dU} {@link SignalModifier}s where
     * the Angle component of the input {@link Vector2dU} is modified, but the Magnitude component of
     * the input {@link Vector2dU} is maintained, resulting in the given output {@link Vector2dU}.
     *
     * @param <U> input/output unit
     * @param angle the signal modifier that will be applied to the Angle component of the input
     * @return the resulting {@link Vector2dU} {@link SignalModifier}
     */
    public static <U extends Unit<U>> SignalModifier<Vector2dU<U>, Vector2dU<U>> angleModifier(
            SignalModifier<Angle, Angle> angle) {
        return magAngleModifier(m -> m, angle);
    }
}
