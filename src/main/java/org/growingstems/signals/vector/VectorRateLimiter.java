/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals.vector;

import java.util.Optional;
import org.growingstems.math.Vector2d;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/**
 * This class is meant to help limit the rate of change of a {@link Vector2d} in a cartesian system.
 */
public class VectorRateLimiter implements SignalModifier<Vector2d, Vector2d> {
    private Vector2d m_currentValue;
    private final Timer m_timer;
    private Frequency m_rate;

    /**
     * Constructs a {@link VectorRateLimiter} with a given rate of change applied in a cartesian
     * system and a time source used to control the rate of change.
     *
     * @param timeSource a time source to track change in time
     * @param rate the rate of change to limit how quickly the {@link Vector2d} can change
     * @param initial the initial starting value
     */
    public VectorRateLimiter(TimeSource timeSource, Frequency rate, Vector2d initial) {
        m_currentValue = initial;
        m_timer = timeSource.createTimer();
        m_timer.start();
        m_rate = rate;
    }

    /**
     * Constructs a {@link VectorRateLimiter} with a given rate of change applied in a cartesian
     * system and a time source used to control the rate of change.
     *
     * @param timeSource a time source to track change in time
     * @param rate the rate of change to limit how quickly the {@link Vector2d} can change
     */
    public VectorRateLimiter(TimeSource timeSource, Frequency rate) {
        this(timeSource, rate, null);
    }

    /**
     * Adjusts the rate of change.
     *
     * @param rate the new rate of change to use as a frequency
     * @return this {@link VectorRateLimiter} instance
     */
    public VectorRateLimiter setRate(Frequency rate) {
        m_rate = rate;
        return this;
    }

    /**
     * Gets the current rate of change.
     *
     * @return rate of change as a frequency
     */
    public Frequency getRate() {
        return m_rate;
    }

    /**
     * Checks what the internal value is that is stored without adjusting it.
     *
     * @return the current internal value
     */
    public Optional<Vector2d> peek() {
        return Optional.ofNullable(m_currentValue);
    }

    /**
     * Resets the {@link VectorRateLimiter} so that the next inputted value to
     * {@link VectorRateLimiter#update(Vector2d)} is accepted as the current value without any rate
     * limiting applied.
     *
     * @return this {@link VectorRateLimiter} instance
     */
    public VectorRateLimiter reset() {
        return forceUpdate(null);
    }

    /**
     * Forces the internal value to be the given input.
     *
     * @param input the value used to force the internal value
     * @return this {@link VectorRateLimiter} instance
     */
    public VectorRateLimiter forceUpdate(Vector2d input) {
        m_currentValue = input;
        return this;
    }

    /**
     * Given the command input {@link Vector2d}, limits how quickly the output {@link Vector2d} will
     * be based on the current rate.
     *
     * @param input input value to be rate limited
     * @return result of the rate limiting
     */
    @Override
    public Vector2d update(Vector2d input) {
        double dist = m_rate.mul(m_timer.reset()).asNone();

        if (m_currentValue == null) {
            return m_currentValue = input;
        }

        Vector2d diff = input.sub(m_currentValue);
        if (diff.getMagnitude() > dist) {
            return m_currentValue = m_currentValue.add(new Vector2d(dist, diff.getAngle()));
        } else {
            return m_currentValue = input;
        }
    }
}
