/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals.vector;

import org.growingstems.math.Vector2d;
import org.growingstems.measurements.Angle;
import org.growingstems.signals.api.SignalModifier;

/** Provides factories to create {@link Vector2d} {@link SignalModifier}s. */
public class VectorModifiers {
    /**
     * Given an X and Y double {@link SignalModifier}s, creates a {@link Vector2d}
     * {@link SignalModifier}s where the X and Y components of the input {@link Vector2d} are
     * separately modified and then recombined into the given output {@link Vector2d}.
     *
     * @param x the signal modifier that will be applied to the X component of the input
     * @param y the signal modifier that will be applied to the Y component of the input
     * @return the resulting {@link Vector2d}
     */
    public static SignalModifier<Vector2d, Vector2d> xyModifier(
            SignalModifier<Double, Double> x, SignalModifier<Double, Double> y) {
        return v -> new Vector2d(x.update(v.getX()), y.update(v.getY()));
    }

    /**
     * Given an X {@link SignalModifier}, creates a {@link Vector2d} {@link SignalModifier}s where the
     * X component of the input {@link Vector2d} is modified, but the Y component of the input
     * {@link Vector2d} is maintained, resulting in the given output {@link Vector2d}.
     *
     * @param x the signal modifier that will be applied to the X component of the input
     * @return the resulting {@link Vector2d}
     */
    public static SignalModifier<Vector2d, Vector2d> xModifier(SignalModifier<Double, Double> x) {
        return xyModifier(x, y -> y);
    }

    /**
     * Given an Y {@link SignalModifier}, creates a {@link Vector2d} {@link SignalModifier}s where the
     * Y component of the input {@link Vector2d} is modified, but the X component of the input
     * {@link Vector2d} is maintained, resulting in the given output {@link Vector2d}.
     *
     * @param y the signal modifier that will be applied to the Y component of the input
     * @return the resulting {@link Vector2d}
     */
    public static SignalModifier<Vector2d, Vector2d> yModifier(SignalModifier<Double, Double> y) {
        return xyModifier(x -> x, y);
    }

    /**
     * Given a Magnitude and Angle {@link SignalModifier}s, creates a {@link Vector2d}
     * {@link SignalModifier}s where the Angle and Magnitude components of the input {@link Vector2d}
     * are separately modified and then recombined into the given output {@link Vector2d}.
     *
     * @param mag the signal modifier that will be applied to the Magnitude component of the input
     * @param angle the signal modifier that will be applied to the Angle component of the input
     * @return the resulting {@link Vector2d}
     */
    public static SignalModifier<Vector2d, Vector2d> magAngleModifier(
            SignalModifier<Double, Double> mag, SignalModifier<Angle, Angle> angle) {
        return v -> new Vector2d(mag.update(v.getMagnitude()), angle.update(v.getAngle()));
    }

    /**
     * Given a Magnitude {@link SignalModifier}, creates a {@link Vector2d} {@link SignalModifier}s
     * where the Magnitude component of the input {@link Vector2d} is modified, but the Angle
     * component of the input {@link Vector2d} is maintained, resulting in the given output
     * {@link Vector2d}.
     *
     * @param mag the signal modifier that will be applied to the Magnitude component of the input
     * @return the resulting {@link Vector2d}
     */
    public static SignalModifier<Vector2d, Vector2d> magModifier(SignalModifier<Double, Double> mag) {
        return magAngleModifier(mag, a -> a);
    }

    /**
     * Given a Angle {@link SignalModifier}, creates a {@link Vector2d} {@link SignalModifier}s where
     * the Angle component of the input {@link Vector2d} is modified, but the Magnitude component of
     * the input {@link Vector2d} is maintained, resulting in the given output {@link Vector2d}.
     *
     * @param angle the signal modifier that will be applied to the Angle component of the input
     * @return the resulting {@link Vector2d}
     */
    public static SignalModifier<Vector2d, Vector2d> angleModifier(
            SignalModifier<Angle, Angle> angle) {
        return magAngleModifier(m -> m, angle);
    }
}
