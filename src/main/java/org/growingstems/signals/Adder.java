package org.growingstems.signals;

import java.util.function.BinaryOperator;
import java.util.stream.Stream;
import org.growingstems.measurements.Unit;

/**
 * Simple Adder class which creates a {@link Reducer} that adds all elements of a {@link Stream}. If
 * there are no elements, the result will be zero.
 */
public class Adder<S> extends Reducer<S> {
    /**
     * Factory function for making an {@link Adder} of {@link Double}s.
     *
     * @return An {@link Adder} of {@link Double}s
     */
    public static Adder<Double> ofDouble() {
        return new Adder<Double>(0.0, Double::sum);
    }

    /**
     * Factory function for making an {@link Adder} of {@link Float}s.
     *
     * @return An {@link Adder} of {@link Float}s
     */
    public static Adder<Float> ofFloat() {
        return new Adder<Float>(0.f, Float::sum);
    }

    /**
     * Factory function for making an {@link Adder} of {@link Long}s.
     *
     * @return An {@link Adder} of {@link Long}s
     */
    public static Adder<Long> ofLong() {
        return new Adder<Long>(0l, Long::sum);
    }

    /**
     * Factory function for making an {@link Adder} of {@link Integer}s.
     *
     * @return An {@link Adder} of {@link Integer}s
     */
    public static Adder<Integer> ofInt() {
        return new Adder<Integer>(0, Integer::sum);
    }

    /**
     * Factory function for making an {@link Adder} of {@link Unit}s.
     *
     * @param <U> The {@link Unit} type to add
     * @param zero A {@link Unit} whose value is zero
     * @return An {@link Adder} of {@link Unit}s
     */
    public static <U extends Unit<U>> Adder<U> ofUnit(U zero) {
        return new Adder<U>(zero, Unit::sum);
    }

    /**
     * Construct an {@link Adder} given a value for zero, and an add function. <br>
     * <br>
     * Note: Please use one of the static factory methods if one already exists for the type you are
     * using.
     *
     * @param zero A value of zero
     * @param addFunction A function that adds two values together and returns the result
     */
    public Adder(S zero, BinaryOperator<S> addFunction) {
        super(zero, addFunction);
    }
}
