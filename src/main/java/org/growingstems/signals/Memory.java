/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.signals.api.SignalModifier;

/**
 * This generic class stores the last value it was given. Upon initialization, the value memorized
 * is null. The value can be updated to be another value. The value saved in this class can be
 * easily retrieved. <br>
 * Memory returns a reference to the value passed in. No copy is made.
 *
 * @param <E> Any variable or class type of which values will be stored.
 */
public class Memory<E> implements SignalModifier<E, E> {

    private E m_memorized;

    public Memory() {
        m_memorized = null;
    }

    public Memory(E startingValue) {
        m_memorized = startingValue;
    }

    /**
     * Returns the last value that was stored
     *
     * @return The value being saved
     */
    public E getLastValue() {
        return m_memorized;
    }

    /**
     * Updates the value stored to the class
     *
     * @param in The value to save
     * @return The unmodified input
     */
    @Override
    public E update(E in) {
        m_memorized = in;
        return in;
    }
}
