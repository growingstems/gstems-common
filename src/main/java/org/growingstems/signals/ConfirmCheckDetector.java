/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StaticTransition;
import org.growingstems.util.statemachine.TimedStateMachine;
import org.growingstems.util.timer.TimeSource;

/**
 * Two-step press-and-hold button verification system
 *
 * <p>Safety so that users do not activate button-associated features without intending to. Returns
 * enum of current step of the verification system.
 */
public class ConfirmCheckDetector
        implements SignalModifier<Boolean, ConfirmCheckDetector.ConfirmState> {
    private boolean m_currentButtonState = false;

    private Time m_confirmingTime;
    private Time m_cancelTime;
    private Time m_confirmedTime;

    public static enum ConfirmState {
        IDLE,
        CONFIRMING,
        CONFIRMED,
    }

    private final TimedStateMachine m_stateMachine;

    private ConfirmState m_currVal;

    /**
     * Constructor for the ConfirmCheckDetector, initializing state machine
     *
     * @param confirmingTime time the button is held to begin confirming
     * @param cancelTime time button is held to cancel
     * @param confirmedTime time button is held to a second time to confirm reset
     * @param timeSource source of time
     */
    public ConfirmCheckDetector(
            Time confirmingTime, Time cancelTime, Time confirmedTime, TimeSource timeSource) {
        m_confirmingTime = confirmingTime;
        m_cancelTime = cancelTime;
        m_confirmedTime = confirmedTime;

        State idleState = new InitState("idleState", () -> m_currVal = ConfirmState.IDLE);
        State triggerState = new InitState("trigger", () -> m_currVal = ConfirmState.IDLE);
        State confBeginState = new InitState("confBegin", () -> m_currVal = ConfirmState.CONFIRMING);
        State confReleaseState =
                new InitState("confRelease", () -> m_currVal = ConfirmState.CONFIRMING);
        State confPressState = new InitState("confPress", () -> m_currVal = ConfirmState.CONFIRMING);
        State confirmedState = new InitState("confirmed", () -> m_currVal = ConfirmState.CONFIRMED);
        State cancelState = new InitState(
                "cancel", () -> m_currVal = ConfirmState.IDLE); // Should not execute anything on call

        m_stateMachine = new TimedStateMachine(idleState, timeSource);

        idleState.addTransition(new StaticTransition(triggerState, () -> m_currentButtonState));
        triggerState.addTransition(new StaticTransition(cancelState, () -> !m_currentButtonState));
        cancelState.addTransition(new StaticTransition(idleState, () -> true));
        triggerState.addTransition(m_stateMachine.getTimedTransition(confBeginState, m_confirmingTime));
        confBeginState.addTransition(
                new StaticTransition(confReleaseState, () -> !m_currentButtonState));
        confReleaseState.addTransition(m_stateMachine.getTimedTransition(cancelState, m_cancelTime));
        confReleaseState.addTransition(
                new StaticTransition(confPressState, () -> m_currentButtonState));
        confPressState.addTransition(new StaticTransition(cancelState, () -> !m_currentButtonState));
        confPressState.addTransition(
                m_stateMachine.getTimedTransition(confirmedState, m_confirmedTime));
        confirmedState.addTransition(new StaticTransition(idleState, () -> !m_currentButtonState));
    }

    /**
     * Walks a boolean input through a state machine to determine state enum
     *
     * @param signal the provided boolean
     * @return the enum category of the current state after stepping
     */
    @Override
    public ConfirmState update(Boolean signal) {
        // step through the state machine
        m_currentButtonState = signal;
        m_stateMachine.step();
        return m_currVal;
    }
}
