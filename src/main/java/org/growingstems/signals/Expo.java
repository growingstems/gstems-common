/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.math.Range;
import org.growingstems.signals.api.SignalModifier;

/**
 * Expo is based off of expo settings commonly found on RC remote controllers. Expo is meant to
 * remap a linear control into an exponential based control to give more control at lower speeds
 * while still providing full speed when the joystick is fully deflected.
 *
 * <p>See math <a href=
 * "https://github.com/ArduPilot/ardupilot/blob/ed0921a4186087c5b2ac13187b2f4af2cc9c33ec/libraries/AP_Math/AP_Math.cpp#L123">here</a>
 */
public class Expo implements SignalModifier<Double, Double> {
    private double m_coefficient;
    // Expo: y = ax + bx^2 + cx^3
    private double m_a;
    private double m_b;
    private double m_c;

    /**
     * Creates an exponential-like signal modifier meant to work with normalized signals [-1.0, 1.0].
     * <br>
     * <br>
     * A positive {@code coefficient} will make {@link Expo} behave more like a exponential equation.
     * Input values with a low magnitude will have less of an effect on the output value, while input
     * values with a higher magnitude will have more of an effect on the output value, giving more
     * "control" at low input magnitudes.<br>
     * <br>
     * A negative {@code coefficient} will make {@link Expo} behave more like a logarithmic equation.
     * Input values with a low magnitude will have more of an effect on the output value, while input
     * values with a higher magnitude will have less of an effect on the output value, giving more
     * "control" at high input magnitudes.<br>
     * <br>
     * A {@code coefficient} of zero will make {@link Expo} behave linearly.
     *
     * @param coefficient the {@code coefficient} used when calculating the {@link Expo} equation.
     */
    public Expo(double coefficient) {
        setCoefficient(coefficient);
    }

    /**
     * Sets the {@code coefficient} of the {@link Expo} equation.
     *
     * @param coefficient the {@code coefficient} used when calculating the {@link Expo} equation.
     */
    public void setCoefficient(double coefficient) {
        m_coefficient = Range.coerceValue(coefficient, -1.0, 1.0);

        if (m_coefficient > 0.0) {
            m_a = 1.0 - m_coefficient;
            m_b = 0.0;
            m_c = m_coefficient;
        } else {
            m_a = 1.0 - 2.0 * m_coefficient;
            m_b = 3.0 * m_coefficient;
            m_c = -m_coefficient;
        }
    }

    /**
     * Gets the {@code coefficient} of the {@link Expo} equation.
     *
     * @return the {@code coefficient} used when calculating the {@link Expo} equation.
     */
    public double getCoefficient() {
        return m_coefficient;
    }

    /**
     * Calculates the {@link Expo} equation with the current {@code coefficient}. The input value is
     * internally coerced to [-1.0, 1.0].
     *
     * @param in input value to be converted
     * @return result of the conversion/modification
     */
    @Override
    public Double update(Double in) {
        double x = Math.min(Math.abs(in), 1.0);
        return Math.copySign(m_a * x + m_b * x * x + m_c * x * x * x, in);
    }
}
