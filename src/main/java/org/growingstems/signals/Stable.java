/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/**
 * A filter that confirms that a logic level input signal has been consistently at a specified logic
 * level for a specified period of time before tranisitioning the output logic level signal.<br>
 * <br>
 * {@link Stable} only supports evaluating high or low signals seperately. Refer to
 * {@link StableBoth} for an implementation where both high and low are evaluated.
 */
public class Stable implements StableI {
    private final Timer m_debounceTimer;
    private Time m_evalPeriod;
    private final boolean m_stabilizeLogicLevel;
    private final EdgeDetector m_timerResetDetector;
    private boolean m_startingLogicLevel;
    private boolean m_ignoreTimer = false;

    /**
     * Creates a {@link Stable} with a starting logic level that will always be the opposite of the
     * {@code stabilizeLogicLevel}, meaning in order for the {@link Stable} to change to the
     * {@code stabilizeLogicLevel}, {@code evalPeriod} will need to elapse since the first update
     * after contruction or after calling {@link #reset()}.<br>
     * <br>
     * Refer to {@link #setStartingLogicLevel(boolean)} to learn more about
     * {@code startingLogicLevel}.
     *
     * @param stabilizeLogicLevel the logic level that must be stable for {@code evalPeriod} amount of
     *     time before the output signal will change.
     * @param evalPeriod the time that the signal must be at {@code stabilizeLogicLevel} in order for
     *     the output signal to update to match the input.
     * @param timeSource the time source to use to keep track of time.
     */
    public Stable(boolean stabilizeLogicLevel, Time evalPeriod, TimeSource timeSource) {
        m_stabilizeLogicLevel = stabilizeLogicLevel;
        setEvalPeriod(evalPeriod);
        m_debounceTimer = timeSource.createTimer();
        m_debounceTimer.start();

        setStartingLogicLevel(!stabilizeLogicLevel);

        m_timerResetDetector =
                new EdgeDetector(stabilizeLogicLevel ? Edge.RISING : Edge.FALLING, m_startingLogicLevel);
    }

    /**
     * Creates a {@link Stable} with a specified {@code startingLogicLevel}.<br>
     * <br>
     * Refer to {@link #setStartingLogicLevel(boolean)} to learn more about
     * {@code startingLogicLevel}.
     *
     * @param stabilizeLogicLevel the logic level that must be stable for {@code evalPeriod} amount of
     *     time before the output signal will change.
     * @param evalPeriod the time that the signal must be at {@code stabilizeLogicLevel} in order for
     *     the output signal to update to match the input.
     * @param startingLogicLevel the starting logic level of {@link Stable} before and up to the first
     *     call to {@link #update(Boolean)}, after contruction or after {@link #reset()} is called.
     * @param timeSource the time source to use to keep track of time.
     */
    public Stable(
            boolean stabilizeLogicLevel,
            Time evalPeriod,
            boolean startingLogicLevel,
            TimeSource timeSource) {
        this(stabilizeLogicLevel, evalPeriod, timeSource);

        setStartingLogicLevel(startingLogicLevel);
        reset();
    }

    /**
     * Sets the Evaluation Period. If a negative evaluation period is provided, an error will be
     * printed, and the value will be set to zero.
     *
     * @param evalPeriod The new evaluation period to be used
     */
    public void setEvalPeriod(Time evalPeriod) {
        if (evalPeriod.lt(Time.ZERO)) {
            System.err.println("Provided Eval Period must be non-negative. Setting to zero.");
            setEvalPeriod(Time.ZERO);
        } else {
            m_evalPeriod = evalPeriod;
        }
    }

    /**
     * Returns the Evaluation Period as currently set
     *
     * @return the evaluation period in seconds
     */
    public Time getEvalPeriod() {
        return m_evalPeriod;
    }

    @Override
    public void setStartingLogicLevel(boolean startingLogicLevel) {
        m_startingLogicLevel = startingLogicLevel;
    }

    @Override
    public void reset() {
        m_debounceTimer.reset();
        m_timerResetDetector.reset();
        if (m_stabilizeLogicLevel == m_startingLogicLevel) {
            m_ignoreTimer = true;
        }
    }

    /**
     * Given an input logic level signal, confirms that a boolean input has been consistently at
     * {@code stabilizeLogicLevel} for {@code evalPeriod} amount of time before transitioning to
     * {@code stabilizeLogicLevel}.
     *
     * @param signal the input logic level signal.
     * @return the logic level result after stabalizing filtering is applied.
     */
    @Override
    public Boolean update(Boolean signal) {
        boolean resetDetected = m_timerResetDetector.update(signal);

        if (resetDetected) {
            m_debounceTimer.reset();
        }

        if ((m_debounceTimer.hasElapsed(m_evalPeriod) || m_ignoreTimer)
                && signal == m_stabilizeLogicLevel) {
            return m_stabilizeLogicLevel;
        } else {
            m_ignoreTimer = false;
            return !m_stabilizeLogicLevel;
        }
    }
}
