/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.math.Range;
import org.growingstems.signals.api.SignalModifier;

/**
 * WrapToWind takes in a sawtoothing signal and converts the signal to a continuous signal. The
 * revolutions is however many times the signal has wrapped around the higher bound or lower bound
 */
public class WrapToWind implements SignalModifier<Double, Double> {
    private final Range m_revRange;
    private int m_revCurrent;
    private double m_lastValue;

    // constructors
    /**
     * Sets the range of the Signal and sets the current revolution count to 0. Defaults starting
     * value to the midpoint of the range.
     *
     * @param revRange The range of the signal
     */
    public WrapToWind(Range revRange) {
        m_revRange = revRange;
        reset();
    }

    /**
     * Sets the range of the Signal and sets the current revolution count to 0. Sets the starting
     * value to the input.
     *
     * @param revRange The range of the signal
     * @param sensorIn the value of the signal
     */
    public WrapToWind(Range revRange, double sensorIn) {
        m_revRange = revRange;
        reset(sensorIn);
    }

    // public functions
    /**
     * Gets the range of the signal
     *
     * @return The range of a single signal loop
     */
    public Range getRange() {
        return m_revRange;
    }

    /**
     * Resets to a newly constructed state. Starts at the value given.
     *
     * @param valIn The value to reset to
     * @throws IllegalArgumentException Sensor value is not in range
     */
    public void reset(double valIn) throws IllegalArgumentException {
        reset();
        update(valIn);
    }

    /**
     * Resets to a newly constructed state. Starts at the midpoint between the higher and lower
     * bounds.
     */
    public void reset() {
        m_revCurrent = 0;
        m_lastValue = ((m_revRange.getHigh() - m_revRange.getLow()) / 2.0) + m_revRange.getLow();
    }

    /**
     * Updates the signal being tracked. The revolution count will go up if the signal wraps past the
     * higher bound and the count will go down if the signal wraps past the lower bound.
     *
     * @param sensorIn The current value of the sensor being tracked
     * @return The continuous sum of the revolutions plus the sensor value
     * @throws IllegalArgumentException Sensor value is not in range
     */
    @Override
    public Double update(Double sensorIn) throws IllegalArgumentException {
        if (!m_revRange.inRange(sensorIn)) {
            throw new IllegalArgumentException("Sensor Value Not In Range. Low and High are "
                    + m_revRange.toString() + " and Sensor value is " + sensorIn.toString());
        }
        if (sensorIn < m_lastValue - ((m_revRange.getHigh() - m_revRange.getLow()) / 2.0)) {
            m_revCurrent++;
        } else if (sensorIn >= m_lastValue + ((m_revRange.getHigh() - m_revRange.getLow()) / 2.0)) {
            m_revCurrent--;
        }
        m_lastValue = sensorIn;
        return m_revCurrent * (m_revRange.getHigh() - m_revRange.getLow()) + sensorIn;
    }
}
