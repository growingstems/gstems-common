/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import java.util.Optional;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/** RateLimiter limits the rate of change of a signal */
public class RateLimiter implements SignalModifier<Double, Double> {
    private Timer m_timer;
    private Frequency m_rate;
    private Double m_currentValue;

    /**
     * Enum defining mode to limit rate of change of a signal
     *
     * <ul>
     *   <li>{@link #INCREASING_MAGNITUDE}
     *   <li>{@link #DECREASING_MAGNITUDE}
     *   <li>{@link #ALL_MAGNITUDE}
     * </ul>
     */
    public enum LimitingMode {
        /** Only limits how fast a value can increase */
        INCREASING_MAGNITUDE,
        /** Only limits how fast a value can decrease */
        DECREASING_MAGNITUDE,
        /** Limits how fast a value can both increase/decrease */
        ALL_MAGNITUDE;
    }

    private LimitingMode m_lMode;

    /**
     * Constructs a new rate limiter.
     *
     * @param timeSource TimeSource object used for internal time calculations
     * @param rate maximum rate of change of signal
     * @param mode whether to limit rate increases, decreases, or both
     * @param initial the initial starting value
     */
    public RateLimiter(TimeSource timeSource, Frequency rate, LimitingMode mode, Double initial) {
        m_currentValue = initial;
        m_rate = rate;
        m_timer = timeSource.createTimer();
        m_lMode = mode;
        m_timer.start();
    }

    /**
     * Constructs a new rate limiter.
     *
     * @param timeSource TimeSource object used for internal time calculations
     * @param rate maximum rate of change of signal
     * @param mode whether to limit rate increases, decreases, or both
     */
    public RateLimiter(TimeSource timeSource, Frequency rate, LimitingMode mode) {
        this(timeSource, rate, mode, null);
    }

    /**
     * Constructs a new rate limiter with both increasing an decreasing rates being limited.
     *
     * @param timeSource TimeSource object used for internal time calculations
     * @param rate maximum rate of change of signal
     */
    public RateLimiter(TimeSource timeSource, Frequency rate) {
        this(timeSource, rate, LimitingMode.ALL_MAGNITUDE);
    }

    /**
     * Limits the rate of change of a signal to not exceed a certain gain
     *
     * <p>Returns the current input of the signal when first run
     *
     * @param inputSignal Input of the signal
     * @return the limited power
     */
    @Override
    public Double update(Double inputSignal) {
        Time delta = m_timer.reset();

        if (m_currentValue == null) {
            return m_currentValue = inputSignal;
        }

        double maxChange = m_rate.mul(delta).asNone();
        switch (m_lMode) {
            case INCREASING_MAGNITUDE:
                m_currentValue += maxChange;
                m_currentValue = Math.min(m_currentValue, inputSignal);
                break;
            case DECREASING_MAGNITUDE:
                m_currentValue -= maxChange;
                m_currentValue = Math.max(m_currentValue, inputSignal);
                break;
            case ALL_MAGNITUDE:
                if (m_currentValue < inputSignal) {
                    m_currentValue += maxChange;
                    m_currentValue = Math.min(m_currentValue, inputSignal);
                } else {
                    m_currentValue -= maxChange;
                    m_currentValue = Math.max(m_currentValue, inputSignal);
                }
                break;
        }

        return m_currentValue;
    }

    /**
     * Checks what the internal value is that is stored without adjusting it.
     *
     * @return the current internal value
     */
    public Optional<Double> peek() {
        return Optional.ofNullable(m_currentValue);
    }

    /**
     * Gets the current rate of change.
     *
     * @return rate of change as a frequency
     */
    public Frequency getRate() {
        return m_rate;
    }

    /**
     * Sets the current rate of change.
     *
     * @param rate rate of change as a frequency
     * @return this {@link RateLimiter} instance
     */
    public RateLimiter setRate(Frequency rate) {
        m_rate = rate;
        return this;
    }

    /**
     * Resets the {@link RateLimiter} so that the next inputted value to
     * {@link RateLimiter#update(Double)} is accepted as the current value without any rate limiting
     * applied.
     *
     * @return this {@link RateLimiter} instance
     */
    public RateLimiter reset() {
        return forceUpdate(null);
    }

    /**
     * Forces the internal value to be the given input.
     *
     * @param input the value used to force the internal value
     * @return this {@link RateLimiter} instance
     */
    public RateLimiter forceUpdate(Double input) {
        m_currentValue = input;
        return this;
    }
}
