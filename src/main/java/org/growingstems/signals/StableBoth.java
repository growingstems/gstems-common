package org.growingstems.signals;

import org.growingstems.logic.SRLatch;
import org.growingstems.logic.SRLatch.SRLatchType;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;

/**
 * Similar to {@link Stable}, but evaluates both high and low signals for stability before
 * transitioning the output.<br>
 * <br>
 * If evaulating only high or low, but not both is desired, refer to {@link Stable}.
 */
public class StableBoth implements StableI {
    private final Stable m_lowStable;
    private final Stable m_highStable;
    private final SRLatch m_latch;
    private boolean m_startingLogicLevel;

    /**
     * A stability filter that requires that the input signal maintains a single logic level for a
     * certain amount of time time before transitioning the output. Both the high and low logic levels
     * have their own evalution periods: {@code highEvalPeriod} and {@code lowEvalPeriod}.<br>
     * <br>
     * If the output is currently {@code false}, the input must maintain {@code true} for
     * {@code highEvalPeriod} amount of time before the output transitions to {@code true}. Conversly,
     * if the output is currently {@code true}, the input must maintain {@code false} for
     * {@code lowEvalPeriod} amount of time before the output transitions to {@code false}.<br>
     * <br>
     * {@code startingLogicLevel} determines the behavior of {@link StableBoth} on construction and
     * after being reset. Refer to {@link StableI#setStartingLogicLevel(boolean)} for more information
     * about {@code startingLogicLevel}.
     *
     * @param lowEvalPeriod the amount of time the input signal must maintain an input of
     *     {@code false} before transitioning the output from {@code true} to {@code false}.
     * @param highEvalPeriod the amount of time the input signal must maintain an input of
     *     {@code true} before transitioning the output from {@code false} to {@code true}.
     * @param startingLogicLevel the starting logic level of {@link StableBoth} before and up to the
     *     first call to {@link #update(Boolean)}, after contruction or after {@link #reset()} is
     *     called.
     * @param timeSource the time source to use to keep track of time.
     */
    public StableBoth(
            Time lowEvalPeriod, Time highEvalPeriod, boolean startingLogicLevel, TimeSource timeSource) {
        m_lowStable = new Stable(false, lowEvalPeriod, startingLogicLevel, timeSource);
        m_highStable = new Stable(true, highEvalPeriod, startingLogicLevel, timeSource);
        m_latch = new SRLatch(startingLogicLevel, SRLatchType.NO_CHANGE);
        m_startingLogicLevel = startingLogicLevel;
    }

    /**
     * A stability filter that requires that the input signal maintains a single logic level for a
     * certain amount of time time before transitioning the output. Both the high and low logic levels
     * have their own evalution periods: {@code highEvalPeriod} and {@code lowEvalPeriod}.<br>
     * <br>
     * The {@code StableBoth} is constructed with a starting logic level of {@code false}.<br>
     * <br>
     * If the output is currently {@code false}, the input must maintain {@code true} for
     * {@code highEvalPeriod} amount of time before the output transitions to {@code true}. Conversly,
     * if the output is currently {@code true}, the input must maintain {@code false} for
     * {@code lowEvalPeriod} amount of time before the output transitions to {@code false}.<br>
     * <br>
     * {@code startingLogicLevel} determines the behavior of {@link StableBoth} on construction and
     * after being reset. Refer to {@link StableI#setStartingLogicLevel(boolean)} for more information
     * about {@code startingLogicLevel}.
     *
     * @param lowEvalPeriod the amount of time the input signal must maintain an input of
     *     {@code false} before transitioning the output from {@code true} to {@code false}.
     * @param highEvalPeriod the amount of time the input signal must maintain an input of
     *     {@code true} before transitioning the output from {@code false} to {@code true}.
     * @param timeSource the time source to use to keep track of time.
     */
    public StableBoth(Time lowEvalPeriod, Time highEvalPeriod, TimeSource timeSource) {
        this(lowEvalPeriod, highEvalPeriod, false, timeSource);
    }

    @Override
    public void setStartingLogicLevel(boolean startingLogicLevel) {
        m_startingLogicLevel = startingLogicLevel;
        m_lowStable.setStartingLogicLevel(startingLogicLevel);
        m_highStable.setStartingLogicLevel(startingLogicLevel);
    }

    @Override
    public void reset() {
        m_highStable.reset();
        m_lowStable.reset();
        m_latch.update(m_startingLogicLevel, !m_startingLogicLevel);
    }

    @Override
    public Boolean update(Boolean signal) {
        return m_latch.update(m_highStable.update(signal), !m_lowStable.update(signal));
    }
}
