/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.api.SignalModifier;

/**
 * EdgeDetector is designed to be used in logic algorithms to detect boolean edges. It can be
 * configured to detect RISING and FALLING edges independently as well as both edges at the same
 * time.
 */
public class EdgeDetector implements SignalModifier<Boolean, Boolean> {
    private final Edge m_trackedEdge;
    private final EdgeFinder m_edgeFinder;

    /**
     * Sets the edge to be tracked on construction and has no starting logic level. Calling
     * {@link #update(Boolean)} is guaranteed to return {@code false} on the first call.
     *
     * @param edge The type of edge to track. Can be {@link Edge#RISING}, {@link Edge#FALLING} or
     *     {@link Edge#ANY}. If {@code edge} is neither {@link Edge#RISING}, {@link Edge#FALLING} nor
     *     {@link Edge#ANY}, an error will be reported and the set edge will fall back to RISING
     */
    public EdgeDetector(Edge edge) {
        if (verifyTrackedEdge(edge)) {
            m_trackedEdge = edge;
        } else {
            m_trackedEdge = Edge.RISING;
        }

        m_edgeFinder = new EdgeFinder();
    }

    /**
     * Sets the edge to be tracked on construction with an initial logic level defined by
     * {@code startingLevel}. If a {@code null} is provided for the starting logic level, the first
     * call to {@link #update(Boolean)} is guaranteed to return {@link Edge#NONE} after construction
     * and after calling {@link #reset()}.
     *
     * @param edge The type of edge to track. Can be {@link Edge#RISING}, {@link Edge#FALLING} or
     *     {@link Edge#ANY}. If {@code edge} is neither {@link Edge#RISING}, {@link Edge#FALLING} nor
     *     {@link Edge#ANY}, an error will be reported and the set edge will fall back to RISING
     * @param startingLevel The logic level that the {@link EdgeDetector} starts with on construction
     *     or when {@link #reset()} is called
     */
    public EdgeDetector(Edge edge, Boolean startingLevel) {
        if (verifyTrackedEdge(edge)) {
            m_trackedEdge = edge;
        } else {
            m_trackedEdge = Edge.RISING;
        }

        m_edgeFinder = new EdgeFinder(startingLevel);
    }

    /**
     * Resets the {@link EdgeDetector}. If a starting logic level is not provided via
     * {@link #EdgeDetector(Edge, Boolean)}, the next call to {@link #update(Boolean)} is guaranteed
     * to return {@code false} on the next call.
     */
    public void reset() {
        m_edgeFinder.reset();
    }

    /**
     * Gets the currently tracked edge.
     *
     * @return The currently tracked edge
     */
    public Edge getTrackedEdge() {
        return m_trackedEdge;
    }

    private boolean verifyTrackedEdge(Edge edge) {
        if (!(edge.isEdge() || edge == Edge.ANY)) {
            System.err.println("Unsupported input: EdgeDetector.setEdge() does not support " + edge);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Used to determine if an edge has been detected in the input boolean signal. The {@link Edge}
     * that is detected is determined by the tracked edge configuration value. If a starting logic
     * level is not provided via {@link #EdgeDetector(Edge, Boolean)}, this function is guaranteed to
     * return {@code false} on the first call.
     *
     * @param signal The boolean signal being checked for edges
     * @return {@code true} if the configured edge was detected, {@code false} otherwise
     */
    @Override
    public Boolean update(Boolean signal) {
        Edge value = m_edgeFinder.update(signal);

        if (m_trackedEdge == Edge.ANY) {
            return value.isEdge();
        } else {
            return value == m_trackedEdge;
        }
    }
}
