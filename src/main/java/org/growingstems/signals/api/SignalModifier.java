/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals.api;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Interface to represent an object that can modify/convert a signal from one form to another. <br>
 *
 * @param <InputType> The data type used to represent data on the input end of the modifier
 * @param <OutputType> The data type used to represent data on the output end of the modifier. <br>
 *     note: InputType and OutputType can be the same data type, but don't have to be
 */
public interface SignalModifier<InputType, OutputType> {
    /**
     * Given the provided input signal value, perform the appropriate modification on that input and
     * return the corresponding output value.
     *
     * @param in Input value to be converted
     * @return Result of the conversion/modification
     */
    OutputType update(InputType in);

    /**
     * Given a SignalModifier of &lt;InputType, OutputType&gt;, appends a new SignalModifier that
     * converts OutputType to a new ResultType
     *
     * @param <ResultType> New return type of the resulting SignalModifier
     * @param second SignalModifier to be chained
     * @return New SignalModifier with the first InputType and a new ResultType
     */
    default <ResultType> SignalModifier<InputType, ResultType> append(
            SignalModifier<OutputType, ResultType> second) {
        return (in) -> second.update(update(in));
    }

    /**
     * Given a Supplier of InputType, returns a Supplier of OutputType that is piped through a
     * SignalModifier update
     *
     * @param in InputType Supplier
     * @return OutputType Supplier
     */
    default Supplier<OutputType> provide(Supplier<InputType> in) {
        return () -> update(in.get());
    }

    /**
     * Given a Consumer of OutputType, returns a Consumer of InputType that can be used to pipe a
     * Consumption of InputType through SignalModifier
     *
     * @param out OutputType Consumer to be linked
     * @return Consumer of InputType
     */
    default Consumer<InputType> handle(Consumer<OutputType> out) {
        return (in) -> out.accept(update(in));
    }

    /**
     * Given a Consumer of OutputType and a Supplier of InputType, creates a closed runnable that uses
     * SignalModifier conversion between the input and output types
     *
     * @param in InputType Supplier
     * @param out OutputType Consumer
     * @return Closed runnable
     */
    default Runnable makeRunnable(Supplier<InputType> in, Consumer<OutputType> out) {
        return () -> out.accept(update(in.get()));
    }
}
