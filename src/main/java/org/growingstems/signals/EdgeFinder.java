/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.api.SignalModifier;

/**
 * {@link EdgeFinder} is designed to be used to find logic level transitions ({@code edges}). It
 * detects {@link Edge#RISING} and {@link Edge#FALLING} edges or {@link Edge#NONE} if no transition
 * was detected.
 */
public class EdgeFinder implements SignalModifier<Boolean, Edge> {
    /** The last signal input */
    private Boolean m_lastSignal = null;
    /** The starting logic level to start at on construction or after a reset */
    private final Boolean m_startingLevel;

    /**
     * Constructs an {@link EdgeFinder} that has no starting logic level. Calling
     * {@link #update(Boolean)} is guaranteed to return {@link Edge#NONE} on the first call.
     */
    public EdgeFinder() {
        this(null);
    }

    /**
     * Constructs an {@link EdgeFinder} with an initial logic level defined by {@code startingLevel}.
     * If a {@code null} is provided for the starting logic level, the first call to
     * {@link #update(Boolean)} is guaranteed to return {@link Edge#NONE} after construction and after
     * calling {@link #reset()}.
     *
     * @param startingLevel the logic level that the {@link EdgeFinder} starts with on construction or
     *     when {@link #reset()} is called
     */
    public EdgeFinder(Boolean startingLevel) {
        m_startingLevel = startingLevel;
        reset();
    }

    /**
     * Resets the {@link EdgeFinder}. If a starting logic level is not provided via
     * {@link #EdgeFinder(Boolean)}, the next call to {@link #update(Boolean)} is guaranteed to return
     * {@link Edge#NONE} on the first call.
     */
    public void reset() {
        if (m_startingLevel != null) {
            m_lastSignal = m_startingLevel;
        } else {
            m_lastSignal = null;
        }
    }

    /**
     * Used to determine if an edge has been detected in the input boolean signal. If a starting logic
     * level is not provided via {@link #EdgeFinder(Boolean)}, this function is guaranteed to return
     * {@link Edge#NONE} on the first call.
     *
     * @param signal the boolean signal being checked for edges
     * @return an edge if one was found, {@link Edge#NONE} if not
     */
    public Edge update(Boolean signal) {
        Boolean lastSignal = m_lastSignal;
        m_lastSignal = signal;

        // If there was no previous signal (because startingLevel wasn't defined)
        // immediately return none.
        if (lastSignal == null) {
            return Edge.NONE;
        }

        if (!lastSignal && signal) {
            return Edge.RISING;
        } else if (lastSignal && !signal) {
            return Edge.FALLING;
        } else {
            return Edge.NONE;
        }
    }
}
