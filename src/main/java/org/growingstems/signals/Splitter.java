package org.growingstems.signals;

import java.util.List;
import java.util.stream.Stream;
import org.growingstems.signals.api.SignalModifier;

/**
 * A {@link SignalModifier} which provides a single input to multiple {@link SignalModifier}s. <br>
 * <br>
 * The results of the various {@link SignalModifier}s are stored in a {@link Stream}. This means
 * that, upon calling {@link Splitter#update(Object)}, the contained {@link SignalModifier}s will
 * not be executed until a terminal operation is called on the stream.
 *
 * @see Stream
 */
public class Splitter<In, Out> implements SignalModifier<In, Stream<Out>> {
    private final List<SignalModifier<In, Out>> m_modifiers;

    /**
     * Constructs a {@link Splitter} given a {@link List} of {@link SignalModifier}s.
     *
     * @param modifiers The {@link SignalModifier}s to be provided with the same input
     */
    public Splitter(List<SignalModifier<In, Out>> modifiers) {
        m_modifiers = modifiers;
    }

    /**
     * Creates a {@link Stream} which provides the input to each {@link SignalModifier}
     *
     * @param in The input to provide to each {@link SignalModifier}
     * @return The {@link Stream} of outputs from each {@link SignalModifier}
     */
    @Override
    public Stream<Out> update(In in) {
        return m_modifiers.stream().map(s -> s.update(in));
    }
}
