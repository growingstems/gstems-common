package org.growingstems.signals;

import java.util.function.BinaryOperator;
import java.util.stream.Stream;
import org.growingstems.signals.api.SignalModifier;

/**
 * A {@link SignalModifier} which reduces a {@link Stream} to a single value.
 *
 * @see Stream#reduce(Object, BinaryOperator) for more information
 */
public class Reducer<S> implements SignalModifier<Stream<S>, S> {
    protected final BinaryOperator<S> m_reduceFunction;
    protected final S m_initial;

    /**
     * Construct a {@link Reducer} given an initial value and a reducing function, which will both be
     * given to {@link Stream#reduce(Object, BinaryOperator)}
     *
     * @param identity The identity value for the accumulating function
     * @param reduceFunction The function to apply to each value of the stream
     */
    public Reducer(S identity, BinaryOperator<S> reduceFunction) {
        m_initial = identity;
        m_reduceFunction = reduceFunction;
    }

    /**
     * Reduces the {@link Stream} to a single value
     *
     * @param in {@link Stream} to reduce
     * @return Value after reduction
     */
    @Override
    public S update(Stream<S> in) {
        return in.reduce(m_initial, m_reduceFunction);
    }
}
