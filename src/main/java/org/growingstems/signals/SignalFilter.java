/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import java.util.function.Predicate;
import org.growingstems.signals.api.SignalModifier;

/**
 * Filters out values in a signal. <br>
 * <br>
 * Like {@link java.util.stream.Stream#filter(Predicate)}, when the filter returns true, the value
 * is passed through normally. When the filter returns false, the value is skipped. When a value is
 * skipped, the previous value is returned instead. If no initial value is provided during
 * construction, null will be returned until an input passes the filter.
 *
 * @param <S> The signal type to apply a filter to
 */
public class SignalFilter<S> implements SignalModifier<S, S> {
    private final Predicate<S> m_filter;
    private S m_previousValue;

    /**
     * Creates a filter for which all inputs pass.
     *
     * @param <S> The signal type of the filter
     * @return A filter which passes all inputs
     */
    public static <S> SignalFilter<S> noFilter() {
        return new SignalFilter<>(unused -> true);
    }

    /**
     * Creates a {@link SignalFilter} of {@link Double} to filter out NaN values, infinite values, or
     * both.
     *
     * @param filterNans If true, NaN inputs will fail the filter.
     * @param filterInfinites If true, infinite inputs will fail the filter.
     * @return A filter meeting the provided criteria
     */
    public static SignalFilter<Double> filterDoubles(boolean filterNans, boolean filterInfinites) {
        return filterDoubles(filterNans, filterInfinites, null);
    }

    /**
     * Creates a {@link SignalFilter} of {@link Double} to filter out NaN values, infinite values, or
     * both.
     *
     * @param filterNans If true, NaN inputs will fail the filter.
     * @param filterInfinites If true, infinite inputs will fail the filter.
     * @param initialValue Initial value if the first input fails the filter.
     * @return A filter meeting the provided criteria
     */
    public static SignalFilter<Double> filterDoubles(
            boolean filterNans, boolean filterInfinites, Double initialValue) {
        if (filterNans) {
            if (filterInfinites) {
                return new SignalFilter<>(Double::isFinite); // isFinite returns false for NaN
            } else {
                return new SignalFilter<>(d -> !d.isNaN());
            }
        } else {
            if (filterInfinites) {
                return new SignalFilter<>(d -> !d.isInfinite()); // isInfinite returns false for NaN
            } else {
                return noFilter();
            }
        }
    }

    /**
     * Construct a {@link SignalFilter} with no initial value
     *
     * @param filter Filter function
     */
    public SignalFilter(Predicate<S> filter) {
        this(filter, null);
    }

    /**
     * Construct a {@link SignalFilter} with an initial value
     *
     * @param filter Filter function
     * @param initialValue Value to provide at the start if the filter returns false
     */
    public SignalFilter(Predicate<S> filter, S initialValue) {
        m_filter = filter;
        m_previousValue = initialValue;
    }

    /**
     * Returns the input value, unchanged, if it passes the filter. <br>
     * <br>
     * If the input does not pass the filter, returns the most recent value that did. If no value ever
     * passed, returns the initial value provided at construction (default = null)
     *
     * @param in Input signal
     * @return The last value to pass the filter
     */
    @Override
    public S update(S in) {
        if (m_filter.test(in)) {
            m_previousValue = in;
        }
        return m_previousValue;
    }
}
