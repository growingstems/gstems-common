package org.growingstems.signals;

import org.growingstems.signals.api.SignalModifier;

/**
 * A filter that confirms that a logic level input signal has been consistently at a specified logic
 * level for a specified period of time before tranisitioning the output logic level signal.
 */
public interface StableI extends SignalModifier<Boolean, Boolean> {
    /**
     * Sets the {@code startingLogicLevel}. The {@code startingLogicLevel} determines the state from
     * before and up to the first call to {@link #update(Boolean)}, after contruction or after calling
     * {@link #reset()}. The effect of {@code startingLogicLevel} is similar to the concept that
     * {@link #update(Boolean)} was being continuously updated with the input value of
     * {@code startingLogicLevel} up to the first actual call to {@link #update(Boolean)}.
     *
     * @param startingLogicLevel the starting logic level of {@link Stable} before and up to the first
     *     call to {@link #update(Boolean)}, after contruction or after {@link #reset()} is called.
     */
    void setStartingLogicLevel(boolean startingLogicLevel);

    /** Resets back to the state it started when constructed. */
    void reset();

    /**
     * Resets back to the state it started when constructed and sets the startingLogicLevel. Future
     * calls to {@link #reset()} will reset with the given {@code startingLogicLevel}.
     *
     * @param startingLogicLevel the starting logic level of {@link Stable} before and up to the first
     *     call to {@link #update(Boolean)}, after contruction or after {@link #reset()} is called.
     */
    default void reset(boolean startingLogicLevel) {
        setStartingLogicLevel(startingLogicLevel);
        reset();
    }

    /**
     * Given an input logic level signal, the output logic level is determined based on the configured
     * stabalizing filter.
     *
     * @param signal the input logic level signal.
     * @return the logic level result after stabalizing filtering is applied.
     */
    @Override
    Boolean update(Boolean signal);
}
