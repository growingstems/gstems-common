/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.api.SignalModifier;

/**
 * PeakDetector is designed to detect when a signal peaks. PeakDetector can find MAXIMUM or MINIMUM
 * peaks in a signal as well as BOTH types of peaks at the same time.
 *
 * <p>There is a 1 frame delay between when the peak occurs and when the peak is reported.
 */
public class PeakDetector implements SignalModifier<Double, Boolean> {

    private Double m_lastSignal = null;
    private Boolean m_isRising = null;
    private final EdgeDetector m_edgeDetector;

    public static enum PeakType {
        /** Rate of change goes from positive to negative */
        MAXIMUM,
        /** Rate of change goes from negative to positive */
        MINIMUM,
        /** both Minimum and Maximum peaks */
        BOTH
    };

    // constructors
    /**
     * Constructor
     *
     * @param type the type of peak to track
     */
    public PeakDetector(PeakType type) {
        switch (type) {
            case MAXIMUM:
                m_edgeDetector = new EdgeDetector(Edge.FALLING);
                break;
            case MINIMUM:
                m_edgeDetector = new EdgeDetector(Edge.RISING);
                break;
            case BOTH:
            default:
                m_edgeDetector = new EdgeDetector(Edge.ANY);
                break;
        }
    }

    // public functions
    /**
     * Used to determine if a peak has been detected in the signal. The peak is detected when the
     * signal's rate of change flips sign. The function is guaranteed to return false on the first two
     * calls.
     *
     * @param in the signal that is being checked for peaks
     * @return true if a peak has been detected
     */
    @Override
    public Boolean update(Double in) {
        // Not enough information, return false
        if (m_lastSignal == null) {
            m_lastSignal = in;
            return false;
        }

        // Determines if signal is currently rising (If the Signal is same as last iteration, m_isRising
        // stays the same)
        if (m_isRising == null) {
            m_isRising = (in == m_lastSignal ? null : in > m_lastSignal);
        } else if (m_isRising) {
            m_isRising = in >= m_lastSignal;
        } else {
            m_isRising = in > m_lastSignal;
        }

        double newLastSignal = m_lastSignal;
        m_lastSignal = in;

        // Signal into the EdgeDetector is dependent on if the signal was Rising
        if (m_isRising) {
            return m_edgeDetector.update(in >= newLastSignal);
        } else {
            return m_edgeDetector.update(in > newLastSignal);
        }
    }
}
