/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import java.util.function.Predicate;
import java.util.stream.Stream;
import org.growingstems.signals.api.SignalModifier;

/**
 * A {@link SignalModifier} which filters elements from a {@link Stream}
 *
 * @see Stream#filter(Predicate)
 */
public class StreamFilter<S> implements SignalModifier<Stream<S>, Stream<S>> {
    protected final Predicate<S> m_filterFunction;

    /**
     * Construct a {@link StreamFilter} given the {@link Predicate} used on the {@link Stream}
     *
     * @param filterFunction {@link Predicate} used on the {@link Stream}
     */
    public StreamFilter(Predicate<S> filterFunction) {
        m_filterFunction = filterFunction;
    }

    /**
     * Construct a {@link StreamFilter} given a {@link SignalModifier} as opposed to a
     * {@link Predicate}
     *
     * @param filterFunction {@link SignalModifier} to filter the {@link Stream}
     * @param <S> Type of {@link SignalModifier} being filtered
     * @return A filtering {@link SignalModifier}
     */
    public static <S> StreamFilter<S> fromSignalModifier(SignalModifier<S, Boolean> filterFunction) {
        return new StreamFilter<>(filterFunction::update);
    }

    /**
     * Filters the {@link Stream}
     *
     * @param in {@link Stream} to apply the {@link StreamFilter} on
     * @return Filtered {@link Stream}
     */
    @Override
    public Stream<S> update(Stream<S> in) {
        return in.filter(m_filterFunction);
    }
}
