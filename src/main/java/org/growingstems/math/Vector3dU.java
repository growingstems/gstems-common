/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;

/**
 * Represents a unit-based vector in 3d space. This object can be used to represent a point or a
 * vector.
 *
 * @param <U> unit-type for the vector
 */
public class Vector3dU<U extends Unit<U>> {
    private final U m_x;
    private final U m_y;
    private final U m_z;

    /**
     * Creates a {@link Vector3dU} object using Cartesian coordinates.
     *
     * @param x the X component
     * @param y the Y component
     * @param z the z component
     */
    public Vector3dU(U x, U y, U z) {
        m_x = x;
        m_y = y;
        m_z = z;
    }

    /**
     * Constructs a Vector3dU from a Cylindrical Coordinate System.
     *
     * <p>The cylinder is flat on the XY plane, with its height in the Z direction. Theta is the angle
     * on the XY plane, where 0 is aligned with the +X axis.
     *
     * @param <U> the type of vector
     * @param radius the magnitude of the XY component of the vector
     * @param theta the angle of the XY component of the vector
     * @param height the magnitude of the Z component of the vector
     * @return a Vector3dU given the equivalent vector in cylindrical coordinates
     */
    public static <U extends Unit<U>> Vector3dU<U> fromCylindrical(U radius, Angle theta, U height) {
        Vector2dU<U> xy = Vector2dU.fromPolar(radius, theta);
        return new Vector3dU<U>(xy.getX(), xy.getY(), height);
    }

    /**
     * Constructs a Vector3dU from a Spherical Coordinate System.
     *
     * <p>Theta is the angle on the XY plane, where 0 is aligned with the +X axis. Phi is the angle
     * from the +Z axis.
     *
     * @param <U> the type of vector
     * @param radius the magnitude of the XY component of the vector
     * @param theta the angle of the XY component of the vector
     * @param phi the angle between the vector and the +Z axis
     * @return a Vector3dU given the equivalent vector in spherical coordinates
     */
    public static <U extends Unit<U>> Vector3dU<U> fromSpherical(U radius, Angle theta, Angle phi) {
        double sinPhi = phi.sin();
        double cosPhi = phi.cos();
        double sinTheta = theta.sin();
        double cosTheta = theta.cos();
        return new Vector3dU<U>(
                radius.mul(sinPhi * cosTheta), radius.mul(sinPhi * sinTheta), radius.mul(cosPhi));
    }

    /**
     * Constructs a Vector3dU from a radius, azimuth, and elevation.
     *
     * <p>This is identical to {@link Vector3dU#fromSpherical(Unit, Angle, Angle)}, except instead of
     * using phi, which is 0 along the +Z axis and +180 degrees along the -Z axis, it uses elevation,
     * which is 0 on the XY plane, and +90 degrees along the +Z axis
     *
     * @param <U> the type of vector
     * @param radius the magnitude of the XY component of the vector
     * @param azimuth the angle of the XY component of the vector
     * @param elevation the angle between the vector and the XY plane
     * @return a Vector3dU given the radius, azimuth, and elevation
     */
    public static <U extends Unit<U>> Vector3dU<U> fromAzEl(
            U radius, Angle azimuth, Angle elevation) {
        return fromSpherical(radius, azimuth, Angle.PI_BY_TWO.sub(elevation));
    }

    /**
     * Generate a String representation of the {@link Vector3dU} in Cartesian coordinates.
     *
     * @return a String representation of the {@link Vector3dU}
     */
    @Override
    public String toString() {
        return toString(CoordinateSystem.CARTESIAN);
    }

    /**
     * Generate a String representation of the {@link Vector3dU} in the provided coordinate system.
     *
     * @param system the coordinate system to use
     * @return a String representation of the {@link Vector3dU}
     */
    public String toString(CoordinateSystem system) {
        switch (system) {
            case CARTESIAN:
                return "(" + m_x.toString() + ", " + m_y.toString() + ", " + m_z.toString() + ")";
            case CYLINDRICAL:
                return "(" + getXY().getMagnitude().toString() + ", "
                        + getXY().getAngle().toString() + ", " + m_z.toString() + ")";
            case SPHERICAL:
                return "(" + getMagnitude().toString() + ", " + getXY().getAngle().toString()
                        + toBaseVector3d().angleTo(new Vector3d(0.0, 0.0, 1.0)).toString() + ")";
            case AZ_EL:
                return "(" + getMagnitude().toString() + ", " + getXY().getAngle().toString()
                        + Angle.PI_BY_TWO
                                .sub(toBaseVector3d().angleTo(new Vector3d(0.0, 0.0, 1.0)))
                                .toString() + ")";
            default:
                return "Vector3d::toString Not yet implemented for coordinate system " + system.toString();
        }
    }

    /**
     * Obtain a {@link Vector3d} using the vector's base values.
     *
     * @return the vector in base units
     */
    public Vector3d toBaseVector3d() {
        return forEach(U::getBaseValue);
    }

    /**
     * Perform a function on each element of this vector
     *
     * @param modFunction function to apply to x, y, and z individually
     * @return new vector containing the result
     */
    public Vector3d forEach(SignalModifier<U, Double> modFunction) {
        return new Vector3d(modFunction.update(m_x), modFunction.update(m_y), modFunction.update(m_z));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public Vector3d forEach(Vector3d other, BiFunction<U, Double, Double> modFunction) {
        return new Vector3d(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <V> second vector's {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <V extends Unit<V>> Vector3d forEach(
            Vector3dU<V> other, BiFunction<U, V, Double> modFunction) {
        return new Vector3d(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Perform a function on each element of this vector.
     *
     * @param <V> output {@link Unit} type
     * @param modFunction function to apply to x, y, and z individually
     * @return new vector containing the result
     */
    public <V extends Unit<V>> Vector3dU<V> forEachU(SignalModifier<U, V> modFunction) {
        return new Vector3dU<>(
                modFunction.update(m_x), modFunction.update(m_y), modFunction.update(m_z));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <V> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <V extends Unit<V>> Vector3dU<V> forEachU(
            Vector3d other, BiFunction<U, Double, V> modFunction) {
        return new Vector3dU<>(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <V> second vector's {@link Unit} type
     * @param <R> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <V extends Unit<V>, R extends Unit<R>> Vector3dU<R> forEachU(
            Vector3dU<V> other, BiFunction<U, V, R> modFunction) {
        return new Vector3dU<>(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Obtain a {@link Vector3dU} of {@link Unitless} using the vector's base values.
     *
     * @return a Unitless vector using base units
     */
    public Vector3dU<Unitless> getAsUnitlessVector() {
        return toBaseVector3d().forEachU(Unitless::none);
    }

    /**
     * Returns the X component of the {@link Vector3dU}.
     *
     * @return the x component
     */
    public U getX() {
        return m_x;
    }

    /**
     * Returns the Y component of the {@link Vector3dU}.
     *
     * @return the y component
     */
    public U getY() {
        return m_y;
    }

    /**
     * Returns the Z component of the {@link Vector3dU}.
     *
     * @return the z component
     */
    public U getZ() {
        return m_z;
    }

    /**
     * Returns a Vector2d to represent the X and Y components of the 3d vector.
     *
     * @return vector2d to represent the X and Y components of the 3d vector
     */
    public Vector2dU<U> getXY() {
        return new Vector2dU<>(m_x, m_y);
    }

    /**
     * Returns a Vector2d to represent the X and Z components of the 3d vector.
     *
     * @return vector2d to represent the X and Z components of the 3d vector
     */
    public Vector2dU<U> getXZ() {
        return new Vector2dU<>(m_x, m_z);
    }

    /**
     * Returns a Vector2d to represent the Y and Z components of the 3d vector.
     *
     * @return Vector2d to represent the Y and Z components of the 3d vector.
     */
    public Vector2dU<U> getYZ() {
        return new Vector2dU<>(m_y, m_z);
    }

    /**
     * Adds two vectors in 3d space and returns the sum. This is similar to vector addition.
     *
     * <p>For example, Vector3dU{1.0, 2.5, 3.0} + Vector3dU{2.0, 5.5, 3.0} = Vector3dU{3.0, 8.0, 6.0}
     *
     * @param other the vector to add
     * @return the sum of the vectors
     */
    public Vector3dU<U> add(Vector3dU<U> other) {
        return forEachU(other, U::add);
    }

    /**
     * Subtracts a vector from another vector and returns the difference.
     *
     * <p>For example, Vector3dU{5.0, 4.0, 3.0} - Vector3dU{1.0, 2.0, 3.0} = Vector3dU{4.0, 2.0, 0.0}.
     *
     * @param other the vector to subtract
     * @return the difference between the two vectors
     */
    public Vector3dU<U> sub(Vector3dU<U> other) {
        return forEachU(other, U::sub);
    }

    /**
     * Multiplies the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector3dU{2.0, 2.5, 3.0} * 2 = Vector3dU{4.0, 5.0, 6.0}.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector3dU<U> mul(double s) {
        return forEachU(v -> v.mul(s));
    }

    /**
     * Divides the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector3dU{2.0, 2.5, 3.0} / 2 = Vector3dU{1.0, 1.25, 1.5}.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector3dU<U> div(double s) {
        return forEachU(v -> v.div(s));
    }

    /**
     * Divides the vector by a scalar and returns the new vector.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector3dU<Unitless> div(U s) {
        return forEachU(v -> v.div(s));
    }

    /**
     * Negates the vector and returns the new vector.
     *
     * <p>For example, -Vector3dU{2.0, 2.5, 3.0} = Vector3dU{-2.0, -2.5, -3.0}
     *
     * @return the negated vector
     */
    public Vector3dU<U> neg() {
        return forEachU(U::neg);
    }

    /**
     * Calculates the total magnitude of the {@link Vector3dU}.
     *
     * @return the magnitude of the {@link Vector3dU}
     */
    public U getMagnitude() {
        double x = m_x.getBaseValue();
        // Use YZ if x == 0.0 to avoid division by zero
        if (x == 0.0) {
            return getYZ().getMagnitude();
        }

        double y = m_y.getBaseValue();
        double z = m_z.getBaseValue();
        // Divide m_x by its own base value to get a "1", then scale it by the correct
        // magnitude.
        return m_x.div(m_x.getBaseValue()).mul(Math.sqrt(x * x + y * y + z * z));
    }

    /**
     * Normalizes the {@link Vector3dU} so that it has a length of 1.0 while maintaining its angle.
     *
     * <p>A Vector3d of value (NaN, NaN) will result from a {@link Vector3dU} of value (0.0, 0.0,
     * 0.0).
     *
     * <p>Note: The "Only Scale Down" functionality of {@link Vector3d#normalize(boolean)} is not
     * implemented since it would be unit-dependent, and this class cannot interpret Unit enums.
     *
     * @return a new Vector3d with a magnitude of 1.0
     */
    public Vector3d normalize() {
        return toBaseVector3d().normalize(false);
    }

    /**
     * Normalizes a group of {@link Vector3dU} objects so that the longest Vector3d is exactly 1.0 and
     * the rest are scaled relatively.
     *
     * <p>A set of Vector3ds of value (NaN, NaN, NaN) will result when given a set of
     * {@link Vector3dU}s of value (0.0, 0.0, 0.0).
     *
     * <p>Note: The "Only Scale Down" functionality of {@link Vector3d#normalizeGroup(List, boolean)}
     * is not implemented since it would be unit-dependent, and this class cannot interpret Unit
     * enums.
     *
     * @param <U> the type of {@link Unit} in the {@link Vector3dU}
     * @param values a group of {@link Vector3d} objects
     * @return a new group of {@link Vector3d} objects after being normalized relative to the longest
     *     Vector3d
     */
    public static <U extends Unit<U>> List<Vector3d> normalizeGroup(List<Vector3dU<U>> values) {
        List<Vector3d> v3ds =
                values.stream().map(Vector3dU<U>::toBaseVector3d).collect(Collectors.toList());
        return Vector3d.normalizeGroup(v3ds, false);
    }

    /**
     * Calculates the dot product between two vectors.
     *
     * @param other second vector in the dot product
     * @return resulting dot product
     */
    public U dot(Vector3d other) {
        return m_x.mul(other.getX()).add(m_y.mul(other.getY())).add(m_z.mul(other.getZ()));
    }

    /**
     * Calculates the dot product between two vectors.
     *
     * @param <V> second vector {@link Unit} type
     * @param <R> resulting vector {@link Unit} type
     * @param other second vector in the dot product
     * @param mulFunction function to multiply U and V
     * @return resulting dot product
     */
    public <V extends Unit<V>, R extends Unit<R>> R dot(
            Vector3dU<V> other, BiFunction<U, V, R> mulFunction) {
        return mulFunction
                .apply(m_x, other.getX())
                .add(mulFunction.apply(m_y, other.getY()))
                .add(mulFunction.apply(m_z, other.getZ()));
    }

    /**
     * Calculates the cross product between two vectors.
     *
     * @param other second vector in the cross product
     * @return resulting cross product
     */
    public Vector3dU<U> cross(Vector3d other) {
        return new Vector3dU<>(
                getYZ().cross(other.getYZ()),
                getXZ().cross(other.getXZ()).neg(),
                getXY().cross(other.getXY()));
    }

    /**
     * Calculates the cross product between two vectors.
     *
     * @param <V> second vector {@link Unit} type
     * @param <R> resulting vector {@link Unit} type
     * @param other second vector in the cross product
     * @param mulFunction function to multiply U and V
     * @return resulting cross product
     */
    public <V extends Unit<V>, R extends Unit<R>> Vector3dU<R> cross(
            Vector3dU<V> other, BiFunction<U, V, R> mulFunction) {
        return new Vector3dU<>(
                getYZ().cross(other.getYZ(), mulFunction),
                getXZ().cross(other.getXZ(), mulFunction).neg(),
                getXY().cross(other.getXY(), mulFunction));
    }

    /**
     * Calculates the range between two vectors.
     *
     * @param other the other vector to be used in the range operation
     * @return the range between the two vectors
     */
    public U rangeTo(Vector3dU<U> other) {
        return this.sub(other).getMagnitude();
    }

    /**
     * Calculates the angle between this vector and another.
     *
     * <p>It does not matter what type of Unit each vector uses. Returns a zero-angle if either vector
     * is all zeros.
     *
     * @param other the other vector in the angle operation
     * @return the angle between vectors
     */
    public Angle angleTo(Vector3dU<?> other) {
        return toBaseVector3d().angleTo(other.toBaseVector3d());
    }

    /**
     * Calculates the angle between this vector and another.
     *
     * <p>Returns a zero-angle if either vector is all zeros.
     *
     * @param other the other vector in the angle operation
     * @return the angle between vectors
     */
    public Angle angleTo(Vector3d other) {
        return toBaseVector3d().angleTo(other);
    }
}
