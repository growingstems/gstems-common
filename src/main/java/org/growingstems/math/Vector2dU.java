/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;

/**
 * {@link Vector2dU} is a class that is used to represent a two dimensional vector.
 *
 * <p>Just like a two dimensional mathematical vector, {@link Vector2dU} is made up of an X
 * component and a Y component and is generally represented as an arrow.
 */
public class Vector2dU<U extends Unit<U>> {
    private static final Angle k_tolerance = Angle.radians(1e-5);

    private final U m_x;
    private final U m_y;

    /**
     * Creates a {@link Vector2dU} object using Cartesian coordinates.
     *
     * @param x the X component
     * @param y the Y component
     */
    public Vector2dU(U x, U y) {
        m_x = x;
        m_y = y;
    }

    /**
     * Creates a {@link Vector2dU} object using polar coordinates.
     *
     * <p>Note: As opposed to {@link Vector2d#Vector2d(double, Angle)}, this is a factory method in
     * order to remove the ambiguity caused when the generic parameter is an {@link Angle}.
     *
     * @param <U> the {@link Unit} type to use for the {@link Vector2dU}
     * @param magnitude the magnitude component
     * @param angle the angle component
     * @return a vector calculated from the polar coordinates
     */
    public static <U extends Unit<U>> Vector2dU<U> fromPolar(U magnitude, Angle angle) {
        return new Vector2dU<U>(magnitude.mul(angle.cos()), magnitude.mul(angle.sin()));
    }

    /**
     * Generate a String representation of the {@link Vector2dU} in Cartesian coordinates.
     *
     * @return a String representation of the {@link Vector2dU}
     */
    @Override
    public String toString() {
        return toString(true);
    }

    /**
     * Standard toString method with configurable coordinate space.
     *
     * @param cartesian true to print cartesian coordinates, false to print polar coordinates
     * @return a String representation of the {@link Vector2dU}
     */
    public String toString(boolean cartesian) {
        if (cartesian) {
            return "(" + m_x.toString() + ", " + m_y.toString() + ")";
        } else {
            return "(" + getMagnitude().toString() + ", " + getAngle().toString() + ")";
        }
    }

    /**
     * Obtain a {@link Vector2d} using the vector's base values.
     *
     * @return the vector in base units
     */
    public Vector2d toBaseVector2d() {
        return forEach(U::getBaseValue);
    }

    /**
     * Perform a function on each element of this vector.
     *
     * @param modFunction function to apply to x and y individually
     * @return new vector containing the result
     */
    public Vector2d forEach(SignalModifier<U, Double> modFunction) {
        return new Vector2d(modFunction.update(m_x), modFunction.update(m_y));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public Vector2d forEach(Vector2d other, BiFunction<U, Double, Double> modFunction) {
        return new Vector2d(modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <V> second vector's {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <V extends Unit<V>> Vector2d forEach(
            Vector2dU<V> other, BiFunction<U, V, Double> modFunction) {
        return new Vector2d(modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Perform a function on each element of this vector.
     *
     * @param <V> output {@link Unit} type
     * @param modFunction function to apply to x and y individually
     * @return new vector containing the result
     */
    public <V extends Unit<V>> Vector2dU<V> forEachU(SignalModifier<U, V> modFunction) {
        return new Vector2dU<>(modFunction.update(m_x), modFunction.update(m_y));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <V> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <V extends Unit<V>> Vector2dU<V> forEachU(
            Vector2d other, BiFunction<U, Double, V> modFunction) {
        return new Vector2dU<>(
                modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <V> second vector's {@link Unit} type
     * @param <R> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <V extends Unit<V>, R extends Unit<R>> Vector2dU<R> forEachU(
            Vector2dU<V> other, BiFunction<U, V, R> modFunction) {
        return new Vector2dU<>(
                modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Obtain a {@link Vector2dU} of {@link Unitless} using the vector's base values.
     *
     * @return a Unitless vector using base units
     */
    public Vector2dU<Unitless> getAsUnitlessVector() {
        return toBaseVector2d().forEachU(Unitless::none);
    }

    /**
     * Returns the X component of the {@link Vector2dU}.
     *
     * @return the x component
     */
    public U getX() {
        return m_x;
    }

    /**
     * Returns the Y component of the {@link Vector2dU}.
     *
     * @return the y component
     */
    public U getY() {
        return m_y;
    }

    /**
     * Adds two vectors in 2d space and returns the sum. This is similar to vector addition.
     *
     * <p>For example, Vector2dU{1.0, 2.5} + Vector2dU{2.0, 5.5} = Vector2dU{3.0, 8.0}
     *
     * @param other the vector to add
     * @return the sum of the vectors
     */
    public Vector2dU<U> add(Vector2dU<U> other) {
        return forEachU(other, U::add);
    }

    /**
     * Subtracts a vector from another vector and returns the difference.
     *
     * <p>For example, Vector2dU{5.0, 4.0} - Vector2dU{1.0, 2.0} = Vector2dU{4.0, 2.0}.
     *
     * @param other the vector to subtract
     * @return the difference between the two vectors
     */
    public Vector2dU<U> sub(Vector2dU<U> other) {
        return forEachU(other, U::sub);
    }

    /**
     * Multiplies the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector2dU{2.0, 2.5} * 2 = Vector2dU{4.0, 5.0}.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector2dU<U> mul(double s) {
        return forEachU(v -> v.mul(s));
    }

    /**
     * Divides the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector2dU{2.0, 2.5} / 2 = Vector2dU{1.0, 1.25}.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector2dU<U> div(double s) {
        return forEachU(v -> v.div(s));
    }

    /**
     * Divides the vector by a scalar and returns the new vector.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector2dU<Unitless> div(U s) {
        return forEachU(v -> v.div(s));
    }

    /**
     * Negates the vector and returns the new vector.
     *
     * <p>For example, -Vector2dU{2.0, 2.5} = Vector2dU{-2.0, -2.5}
     *
     * @return the negated vector
     */
    public Vector2dU<U> neg() {
        return forEachU(U::neg);
    }

    /**
     * Calculates the total magnitude of the {@link Vector2dU}.
     *
     * @return the magnitude of the {@link Vector2dU}
     */
    public U getMagnitude() {
        double x = m_x.getBaseValue();
        // Quickly return if x == 0.0 to avoid division by zero
        if (x == 0.0) {
            return m_y.abs();
        }

        double y = m_y.getBaseValue();
        // Divide m_x by its own base value to get a "1", then scale it by the correct
        // magnitude.
        return m_x.div(m_x.getBaseValue()).mul(Math.hypot(x, y));
    }

    /**
     * Normalizes the {@link Vector2dU} so that it has a length of 1.0 while maintaining its angle.
     *
     * <p>A Vector2d of value (NaN, NaN) will result from a {@link Vector2dU} of value (0.0, 0.0).
     *
     * <p>Note: The "Only Scale Down" functionality of {@link Vector2d#normalize(boolean)} is not
     * implemented since it would be unit-dependent, and this class cannot interpret Unit enums.
     *
     * @return a new Vector2d with a magnitude of 1.0
     */
    public Vector2d normalize() {
        return toBaseVector2d().normalize(false);
    }

    /**
     * Normalizes a group of {@link Vector2dU} objects so that the longest Vector2d is exactly 1.0 and
     * the rest are scaled relatively.
     *
     * <p>A set of Vector2ds of value (NaN, NaN) will result when given a set of {@link Vector2dU}s of
     * value (0.0, 0.0).
     *
     * <p>Note: The "Only Scale Down" functionality of {@link Vector2d#normalizeGroup(List, boolean)}
     * is not implemented since it would be unit-dependent, and this class cannot interpret Unit
     * enums.
     *
     * @param <U> the type of {@link Unit} in the {@link Vector2dU}
     * @param values a group of {@link Vector2d} objects
     * @return a new group of {@link Vector2d} objects after being normalized relative to the longest
     *     Vector2d
     */
    public static <U extends Unit<U>> List<Vector2d> normalizeGroup(List<Vector2dU<U>> values) {
        List<Vector2d> v2ds =
                values.stream().map(Vector2dU<U>::toBaseVector2d).collect(Collectors.toList());
        return Vector2d.normalizeGroup(v2ds, false);
    }

    /**
     * Calculates the dot product between two vectors.
     *
     * @param other second vector in the dot product
     * @return resulting dot product
     */
    public U dot(Vector2d other) {
        return m_x.mul(other.getX()).add(m_y.mul(other.getY()));
    }

    /**
     * Calculates the dot product between two vectors.
     *
     * @param <V> second vector {@link Unit} type
     * @param <R> resulting vector {@link Unit} type
     * @param other second vector in the dot product
     * @param mulFunction function to multiply U and V
     * @return resulting dot product
     */
    public <V extends Unit<V>, R extends Unit<R>> R dot(
            Vector2dU<V> other, BiFunction<U, V, R> mulFunction) {
        return mulFunction.apply(m_x, other.getX()).add(mulFunction.apply(m_y, other.getY()));
    }

    /**
     * Calculates the cross product between two vectors.
     *
     * @param other second vector in the cross product
     * @return resulting cross product
     */
    public U cross(Vector2d other) {
        return m_x.mul(other.getY()).sub(m_y.mul(other.getX()));
    }

    /**
     * Calculates the cross product between two vectors.
     *
     * @param <V> second vector {@link Unit} type
     * @param <R> resulting vector {@link Unit} type
     * @param other second vector in the cross product
     * @param mulFunction function to multiply U and V
     * @return resulting cross product
     */
    public <V extends Unit<V>, R extends Unit<R>> R cross(
            Vector2dU<V> other, BiFunction<U, V, R> mulFunction) {
        return mulFunction.apply(m_x, other.getY()).sub(mulFunction.apply(m_y, other.getX()));
    }

    /**
     * Calculates the range between two vectors.
     *
     * @param other the other {@link Vector2dU} object to be used in the range finding operation
     * @return the range between the two vectors
     */
    public U rangeTo(Vector2dU<U> other) {
        return this.sub(other).getMagnitude();
    }

    /**
     * Rotates the {@link Vector2dU} while maintaining its magnitude.
     *
     * @param angle the amount to rotate by. A positive value will rotate in a CCW direction. (-inf,
     *     inf)
     * @return the {@link Vector2dU} after being rotated
     */
    public Vector2dU<U> rotate(Angle angle) {
        // Optimization if the rotation is by 0 degrees
        if (angle.difference(Angle.ZERO).abs().lt(k_tolerance)) {
            return this;
        }

        // Optimization if the rotation is by 90 degrees
        if (angle.difference(Angle.PI_BY_TWO).abs().lt(k_tolerance)) {
            return rotateByNinety(true);
        }
        if (angle.difference(Angle.PI_BY_TWO.neg()).abs().lt(k_tolerance)) {
            return rotateByNinety(false);
        }

        // Optimization if the rotation is by 180 degrees
        if (angle.difference(Angle.PI).abs().lt(k_tolerance)) {
            return neg();
        }

        double s = angle.sin();
        double c = angle.cos();
        U x = m_x.mul(c).sub(m_y.mul(s));
        U y = m_x.mul(s).add(m_y.mul(c));

        return new Vector2dU<U>(x, y);
    }

    private Vector2dU<U> rotateByNinety(boolean positive) {
        U x = m_y;
        U y = m_x;

        if (positive) {
            x = x.neg();
        } else {
            y = y.neg();
        }

        return new Vector2dU<U>(x, y);
    }

    /**
     * Returns the angle of the vector.
     *
     * @return the angle of the vector
     */
    public Angle getAngle() {
        return toBaseVector2d().getAngle();
    }
}
