/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.MulByTime;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/**
 * Provides a signal integrator service, which is integrated with respect to a supplied timer.
 *
 * @param <In> Input type, to be multiplied by {@link Time}
 * @param <Out> Output type. Must be the result of multiplying Input type by {@link Time}
 */
public class TimedIntegratorU<
                In extends Unit<In> & MulByTime<Out>, Out extends Unit<Out> & DivByTime<In>>
        implements SignalModifier<In, Out> {
    private final Timer m_timer;
    private final AccumulatorU<Out> m_integralAccumulator;

    private In m_previousInput = null;

    /**
     * Constructs a new TimedIntegrator.<br>
     * <br>
     * The {@link TimedIntegratorU} will begin with the provided starting value. Resetting the
     * {@link TimedIntegratorU} will reset the accumulated value to the one provided at construction.
     * <br>
     *
     * @param timeSource The time source to use for integration
     * @param startingValue The initial value of the integrator
     */
    public TimedIntegratorU(TimeSource timeSource, Out startingValue) {
        m_timer = timeSource.createTimer();
        m_timer.start();
        m_integralAccumulator = new AccumulatorU<>(startingValue);
    }

    /**
     * Resets the internally accumulated integral value to the starting value provided at construction
     */
    public void reset() {
        m_previousInput = null;
        m_integralAccumulator.reset();
    }

    /**
     * Sets the internally accumulated integral value to a supplied value
     *
     * @param integralValue Supplied value that the integral will be set to
     */
    public void setIntegralValue(Out integralValue) {
        m_integralAccumulator.setValue(integralValue);
    }

    /**
     * Gets the internally accumulated integral value
     *
     * @return The internally accumulated integral value
     */
    public Out getAccumulatedValue() {
        return m_integralAccumulator.getValue();
    }

    /**
     * Updates integral accumulation from a supplied input value Does not support null values as a
     * method input
     *
     * @param input Input value used in the integration
     * @return New internally accumulated integral value
     */
    @Override
    public Out update(In input) {
        Time dt = m_timer.reset();

        if (m_previousInput != null) {
            m_integralAccumulator.update(input.add(m_previousInput).div(2.0).mul(dt));
        }

        m_previousInput = input;

        return m_integralAccumulator.getValue();
    }
}
