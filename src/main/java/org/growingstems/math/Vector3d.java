/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;

/** Represents a vector in 3d space. This object can be used to represent a point or a vector. */
public class Vector3d {
    private final double m_x;
    private final double m_y;
    private final double m_z;

    /*
     * Constructs a Vector3d with X, Y, and Z components equal to zero.
     */
    public Vector3d() {
        this(0.0, 0.0, 0.0);
    }

    /**
     * Constructs a {@link Vector3d} using Cartesian coordinates.
     *
     * @param x the x component of the vector
     * @param y the y component of the vector
     * @param z the x component of the vector
     */
    public Vector3d(double x, double y, double z) {
        m_x = x;
        m_y = y;
        m_z = z;
    }

    /**
     * Constructs a Vector3d from a Cylindrical Coordinate System.
     *
     * <p>The cylinder is flat on the XY plane, with its height in the Z direction. Theta is the angle
     * on the XY plane, where 0 is aligned with the +X axis.
     *
     * @param radius the magnitude of the XY component of the vector
     * @param theta the angle of the XY component of the vector
     * @param height the magnitude of the Z component of the vector
     * @return a Vector3d given the equivalent vector in cylindrical coordinates
     */
    public static Vector3d fromCylindrical(double radius, Angle theta, double height) {
        Vector2d xy = new Vector2d(radius, theta);
        return new Vector3d(xy.getX(), xy.getY(), height);
    }

    /**
     * Constructs a Vector3d from a Spherical Coordinate System.
     *
     * <p>Theta is the angle on the XY plane, where 0 is aligned with the +X axis. Phi is the angle
     * from the +Z axis.
     *
     * @param radius the magnitude of the XY component of the vector
     * @param theta the angle of the XY component of the vector
     * @param phi the angle between the vector and the +Z axis
     * @return a Vector3d given the equivalent vector in spherical coordinates
     */
    public static Vector3d fromSpherical(double radius, Angle theta, Angle phi) {
        double sinPhi = phi.sin();
        double cosPhi = phi.cos();
        double sinTheta = theta.sin();
        double cosTheta = theta.cos();
        return new Vector3d(radius * sinPhi * cosTheta, radius * sinPhi * sinTheta, radius * cosPhi);
    }

    /**
     * Constructs a Vector3d from a radius, azimuth, and elevation.
     *
     * <p>This is identical to {@link Vector3d#fromSpherical(double, Angle, Angle)}, except instead of
     * using phi, which is 0 along the +Z axis and +180 degrees along the -Z axis, it uses elevation,
     * which is 0 on the XY plane, and +90 degrees along the +Z axis.
     *
     * @param radius the magnitude of the XY component of the vector
     * @param azimuth the angle of the XY component of the vector
     * @param elevation the angle between the vector and the XY plane
     * @return a Vector3d given the radius, azimuth, and elevation
     */
    public static Vector3d fromAzEl(double radius, Angle azimuth, Angle elevation) {
        return fromSpherical(radius, azimuth, Angle.PI_BY_TWO.sub(elevation));
    }

    /**
     * Generate a String representation of the {@link Vector3d} in Cartesian coordinates.
     *
     * @return a String representation of the {@link Vector3d}
     */
    @Override
    public String toString() {
        return toString(CoordinateSystem.CARTESIAN);
    }

    /**
     * Generate a String representation of the {@link Vector3d} in the provided coordinate system.
     *
     * @param system the coordinate system to use
     * @return a String representation of the {@link Vector3d}
     */
    public String toString(CoordinateSystem system) {
        switch (system) {
            case CARTESIAN:
                return "(" + m_x + ", " + m_y + ", " + m_z + ")";
            case CYLINDRICAL:
                return "(" + getXY().getMagnitude() + ", " + getXY().getAngle().toString() + ", " + m_z
                        + ")";
            case SPHERICAL:
                return "(" + getMagnitude() + ", " + getXY().getAngle().toString() + ", "
                        + angleTo(new Vector3d(0.0, 0.0, 1.0)).toString() + ")";
            case AZ_EL:
                return "(" + getMagnitude() + ", " + getXY().getAngle().toString() + ", "
                        + Angle.PI_BY_TWO.sub(angleTo(new Vector3d(0.0, 0.0, 1.0))).toString() + ")";
            default:
                return "Vector3d::toString Not yet implemented for coordinate system " + system.toString();
        }
    }

    /**
     * Perform a function on each element of this vector.
     *
     * @param modFunction function to apply to x, y, and z individually
     * @return new vector containing the result
     */
    public Vector3d forEach(SignalModifier<Double, Double> modFunction) {
        return new Vector3d(modFunction.update(m_x), modFunction.update(m_y), modFunction.update(m_z));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public Vector3d forEach(Vector3d other, BiFunction<Double, Double, Double> modFunction) {
        return new Vector3d(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <U> second vector's {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <U extends Unit<U>> Vector3d forEach(
            Vector3dU<U> other, BiFunction<Double, U, Double> modFunction) {
        return new Vector3d(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Perform a function on each element of this vector.
     *
     * @param <U> output {@link Unit} type
     * @param modFunction function to apply to x, y, and z individually
     * @return new vector containing the result
     */
    public <U extends Unit<U>> Vector3dU<U> forEachU(SignalModifier<Double, U> modFunction) {
        return new Vector3dU<>(
                modFunction.update(m_x), modFunction.update(m_y), modFunction.update(m_z));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <U> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <U extends Unit<U>> Vector3dU<U> forEachU(
            Vector3d other, BiFunction<Double, Double, U> modFunction) {
        return new Vector3dU<>(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <U> second vector's {@link Unit} type
     * @param <R> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <U extends Unit<U>, R extends Unit<R>> Vector3dU<R> forEachU(
            Vector3dU<U> other, BiFunction<Double, U, R> modFunction) {
        return new Vector3dU<>(
                modFunction.apply(m_x, other.getX()),
                modFunction.apply(m_y, other.getY()),
                modFunction.apply(m_z, other.getZ()));
    }

    /**
     * Returns the X component of the vector.
     *
     * @return the x component of the vector
     */
    public double getX() {
        return m_x;
    }

    /**
     * Returns the Y component of the vector.
     *
     * @return the y component of the vector
     */
    public double getY() {
        return m_y;
    }

    /**
     * Returns the Z component of the vector.
     *
     * @return the Z component of the vector
     */
    public double getZ() {
        return m_z;
    }

    /**
     * Returns a Vector2d to represent the X and Y components of the 3d vector.
     *
     * @return vector2d to represent the X and Y components of the 3d vector
     */
    public Vector2d getXY() {
        return new Vector2d(m_x, m_y);
    }

    /**
     * Returns a Vector2d to represent the X and Z components of the 3d vector.
     *
     * @return vector2d to represent the X and Z components of the 3d vector
     */
    public Vector2d getXZ() {
        return new Vector2d(m_x, m_z);
    }

    /**
     * Returns a Vector2d to represent the Y and Z components of the 3d vector.
     *
     * @return vector2d to represent the Y and Z components of the 3d vector
     */
    public Vector2d getYZ() {
        return new Vector2d(m_y, m_z);
    }

    /**
     * Adds two vectors in 3d space and returns the sum. This is similar to vector addition.
     *
     * <p>For example, Vector3d{1.0, 2.5, 3.0} + Vector3d{2.0, 5.5, 6.5} = Vector3d{3.0, 8.0, 9.5}.
     *
     * @param other the vector to add
     * @return the sum of the vectors
     */
    public Vector3d add(Vector3d other) {
        return forEach(other, Double::sum);
    }

    /**
     * Subtracts a vector from another vector and returns the difference.
     *
     * <p>For example, Vector3d{5.0, 4.0, 3.0} - Vector3d{1.0, 2.0, 3.0} = Vector3d{4.0, 2.0, 0.0}.
     *
     * @param other the vector to subtract
     * @return the difference between the two vectors
     */
    public Vector3d sub(Vector3d other) {
        return add(other.neg());
    }

    /**
     * Multiplies the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector3d{2.0, 2.5, 3.0} * 2 = Vector3d{4.0, 5.0, 6.0}.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector3d mul(double s) {
        return forEach(v -> v * s);
    }

    /**
     * Divides the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector3d{2.0, 2.5, 3.0} / 2 = Vector3d{1.0, 1.25, 1.5}.
     *
     * @param s The scalar to divide by
     * @return the scaled vector
     */
    public Vector3d div(double s) {
        return forEach(v -> v / s);
    }

    /**
     * Negates the vector and returns the new vector.
     *
     * <p>For example, -Vector3d{2.0, 2.5, 6.0} = Vector3d{-2.0, -2.5, -6.0}.
     *
     * @return the negated vector
     */
    public Vector3d neg() {
        return forEach(v -> -v);
    }

    /**
     * Calculates the magnitude of the vector.
     *
     * @return the magnitude of the vector
     */
    public double getMagnitude() {
        return Math.sqrt(Math.pow((m_x), 2) + Math.pow((m_y), 2) + Math.pow((m_z), 2));
    }

    /**
     * Normalizes the {@link Vector3d} so that it has a length of 1.0 while maintaining its angle.
     *
     * <p>A {@link Vector3d} of value (NaN, NaN, NaN) will result from a {@link Vector3d} of value
     * (0.0, 0.0, 0.0).
     *
     * @param onlyScaleDown if true, the output vector will only be normalized if its magnitude is
     *     greater than 1
     * @return a new {@link Vector3d} with a length of 1.0
     */
    public Vector3d normalize(boolean onlyScaleDown) {
        double mag = this.getMagnitude();

        if (!onlyScaleDown || mag > 1.0) {
            return mul(1.0 / mag);
        }
        return this;
    }

    /**
     * Normalizes a group of {@link Vector3d} objects so that the longest {@link Vector3d} is exactly
     * 1.0 and the rest are scaled relatively.
     *
     * <p>A set of {@link Vector3d}s of value (NaN, NaN, NaN) will result when given a set of
     * {@link Vector3d}s of value (0.0, 0.0, 0.0).
     *
     * @param values a group of {@link Vector3d} objects
     * @param onlyScaleDown if true, the output vector will only be normalized if its magnitude is
     *     greater than 1
     * @return a new group of {@link Vector3d} objects after being normalized relative to the longest
     *     {@link Vector3d}
     */
    public static List<Vector3d> normalizeGroup(List<Vector3d> values, boolean onlyScaleDown) {
        if (values.isEmpty()) {
            return values;
        }

        double maxMagnitude =
                values.stream().mapToDouble(Vector3d::getMagnitude).max().getAsDouble();
        if (!onlyScaleDown || maxMagnitude > 1.0) {
            double scaleFactor = 1.0 / maxMagnitude;
            return values.stream().map(value -> value.mul(scaleFactor)).collect(Collectors.toList());
        }

        return values;
    }

    /**
     * Calculates the dot product between two vectors.
     *
     * @param other second vector in the dot product
     * @return resulting dot product
     */
    public double dot(Vector3d other) {
        return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z;
    }

    /**
     * Calculates the dot product between two vectors.
     *
     * @param other second vector in the dot product
     * @return resulting dot product
     */
    public <U extends Unit<U>> U dot(Vector3dU<U> other) {
        return other.getX().mul(m_x).add(other.getY().mul(m_y)).add(other.getZ().mul(m_z));
    }

    /**
     * Calculates the cross product between two vectors.
     *
     * @param other second vector in the cross product
     * @return resulting cross product
     */
    public Vector3d cross(Vector3d other) {
        return new Vector3d(
                getYZ().cross(other.getYZ()), -getXZ().cross(other.getXZ()), getXY().cross(other.getXY()));
    }

    /**
     * Calculates the cross product between two vectors.
     *
     * @param <U> resulting vector {@link Unit} type
     * @param other second vector in the cross product
     * @return resulting cross product
     */
    public <U extends Unit<U>> Vector3dU<U> cross(Vector3dU<U> other) {
        return new Vector3dU<>(
                getYZ().cross(other.getYZ()),
                getXZ().cross(other.getXZ()).neg(),
                getXY().cross(other.getXY()));
    }

    /**
     * Calculates the range between two vectors.
     *
     * @param other the other vector to be used in the range operation
     * @return the range between the two vectors
     */
    public double rangeTo(Vector3d other) {
        return this.sub(other).getMagnitude();
    }

    /**
     * Calculates the angle between this vector and another.
     *
     * <p>Returns a zero-angle if either vector is all zeros.
     *
     * @param other the other vector in the angle operation
     * @return the angle between vectors
     */
    public Angle angleTo(Vector3d other) {
        double dotProd = dot(other);
        double mags = getMagnitude() * other.getMagnitude();
        if (mags == 0) {
            return Angle.ZERO;
        }
        double ratio = dotProd / mags;
        if (ratio <= -1.0) {
            return Angle.PI;
        } else if (ratio >= 1.0) {
            return Angle.ZERO;
        }
        return Angle.acos(dotProd / mags);
    }

    /**
     * Calculates the angle between this vector and another.
     *
     * <p>It does not matter what type of Unit the other vector uses. Returns a zero-angle if either
     * vector is all zeros.
     *
     * @param other the other vector in the angle operation
     * @return the angle between vectors
     */
    public Angle angleTo(Vector3dU<?> other) {
        return angleTo(other.toBaseVector3d());
    }
}
