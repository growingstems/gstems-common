package org.growingstems.math;

import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Unit;

public class Pose2dU<U extends Unit<U>> {
    private final Vector2dU<U> m_vector;
    private final Angle m_rotation;

    /**
     * Creates a Pose2d at a given vector and rotation
     *
     * @param vector vector component
     * @param rotation rotational component
     */
    public Pose2dU(Vector2dU<U> vector, Angle rotation) {
        m_vector = vector;
        m_rotation = rotation;
    }

    /**
     * Creates a Pose2d at given x-y coordinates and rotation
     *
     * @param x x component of vector
     * @param y y component of vector
     * @param rotation rotational component
     */
    public Pose2dU(U x, U y, Angle rotation) {
        m_vector = new Vector2dU<U>(x, y);
        m_rotation = rotation;
    }

    /**
     * Generate a String representation of the Pose2d in Cartesian coordinates.
     *
     * @return A String representation of the Pose2d
     */
    @Override
    public String toString() {
        return toString(true, Angle.Unit.DEGREES);
    }

    /**
     * Standard toString method with configurable coordinate space and angle unit
     *
     * @param cartesian true to print cartesian coordinates, false to print polar coordinates
     * @param unit Unit to print the Angle in
     * @return A String representation of the Pose2d
     */
    public String toString(boolean cartesian, Angle.Unit unit) {
        return m_vector.toString(cartesian) + ", " + m_rotation.toString(unit);
    }

    public Angle getRotation() {
        return m_rotation;
    }

    public Vector2dU<U> getVector() {
        return m_vector;
    }

    public U getX() {
        return m_vector.getX();
    }

    public U getY() {
        return m_vector.getY();
    }

    /**
     * Creates a new Pose2d by transforming by given translation and rotation values
     *
     * @param translation Vector2d translation value that the Pose2d is translated by
     * @param rotation rotation value that the Pose2d is rotated by
     * @return new Pose2d that is a copy of original transformed by input values
     */
    public Pose2dU<U> transform(Vector2dU<U> translation, Angle rotation) {
        Vector2dU<U> v = m_vector.add(translation);
        Angle a = m_rotation.add(rotation);
        return new Pose2dU<U>(v, a);
    }
}
