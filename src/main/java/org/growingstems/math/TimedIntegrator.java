/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/** Provides a signal integrator service, which is integrated with respect to a supplied timer */
public class TimedIntegrator implements SignalModifier<Double, Time> {
    private final Timer m_timer;
    private final AccumulatorU<Time> m_integralAccumulator = new AccumulatorU<>(Time.ZERO);

    private Double m_previousInput = null;

    /**
     * Constructs a new TimedIntegrator
     *
     * @param timeSource The time source to use for integration
     */
    public TimedIntegrator(TimeSource timeSource) {
        m_timer = timeSource.createTimer();
        m_timer.start();
    }

    /** Resets the internally accumulated integral value */
    public void reset() {
        m_previousInput = null;
        m_integralAccumulator.setValue(Time.ZERO);
    }

    /**
     * Sets the internally accumulated integral value to a supplied value
     *
     * @param integralValue Supplied value that the integral will be set to
     */
    public void setIntegralValue(Time integralValue) {
        m_integralAccumulator.setValue(integralValue);
    }

    /**
     * Gets the internally accumulated integral value
     *
     * @return The internally accumulated integral value
     */
    public Time getAccumulatedValue() {
        return m_integralAccumulator.getValue();
    }

    /**
     * Updates integral accumulation from a supplied input value Does not support null values as a
     * method input
     *
     * @param input Input value used in the integration
     * @return New internally accumulated integral value
     */
    @Override
    public Time update(Double input) {
        Time dt = m_timer.reset();
        Time integralStepValue = Time.ZERO;

        if (m_previousInput != null) {
            integralStepValue = dt.mul((input + m_previousInput) / 2.0);
        }

        m_previousInput = input;

        return m_integralAccumulator.update(integralStepValue);
    }
}
