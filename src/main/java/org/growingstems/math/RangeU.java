/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.measurements.Unit;

/**
 * Stores a range between two {@link Unit}s <br>
 * This class will correct input error to ensure that the high endpoint is always greater than or
 * equal to the lower endpoint.
 */
public class RangeU<U extends Unit<U>> {
    private U m_low;
    private U m_high;

    /**
     * Construct with no default arguments.
     *
     * @param low Initial low endpoint
     * @param high Initial high endpoint
     */
    public RangeU(U low, U high) {
        m_low = low;
        m_high = high;

        enforceOrder();
    }

    /** Convert to string containing the Range endpoints in the form '[low, high]' */
    @Override
    public String toString() {
        return "[" + m_low + ", " + m_high + "]";
    }

    /**
     * Sets the values of the range.<br>
     * If the values are not provided in the correct order, they will be swapped.
     *
     * @param low New value of low endpoint
     * @param high New value of high endpoint
     */
    public void setRange(U low, U high) {
        m_high = high;
        m_low = low;

        enforceOrder();
    }

    /**
     * Simple accessor for the high endpoint
     *
     * @return high endpoint
     */
    public U getHigh() {
        return m_high;
    }

    /**
     * Simple accessor for the low endpoint
     *
     * @return low endpoint
     */
    public U getLow() {
        return m_low;
    }

    /**
     * Clamp this range to fit fully within another range.
     *
     * @param range Range to clamp to. The parameter remains unmodified.
     */
    public void clampTo(RangeU<U> range) {
        m_high = Unit.min(m_high, range.getHigh());
        m_low = Unit.max(m_low, range.getLow());
    }

    /**
     * Determines the difference between the high and low endpoints
     *
     * @return Total width of range
     */
    public U getWidth() {
        return m_high.sub(m_low);
    }

    /**
     * Finds the center value between the high and low endpoints
     *
     * @return the center between endpoints
     */
    public U getCenter() {
        return m_high.add(m_low).div(2.0);
    }

    /**
     * Determines if a given value is within a range of two values, including the values themselves
     *
     * @param value - value compared with the two bounds
     * @return boolean whether the value is within the two bounds
     */
    public boolean inRange(U value) {
        return value.ge(m_low) && value.le(m_high);
    }

    /**
     * Forces a value to be within a range of two values
     *
     * @param value value compared with the two bounds
     * @return the minimum or maximum bound if the value is passed them, and the value if it is
     *     between them
     */
    public U coerceValue(U value) {
        return coerceValue(value, m_low, m_high);
    }

    /**
     * Applies a linear interpolation between the min and max values based on the given {@code m}m
     * value. If an {@code m} value is provided beyond {@code 0.0 - 1.0}, the output value will be
     * coerced to stay within the min and max values of the range. {@code m} must be finite.
     *
     * @see RangeU#extrapolate(double)
     * @see RangeU#inverseInterpolate(Unit)
     * @param m
     *     <ul>
     *       <li>{@code m = 0.0} - The min value of the range is returned.
     *       <li>{@code m = 1.0} - The max value of the range is returned.
     *       <li>{@code 0.0 < m < 1.0} - A linearly interpolated value between min and max is
     *           returned.
     *     </ul>
     *
     * @return the interpolated value based on {@code m}.
     */
    public U interpolate(double m) {
        return coerceValue(extrapolate(m));
    }

    /**
     * Applies a linear interpolation between the min and max values based on the given {@code m}
     * value. If an {@code m} value is provided beyond {@code 0.0 - 1.0}, the same linear
     * interpolation is applied to extrapolate the output value. {@code m} must be finite.
     *
     * @see RangeU#interpolate(double)
     * @see RangeU#inverseExtrapolate(Unit)
     * @param m
     *     <ul>
     *       <li>{@code m = 0.0} - The min value of the range is returned.
     *       <li>{@code m = 1.0} - The max value of the range is returned.
     *       <li>{@code 0.0 < m < 1.0} - A linearly interpolated value between min and max is
     *           returned.
     *       <li>{@code m < 0.0 or m > 1.0} - The same linear equation is applied beyond the range,
     *           extrapolating the output value.
     *     </ul>
     *
     * @return the interpolated/extrapolated value based on {@code m}.
     */
    public U extrapolate(double m) {
        // Checking for an exact value of 0.0 or 1.0 is required to handle a range with one
        // finite limit and one infinite limit.
        if (m == 0.0) {
            return m_low;
        }
        if (m == 1.0) {
            return m_high;
        }
        return m_high.mul(m).add(m_low.mul(1.0 - m));
    }

    /**
     * Given an input value, this function will return a value that represents the linearly
     * interpolated position of the input value within the {@link RangeU} as a normalized number. The
     * returned normalized number will also be coerced within {@code 0.0 - 1.0}.
     *
     * @see RangeU#interpolate(double)
     * @see RangeU#inverseExtrapolate(Unit)
     * @param value the value to be compared to this {@link RangeU}
     * @return
     *     <ul>
     *       <li>{@code 0.0} - The given value is equal to the min value of the range
     *       <li>{@code 1.0} - The given value is equal to the max value of the range
     *       <li>{@code > 0.0 and < 1.0} - A normalized value that represents the linearly
     *           interpolated position of the input value within the {@link RangeU}
     *     </ul>
     */
    public double inverseInterpolate(U value) {
        return inverseExtrapolate(coerceValue(value));
    }

    /**
     * Given an input value, this function will return a value that represents the linearly
     * interpolated position of the input value within (and outside of) the {@link RangeU} as a
     * normalized number.
     *
     * @see RangeU#extrapolate(double)
     * @see RangeU#inverseInterpolate(Unit)
     * @param value the value to be compared to this {@link RangeU}
     * @return
     *     <ul>
     *       <li>{@code 0.0} - The given value is equal to the min value of the range
     *       <li>{@code 1.0} - The given value is equal to the max value of the range
     *       <li>{@code > 0.0 and < 1.0} - A normalized value that represents the linearly
     *           interpolated position of the input value within the {@link RangeU}
     *       <li>{@code < 0.0} - A normalized value that represents the linearly interpolated position
     *           of the input value outside and below the {@link RangeU}
     *       <li>{@code > 1.0} - A normalized value that represents the linearly interpolated position
     *           of the input value outside and above the {@link RangeU}
     *     </ul>
     */
    public double inverseExtrapolate(U value) {
        if (value.eq(m_low)) {
            return 0.0;
        }
        if (value.eq(m_high)) {
            return 1.0;
        }

        if (Unit.isFinite(m_low)) {
            return value.sub(m_low).div(getWidth()).asNone();
        } else {
            return 1.0 - m_high.sub(value).div(getWidth()).asNone();
        }
    }

    /**
     * The given input value will be linearly remapped based off of the current {@link RangeU} to the
     * provided {@code otherRange}. {@code in} must be finite.
     *
     * @see <a href="https://docs.arduino.cc/language-reference/en/functions/math/map/">Arduino's
     *     map()</a> for another explanation
     * @param <V> Type being mapped to
     * @param in the value to remap
     * @param otherRange the other range to remap the given value to
     * @param extrapolate whether or not the remap should extrapolate if its outside of the current
     *     {@link RangeU}
     * @return the value remapped to the {@code otherRange}
     */
    public <V extends Unit<V>> V remap(U in, RangeU<V> otherRange, boolean extrapolate) {
        if (extrapolate) {
            return otherRange.extrapolate(inverseExtrapolate(in));
        } else {
            return otherRange.interpolate(inverseInterpolate(in));
        }
    }

    /**
     * Coerces a given value to the range of a provided minimum and maximum, with range being (min,
     * max) <br>
     * Inputs must form a valid range
     *
     * @param value value to be coerced
     * @param min minimum range bound
     * @param max maximum range bound
     * @param <U> unit type for coercion
     * @return coerced value
     */
    public static <U extends Unit<U>> U coerceValue(U value, U min, U max) {
        if (value.lt(min)) {
            return min;
        } else if (value.gt(max)) {
            return max;
        } else {
            return value;
        }
    }

    private void enforceOrder() {
        if (m_high.lt(m_low)) {
            U temp = m_high;
            m_high = m_low;
            m_low = temp;
        }
    }
}
