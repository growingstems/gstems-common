/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

/**
 * Used for adjusting a wave signal and its ON and OFF time period (duty cycle) based on its signal
 * period Takes in two variables, representing the duty cycle and signal period, and returns the
 * values back to user as a double
 */
public class PwmSignal {
    /**
     * doubles representing the signal period (duration) of the wave and the duty cycle (high period)
     */
    private double m_dutyCycle;

    private double m_period;

    private PwmSignal() {}

    /**
     * Sets up pwm signal and returns it back to the user
     *
     * @param dutyCycle - period at which the signal is HIGH
     * @param period - signal period (duration)
     * @return - return signal
     */
    public static PwmSignal fromDutyCycleAndPeriod(double dutyCycle, double period) {
        PwmSignal pwmSignal = new PwmSignal();
        pwmSignal.setPeriodAndDutyCycle(period, dutyCycle);
        return pwmSignal;
    }

    /**
     * returns signal as high and low parameters
     *
     * @param high - the duty cycle period - when the signal is ON
     * @param low - the period at which the signal is OFF
     * @return pwm signal
     */
    public static PwmSignal fromHighAndLow(double high, double low) {
        PwmSignal pwmSignal = new PwmSignal();
        pwmSignal.setHighAndLow(high, low);
        return pwmSignal;
    }

    public double getDutyCycle() {
        return m_dutyCycle;
    }

    public double getPeriod() {
        return m_period;
    }

    public double getHigh() {
        return m_dutyCycle * m_period;
    }

    public double getLow() {
        return m_period * (1.0 - m_dutyCycle);
    }

    /**
     * Sets the period and duty cycle
     *
     * @param period - signal period
     * @param dutyCycle - percent of how long the signal was ON
     */
    public void setPeriodAndDutyCycle(double period, double dutyCycle) {
        m_period = period;
        m_dutyCycle = dutyCycle;
    }

    /**
     * updates the high and low values to the current high and low values if the sum of the high and
     * low period equals zero, then no- ops
     *
     * @param high - value of how long the signal was ON
     * @param low - value of how long the signal was OFF
     */
    public void setHighAndLow(double high, double low) {
        if (high + low == 0.0) {
            return;
        }
        m_period = high + low;
        m_dutyCycle = high / (high + low);
    }
}
