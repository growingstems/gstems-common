/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/** RateCalculator calculates the rate of change of a signal. */
public class RateCalculator implements SignalModifier<Double, Frequency> {
    private final Timer m_timer;
    private Double m_previousInput = null;
    private Frequency m_output = Frequency.ZERO;

    /**
     * Construct RateCalculator and start a timer
     *
     * @param timeSource Source of time for derivation
     */
    public RateCalculator(TimeSource timeSource) {
        m_timer = timeSource.createTimer();
        m_timer.start();
    }

    /** Reset to the state it was in when newly constructed */
    public void reset() {
        m_previousInput = null;
        m_output = Frequency.ZERO;
    }

    /**
     * Calculates the rate of change <br>
     * <br>
     * Always returns 0 on first update. If the TimeSource reports no change since the last update,
     * the previous output will be returned again.
     *
     * @param input Input of the signal
     * @return The rate of change in Hertz
     */
    @Override
    public Frequency update(Double input) {
        Time dt = m_timer.reset();
        if (m_previousInput != null && dt.ne(Time.ZERO)) {
            m_output = Unitless.none(input - m_previousInput).div(dt);
        }

        m_previousInput = input;
        return m_output;
    }
}
;
