/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;

/**
 * A {@link SignalModifier} which adds all {@link Unit}s from a signal. <br>
 * Each value provided to the {@link AccumulatorU} is added to the current running sum. <br>
 * Each update provides the sum of all previous values. <br>
 * Includes the ability to limit the output to a given range. The output signal will be coerced
 * using {@link RangeU#coerceValue(Unit)} to keep the range within the provided limits. <br>
 * The value of the {@link AccumulatorU} will always be within the provided limits. This is enforced
 * by coercing the stored value every time it is modified, as well as every time the limits are set.
 */
public class AccumulatorU<U extends Unit<U>> implements SignalModifier<U, U> {
    private U m_startingValue;
    private U m_storedValue;
    private RangeU<U> m_limits = null;

    /**
     * Constructs an accumulator with a provided starting value
     *
     * @param startingValue Starting value of the accumulator
     */
    public AccumulatorU(U startingValue) {
        m_startingValue = startingValue;
        m_storedValue = startingValue;
    }

    /** Resets to the initial starting value provided on construction */
    public void reset() {
        m_storedValue = m_startingValue;
    }

    /**
     * Sets the current value. <br>
     * If limits have been provided, and the provided value is outside of those limits, it will be
     * coerced.
     *
     * @param value Value to set the {@link AccumulatorU} to
     */
    public void setValue(U value) {
        if (m_limits == null) {
            m_storedValue = value;
        } else {
            m_storedValue = m_limits.coerceValue(value);
        }
    }

    public U getValue() {
        return m_storedValue;
    }

    /**
     * Sets the limits of the {@link AccumulatorU} <br>
     * <br>
     * The currently stored value will be coerced into this range.
     *
     * @param limits The new limits to restrict to.
     */
    public void setLimits(RangeU<U> limits) {
        m_limits = limits;
        m_storedValue = m_limits.coerceValue(m_storedValue);
    }

    /**
     * Gets the current allowable limits. <br>
     * If no limits have been set, this will be null!
     *
     * @return The current allowable limits
     */
    public RangeU<U> getLimits() {
        return m_limits;
    }

    /**
     * Updates the internal accumulated value and returns updated value
     *
     * @param input Input value (does not support null values)
     * @return Updated accumulated value
     */
    @Override
    public U update(U input) {
        setValue(m_storedValue.add(input));
        return m_storedValue;
    }
}
