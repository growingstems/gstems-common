/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.measurements.Angle;

/**
 * Pose2d is a class used to represent a two-dimensional vector and rotation. It is composed of a
 * two-dimensional vector position, and an angular rotation value.
 */
public class Pose2d {
    private final Vector2d m_vector;
    private final Angle m_rotation;

    /*
     * Creates Pose2d at the origin with no rotation
     */
    public Pose2d() {
        m_vector = new Vector2d();
        m_rotation = Angle.ZERO;
    }

    /**
     * Creates a Pose2d at a given vector and rotation
     *
     * @param vector vector component
     * @param rotation rotational component
     */
    public Pose2d(Vector2d vector, Angle rotation) {
        m_vector = vector;
        m_rotation = rotation;
    }

    /**
     * Creates a Pose2d at given x-y coordinates and rotation
     *
     * @param x x component of vector
     * @param y y component of vector
     * @param rotation rotational component
     */
    public Pose2d(double x, double y, Angle rotation) {
        m_vector = new Vector2d(x, y);
        m_rotation = rotation;
    }

    /**
     * Generate a String representation of the Pose2d in Cartesian coordinates.
     *
     * @return A String representation of the Pose2d
     */
    @Override
    public String toString() {
        return toString(true, Angle.Unit.DEGREES);
    }

    /**
     * Standard toString method with configurable coordinate space and angle unit
     *
     * @param cartesian true to print cartesian coordinates, false to print polar coordinates
     * @param unit Unit to print the Angle in
     * @return A String representation of the Pose2d
     */
    public String toString(boolean cartesian, Angle.Unit unit) {
        return m_vector.toString(cartesian) + ", " + m_rotation.toString(unit);
    }

    public Angle getRotation() {
        return m_rotation;
    }

    public Vector2d getVector() {
        return m_vector;
    }

    public double getX() {
        return m_vector.getX();
    }

    public double getY() {
        return m_vector.getY();
    }

    /**
     * Creates a new Pose2d by transforming by given translation and rotation values
     *
     * @param translation Vector2d translation value that the Pose2d is translated by
     * @param rotation rotation value that the Pose2d is rotated by
     * @return new Pose2d that is a copy of original transformed by input values
     */
    public Pose2d transform(Vector2d translation, Angle rotation) {
        Vector2d v = m_vector.add(translation);
        Angle a = m_rotation.add(rotation);
        return new Pose2d(v, a);
    }
}
