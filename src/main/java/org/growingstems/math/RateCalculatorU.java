/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.MulByTime;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/** Calculates the rate of change of a {@link Unit}-based signal. */
public class RateCalculatorU<
                In extends Unit<In> & DivByTime<Out>, Out extends Unit<Out> & MulByTime<In>>
        implements SignalModifier<In, Out> {
    private final Timer m_timer;
    private final Out m_zero;
    private In m_previousInput = null;
    private Out m_output;

    /**
     * Create a {@link Unit}-based Rate Calculator<br>
     * Requires the value ZERO due to type erasure.<br>
     * For example, a {@code RateCalculatorU<Length, Velocity>} may be constructed with Velocity.ZERO.
     *
     * @param timeSource Source of time for derivation
     * @param zero The value ZERO in the selected output unit
     */
    public RateCalculatorU(TimeSource timeSource, Out zero) {
        m_zero = zero;
        m_timer = timeSource.createTimer();
        m_timer.start();
        reset();
    }

    /** Reset to the state it was in when newly constructed */
    public void reset() {
        m_previousInput = null;
        m_output = m_zero;
    }

    /**
     * Calculates the rate of change <br>
     * <br>
     * Always returns 0 on first update. If the TimeSource reports no change since the last update,
     * the previous output will be returned again.
     *
     * @param input Input of the signal
     * @return The rate of change in Hertz
     */
    @Override
    public Out update(In input) {
        Time dt = m_timer.reset();
        if (m_previousInput != null && dt.ne(Time.ZERO)) {
            m_output = input.sub(m_previousInput).div(dt);
        }

        m_previousInput = input;
        return m_output;
    }
}
;
