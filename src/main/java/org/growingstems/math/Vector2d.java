/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;

/**
 * {@link Vector2d} is a class that is used to represent a two dimensional vector.
 *
 * <p>Just like a two dimensional mathematical vector, {@link Vector2d} is made up of an X component
 * and a Y component and is generally represented as an arrow.
 */
public class Vector2d {
    private static final Angle k_tolerance = Angle.radians(1e-5);

    private final double m_x;
    private final double m_y;

    /** Creates a {@link Vector2d} object with X and Y set to 0. */
    public Vector2d() {
        this(0.0, 0.0);
    }

    /**
     * Creates a {@link Vector2d} object using Cartesian coordinates.
     *
     * @param x the X component
     * @param y the Y component
     */
    public Vector2d(double x, double y) {
        m_x = x;
        m_y = y;
    }

    /**
     * Creates a {@link Vector2d} object using polar coordinates.
     *
     * @param magnitude the magnitude component
     * @param angle the angle component
     */
    public Vector2d(double magnitude, Angle angle) {
        this(magnitude * angle.cos(), magnitude * angle.sin());
    }

    /**
     * Generate a String representation of the {@link Vector2d} in Cartesian coordinates.
     *
     * @return a String representation of the {@link Vector2d}
     */
    @Override
    public String toString() {
        return toString(true);
    }

    /**
     * Standard toString method with configurable coordinate space.
     *
     * @param cartesian true to print cartesian coordinates, false to print polar coordinates
     * @return a String representation of the {@link Vector2d}
     */
    public String toString(boolean cartesian) {
        if (cartesian) {
            return "(" + m_x + ", " + m_y + ")";
        } else {
            return "(" + getMagnitude() + ", " + getAngle().toString() + ")";
        }
    }

    /**
     * Perform a function on each element of this vector.
     *
     * @param modFunction function to apply to x and y individually
     * @return new vector containing the result
     */
    public Vector2d forEach(SignalModifier<Double, Double> modFunction) {
        return new Vector2d(modFunction.update(m_x), modFunction.update(m_y));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public Vector2d forEach(Vector2d other, BiFunction<Double, Double, Double> modFunction) {
        return new Vector2d(modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <U> second vector's {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <U extends Unit<U>> Vector2d forEach(
            Vector2dU<U> other, BiFunction<Double, U, Double> modFunction) {
        return new Vector2d(modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Perform a function on each element of this vector.
     *
     * @param <U> output {@link Unit} type
     * @param modFunction function to apply to x and y individually
     * @return new vector containing the result
     */
    public <U extends Unit<U>> Vector2dU<U> forEachU(SignalModifier<Double, U> modFunction) {
        return new Vector2dU<>(modFunction.update(m_x), modFunction.update(m_y));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <U> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <U extends Unit<U>> Vector2dU<U> forEachU(
            Vector2d other, BiFunction<Double, Double, U> modFunction) {
        return new Vector2dU<>(
                modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Perform a function on each pair of elements of this and another vector.
     *
     * <p>This vector's members will be used as the first input to the function.
     *
     * @param <U> second vector's {@link Unit} type
     * @param <R> output {@link Unit} type
     * @param other second vector in the operation
     * @param modFunction function to apply to each pair of elements
     * @return new vector containing the result
     */
    public <U extends Unit<U>, R extends Unit<R>> Vector2dU<R> forEachU(
            Vector2dU<U> other, BiFunction<Double, U, R> modFunction) {
        return new Vector2dU<>(
                modFunction.apply(m_x, other.getX()), modFunction.apply(m_y, other.getY()));
    }

    /**
     * Returns the X component of the {@link Vector2d}.
     *
     * @return the x component
     */
    public double getX() {
        return m_x;
    }

    /**
     * Returns the Y component of the {@link Vector2d}.
     *
     * @return the y component
     */
    public double getY() {
        return m_y;
    }

    /**
     * Adds two vectors in 2d space and returns the sum. This is similar to vector addition.
     *
     * <p>For example, Vector2d{1.0, 2.5} + Vector2d{2.0, 5.5} = Vector2d{3.0, 8.0}
     *
     * @param other the vector to add
     * @return the sum of the vectors
     */
    public Vector2d add(Vector2d other) {
        return forEach(other, Double::sum);
    }

    /**
     * Subtracts a vector from another vector and returns the difference.
     *
     * <p>For example, Vector2d{5.0, 4.0} - Vector2d{1.0, 2.0} = Vector2d{4.0, 2.0}.
     *
     * @param other the vector to subtract
     * @return the difference between the two vectors
     */
    public Vector2d sub(Vector2d other) {
        return add(other.neg());
    }

    /**
     * Multiplies the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector2d{2.0, 2.5} * 2 = Vector2d{4.0, 5.0}.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector2d mul(double s) {
        return forEach(v -> v * s);
    }

    /**
     * Divides the vector by a scalar and returns the new vector.
     *
     * <p>For example, Vector2d{2.0, 2.5} / 2 = Vector2d{1.0, 1.25}.
     *
     * @param s the scalar to multiply by
     * @return the scaled vector
     */
    public Vector2d div(double s) {
        return forEach(v -> v / s);
    }

    /**
     * Negates the vector and returns the new vector.
     *
     * <p>For example, -Vector2d{2.0, 2.5} = Vector2d{-2.0, -2.5}.
     *
     * @return the negated vector
     */
    public Vector2d neg() {
        return forEach(v -> -v);
    }

    /**
     * Calculates the total length of the {@link Vector2d}.
     *
     * @return the length of the {@link Vector2d}
     */
    public double getMagnitude() {
        return Math.hypot(m_x, m_y);
    }

    /**
     * Normalizes the {@link Vector2d} so that it has a length of 1.0 while maintaining its angle.
     *
     * <p>A {@link Vector2d} of value (NaN, NaN) will result from a {@link Vector2d} of value (0.0,
     * 0.0).
     *
     * @param onlyScaleDown if true, the output vector will only be normalized if its magnitude is
     *     greater than 1
     * @return a new {@link Vector2d} with a length of 1.0
     */
    public Vector2d normalize(boolean onlyScaleDown) {
        double mag = this.getMagnitude();

        if (!onlyScaleDown || mag > 1.0) {
            return mul(1.0 / mag);
        }
        return this;
    }

    /**
     * Normalizes a group of {@link Vector2d} objects so that the longest {@link Vector2d} is exactly
     * 1.0 and the rest are scaled relatively.
     *
     * <p>A set of {@link Vector2d}s of value (NaN, NaN) will result when given a set of
     * {@link Vector2d}s of value (0.0, 0.0).
     *
     * @param values a group of {@link Vector2d} objects
     * @param onlyScaleDown if true, the output vector will only be normalized if its magnitude is
     *     greater than 1
     * @return a new group of {@link Vector2d} objects after being normalized relative to the longest
     *     {@link Vector2d}
     */
    public static List<Vector2d> normalizeGroup(List<Vector2d> values, boolean onlyScaleDown) {
        if (values.isEmpty()) {
            return values;
        }

        double maxMagnitude =
                values.stream().mapToDouble(Vector2d::getMagnitude).max().getAsDouble();
        if (!onlyScaleDown || maxMagnitude > 1.0) {
            double scaleFactor = 1.0 / maxMagnitude;
            return values.stream().map(value -> value.mul(scaleFactor)).collect(Collectors.toList());
        }

        return values;
    }

    /**
     * Performs a dot product operation on two {@link Vector2d} objects, one being the original and
     * the other being the argument.
     *
     * @param other the other {@link Vector2d} object to be used in the dot product operation
     * @return the result after performing the dot product operation
     */
    public double dot(Vector2d other) {
        return m_x * other.m_x + m_y * other.m_y;
    }

    /**
     * Calculates the dot product between two vectors.
     *
     * @param <U> resulting vector {@link Unit} type
     * @param other second vector in the dot product
     * @return resulting dot product
     */
    public <U extends Unit<U>> U dot(Vector2dU<U> other) {
        return other.getX().mul(m_x).add(other.getY().mul(m_y));
    }

    /**
     * Performs a cross product operation on two {@link Vector2d} objects, one being the original and
     * the other being the argument.
     *
     * @param other the other {@link Vector2d} object to be used in the cross product operation
     * @return the result after performing the cross product operation
     */
    public double cross(Vector2d other) {
        return m_x * other.m_y - m_y * other.m_x;
    }

    /**
     * Calculates the cross product between two vectors.
     *
     * @param <U> resulting vector {@link Unit} type
     * @param other second vector in the cross product
     * @return resulting cross product
     */
    public <U extends Unit<U>> U cross(Vector2dU<U> other) {
        return other.getY().mul(m_x).sub(other.getX().mul(m_y));
    }

    /**
     * Calculates the range between two vectors.
     *
     * @param other the other vector to be used in the range operation
     * @return the range between the two vectors
     */
    public double rangeTo(Vector2d other) {
        return this.sub(other).getMagnitude();
    }

    /**
     * Rotates the {@link Vector2d} while maintaining its length.
     *
     * @param angle the amount to rotate by. A positive value will rotate in a CCW direction. (-inf,
     *     inf)
     * @return the {@link Vector2d} after being rotated
     */
    public Vector2d rotate(Angle angle) {
        // Optimization if the rotation is by 0 degrees
        if (angle.difference(Angle.ZERO).abs().lt(k_tolerance)) {
            return this;
        }

        // Optimization if the rotation is by 90 degrees
        if (angle.difference(Angle.PI_BY_TWO).abs().lt(k_tolerance)) {
            return rotateByNinety(true);
        }
        if (angle.difference(Angle.PI_BY_TWO.neg()).abs().lt(k_tolerance)) {
            return rotateByNinety(false);
        }

        // Optimization if the rotation is by 180 degrees
        if (angle.difference(Angle.PI).abs().lt(k_tolerance)) {
            return neg();
        }

        double x = m_x * angle.cos() - m_y * angle.sin();
        double y = m_x * angle.sin() + m_y * angle.cos();

        return new Vector2d(x, y);
    }

    private Vector2d rotateByNinety(boolean positive) {
        double x = m_y;
        double y = m_x;

        if (positive) {
            x *= -1.0;
        } else {
            y *= -1.0;
        }

        return new Vector2d(x, y);
    }

    /**
     * Returns the angle of the vector.
     *
     * @return the angle of the vector
     */
    public Angle getAngle() {
        return Angle.atan2(m_y, m_x);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Vector2d) {
            Vector2d vector = (Vector2d) other;
            return this.m_x == vector.m_x && this.m_y == vector.m_y;
        } else {
            return false;
        }
    }

    /**
     * Compares two {@link Vector2d}s to see if they are the same with a given epsilon. The comparison
     * is handled as a cartesian system by the comparing the x and y components.
     *
     * @param other the other {@link Vector2d} to compare
     * @param epsilon the epsilon value used to compare. This determines how close the x and y
     *     components need to be (individually) in order to be considered equal
     * @return if the two {@link Vector2d}s are determined to be equal to each other
     */
    public boolean equals(Object other, double epsilon) {
        if (other instanceof Vector2d) {
            Vector2d vector = (Vector2d) other;
            return Math.abs(this.m_x - vector.m_x) < epsilon && Math.abs(this.m_y - vector.m_y) < epsilon;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(m_x, m_y);
    }
}
