/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

/** A class for common mathmatical equations. */
public class Equations {
    /**
     * Calculates the percent error. <br>
     * A percentage value is returned, e.g. 100% is represented by 100.0, not 1.0
     *
     * @param expected the expected value
     * @param actual the actual value
     * @return the percent error
     */
    public static double percentError(double expected, double actual) {
        if (expected == 0 && actual == 0) {
            return 0.0;
        } else if (expected == 0) {
            return Double.POSITIVE_INFINITY;
        }

        return Math.abs((actual - expected) / expected) * 100.0;
    }
}
