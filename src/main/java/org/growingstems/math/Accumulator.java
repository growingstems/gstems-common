/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import org.growingstems.signals.api.SignalModifier;

/**
 * A {@link SignalModifier} which adds all values from a signal. <br>
 * Each value provided to the {@link Accumulator} is added to the current running sum. <br>
 * Each update provides the sum of all previous values. <br>
 * Includes the ability to limit the output to a given range. The output signal will be coerced
 * using {@link Range#coerceValue(double)} to keep the range within the provided limits. <br>
 * The value of the {@link Accumulator} will always be within the provided limits. This is enforced
 * by coercing the stored value every time it is modified, as well as every time the limits are set.
 */
public class Accumulator implements SignalModifier<Double, Double> {
    private double m_storedValue;
    private Range m_limits = new Range(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);

    /** Constructs an accumulator with a starting value of 0.0 */
    public Accumulator() {
        m_storedValue = 0.0;
    }

    /**
     * Constructs an accumulator with a provided starting value
     *
     * @param startingValue Starting value of the accumulator
     */
    public Accumulator(double startingValue) {
        m_storedValue = startingValue;
    }

    /**
     * Resets the accumulated value to 0.0 <br>
     * <br>
     * If the limits do not include the value 0.0, the value will be set to the limit closest to 0.0.
     */
    public void reset() {
        setValue(0.0);
    }

    /**
     * Sets the current value. <br>
     * If the provided value is outside of the current limits, it will be coerced.
     *
     * @param value Value to set the {@link Accumulator} to
     */
    public void setValue(double value) {
        m_storedValue = m_limits.coerceValue(value);
    }

    public double getValue() {
        return m_storedValue;
    }

    /**
     * Sets the limits of the Accumulator <br>
     * <br>
     * The currently stored value will be coerced into this range.
     *
     * @param limits The new limits to restrict to.
     */
    public void setLimits(Range limits) {
        m_limits = limits;
        m_storedValue = m_limits.coerceValue(m_storedValue);
    }

    /**
     * Gets the current allowable limits. <br>
     * If no limits have been set, this will be a {@link Range} of {@link Double#NEGATIVE_INFINITY} to
     * {@link Double#POSITIVE_INFINITY}
     *
     * @return The current allowable limits
     */
    public Range getLimits() {
        return m_limits;
    }

    /**
     * Updates the internal accumulated value and returns updated value
     *
     * @param input Input value (does not support null values)
     * @return Updated accumulated value
     */
    @Override
    public Double update(Double input) {
        setValue(m_storedValue + input);
        return m_storedValue;
    }
}
