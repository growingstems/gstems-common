/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public class PrintStreamTest {
    private ByteArrayOutputStream errorStream;
    private PrintStream systemErr;

    // Redirect System.err to stored errorStream
    @BeforeEach
    public void setupErrorStream() {
        errorStream = new ByteArrayOutputStream();
        systemErr = System.err;
        System.setErr(new PrintStream(errorStream));
    }

    // Redirect System.err back to original output
    @AfterEach
    public void replaceErrorStream() {
        System.setErr(systemErr);
    }

    // Assert that an error occurred and clear the error stream
    public void assertErrorOccurred() {
        assertNotEquals("", errorStream.toString());
        errorStream.reset();
    }

    // Assert that no error has occurred
    protected void assertNoError() {
        assertEquals("", errorStream.toString());
    }
}
