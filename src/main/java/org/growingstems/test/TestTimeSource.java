/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.test;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;

/**
 * {@link TimeSource} implementation for use in unit testing. <br>
 * Allows user to control the Timer's output.
 */
public class TestTimeSource implements TimeSource {
    /** The current value of the Timer. */
    protected Time m_time;

    /** Construct with an initial system time of 0.0 seconds */
    public TestTimeSource() {
        this(Time.ZERO);
    }

    /**
     * Constructs with a specified initial system time.
     *
     * @param initialTime Time in seconds
     */
    public TestTimeSource(Time initialTime) {
        m_time = initialTime;
    }

    @Override
    public Time clockTime() {
        return m_time;
    }

    public void setTime(Time time) {
        m_time = time;
    }

    public void addTime(Time time) {
        m_time = m_time.add(time);
    }
}
