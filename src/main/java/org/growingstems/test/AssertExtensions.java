/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.test;

import static org.growingstems.math.Equations.percentError;
import static org.junit.jupiter.api.Assertions.assertTrue;

/** A collection of static assert methods. */
public class AssertExtensions {
    /**
     * Checks the actual value against the expected value to see if it is within an acceptable error
     * range. <br>
     * The error, by percent of the expected value, is compared to the passed allowed tolerance. <br>
     * Whether the error is positive or negative is ignored, only the magnitude is compared.
     *
     * @param message the identifying message for the {@link AssertionError}, or null
     * @param expected the value that is expected
     * @param actual the value that is being tested
     * @param percentError the acceptable error in either direction by percent of the expected value
     *     as a decimal.
     */
    public static void assertPercentError(
            String message, double expected, double actual, double percentError) {
        double pErr = percentError(expected, actual);
        String usefulError = "Expected: " + expected + "\nActual: " + actual + "\nError: " + pErr
                + "%\nAllowed Error: " + percentError + "%";
        assertTrue(pErr <= percentError, usefulError + "\n" + message);
    }

    /**
     * Equivalent to {@link #assertPercentError(String, double, double, double)} with no message. <br>
     *
     * @param expected the value that is expected
     * @param actual the value that is being tested
     * @param percentError the acceptable error in either direction by percent of the expected value
     *     as a decimal.
     */
    public static void assertPercentError(double expected, double actual, double percentError) {
        assertPercentError(null, expected, actual, percentError);
    }
}
