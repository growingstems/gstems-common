package org.growingstems.test;

/** Utilities code for use with unit testing code. */
public class TestCore {
    /**
     * A common floating point Epsilon value used for comparing doubles within tests.
     *
     * <p>Example: {@code if (Math.abs(foo - bar) < TestCore.k_eps)}
     *
     * <p>Example: {@code assertEquals(expected, x, TestCore.k_eps);}
     */
    public static final double k_eps = 1.0e-9;
}
