/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import org.growingstems.math.Range;
import org.growingstems.sensor.SensorCore.Correlation;

/**
 * Provides software stop (SoftStop) functionality for a mechanism. <br>
 * <br>
 * This class can be used to implement software defined motion boundries for a mechanism. There are
 * 2 sets of limits that can be configured: extreme limits (refered to simply as limit), and near
 * limits. The purpose of this is to allow the mechanism to slow down as it approaches the limits to
 * help compensate for momentum.<br>
 * All units for any sensor value input into the function are required to be consistent. No units
 * checking is performed. <br>
 * <br>
 * Operation:<br>
 * While the sensor reports that the mechanism is not at any limit (near or extreme), the input
 * power will simply be passed through.<br>
 * When at a near limit, input power will only be used if it is determined to be slower than the
 * corresponding near speed for that limit. Otherwise the near speed will be returned.<br>
 * When at an extreme limit the input power will only be used if it is determined that doing so
 * would case the mechanism to move away from the limit. Otherwise a power of 0 will be returned.
 * <br>
 * <br>
 * Notes:<br>
 * Limit values (both high and low) should always be set such that they numerically make sense.
 * Limit values do not take into account correlation settings, therefore a high limit should always
 * be a larger number than a low limit.<br>
 * This class does not attempt to perform any form of input correction based on the set correlation.
 * The user should alway assign near motor speeds that make sense for the configured correlation.
 */
public class SoftStop {
    private Correlation m_correlation; // how the sensor responds to command inputs
    private Range m_limits; // sensor reading limits before activating soft stop
    private Range m_nearLimits; // sensor readings past which the motor will slow down
    private double
            m_lowNearSpeed; // speed the motor will go when approaching the low limit from beyond the low
    // near limit
    private double
            m_highNearSpeed; // speed the motor will go when approaching the high limit from beyong the
    // high near limit
    private boolean m_enabled;

    /**
     * Overloaded Constructor <br>
     * <br>
     * Defaults nearLimits to 10% below/above limits provided (percentage is of the total limit range)
     * <br>
     * Defaults nearSpeeds to -0.4 for low and 0.4 for high <br>
     * Defaults correlation to POSITIVE_INCREASING <br>
     *
     * @param limits pair of limits the sensor is allowed to read before stopping the motor
     */
    public SoftStop(Range limits) {
        this(
                limits,
                new Range(
                        limits.getLow() + (limits.getHigh() - limits.getLow()) * 0.1,
                        limits.getHigh() - (limits.getHigh() - limits.getLow()) * 0.1),
                -0.4,
                0.4,
                Correlation.POSITIVE_INCREASING);
    }
    /**
     * Overloaded Constructor <br>
     * <br>
     * Defaults nearSpeeds to -0.4 for low and 0.4 for high <br>
     * Defaults correlation to POSITIVE_INCREASING <br>
     *
     * @param limits pair of limits the sensor is allowed to read before stopping the motor
     * @param nearLimits pair of sensor values past which the motor will slow down
     */
    public SoftStop(Range limits, Range nearLimits) {
        this(limits, nearLimits, -0.4, 0.4, Correlation.POSITIVE_INCREASING);
    }
    /**
     * Overloaded Constructor<br>
     * <br>
     * Defaults correlation to POSITIVE_INCREASING
     *
     * @param limits pair of limits the sensor is allowed to read before stopping the motor
     * @param nearLimits pair of sensor values past which the motor will slow down
     * @param lowNearSpeed motor value that should be used when approaching the low limit from the low
     *     near limit
     * @param highNearSpeed motor value that should be used when approaching the high limit from the
     *     high near limit
     */
    public SoftStop(Range limits, Range nearLimits, double lowNearSpeed, double highNearSpeed) {
        this(limits, nearLimits, lowNearSpeed, highNearSpeed, Correlation.POSITIVE_INCREASING);
    }
    /**
     * Full Constructor
     *
     * @param limits pair of limits the sensor is allowed to read before stopping the motor
     * @param nearLimits pair of sensor values past which the motor will slow down
     * @param lowNearSpeed motor value that should be used when approaching the low limit from the low
     *     near limit
     * @param highNearSpeed motor value that should be used when approaching the high limit from the
     *     high near limit
     * @param correlation used to denote how the sensor responds to commanded input
     */
    public SoftStop(
            Range limits,
            Range nearLimits,
            double lowNearSpeed,
            double highNearSpeed,
            Correlation correlation) {
        setCorrelation(correlation);
        setLimits(limits);
        setNearLimits(nearLimits);
        setNearSpeeds(lowNearSpeed, highNearSpeed);

        // start soft stops enabled by default
        m_enabled = true;
    }

    // getter functions
    public Correlation getCorrelation() {
        return m_correlation;
    }

    public Range getLimits() {
        return m_limits; // sensor reading limits before activating soft stop
    }

    public Range getNearLimits() {
        return m_nearLimits; // sensor readings past which the motor will slow down
    }

    public double getLowNearSpeed() {
        return m_lowNearSpeed;
    }

    public double getHighNearSpeed() {
        return m_highNearSpeed;
    }

    public boolean getEnabled() {
        return m_enabled;
    }

    // setter functions
    /**
     * Set the power input correlation. This value is used to infer how a power input will affect the
     * sensor reading.
     *
     * @param correlation value to set power input correlation to
     */
    public void setCorrelation(Correlation correlation) {
        m_correlation = correlation;
    }
    /**
     * Set the upper and lower sensor limits.
     *
     * @param limits pair of the upper and lower limits, X: lower limit, Y: upper limit
     */
    public void setLimits(Range limits) {
        m_limits = limits;
    }
    /**
     * Set the near upper and near lower limits.<br>
     * This function will correct input provided in reverse order.<br>
     * The function will also coerce input to the already established upper and lower limit if the
     * input excedes these values.
     *
     * @param nearLimits pair of the new near upper and near lower limits, X: near lower, Y: near
     *     upper
     */
    public void setNearLimits(Range nearLimits) {
        // set limits
        m_nearLimits = nearLimits;

        // coerce to extreme limits if necessary
        m_nearLimits.clampTo(m_limits);
    }
    /**
     * Set the speeds that will be used while the sensor is inbetween a near and extreme limit.<br>
     * The speeds are the value that will be used when at a near limit. The low nearSpeed value will
     * be used if the powerIn is going faster than the low nearSpeed when at a low nearLimit. The same
     * is true for the high nearSpeed.<br>
     * This function does not attempt to perform input correction based on correlation. Ensure You
     * input the nearSpeeds in the correct order.
     *
     * @param lowNearSpeed motor value that should be used when approaching the low limit from the low
     *     near limit
     * @param highNearSpeed motor value that should be used when approaching the high limit from the
     *     high near limit
     */
    public void setNearSpeeds(double lowNearSpeed, double highNearSpeed) {
        m_lowNearSpeed = lowNearSpeed;
        m_highNearSpeed = highNearSpeed;
    }

    // interaction functions
    /**
     * Disable soft stop functionality. While diabeled the getPowerOut function will simply pass the
     * powerIn through as powerOut.
     */
    public void disable() {
        m_enabled = false;
    }

    /** Enable soft stop functionality. */
    public void enable() {
        m_enabled = true;
    }

    /**
     * Adjust power input based on current correlation value.
     *
     * @param powerIn power input to be coerced
     * @param fallbackPower power to coerce powerIn to if necessary
     * @param atHighLimit true if the current limit the soft stop is at is a high limit
     * @return coerced power input if coercion took place, otherwise powerIn
     */
    private double correlationPowerAdjust(double powerIn, double fallbackPower, boolean atHighLimit) {
        boolean posCor = m_correlation == Correlation.POSITIVE_INCREASING;

        /**
         * determine wether or not power check should be using > or < positive correlation | at high
         * limit | Q 0 | 0 | > 0 | 1 | < 1 | 0 | < 1 | 1 | >
         */
        if (posCor != atHighLimit) {
            return (powerIn < fallbackPower ? fallbackPower : powerIn);
        } else {
            return (powerIn > fallbackPower ? fallbackPower : powerIn);
        }
    }

    /**
     * Return the appropriate power based on desired power and current sensor position.<br>
     * While enabled the powerOut will be set based on the soft stop configuration. This process is
     * fully defined in the class description.<br>
     * There is no garuntee that the power out value will be the same sign as what was input into the
     * function. Specifically, if the current sensor position is at one of the near limits, the
     * nearSpeed for that limit will be used without doing a sign check between it and power in.
     *
     * @param powerIn desired motor value
     * @param senseIn current sensor position
     * @return power with soft stop compensation applied
     */
    public double getPowerOut(double powerIn, double senseIn) {
        // if disabled, simply pass through power in
        if (!m_enabled) {
            return powerIn;
        }

        double powerOut = 0.0;
        if (atHighLimit(senseIn)) {
            // at/past high limit
            powerOut = correlationPowerAdjust(powerIn, 0.0, true);
        } else if (atLowLimit(senseIn)) {
            // at/past low
            powerOut = correlationPowerAdjust(powerIn, 0.0, false);
        } else if (atHighNearLimit(senseIn)) {
            // between near high and high
            powerOut = correlationPowerAdjust(powerIn, m_highNearSpeed, true);
        } else if (atLowNearLimit(senseIn)) {
            // between near low and low
            powerOut = correlationPowerAdjust(powerIn, m_lowNearSpeed, false);
        } else {
            // case 1: not at any limit
            powerOut = powerIn;
        }

        return powerOut;
    }

    // introspection functions
    /**
     * Determine if the sensor position is at or past the upper extreme limit
     *
     * @param senseIn current sensor position
     * @return true if senseIn is at or further than the upper extreme limit
     */
    public boolean atHighLimit(double senseIn) {
        return senseIn >= m_limits.getHigh();
    }
    /**
     * Determine if the sensor position is at or past the upper near limit
     *
     * @param senseIn current sensor position
     * @return true if senseIn is at or further than the upper near limit
     */
    public boolean atLowLimit(double senseIn) {
        return senseIn <= m_limits.getLow();
    }
    /**
     * Determine if the sensor position is at or past the lower extreme limit
     *
     * @param senseIn current sensor position
     * @return true if senseIn is at or further than the lower extreme limit
     */
    public boolean atHighNearLimit(double senseIn) {
        return senseIn >= m_nearLimits.getHigh();
    }
    /**
     * Determine if the sensor position is at or past the lower near limit
     *
     * @param senseIn current sensor position
     * @return true if senseIn is at or further than the lower near limit
     */
    public boolean atLowNearLimit(double senseIn) {
        return senseIn <= m_nearLimits.getLow();
    }
}
;
