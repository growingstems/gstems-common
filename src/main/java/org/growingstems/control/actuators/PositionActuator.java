/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.actuators;

import org.growingstems.math.RangeU;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Unit;

/**
 * Represents an actuator that can both do open loop control and also closed loop control using a
 * feedback sensor capable of measuring the actuator's current position. References to the feedback
 * sensors native units are referred to as as a sensor unit (SU).
 *
 * @param <P> the position unit used by this actuator
 */
public abstract class PositionActuator<P extends Unit<P>> extends MotorActuator {
    protected RangeU<P> m_positionalRange;
    protected final P m_unitPerSensorUnit;
    protected final P m_zeroP;

    protected P m_positionalOffset;

    protected P m_goalPosition;

    /**
     * Creates a position actuator with the given parameters.
     *
     * @param unitPerSensorUnit the conversion factor in {@code P} units for every native sensor unit
     *     of the actuator's feedback sensor
     * @param startingPositionOffset the offset applied to the actuator's position after being
     *     converted to {@code P} units
     * @param positionRange the max allowed positional range of the actuator in {@code P} units after
     *     the conversion factor and offset is applied
     */
    protected PositionActuator(
            P unitPerSensorUnit, P startingPositionOffset, RangeU<P> positionRange) {
        m_unitPerSensorUnit = unitPerSensorUnit;
        m_positionalOffset = startingPositionOffset;
        m_positionalRange = positionRange;
        m_zeroP = unitPerSensorUnit.sub(unitPerSensorUnit);
        m_goalPosition = unitPerSensorUnit.sub(unitPerSensorUnit);
    }

    /**
     * Commands the actuator controller to run in closed loop position mode. The actuator controller
     * will actively try to go to the requested position based on the position PID settings of the
     * actuator.
     *
     * @param position_su the position that the actuator is being commanded to in the feedback
     *     sensor's native sensor units.
     */
    protected abstract void setHalPosition(double position_su);

    /**
     * Commands the actuator controller to run in closed loop position mode with an additional feed
     * forward term. The actuator controller will actively try to go to the requested position based
     * on the position PID settings of the actuator. Use the feed forward term to add an additional
     * voltage power offset to the closed loop controller. An example of using feed forward is for
     * compensating for gravity on the actuator.
     *
     * @param position_su the position that the actuator controller is being commanded to in the
     *     feedback sensor's native sensor units.
     * @param arbitraryFeedForward the amount of additional voltage to be applied to the actuator
     *     controller
     */
    protected abstract void setHalPosition(double position_su, Voltage arbitraryFeedForward);

    /**
     * Gets the actuator's current position directly from the actuator's feedback sensor.
     *
     * @return the measured position of the actuator's feedback sensor in raw sensor units.
     */
    protected abstract double getHalPosition_su();

    /**
     * Gets the position of the actuator without applying the current offset.
     *
     * @return the position without the applied offset
     */
    public P getRawPosition() {
        return m_unitPerSensorUnit.mul(getHalPosition_su());
    }

    /**
     * Converts the given position in {@code P} units to the actuator feedback sensor's native sensor
     * units.
     *
     * @param position the position in {@code P} units
     * @return the position in the actuator feedback sensor's native sensor units
     */
    protected double positionToSensorUnits(P position) {
        return position.sub(m_positionalOffset).div(m_unitPerSensorUnit).asNone();
    }

    /**
     * Gets the position of the actuator.
     *
     * @return the position of the actuator
     */
    public P getPosition() {
        return getRawPosition().add(m_positionalOffset);
    }

    /**
     * Gets the max positional range of motion the actuator is allowed to use.
     *
     * @return the positional range of motion of the actuator
     */
    public RangeU<P> getPositionalRange() {
        return m_positionalRange;
    }

    /**
     * Gets the error of the closed loop position controller. The error represents how far away the
     * actuator is from its current goal, and the direction the error is in. A positive error means
     * that a positive power needs to be applied in order to reduce the error. The result is only
     * valid if the current use of the actuator is for position closed loop control.
     *
     * @return the positional error of the closed loop position controller
     */
    public P getPositionError() {
        return getGoalPosition().sub(getPosition());
    }

    /**
     * Modifies the offset of the actuator based on where the actuator is currently physically
     * positioned to match the position that is provided.
     *
     * @param physicalPosition the current physical position of the actuator
     */
    public void calibrateOffset(P physicalPosition) {
        m_positionalOffset = physicalPosition.sub(getRawPosition());
    }

    /**
     * Commands the actuator to run in closed loop position mode. The actuator will actively try to go
     * to the requested position based on the position PID settings of the actuator's controller.
     *
     * @param position the position that the actuator is being commanded to
     */
    public void setPosition(P position) {
        setPosition(position, Voltage.ZERO);
    }

    /**
     * Commands the actuator to run in closed loop position mode with an additional feed forward term.
     * The actuator will actively try to go to the requested position based on the position PID
     * settings of the actuator's controller. Use the feed forward term to add an additional voltage
     * offset to the closed loop controller. An example of using feed forward is for compensating for
     * gravity on the actuator.
     *
     * @param position the position that the actuator is being commanded to
     * @param arbitraryFeedForward the amount of additional voltage to be applied to the actuator
     */
    public void setPosition(P position, Voltage arbitraryFeedForward) {
        if (m_disabled) {
            stop();
            return;
        }

        m_goalPosition = m_positionalRange.coerceValue(position);
        setHalPosition(positionToSensorUnits(m_goalPosition), arbitraryFeedForward);
    }

    /**
     * The current positional goal of the actuator in closed loop position control. The result is only
     * valid if the current use of the actuator is for position closed loop control.
     *
     * @return the current positional goal of the closed loop position controller
     */
    public P getGoalPosition() {
        return m_goalPosition;
    }
}
