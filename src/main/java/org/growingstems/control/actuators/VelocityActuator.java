/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.actuators;

import org.growingstems.math.RangeU;
import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.MulByTime;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Unit;

/**
 * Represents an actuator that can both do open loop control and also closed loop control using a
 * feedback sensor capable of measuring the actuator's current position and velocity. References to
 * the feedback sensor's native units are referred to as as a sensor unit (SU).
 *
 * @param <P> the position unit used by this actuator
 * @param <V> the velocity unit used by this actuator
 */
public abstract class VelocityActuator<
                P extends Unit<P> & DivByTime<V>, V extends Unit<V> & MulByTime<P>>
        extends PositionActuator<P> {

    protected final RangeU<V> m_velocityRange;
    protected final V m_velocityUnitPerSensorUnit;
    protected final V m_zeroV;

    protected V m_goalVelocity;

    /**
     * Creates a velocity actuator with the given parameters.
     *
     * @param unitPerSensorUnit the conversion factor in {@code P} units for every native sensor unit
     *     of the actuator's feedback sensor
     * @param startingPositionOffset the offset applied to the actuator's position after being
     *     converted to {@code P} units
     * @param positionRange the max allowed positional range of the actuator in {@code P} units after
     *     the conversion factor and offset is applied
     * @param velocityRange the max allowed velocity range of the actuator in {@code V} units after
     *     the conversion factor is applied
     */
    protected VelocityActuator(
            P unitPerSensorUnit,
            P startingPositionOffset,
            RangeU<P> positionRange,
            RangeU<V> velocityRange) {
        super(unitPerSensorUnit, startingPositionOffset, positionRange);
        m_velocityUnitPerSensorUnit = unitPerSensorUnit.div(Time.seconds(1.0));
        m_velocityRange = velocityRange;
        m_zeroV = m_velocityUnitPerSensorUnit.sub(m_velocityUnitPerSensorUnit);
        m_goalVelocity = m_zeroV;
    }

    /**
     * Commands the actuator controller to run in closed loop velocity mode. The actuator controller
     * will actively try to go to the requested velocity based on the velocity PID settings of the
     * actuator's controller.
     *
     * @param velocity_sups the velocity that the actuator controller is being commanded to
     */
    protected abstract void setHalVelocity(double velocity_sups);

    /**
     * Commands the actuator controller to run in closed loop velocity mode with an additional feed
     * forward term. The actuator controller will actively try to go to the requested velocity based
     * on the velocity PID settings of the actuator's controller. Use the forward term to add an
     * additional voltage power offset to the closed loop controller.
     *
     * @param velocity_sups the velocity that the actuator controller is being commanded to
     * @param arbitraryFeedForward the amount of additional voltage to be applied to the actuator
     *     controller
     */
    protected abstract void setHalVelocity(double velocity_sups, Voltage arbitraryFeedForward);

    /**
     * Gets the actuator's current velocity directly from the actuator's feedback sensor.
     *
     * @return the measured velocity of the actuator's feedback sensor in raw sensor units per second.
     */
    protected abstract double getHalVelocity_sups();

    /**
     * Converts the given velocity in {@code V} units to the actuator feedback sensor's native sensor
     * units.
     *
     * @param velocity the velocity in {@code V} units
     * @return the velocity in the actuator feedback sensor's native sensor units
     */
    protected double velocityToSensorUnits(V velocity) {
        return velocity.div(m_velocityUnitPerSensorUnit).asNone();
    }

    /**
     * Gets the velocity range that the actuator is allowed to use.
     *
     * @return the velocity range of the actuator
     */
    public RangeU<V> getVelocityRange() {
        return m_velocityRange;
    }

    /**
     * Commands the actuator to run in closed loop velocity mode. The actuator will actively try to go
     * to the requested velocity based on the velocity PID settings of the actuator's controller.
     *
     * @param velocity the velocity that the actuator is being commanded to
     */
    public void setVelocity(V velocity) {
        setVelocity(velocity, Voltage.ZERO);
    }

    /**
     * Commands the actuator to run in closed loop velocity mode with an additional feed forward term.
     * The actuator will actively try to go to the requested velocity based on the velocity PID
     * settings of the actuator's controller. Use the feed forward term to add an additional voltage
     * offset to the closed loop controller.
     *
     * @param velocity the velocity that the actuator is being commanded to
     * @param arbitraryFeedForward the amount of additional voltage to be applied to the actuator
     */
    public void setVelocity(V velocity, Voltage arbitraryFeedForward) {
        if (m_disabled) {
            stop();
            return;
        }

        m_goalVelocity = m_velocityRange.coerceValue(velocity);
        setHalVelocity(velocityToSensorUnits(m_goalVelocity), arbitraryFeedForward);
    }

    /**
     * Gets the velocity of the actuator.
     *
     * @return the velocity of the actuator
     */
    public V getVelocity() {
        return m_velocityUnitPerSensorUnit.mul(getHalVelocity_sups());
    }

    /**
     * The current velocity goal of the actuator in closed loop velocity control. The result is only
     * valid if the current use of the actuator is for velocity closed loop control.
     *
     * @return the current velocity goal of the closed loop velocity controller
     */
    public V getGoalVelocity() {
        return m_goalVelocity;
    }

    /**
     * Gets the error of the closed loop velocity controller. The error represents how far away the
     * actuator is from its current goal, and the direction the error is in. A positive error means
     * that a positive power needs to be applied in order to reduce the error. The result is only
     * valid if the current use of the actuator is for velocity closed loop control.
     *
     * @return the velocity error of the closed loop velocity controller
     */
    public V getVelocityError() {
        return getGoalVelocity().sub(getVelocity());
    }

    @Override
    public void stop() {
        super.stop();
        m_goalVelocity = m_zeroV;
    }
}
