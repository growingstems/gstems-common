/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.actuators;

import org.growingstems.math.RangeU;
import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.MulByTime;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Unit;

/**
 * A dummy actuator class meant to fake an actuator that user code expects to exist, but the
 * hardware might not be present. The only supported feature is that this actuator will always say
 * its at the position or velocity that you tell it to go to.
 */
public class DummyActuator<P extends Unit<P> & DivByTime<V>, V extends Unit<V> & MulByTime<P>>
        extends VelocityActuator<P, V> {
    protected Voltage m_currentlySetOutputVoltage = Voltage.ZERO;
    protected double m_currentlySetPosition_su = 0.0;
    protected double m_currentlySetVelocity_sups = 0.0;

    /**
     * Creates a basic dummy velocity actuator which can also function as a position and motor
     * actuator.
     *
     * @param posNonZeroValue a non-zero value for the {@code P} unit
     */
    public DummyActuator(P posNonZeroValue) {
        this(
                posNonZeroValue,
                posNonZeroValue,
                new RangeU<>(posNonZeroValue.sub(posNonZeroValue), posNonZeroValue.sub(posNonZeroValue)),
                new RangeU<>(
                        posNonZeroValue.sub(posNonZeroValue).div(new Time(1.0)),
                        posNonZeroValue.sub(posNonZeroValue).div(new Time(1.0))));
    }

    /**
     * Creates a dummy velocity actuator with the given parameters, which can also function as a
     * position and motor actuator.
     *
     * @param unitPerSensorUnit the conversion factor in {@code P} units for every native sensor unit
     *     of the actuator's feedback sensor
     * @param startingPositionOffset the offset applied to the actuator's position after being
     *     converted to {@code P} units
     * @param positionRange the max positional range of the actuator in {@code P} units after the
     *     conversion factor and offset is applied
     * @param velocityRange the max velocity range of the actuator in {@code V} units after the
     *     conversion factor is applied
     */
    public DummyActuator(
            P unitPerSensorUnit,
            P startingPositionOffset,
            RangeU<P> positionRange,
            RangeU<V> velocityRange) {
        super(unitPerSensorUnit, startingPositionOffset, positionRange, velocityRange);
        m_currentlySetPosition_su =
                startingPositionOffset.div(m_unitPerSensorUnit).neg().asNone();
    }

    @Override
    public void stop() {
        super.stop();
        m_currentlySetOutputVoltage = Voltage.ZERO;
        m_currentlySetVelocity_sups = 0.0;
    }

    public Voltage getCurrentlySetOutputVoltage() {
        return m_currentlySetOutputVoltage;
    }

    @Override
    protected void setHalVelocity(double velocity_sups) {
        setHalVelocity(velocity_sups, Voltage.ZERO);
    }

    @Override
    protected void setHalVelocity(double velocity_sups, Voltage arbitraryFeedForward) {
        m_currentlySetVelocity_sups = velocity_sups;
    }

    @Override
    protected double getHalVelocity_sups() {
        return m_currentlySetVelocity_sups;
    }

    @Override
    protected void setHalPosition(double position_su) {
        setHalPosition(position_su, Voltage.ZERO);
    }

    @Override
    protected void setHalPosition(double position_su, Voltage arbitraryFeedForward) {
        m_currentlySetPosition_su = position_su;
    }

    @Override
    protected double getHalPosition_su() {
        return m_currentlySetPosition_su;
    }

    @Override
    protected void setHalVoltage(Voltage voltage) {
        m_currentlySetOutputVoltage = voltage;
    }
}
