/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.actuators;

import org.growingstems.measurements.Measurements.Voltage;

/** Represents a motor based actuator that only has open loop control with no feedback. */
public abstract class MotorActuator {
    protected boolean m_disabled = false;

    /**
     * Commands the actuator controller to move at a given output voltage.
     *
     * @param outputVoltage output actuator voltage to move at
     */
    protected abstract void setHalVoltage(Voltage outputVoltage);

    /**
     * Commands the actuator to move at a given output voltage.
     *
     * @param outputVoltage output voltage to move at
     */
    public void setOutputVoltage(Voltage outputVoltage) {
        if (m_disabled) {
            stop();
            return;
        }

        setHalVoltage(outputVoltage);
    }

    /**
     * Disables the actuator, forcing the actuator to stop until {@link MotorActuator#enable} is
     * called.
     */
    public void disable() {
        stop();
        m_disabled = true;
    }

    /**
     * Enables the actuator. Does not resume the previous state of the actuator before it was
     * disabled.
     */
    public void enable() {
        m_disabled = false;
    }

    /**
     * Checks if the actuator is currently enabled.
     *
     * @return if the actuator is currently enabled
     */
    public boolean isEnabled() {
        return !m_disabled;
    }

    /** Commands the actuator to stop. */
    public void stop() {
        setHalVoltage(Voltage.ZERO);
    }
}
