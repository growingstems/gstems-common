/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import java.util.function.Supplier;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.statemachine.ExecState;
import org.growingstems.util.statemachine.InitExecExitState;
import org.growingstems.util.statemachine.StateMachine;
import org.growingstems.util.statemachine.StaticTransition;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/**
 * Apply a momentum killing algorithm to a motor input signal to help stop a physical system
 * quicker. <br>
 * This class is useful for stopping physical systems faster than simply turning off the motor in
 * question. The motor signal provided to this class will be modified on command to apply a reverse
 * power to help kill the momentum of the physical system being driven by the motor. <br>
 * This is achieved by modifying the input motor signal based on the current state of the class
 * instance. The class can exist in 3 separate states:
 *
 * <ul>
 *   <li>Idle: The provided motor input is output unmodified
 *   <li>Kill: The configured reversePower will be output instead of the input power
 *   <li>Stop: The output motor power will be 0.0 instead of the provided input
 * </ul>
 *
 * The states transition to one another in the following manners
 *
 * <ul>
 *   <li>Idle transitions to:
 *       <ul>
 *         <li>Kill when the provided start supplier returns true
 *       </ul>
 *   <li>Kill transitions to:
 *       <ul>
 *         <li>Idle when the provided stop supplier returns true
 *         <li>Stop when a configurable amount of time has passed
 *       </ul>
 *   <li>Stop transitions to:
 *       <ul>
 *         <li>Idle when the provided stop supplier returns true
 *         <li>Idle when a configurable amount of time has passed
 *       </ul>
 * </ul>
 *
 * The times to remain in the Kill and Stop state are independently configurable. <br>
 * <br>
 * Due to the fact that this class only checks time based transitions each call to
 * {@link #update(Double)}, It is incredibly probable that the exact time spent in the reverse state
 * and stop state does not match what was provided at construction. This class guarantees that the
 * amount of time spent in reverse and stop is AT LEAST the amount of time provided.<br>
 * This means that if the timing of the calls to {@link #update(Double)} cause a scenario where the
 * reverse time is exceeded by X seconds, the stop state will not have its time cut by X seconds.
 * The stop state's timing will not start until it is transitioned into.
 */
public class MomentumKiller implements SignalModifier<Double, Double> {
    private final StateMachine m_stateMachine;

    // transition information
    private final Timer m_stateTimer; // timer used for state transitions

    // kill momentum state config
    private final double m_reversePower; // power to apply while killing momentum
    private final Time m_reverseTime; // amount of time to kill momentum

    // stop state config
    private final Time m_stopTime; // amount of time spent with the motor stopped

    // state
    private double m_powerIn; // power input that needs converted
    private double m_powerOut; // return of the momentum killer

    /**
     * Construct a momentum killer
     *
     * @param reversePower power to apply while the momentum is being killed
     * @param reverseTime number of seconds to kill momentum
     * @param startCommand supplier that returns true when the momentum killer should start its
     *     process
     * @param stopTime number of seconds to have the motor stopped after momentum was killed
     * @param stopCommand supplier that returns true when the momentum killer should be stopped and
     *     returned to idle
     * @param timeSource source of time
     */
    public MomentumKiller(
            double reversePower,
            Time reverseTime,
            Supplier<Boolean> startCommand,
            Time stopTime,
            Supplier<Boolean> stopCommand,
            TimeSource timeSource) {
        m_stateTimer = timeSource.createTimer();
        m_reverseTime = reverseTime;
        m_reversePower = reversePower;
        m_stopTime = stopTime;

        // create all states needed
        ExecState idleState = new ExecState("Idle State", this::idle);
        InitExecExitState killMomentumState = new InitExecExitState(
                "Kill Momentum State", this::killMomentumInit, this::killMomentum, this::killMomentumExit);
        InitExecExitState stopState =
                new InitExecExitState("Stop State", this::stopInit, this::stop, this::stopExit);

        // idle state transition
        idleState.addTransition(new StaticTransition(killMomentumState, startCommand));

        // kill momentum state transitions
        killMomentumState.addTransition(new StaticTransition(idleState, stopCommand));
        killMomentumState.addTransition(
                new StaticTransition(stopState, () -> m_stateTimer.hasElapsed(m_reverseTime)));

        // stop state transitions
        stopState.addTransition(new StaticTransition(idleState, stopCommand));
        stopState.addTransition(
                new StaticTransition(idleState, () -> m_stateTimer.hasElapsed(m_stopTime)));

        // setup state machine
        m_stateMachine = new StateMachine(idleState);
    }

    /**
     * Modify the input signal based upon the current state of the momentum killer as described in the
     * class level documentation.
     *
     * @param powerIn input motor power to be modified
     * @return modified motor power
     */
    @Override
    public Double update(Double powerIn) {
        // update context for state machine
        m_powerIn = powerIn;

        // advance FSM
        m_stateMachine.step();

        // return state machine modified context
        return m_powerOut;
    }

    // getter functions
    public Time getReverseTime() {
        return m_reverseTime;
    }

    public double getReversePower() {
        return m_reversePower;
    }

    public Time getStopTime() {
        return m_stopTime;
    }

    // state functions
    private void idle() {
        m_powerOut = m_powerIn;
    }

    private void killMomentumInit() {
        m_stateTimer.start();
    }

    private void killMomentum() {
        m_powerOut = m_reversePower;
    }

    private void killMomentumExit() {
        m_stateTimer.stop();
        m_stateTimer.reset();
    }

    private void stopInit() {
        m_stateTimer.start();
    }

    private void stop() {
        m_powerOut = 0.0;
    }

    private void stopExit() {
        m_stateTimer.stop();
        m_stateTimer.reset();
    }
}
;
