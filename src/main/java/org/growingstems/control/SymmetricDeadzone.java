/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import org.growingstems.math.Range;
import org.growingstems.signals.api.SignalModifier;

/**
 * Provides a symmetric scaling deadzone for numeric values. <br>
 * <br>
 * The deadzone takes a range of numbers (refered to as the operating range) and provides a subrange
 * (refered to as the deadzone) where outputs are no longer based upon the input value. The input
 * value is modified in the following ways:
 *
 * <ul>
 *   <li>input value inside deadzone: the output value will be a constant value. This value is
 *       configurable
 *   <li>input value outside deadzone but inside of the operating range: the ouput is scaled and
 *       returned. The scale is calculated based on the output range and the operating range. This
 *       allows a full range of returns even though the range of allowed inputs is a subset of the
 *       full operating range.
 * </ul>
 *
 * <p>Note: While it is not technically invalid to provide inputs that are outside of the specified
 * operating range, any output calculated based on those inputs will continue to have scaling
 * applied to them.
 */
public class SymmetricDeadzone implements SignalModifier<Double, Double> {
    private final double m_deadzone;
    private final double m_centerValue;
    private final double m_radius;
    private double m_deadOutput;

    /**
     * Construct a SymmetricDeadzone
     *
     * @param centerValue value at the center of the range the deadzone operates on
     * @param radius distance between the center and endpoints of the operating range
     * @param deadzone distance between the center of the operating range and the endpoints of the
     *     deadzone
     * @param deadOutput value to output when the input is inside of the deadzone range
     */
    public SymmetricDeadzone(double centerValue, double radius, double deadzone, double deadOutput) {
        m_centerValue = centerValue;
        m_radius = radius;
        m_deadzone = deadzone;
        m_deadOutput = deadOutput;
    }

    /**
     * Construct a SymmetricDeadzone defaults deadOuptut to be the same as centerValue
     *
     * @param centerValue value at the center of the range the deadzone operates on
     * @param radius distance between the center and endpoints of the operating range
     * @param deadzone distance between the center of the operating range and the endpoints of the
     *     deadzone
     */
    public SymmetricDeadzone(double centerValue, double radius, double deadzone) {
        this(centerValue, radius, deadzone, centerValue);
    }

    /**
     * Construct a SymmetricDeadzone defaults centerValue to 0.0 defaults deadOutput to 0.0
     *
     * @param radius distance between the center and endpoints of the operating range
     * @param deadzone distance between the center of the operating range and the endpoints of the
     *     deadzone
     */
    public SymmetricDeadzone(double radius, double deadzone) {
        this(0.0, radius, deadzone);
    }

    /**
     * Construct a SymmetricDeadzone
     *
     * @param range Enpoints of the operating range of the deadzone
     * @param deadzone distance between the center of the operating range and the endpoints of the
     *     deadzone
     * @param deadOutput value to output when the input is inside of the deadzone range
     */
    public SymmetricDeadzone(Range range, double deadzone, double deadOutput) {
        this(
                ((range.getHigh() - range.getLow()) / 2.0) + range.getLow(),
                (range.getHigh() - range.getLow()) / 2.0,
                deadzone,
                deadOutput);
    }

    /**
     * Construct a SymmetricDeadzone defaults deadOutput to the center value of range
     *
     * @param range Enpoints of the operating range of the deadzone
     * @param deadzone distance between the center of the operating range and the endpoints of the
     *     deadzone
     */
    public SymmetricDeadzone(Range range, double deadzone) {
        this(
                ((range.getHigh() - range.getLow()) / 2.0) + range.getLow(),
                (range.getHigh() - range.getLow()) / 2.0,
                deadzone);
    }

    public double getDeadzone() {
        return m_deadzone;
    }

    public double getCenterValue() {
        return m_centerValue;
    }

    public double getRadius() {
        return m_radius;
    }

    public double getDeadOutput() {
        return m_deadOutput;
    }

    public void setDeadOutput(double newDeadOutput) {
        m_deadOutput = newDeadOutput;
    }

    /**
     * Calculate the new output value based on the deadzone configuration.
     *
     * <p>Note: While it is not technically invalid to provide inputs that are outside of the
     * specified operating range, any output calculated based on those inputs will continue to have
     * scaling applied to them.
     *
     * @param input the input value to the deadzone
     * @return output value after deadzone is applied
     */
    @Override
    public Double update(Double input) {
        // if value is within deadzone, provide output value
        if (Math.abs(m_centerValue - input) < m_deadzone) {
            return m_deadOutput;
        }

        // operate on one half of the range
        double oldMin = m_centerValue + m_deadzone; // 0.0 + 0.0 => 0.0
        double oldMax = m_centerValue + m_radius; // 0.0 + 1.0 -> 1.0
        double oldRange = oldMax - oldMin; // 1.0 - 0.0 -> 0.0

        double newMin = m_centerValue; // 0.0
        double newMax = m_centerValue + m_radius; // 0.0 + 1.0 -> 1.0
        double newRange = newMax - newMin; // 1.0 - 0.0 -> 1.0

        double inputVal = input;
        if (input < m_centerValue) {
            inputVal = m_centerValue + (m_centerValue - inputVal);
        }

        double newVal = (((inputVal - oldMin) * newRange) / oldRange) + newMin;

        // determine if input was in bottom half of range
        if (input < m_centerValue) {
            return m_centerValue - (newVal - m_centerValue);
        } else {
            return newVal;
        }
    }
}
