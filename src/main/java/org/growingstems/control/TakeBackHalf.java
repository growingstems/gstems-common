/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import org.growingstems.signals.api.SignalModifier;

/**
 * Uses an algorithm to generate an output based on the error of the given target and current
 * values. The gain is used to dampen the error. If the error has the same sign as the previous
 * iteration, the output is the error times gain, plus previous output. If the signage changed, then
 * the average of (error * gain) + previous and a stored value is gotten. The stored value and
 * output then both become that average.
 */
public class TakeBackHalf implements SignalModifier<Double, Double> {

    private double m_gain;
    private double m_min;
    private double m_max;
    private double m_previousError;
    private double m_h0Last;
    private double m_out;
    private double m_setPoint;

    /**
     * Sets gain as 0.001, min as 0.0, and max as 1.0.
     *
     * @param setpoint the desired end point
     */
    public TakeBackHalf(double setpoint) {
        this(setpoint, 0.001, 0.0, 1.0);
    }

    /**
     * Allows user to configure gain.
     *
     * @param setpoint the desired end point
     * @param gain the value by which to multiply the error by.
     * @param min the minimum output value.
     * @param max the maximum output value.
     */
    public TakeBackHalf(double setpoint, double gain, double min, double max) {
        setGain(gain);
        setMin(min);
        setMax(max);
        setSetpoint(setpoint);
        reset();
    }

    public void setGain(double gain) {
        m_gain = gain;
    }

    public void setMin(double min) {
        m_min = min;
    }

    public void setMax(double max) {
        m_max = max;
    }

    public void setSetpoint(double setpoint) {
        m_setPoint = setpoint;
    }

    public double getGain() {
        return m_gain;
    }

    public double getMin() {
        return m_min;
    }

    public double getMax() {
        return m_max;
    }

    public double getSetpoint() {
        return m_setPoint;
    }

    /** Resets the stored variables to 0. */
    public void reset() {
        m_h0Last = 0;
        m_out = 0;
        m_previousError = 0;
    }

    /**
     * Gives an output based on the difference between the current and target values. If the
     * difference changes from positive to negative, provies a different output. More details found in
     * class level doc
     *
     * @param processValue the current value, must be in the same units as setpoint
     * @return the output based on the difference of the processValue and setPoint
     */
    @Override
    public Double update(Double processValue) {
        double error = m_setPoint - processValue;

        m_out += m_gain * error;

        if ((m_previousError >= 0) != (error >= 0)) {
            m_h0Last = (m_out + m_h0Last) / 2;
            m_out = m_h0Last;
        }

        m_out = Math.max(m_min, Math.min(m_max, m_out));
        m_previousError = error;
        return m_out;
    }
}
