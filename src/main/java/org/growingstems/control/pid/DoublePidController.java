/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.pid;

import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;

public class DoublePidController extends PidController<Double> {
    /**
     * Constructs a new Double PID controller with a setpoint of 0.0
     *
     * @param p Proportional gain value
     * @param i Integral gain value
     * @param d Derivative gain value
     * @param timeSource Time source used for time calculations internal to the PID controller.
     */
    public DoublePidController(double p, Frequency i, Time d, TimeSource timeSource) {
        super(p, i, d, timeSource, 0.0);
    }

    @Override
    protected double calculateError(Double input) {
        return m_setpoint - input;
    }

    @Override
    protected double getValueToDerive(Double input) {
        return input;
    }
}
