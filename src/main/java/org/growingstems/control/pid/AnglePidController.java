/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.pid;

import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;

/**
 * A {@link PidController} specialized for wrapping {@link Angle}s. <br>
 * <br>
 * This PID Controller will guide the input towards a desired wrapping angle. This means that it
 * does not matter how many times the input angle or setpoint angle have wrapped. For example, an
 * input angle of +5.0 revolutions and a setpoint of -2.1 revolutions will have the same result as
 * an angle of 0.0 revolutions and a setpoint of -0.1 revolutions.
 *
 * <p>The error used in the proportional and integral controllers is calculated using
 * {@link Angle#difference(Angle)}. This means that the input will be guided towards the closest
 * equivalent wrapped angle to the setpoint.
 *
 * <p>The derivative is calculated based on the wound input. This means that changing the setpoint
 * does not affect the derivative controller, and that all inputs are treated as if they are within
 * 180 degrees of the previous input. For example, if updated with an input of +170 degrees the
 * previous frame, and -170 degrees the current frame, the derivative will be calculated based on a
 * +20 degree delta, as opposed to a -340 degree delta. If the current frame were instead provided
 * an input of +190 degrees, the same result would occur (from all three controllers).
 */
public class AnglePidController extends PidController<Angle> {
    /**
     * Angle Unit the controller is based on. <br>
     * Calculations are done in this unit. For example, an angle unit of
     * {@link org.growingstems.measurements.Measurements.AngleImpl.Unit#DEGREES} means that the
     * proportional gain is in units of "per degree".
     */
    protected final Angle.Unit m_angleUnit;
    /**
     * Last input provided to the deriver. <br>
     * This value is needed to shift the input to be within 180 degrees of the previous input.
     */
    protected Angle m_lastDeriverInput = null;

    /**
     * Constructs a new Angle PID controller with a setpoint of 0.0. <br>
     * The PID constants are multiplied by the error in the unit specified by angleUnit
     *
     * @param p Proportional gain value
     * @param i Integral gain value
     * @param d Derivative gain value
     * @param angleUnit The unit to use for PID calculations.
     * @param timeSource Time source used for time calculations internal to the PID controller.
     */
    public AnglePidController(
            double p, Frequency i, Time d, Angle.Unit angleUnit, TimeSource timeSource) {
        super(p, i, d, timeSource, Angle.ZERO);
        m_angleUnit = angleUnit;
    }

    /**
     * Calculate the error for the PID calculation. <br>
     * <br>
     * Calculated using {@link Angle#difference(Angle)}.
     *
     * @param input The input signal
     * @return Error for PID calculations, in the unit specified at construction
     */
    @Override
    protected double calculateError(Angle input) {
        return m_setpoint.difference(input).getValue(m_angleUnit);
    }

    /**
     * Calculates the value to use in the derivative controller. <br>
     * <br>
     * The input is forced on the scope of the previous angle. This avoids spikes in the derivative
     * controller if the angle switches scope. For example, if the angle is +170 degrees, then -170
     * degrees, this will treat the change as +20 degrees instead of -340 degrees.
     *
     * @param input The input signal
     * @return Value of the input to derive, in the unit specified at construction
     */
    @Override
    protected double getValueToDerive(Angle input) {
        if (m_lastDeriverInput != null) {
            // TODO: Remove ".difference(Angle.ZERO)" when shiftWrappedAngleScope supports wider inputs
            input = input.difference(Angle.ZERO).shiftWrappedAngleScope(m_lastDeriverInput);
        }
        m_lastDeriverInput = input;
        return input.getValue(m_angleUnit);
    }
}
