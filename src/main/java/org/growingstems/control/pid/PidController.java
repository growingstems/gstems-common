/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.pid;

import org.growingstems.math.RateCalculator;
import org.growingstems.math.TimedIntegrator;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;

/**
 * Provides functionality for the PID control loop algorithm.<br>
 * The PID gains use {@link Unit}s, where applicable.<br>
 * The input unit "I" and output unit "O" currently always unitless. The error is in the same units
 * as the input: I.<br>
 * <br>
 * The Proportional gain is multiplied by the error to get the output. Therefore, the Proportional
 * gain's unit is O/I (currently, unitless)<br>
 * The Integral gain is multiplied by the error and the change in Time (t) to get the output.
 * Therefore, the Integral gain's unit is O/tI (currently, per-unit Time, i.e. Frequency)<br>
 * The Derivative gain is multiplied by the error and divided by the change in Time (t) to get the
 * output. Therefore, the Derivative gain's unit is Ot/I (currently, Time)<br>
 * <br>
 * For an example, if using an Angle as an input, and outputting a Voltage, "I" represents an Angle,
 * and "O" represents a Voltage. The Proportional gain is then Voltage-per-Angle, i.e. "The Voltage
 * to apply for the Angular Error". The Integral gain is Voltage-per-Time-Angle, i.e. "The Voltage
 * to apply for the Angular Error per unit Time". The Derivative gain is Voltage-Time-per-Angle, or
 * equivalently Voltage / (Angle/Time), i.e. "The Voltage to apply for the Time-rate of change of
 * the Angle". To make the Proportional controller output 10 Volts for an error of 100 degrees, the
 * Proportional gain should be 0.1 Volts per Degree. Since the input is currently unitless, you'd
 * only specify "0.1". Note that this requires all further inputs to be in degrees, and that you
 * treat the output as Volts. To make the Integral controller output 10 Volts for an error of 20
 * degrees that lasts 4 seconds, the Integral gain should be 10V / (20 deg * 4 s) = 0.125 V/deg-s.
 * The Integral gain provided to the PID Controller would then be 0.125/s, i.e. 0.125 Hz, since
 * volts and degrees are assumed. Lastly, to make the Derivative controller output 5V for an input
 * that is changing at a rate of 20 degrees per second, the Derivative gain must be set to 5V / (20
 * deg/s) = 0.25 V/(deg/s), i.e. 0.25 V-s/deg. The Derivative gain provided to the PID controller
 * would then be 0.25s, since Volts and Degrees are assumed.
 *
 * @see <a href="https://en.wikipedia.org/wiki/PID_controller">PID Controller Wiki Article</a>
 */
public abstract class PidController<U> implements SignalModifier<U, Double> {
    /** Proportional Controller Gain */
    protected double m_pGain;
    /** Integral Controller Gain */
    protected Frequency m_iGain;
    /** Derivative Controller Gain */
    protected Time m_dGain;
    /** Current Controller Setpoint */
    protected U m_setpoint;

    /** Integrator used for the Integral Controller */
    protected TimedIntegrator m_integrator;
    /** Deriver used for the Derivative Controller */
    protected RateCalculator m_deriver;

    /**
     * Constructs a new PID controller
     *
     * @param p Proportional gain value
     * @param i Integral gain value
     * @param d Derivative gain value
     * @param timeSource Time source used for time calculations internal to the PID controller.
     * @param initialSetpoint The setpoint to use at the start
     */
    protected PidController(double p, Frequency i, Time d, TimeSource timeSource, U initialSetpoint) {
        setGains(p, i, d);
        setSetpoint(initialSetpoint);

        m_integrator = new TimedIntegrator(timeSource);
        m_deriver = new RateCalculator(timeSource);

        reset();
    }

    /** Resets timer and Integral buildup inside the PID controller */
    public void reset() {
        resetIntegral();
        m_deriver.reset();
    }

    /** Resets Integral buildup inside the PID controller */
    public void resetIntegral() {
        m_integrator.reset();
    }

    public void setSetpoint(U setpoint) {
        m_setpoint = setpoint;
    }

    public void setPGain(double p) {
        m_pGain = p;
    }

    public void setIGain(Frequency i) {
        m_iGain = i;
    }

    public void setDGain(Time d) {
        m_dGain = d;
    }

    public U getSetpoint() {
        return m_setpoint;
    }

    public double getPGain() {
        return m_pGain;
    }

    public Frequency getIGain() {
        return m_iGain;
    }

    public Time getDGain() {
        return m_dGain;
    }

    /**
     * Sets all PID gains at once
     *
     * @param p - Proportional gain value
     * @param i - Integral gain value
     * @param d - Derivative gain value
     */
    public void setGains(double p, Frequency i, Time d) {
        setPGain(p);
        setIGain(i);
        setDGain(d);
    }

    /**
     * Returns output value from PID control algorithm
     *
     * @param input The measured process variable
     */
    @Override
    public Double update(U input) {
        double error = calculateError(input);

        double proportional = error * m_pGain;
        // m_iGain.getBaseValue is in Hertz, so the resulting "Time" has really been
        // converted
        // to a Unitless underneath. Getting the base value is then equivalent to
        // calling "asNone".
        // This is done inside of `m_integrator.update()` to avoid a jump in PID Output
        // if the iGain
        // is modified.
        double integral = m_integrator.update(error * m_iGain.getBaseValue()).getBaseValue();
        double derivative = m_deriver.update(-getValueToDerive(input)).mul(m_dGain).asNone();

        return proportional + integral + derivative;
    }

    /**
     * Calculate the error for the PID calculation.<br>
     * <br>
     * Generally, this value should be equal to the setpoint minus the input.<br>
     * This needs to be in the same unit as {@link PidController#getValueToDerive(Object)}<br>
     *
     * @param input The input signal
     * @return Error for PID calculations
     */
    protected abstract double calculateError(U input);

    /**
     * Calculate the value to use in the Derivative controller. <br>
     * <br>
     * Generally, this should just be the value of the input, as a double.<br>
     * This needs to be in the same unit as {@link PidController#calculateError(Object)}<br>
     *
     * @param input The input signal
     * @return Value of the input to derive
     */
    protected abstract double getValueToDerive(U input);
}
