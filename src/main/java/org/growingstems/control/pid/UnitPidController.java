/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.pid;

import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Unit;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimeSource;

public class UnitPidController<U extends Unit<U>> extends PidController<U> {
    /** Function to convert the {@link Unit} to a double consumable by the {@link PidController} */
    protected SignalModifier<U, Double> m_toDouble;

    /**
     * Constructs a new Unit-based PID controller.<br>
     * The {@code toDouble} input defines which unit the PID Controller will operate in.
     *
     * @param p Proportional gain value
     * @param i Integral gain value
     * @param d Derivative gain value
     * @param timeSource Time source used for time calculations internal to the PID controller.
     * @param initialSetpoint The setpoint to use at the start
     * @param toDouble A function that will convert the Unit to a `Double`.
     */
    public UnitPidController(
            double p,
            Frequency i,
            Time d,
            TimeSource timeSource,
            U initialSetpoint,
            SignalModifier<U, Double> toDouble) {
        super(p, i, d, timeSource, initialSetpoint);
        m_toDouble = toDouble;
    }

    @Override
    protected double calculateError(U input) {
        return m_toDouble.update(m_setpoint.sub(input));
    }

    @Override
    protected double getValueToDerive(U input) {
        return m_toDouble.update(input);
    }
}
