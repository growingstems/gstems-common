/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.Timer;
import org.junit.jupiter.api.Test;

public class TestTimeSource_Test {
    private static final Time k_0s = Time.ZERO;
    private static final Time k_1s = Time.seconds(1.0);
    private static final Time k_2s = Time.seconds(2.0);
    private static final Time k_3s = Time.seconds(3.0);

    @Test
    public void construction() {
        TestTimeSource timeSource = new TestTimeSource();
        Timer timer = timeSource.createTimer();
        assertEquals(k_0s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_0s.getBaseValue(), timeSource.clockTime().getBaseValue());
    }

    @Test
    public void constructionWithTime() {
        TestTimeSource timeSource = new TestTimeSource(k_1s);
        Timer timer = timeSource.createTimer();

        assertEquals(k_1s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_1s.getBaseValue(), timeSource.clockTime().getBaseValue());
    }

    @Test
    public void setTime() {
        TestTimeSource timeSource = new TestTimeSource(k_1s);
        Timer timer = timeSource.createTimer();

        assertEquals(k_1s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_1s.getBaseValue(), timeSource.clockTime().getBaseValue());

        timeSource.setTime(k_2s);
        assertEquals(k_2s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_2s.getBaseValue(), timeSource.clockTime().getBaseValue());

        timer.start();
        assertEquals(k_2s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_2s.getBaseValue(), timeSource.clockTime().getBaseValue());

        timeSource.setTime(k_3s);
        assertEquals(k_3s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_1s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_3s.getBaseValue(), timeSource.clockTime().getBaseValue());
    }

    @Test
    public void addTime() {
        TestTimeSource timeSource = new TestTimeSource(k_1s);
        Timer timer = timeSource.createTimer();

        assertEquals(k_1s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_1s.getBaseValue(), timeSource.clockTime().getBaseValue());

        timeSource.addTime(k_1s);
        assertEquals(k_2s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_2s.getBaseValue(), timeSource.clockTime().getBaseValue());

        timer.start();
        assertEquals(k_2s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_0s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_2s.getBaseValue(), timeSource.clockTime().getBaseValue());

        timeSource.addTime(k_1s);
        assertEquals(k_3s.getBaseValue(), timeSource.m_time.getBaseValue());
        assertEquals(k_1s.getBaseValue(), timer.get().getBaseValue());
        assertEquals(k_3s.getBaseValue(), timeSource.clockTime().getBaseValue());
    }
}
