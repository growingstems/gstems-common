/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.PrintStreamTest;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class PulseStretchDelay_Test extends PrintStreamTest {
    private TestTimeSource m_timeSource = new TestTimeSource();
    PulseStretchDelay m_psd;

    @Test
    public void defaultConstructorTest() {
        m_psd = new PulseStretchDelay(Time.seconds(1.0), Time.ZERO, Edge.RISING, m_timeSource, false);
        assertFalse(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.5));
        assertTrue(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(0.5));
        assertTrue(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(0.5));
        assertFalse(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.5));
        assertFalse(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.5));
        assertFalse(m_psd.update(false));
        assertNoError();
    }

    @Test
    public void fullConstructorTest() {
        m_psd = new PulseStretchDelay(
                Time.seconds(1.0), Time.seconds(0.5), Edge.RISING, m_timeSource, false);

        assertFalse(m_psd.update(false));
        assertFalse(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(0.25));
        assertFalse(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.25));
        assertTrue(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.25));
        assertTrue(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.25));
        assertTrue(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.25));
        assertTrue(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(0.25));
        assertFalse(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(0.25));
        assertFalse(m_psd.update(false));
        assertNoError();
    }

    @Test
    public void resetTest() {
        m_psd = new PulseStretchDelay(
                Time.seconds(1.5), Time.seconds(1.0), Edge.FALLING, m_timeSource, false);
        assertFalse(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(1.5));
        assertFalse(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(1.5));
        assertFalse(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(1.5));
        assertTrue(m_psd.update(true));
        m_psd.reset(false);
        assertFalse(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(1.5));
        assertFalse(m_psd.update(true));
        m_timeSource.addTime(Time.seconds(1.5));
        assertFalse(m_psd.update(false));
        m_timeSource.addTime(Time.seconds(1.5));
        assertTrue(m_psd.update(true));
        assertNoError();
    }

    @Test
    public void invalidEdgesTest() {
        m_psd =
                new PulseStretchDelay(Time.seconds(2.0), Time.seconds(1.5), Edge.ANY, m_timeSource, false);
        assertErrorOccurred();
        assertEquals(m_psd.getEdge(), Edge.RISING);

        m_psd =
                new PulseStretchDelay(Time.seconds(2.0), Time.seconds(1.5), Edge.NONE, m_timeSource, false);
        assertErrorOccurred();
        assertEquals(m_psd.getEdge(), Edge.RISING);
    }
}
