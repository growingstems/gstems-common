/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.test.PrintStreamTest;
import org.junit.jupiter.api.Test;

public class UpDownCounter_Test extends PrintStreamTest {
    UpDownCounter m_udc;

    // Tests going up by 3.
    @Test
    public void upTest() {
        m_udc = new UpDownCounter();
        assertEquals(0, m_udc.count(false, false));
        assertEquals(1, m_udc.count(true, false));
        assertEquals(1, m_udc.count(false, false));
        assertEquals(2, m_udc.count(true, false));
        assertEquals(2, m_udc.count(false, false));
        assertEquals(3, m_udc.count(true, false));
        assertEquals(3, m_udc.count(true, false));
    }

    // Tests going down by 3.
    @Test
    public void downTest() {
        m_udc = new UpDownCounter();
        assertEquals(0, m_udc.count(false, false));
        assertEquals(-1, m_udc.count(false, true));
        assertEquals(-1, m_udc.count(false, false));
        assertEquals(-2, m_udc.count(false, true));
        assertEquals(-2, m_udc.count(false, false));
        assertEquals(-3, m_udc.count(false, true));
        assertEquals(-3, m_udc.count(false, false));
    }

    // Tests the 'locking' mechanism for when both inputs become true at the same time.
    @Test
    public void lockTest() {
        m_udc = new UpDownCounter();
        assertEquals(0, m_udc.count(false, false));
        assertEquals(0, m_udc.count(true, true));
        assertEquals(0, m_udc.count(false, false));
        assertEquals(1, m_udc.count(true, false));
        assertEquals(0, m_udc.count(false, true));
        assertEquals(0, m_udc.count(false, false));
    }

    // Tests the reset and setInitial methods.
    @Test
    public void resetToInitalTest() {
        m_udc = new UpDownCounter();
        m_udc.setInitial(5);
        assertEquals(0, m_udc.count(false, false));
        assertEquals(-1, m_udc.count(false, true));
        assertEquals(-1, m_udc.count(false, false));
        m_udc.reset();
        assertEquals(5, m_udc.count(false, false));
    }

    // Tests the alternate constructor
    @Test
    public void constructorTest() {
        m_udc = new UpDownCounter(5);
        assertEquals(5, m_udc.count(false, false));
        assertEquals(6, m_udc.count(true, false));
        assertEquals(6, m_udc.count(false, false));
        assertEquals(7, m_udc.count(true, false));
        assertEquals(7, m_udc.count(false, false));
        assertEquals(8, m_udc.count(true, false));
        assertEquals(8, m_udc.count(false, false));
    }

    // Tests that the current count is unaffected by a new inital count
    @Test
    public void SetInitalIsolatedTest() {
        m_udc = new UpDownCounter(5);
        assertEquals(5, m_udc.count(false, false));
        assertEquals(6, m_udc.count(true, false));
        assertEquals(6, m_udc.count(false, false));
        assertEquals(7, m_udc.count(true, false));
        assertEquals(7, m_udc.count(false, false));
        assertEquals(8, m_udc.count(true, false));
        m_udc.setInitial(2);
        assertEquals(8, m_udc.count(false, false));
    }

    // Tests that the getInital method works.
    @Test
    public void getinitalTest() {
        m_udc = new UpDownCounter(4);
        assertEquals(4, m_udc.count(false, false));
        assertEquals(5, m_udc.count(true, false));
        m_udc.setInitial(3);
        assertEquals(3, m_udc.getInitial());
    }

    // Tests that the full constructor works
    @Test
    public void fullConstructorTest() {
        m_udc = new UpDownCounter(4, Edge.RISING, Edge.FALLING);
        assertEquals(4, m_udc.count(false, false));
        assertEquals(5, m_udc.count(true, true));
        assertEquals(5, m_udc.count(false, true));
        assertEquals(4, m_udc.count(false, false));
    }

    @Test
    public void invalidEdgesTest() {
        // Testing single edge set constuctor
        m_udc = new UpDownCounter(4, Edge.ANY);
        assertErrorOccurred();
        assertEquals(m_udc.getUpEdge(), Edge.RISING);
        assertEquals(m_udc.getDownEdge(), Edge.RISING);

        m_udc = new UpDownCounter(4, Edge.NONE);
        assertErrorOccurred();
        assertEquals(m_udc.getUpEdge(), Edge.RISING);
        assertEquals(m_udc.getDownEdge(), Edge.RISING);

        // Testing up edge set constuctor
        m_udc = new UpDownCounter(4, Edge.ANY, Edge.FALLING);
        assertErrorOccurred();
        assertEquals(m_udc.getUpEdge(), Edge.RISING);
        assertEquals(m_udc.getDownEdge(), Edge.FALLING);

        m_udc = new UpDownCounter(4, Edge.NONE, Edge.FALLING);
        assertErrorOccurred();
        assertEquals(m_udc.getUpEdge(), Edge.RISING);
        assertEquals(m_udc.getDownEdge(), Edge.FALLING);

        // Testing down edge set constuctor
        m_udc = new UpDownCounter(4, Edge.FALLING, Edge.ANY);
        assertErrorOccurred();
        assertEquals(m_udc.getUpEdge(), Edge.FALLING);
        assertEquals(m_udc.getDownEdge(), Edge.RISING);

        m_udc = new UpDownCounter(4, Edge.FALLING, Edge.NONE);
        assertErrorOccurred();
        assertEquals(m_udc.getUpEdge(), Edge.FALLING);
        assertEquals(m_udc.getDownEdge(), Edge.RISING);
    }
}
