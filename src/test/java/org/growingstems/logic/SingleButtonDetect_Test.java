/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.junit.jupiter.api.Test;

public class SingleButtonDetect_Test {
    @Test
    public void trueTest() {
        SingleButtonDetect sbd = new SingleButtonDetect();
        ArrayList<Boolean> array = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            array.add(false);
        }
        assertEquals(Integer.valueOf(-1), sbd.update(array));
        array.set(2, true);
        assertEquals(Integer.valueOf(2), sbd.update(array));
        array.set(0, true);
        assertEquals(Integer.valueOf(0), sbd.update(array));
        array.set(2, false);
        assertEquals(Integer.valueOf(0), sbd.update(array));
        array.set(0, false);
        assertEquals(Integer.valueOf(0), sbd.update(array));
        array.set(0, true);
        assertEquals(Integer.valueOf(-1), sbd.update(array));
    }

    // Find proper syntax for this DimensionMismatchException.class
    @Test
    public void exceptionTest() {
        SingleButtonDetect sbd = new SingleButtonDetect();
        ArrayList<Boolean> array = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            array.add(false);
        }
        assertEquals(Integer.valueOf(-1), sbd.update(array));
        array.add(false);
        assertThrows(DimensionMismatchException.class, () -> sbd.update(array));
    }
}
