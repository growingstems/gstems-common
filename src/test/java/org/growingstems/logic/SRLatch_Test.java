/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.logic.SRLatch.SRLatchType;
import org.junit.jupiter.api.Test;

public class SRLatch_Test {
    SRLatch m_latch = null;

    @Test
    public void defaultConstructor() {
        m_latch = new SRLatch();
        assertFalse(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
    }

    @Test
    public void initialStateConstructor() {
        m_latch = new SRLatch(false);
        assertFalse(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch

        m_latch = new SRLatch(true);
        assertTrue(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
    }

    @Test
    public void priorityResetConstructor() {
        // Priority Reset
        m_latch = new SRLatch(false, SRLatchType.PRIORITIZE_RESET);
        assertFalse(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch

        m_latch = new SRLatch(true, SRLatchType.PRIORITIZE_RESET);
        assertTrue(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
    }

    @Test
    public void prioritySetConstructor() {
        // Priority Set
        m_latch = new SRLatch(false, SRLatchType.PRIORITIZE_SET);
        assertFalse(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch

        m_latch = new SRLatch(true, SRLatchType.PRIORITIZE_SET);
        assertTrue(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
    }

    @Test
    public void noChangeConstructor() {
        // No Change
        m_latch = new SRLatch(false, SRLatchType.NO_CHANGE);
        assertFalse(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch

        m_latch = new SRLatch(true, SRLatchType.NO_CHANGE);
        assertTrue(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
    }

    @Test
    public void toggleConstructor() {
        // Toggle
        m_latch = new SRLatch(false, SRLatchType.TOGGLE);
        assertFalse(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch

        // Verify Toggling
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch

        m_latch = new SRLatch(true, SRLatchType.TOGGLE);
        assertTrue(m_latch.update(false, false)); // Initial do nothing
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, false)); // Set
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(false, true)); // Reset
        assertFalse(m_latch.update(false, false)); // Latch

        // Verify Toggling
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
        assertTrue(m_latch.update(true, true)); // Test Both Set and Reset
        assertTrue(m_latch.update(false, false)); // Latch
        assertFalse(m_latch.update(true, true)); // Test Both Set and Reset
        assertFalse(m_latch.update(false, false)); // Latch
    }
}
