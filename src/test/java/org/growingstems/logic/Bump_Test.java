/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class Bump_Test {
    // Tests bumping up
    @Test
    public void testBumpsExp2() {
        int bumps = 0;
        BumpCounter bumpCounter = new BumpCounter();
        bumpCounter.getBumps(0, 0, false, 1);
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(0, 0, false, 1);
        bumpCounter.getBumps(1, 0, false, 1);
        bumps = bumpCounter.getBumps(2, 0, false, 1).m_bumps;
        assertEquals(2, bumps);
    }

    // tests that the first up isn't counted, and that multiple ups with no neutrals in between are
    // counted once.
    @Test
    public void testBumpsExp1() {
        int bumps = 0;
        BumpCounter bumpCounter = new BumpCounter();
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(0, 0, false, 1);
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(1, 0, false, 1);
        bumps = bumpCounter.getBumps(1, 0, false, 1).m_bumps;
        assertEquals(1, bumps);
    }

    // Tests ups and downs
    @Test
    public void testBumpsExpNeg1() {
        int bumps = 0;
        BumpCounter bumpCounter = new BumpCounter();
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(0, 0, false, 1);
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(-1, 0, false, 1);
        bumpCounter.getBumps(0, 0, false, 1);
        bumpCounter.getBumps(-1, 0, false, 1);
        bumps = bumpCounter.getBumps(0, 0, false, 1).m_bumps;
        assertEquals(-1, bumps);
    }

    // Tests reset functionality
    @Test
    public void testBumpsReset() {
        int bumps = 0;
        BumpCounter bumpCounter = new BumpCounter();
        for (int i = 0; i < 5; i++) {
            bumpCounter.getBumps(0, 0, false, 1);
            bumpCounter.getBumps(1, 0, false, 1);
        }
        assertEquals(0, bumpCounter.getBumps(-1, 0, true, 1).m_bumps);
        bumps = bumpCounter.getBumps(0, 0, false, 1).m_bumps;
        assertEquals(0, bumps);
    }

    // Tests that ref applies to the quanta'd output even if there are 0 bumps.
    @Test
    public void testOutResetwRef() {
        double out = 0;
        BumpCounter bumpCounter = new BumpCounter();
        for (int i = 0; i < 5; i++) {
            bumpCounter.getBumps(0, 0, false, 1);
            bumpCounter.getBumps(1, 0, false, 1);
        }
        bumpCounter.getBumps(-1, 0, true, 1);
        out = bumpCounter.getBumps(0, 5, false, 1).m_out;
        assertEquals(5, out, 0.0);
    }

    // Tests that quanta works
    @Test
    public void testOut2dot5() {
        double out = 0;
        BumpCounter bumpCounter = new BumpCounter();
        bumpCounter.getBumps(0, 0, false, 0.5);
        for (int i = 0; i < 6; i++) {
            bumpCounter.getBumps(1, 0, false, 5);
            bumpCounter.getBumps(0, 0, false, -5);
        }
        bumpCounter.getBumps(-1, 0, false, 0);
        bumpCounter.getBumps(0, 0, false, 0.75);

        out = bumpCounter.getBumps(0, 0, false, 0.5).m_out;
        assertEquals(2.5, out, 0.0);
    }

    // Tests that ref works with a quanta
    @Test
    public void testOutwRef5dot5() {
        double out = 0;
        BumpCounter bumpCounter = new BumpCounter();
        bumpCounter.getBumps(0, 0, false, 0.5);
        for (int i = 0; i < 6; i++) {
            bumpCounter.getBumps(1, 0, false, 5);
            bumpCounter.getBumps(0, 0, false, -5);
        }
        bumpCounter.getBumps(-1, 0, false, 0);
        bumpCounter.getBumps(0, 0, false, 0.75);
        out = bumpCounter.getBumps(0, 3, false, 0.5).m_out;
        assertEquals(5.5, out, 0.0);
    }

    // Tests that using alternate activation points work properly
    @Test
    public void testAltActivationPoint() {
        BumpCounter bumpCounter = new BumpCounter();
        bumpCounter.getBumps(0, 0, false, 0.5);
        bumpCounter.getBumps(1, 0, false, 1, 1);
        bumpCounter.getBumps(0, 0, false, 0.5);
        bumpCounter.getBumps(1, 0, false, 1, 0.5);
        assertEquals(1, bumpCounter.getBumpCount());
    }

    // Tests that bumps will not be counted until something changes if the first input is not neutral.
    @Test
    public void testInital() {
        BumpCounter bumpCounter = new BumpCounter();
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(1, 0, false, 1);
        bumpCounter.getBumps(0, 0, false, 1);
        assertEquals(1, bumpCounter.getBumps(1, 0, false, 1).m_bumps);
    }
}
