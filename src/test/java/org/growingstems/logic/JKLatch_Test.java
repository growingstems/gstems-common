/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.logic;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.logic.LogicCore.Edge;
import org.junit.jupiter.api.Test;

public class JKLatch_Test {
    @Test
    public void doubleRisingLatchTest() {
        JKLatch latch = new JKLatch(false, Edge.RISING, Edge.RISING);

        assertFalse(latch.step(false, false));
        assertTrue(latch.step(true, false));
        assertTrue(latch.step(false, false));
        assertFalse(latch.step(false, true));
        assertFalse(latch.step(false, false));
        assertTrue(latch.step(true, false));
        assertFalse(latch.step(true, true));
        assertFalse(latch.step(true, false));
        assertFalse(latch.step(false, false));
        assertTrue(latch.step(true, true));
        assertTrue(latch.step(false, false));
        assertFalse(latch.step(true, true));
        assertFalse(latch.step(false, false));
    }

    @Test
    public void doubleFallingLatchTest() {
        JKLatch latch = new JKLatch(false, Edge.FALLING);

        assertFalse(latch.step(false, false));
        assertFalse(latch.step(true, false));
        assertTrue(latch.step(false, false));
        assertTrue(latch.step(false, true));
        assertFalse(latch.step(false, false));
        assertFalse(latch.step(true, false));
        assertFalse(latch.step(true, true));
        assertFalse(latch.step(true, false));
        assertTrue(latch.step(false, false));
        assertTrue(latch.step(true, true));
        assertFalse(latch.step(false, false));
        assertFalse(latch.step(true, true));
        assertTrue(latch.step(false, false));
    }

    @Test
    public void risingFallingLatchTest() {
        JKLatch latch = new JKLatch(true, Edge.RISING, Edge.FALLING);

        assertTrue(latch.step(false, false));
        assertTrue(latch.step(true, false));
        assertTrue(latch.step(false, false));
        assertTrue(latch.step(false, true));
        assertFalse(latch.step(false, false));
        assertTrue(latch.step(true, false));
        assertTrue(latch.step(true, true));
        assertFalse(latch.step(true, false));
        assertFalse(latch.step(false, false));
        assertTrue(latch.step(true, true));
        assertTrue(latch.step(false, true));
        assertFalse(latch.step(true, false));
        assertFalse(latch.step(false, false));
    }

    @Test
    public void fallingRisingLatchTest() {
        JKLatch latch = new JKLatch(true, Edge.FALLING, Edge.RISING);

        assertTrue(latch.step(false, false));
        assertTrue(latch.step(true, false));
        assertTrue(latch.step(false, false));
        assertFalse(latch.step(false, true));
        assertFalse(latch.step(false, false));
        assertFalse(latch.step(true, false));
        assertFalse(latch.step(true, true));
        assertFalse(latch.step(true, false));
        assertTrue(latch.step(false, false));
        assertFalse(latch.step(true, true));
        assertTrue(latch.step(false, true));
        assertTrue(latch.step(true, false));
        assertTrue(latch.step(false, false));
    }
}
