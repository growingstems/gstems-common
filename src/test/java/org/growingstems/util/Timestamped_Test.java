/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;
import org.junit.jupiter.api.Test;

public class Timestamped_Test {
    private static class TestTimeSource implements TimeSource {
        private Time m_time = Time.ZERO;

        @Override
        public Time clockTime() {
            return m_time;
        }
    }

    @Test
    public void testTime() {
        Timestamped<Integer> a = new Timestamped<>(1, Time.seconds(100.0));
        assertEquals(Integer.valueOf(1), a.getData());
        assertEquals(Double.valueOf(100.0), a.getTimestamp().getValue(Time.Unit.SECONDS));
        assertTrue(a.getData() instanceof Integer);
        assertTrue(a.getTimestamp() instanceof Time);

        Timestamped<Double> b = new Timestamped<>(3.14159, Time.minutes(60.0));
        assertEquals(Double.valueOf(3.14159), b.getData());
        assertEquals(Double.valueOf(60.0), b.getTimestamp().getValue(Time.Unit.MINUTES));
        assertTrue(b.getData() instanceof Double);
        assertTrue(b.getTimestamp() instanceof Time);
    }

    @Test
    public void testTimeSource() {
        TestTimeSource ts = new TestTimeSource();
        Timestamped<Integer> a = new Timestamped<>(1, ts);
        assertEquals(Integer.valueOf(1), a.getData());
        assertEquals(Double.valueOf(0.0), a.getTimestamp().getValue(Time.Unit.SECONDS));
        assertTrue(a.getData() instanceof Integer);
        assertTrue(a.getTimestamp() instanceof Time);

        ts.m_time = ts.clockTime().add(Time.seconds(1.0));
        Timestamped<Double> b = new Timestamped<>(3.14159, ts);
        assertEquals(Double.valueOf(3.14159), b.getData());
        assertEquals(Double.valueOf(1.0), b.getTimestamp().getValue(Time.Unit.SECONDS));
        assertTrue(b.getData() instanceof Double);
        assertTrue(b.getTimestamp() instanceof Time);
    }
}
