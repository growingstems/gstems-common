/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.timer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.TestCore;
import org.junit.jupiter.api.Test;

public class Timer_Test {
    private static class TestTimeSource implements TimeSource {
        private Time m_time = Time.ZERO;

        @Override
        public Time clockTime() {
            return m_time;
        }
    }

    private TestTimeSource m_timeSource = new TestTimeSource();
    private Timer m_timer = m_timeSource.createTimer();

    private void assertAtTime(double time_s) {
        assertEquals(time_s, m_timer.get().asSeconds(), TestCore.k_eps);
    }

    @Test
    public void noStart() {
        // Verify the m_timer is unresponsive to time when it hasn't started
        assertAtTime(0.0);
        m_timeSource.m_time = Time.seconds(1.0);
        assertAtTime(0.0);
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(0.0);
        assertFalse(m_timer.isRunning());
    }

    @Test
    public void timeChangedBeforeStart() {
        // Change time before starting to verify that doesn't matter
        m_timeSource.m_time = Time.seconds(1.0);
        m_timer.start();
        assertAtTime(0.0);
        assertTrue(m_timer.isRunning());
    }

    @Test
    public void recordOneSecond() {
        // Record one second
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));

        assertAtTime(1.0);

        // Verify calling get multiple times in a row with no time difference causes no
        // issue
        assertAtTime(1.0);
        assertTrue(m_timer.isRunning());
    }

    @Test
    public void recordOneSecondStop() {
        // Record one second and stop the m_timer
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        m_timer.stop();

        assertAtTime(1.0);
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        assertFalse(m_timer.isRunning());
    }

    @Test
    public void recordTwoSecondsNoBreak() {
        // Record one second
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);

        // Record another second
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(2.0);
        assertTrue(m_timer.isRunning());
    }

    @Test
    public void recordTwoSecondsBreak() {
        // Record one second and stop the m_timer
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        m_timer.stop();

        // Add time while not recording
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));

        // Record another second
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(2.0);
        assertTrue(m_timer.isRunning());
    }

    @Test
    public void recordTwoSecondsTwoStops() {
        // Record one second and stop the m_timer
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        m_timer.stop();

        // Add time while not recording
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));

        // Record another second
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(2.0);
        m_timer.stop();

        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));
        assertAtTime(2.0);
        assertFalse(m_timer.isRunning());

        // Record another second
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(3.0);
    }

    /*
     * This test is equivalent to recordTwoSecondsTwoStops, but would fail if we
     * used the logic in edu.wpi.first.wpilibj.Timer. It passes here because
     * TimerBase no-ops if start or stop is called when the m_timer is already
     * started/stopped.
     */
    @Test
    public void noop() {
        // Record one second and stop the m_timer
        m_timer.start();
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        m_timer.stop();
        m_timer.stop();

        // Add time while not recording
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));

        // Record another second
        m_timer.start();
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(2.0);
        m_timer.stop();
        m_timer.stop();

        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));
        assertAtTime(2.0);
        assertFalse(m_timer.isRunning());
    }

    @Test
    public void resetWhileRunning() {
        // Record one second
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        assertTrue(m_timer.isRunning());

        // Reset the m_timer
        m_timer.reset();
        assertAtTime(0.0);
        assertTrue(m_timer.isRunning());

        // Record another second
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        assertTrue(m_timer.isRunning());
    }

    @Test
    public void resetWhileStopped() {
        // Record one second and reset the m_timer
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        m_timer.stop();
        assertFalse(m_timer.isRunning());

        // Reset the m_timer
        m_timer.reset();
        assertAtTime(0.0);
        assertFalse(m_timer.isRunning());

        // Let a second pass while stopped
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(0.0);
        assertFalse(m_timer.isRunning());

        // Record one last second
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(1.0));
        assertAtTime(1.0);
        assertTrue(m_timer.isRunning());
    }

    @Test
    public void hasElapsedBehavior() {
        // Test no time elapsing under default conditions
        assertFalse(m_timer.hasElapsed(Time.seconds(0.001)));
        // Test before starting
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));
        assertFalse(m_timer.hasElapsed(Time.seconds(0.001)));
        // Start and test again
        m_timer.start();
        assertFalse(m_timer.hasElapsed(Time.seconds(0.001)));
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(10.0));
        assertTrue(m_timer.hasElapsed(Time.seconds(9.9)));
        assertFalse(m_timer.hasElapsed(Time.seconds(10.1)));
        // Stop the m_timer and verify nothing has changed
        m_timer.stop();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(10.0));
        assertTrue(m_timer.hasElapsed(Time.seconds(9.9)));
        assertFalse(m_timer.hasElapsed(Time.seconds(10.1)));
        // Restart the m_timer and verify it continues to accumulate
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(10.0));
        assertTrue(m_timer.hasElapsed(Time.seconds(19.9)));
        assertFalse(m_timer.hasElapsed(Time.seconds(20.1)));
    }

    @Test
    public void periodPassedBehavior() {
        // Test no time elapsing under default conditions
        assertFalse(m_timer.hasPeriodPassed(Time.seconds(0.001)));
        // Test before starting
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));
        assertFalse(m_timer.hasPeriodPassed(Time.seconds(0.001)));
        // Start and test again
        m_timer.start();
        assertFalse(m_timer.hasPeriodPassed(Time.seconds(0.001)));
        // Accrue time and verify time decrements with true test
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(10.0));
        assertTrue(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(7.0);
        assertTrue(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(4.0);
        assertTrue(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(1.0);
        assertFalse(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(1.0);
        // Stop the m_timer and verify nothing has changed
        m_timer.stop();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(10.0));
        assertFalse(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        // Verify period is checkable on stopped m_timer
        assertTrue(m_timer.hasPeriodPassed(Time.seconds(0.5)));
        assertAtTime(0.5);
        // Restart the m_timer and verify it continues to accumulate
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(10.0));
        assertAtTime(10.5);
        assertTrue(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(7.5);
        assertTrue(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(4.5);
        assertTrue(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(1.5);
        assertFalse(m_timer.hasPeriodPassed(Time.seconds(3.0)));
        assertAtTime(1.5);
    }

    @Test
    public void stopWhenElapsedBehavior() {
        // Test no time elapsing under default conditions
        assertTrue(m_timer.stopWhenElapsed(Time.seconds(0.001)));

        // Test before starting
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));
        assertTrue(m_timer.stopWhenElapsed(Time.seconds(0.001)));

        // Start and test for stop when not incremented
        m_timer.start();
        assertFalse(m_timer.stopWhenElapsed(Time.seconds(0.001)));
        assertTrue(m_timer.m_isRunning);

        // Test for stop on exact value after proper increment
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(20.0));
        assertTrue(m_timer.stopWhenElapsed(Time.seconds(20.0)));
        assertFalse(m_timer.m_isRunning);

        // Restart, test on greater value after increment
        m_timer.start();
        m_timeSource.m_time = m_timeSource.m_time.add(Time.seconds(10.0));
        assertTrue(m_timer.stopWhenElapsed(Time.seconds(5.0)));
        assertFalse(m_timer.m_isRunning);
    }

    @Test
    public void returnThis() {
        Timer ogTimer = m_timer;

        assertTrue(ogTimer == m_timer.start());
        assertTrue(ogTimer == m_timer.stop());
    }
}
