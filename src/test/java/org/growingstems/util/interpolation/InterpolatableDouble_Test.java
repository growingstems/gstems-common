/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.interpolation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.test.TestCore;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class InterpolatableDouble_Test extends InterpolatableTest {
    @ParameterizedTest
    @MethodSource("testInterpolateData")
    public void testInterpolate(double a, double b, double x, double expected, String testName) {
        InterpolatableDouble aObj = new InterpolatableDouble(a);
        InterpolatableDouble bObj = new InterpolatableDouble(b);

        InterpolatableDouble interpolatedValue = aObj.linearInterpolate(bObj, x);
        assertEquals(expected, interpolatedValue.getValue(), TestCore.k_eps, testName);
    }

    @ParameterizedTest
    @MethodSource("testInverseInterpolateData")
    public void testInverseInterpolate(
            double a, double b, double other, double expected, String testName) {
        InterpolatableDouble aObj = new InterpolatableDouble(a);
        InterpolatableDouble bObj = new InterpolatableDouble(b);
        InterpolatableDouble otherObj = new InterpolatableDouble(other);

        double x = aObj.linearInverseInterpolate(bObj, otherObj);
        assertEquals(expected, x, 1E-9, testName);
    }

    @ParameterizedTest
    @MethodSource("testComparableData")
    public void testComparable(double a, double b, int expectedResult, String testName) {
        InterpolatableDouble aObj = new InterpolatableDouble(a);
        InterpolatableDouble bObj = new InterpolatableDouble(b);

        assertEquals(expectedResult, aObj.compareTo(bObj), testName);
    }
}
