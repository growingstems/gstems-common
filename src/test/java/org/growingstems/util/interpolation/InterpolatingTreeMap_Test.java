/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.interpolation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.test.TestCore;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class InterpolatingTreeMap_Test {

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, true, Default test",
        "5.0, 5.0, true, 1 to 1 interpolation test",
        "10.0, 10.0, true, Exact key test",
        "15.0, 30.0, true, Interpolation test",
        "20.0, 50.0, true, Identical values pt.1 test",
        "25.0, 50.0, true, Interpolate identical values test",
        "30.0, 50.0, true, Identical values pt.2 test",
        "45.0, 70.0, true, Negative slope interpolation test",
        "45.0, 70.0, false, Negative slope interpolation test - without extrapolation",
        "60.0, 20.0, false, Beyond max bounds - without extrapolation",
        "-10.0, 0.0, false, Beyond min bounds - without extrapolation",
        "60.0, -80.0, true, Beyond max bounds - with extrapolation",
        "-10.0, -10.0, true, Beyond min bounds - with extrapolation"
    })
    public void testInterpolation(
            double key, double expectedValue, boolean extrapolate, String testName) {
        InterpolatingTreeMap<InterpolatableDouble, InterpolatableDouble> testTree =
                new InterpolatingTreeMap<InterpolatableDouble, InterpolatableDouble>();

        testTree.put(new InterpolatableDouble(0.0), new InterpolatableDouble(0.0));
        testTree.put(new InterpolatableDouble(10.0), new InterpolatableDouble(10.0));
        testTree.put(new InterpolatableDouble(20.0), new InterpolatableDouble(50.0));
        testTree.put(new InterpolatableDouble(30.0), new InterpolatableDouble(50.0));
        testTree.put(new InterpolatableDouble(40.0), new InterpolatableDouble(120.0));
        testTree.put(new InterpolatableDouble(50.0), new InterpolatableDouble(20.0));

        InterpolatableDouble keyObj = new InterpolatableDouble(key);
        InterpolatableDouble obj = testTree.getValue(keyObj, extrapolate);
        double value = obj.getValue();

        assertEquals(expectedValue, value, TestCore.k_eps, testName);
    }
}
