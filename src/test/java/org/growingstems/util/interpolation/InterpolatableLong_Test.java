/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package org.growingstems.util.interpolation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.test.TestCore;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class InterpolatableLong_Test extends InterpolatableTest {
    @ParameterizedTest
    @MethodSource("testInterpolateData")
    public void testInterpolate(long a, long b, double x, long expected, String testName) {
        InterpolatableLong aObj = new InterpolatableLong(a);
        InterpolatableLong bObj = new InterpolatableLong(b);

        InterpolatableLong interpolatedValue = aObj.linearInterpolate(bObj, x);
        assertEquals(expected, interpolatedValue.getValue(), TestCore.k_eps, testName);
    }

    @ParameterizedTest
    @MethodSource("testInverseInterpolateData")
    public void testInverseInterpolate(long a, long b, long other, double expected, String testName) {
        InterpolatableLong aObj = new InterpolatableLong(a);
        InterpolatableLong bObj = new InterpolatableLong(b);
        InterpolatableLong otherObj = new InterpolatableLong(other);

        double x = aObj.linearInverseInterpolate(bObj, otherObj);
        assertEquals(expected, x, TestCore.k_eps, testName);
    }

    @ParameterizedTest
    @MethodSource("testComparableData")
    public void testComparable(long a, long b, int expectedResult, String testName) {
        InterpolatableLong aObj = new InterpolatableLong(a);
        InterpolatableLong bObj = new InterpolatableLong(b);

        assertEquals(expectedResult, aObj.compareTo(bObj), testName);
    }
}
