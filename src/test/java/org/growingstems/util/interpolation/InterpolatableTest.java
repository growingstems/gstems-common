package org.growingstems.util.interpolation;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class InterpolatableTest {
    protected static Stream<Arguments> testInterpolateData() {
        return Stream.of(
                Arguments.of(0, 0, 0.0, 0, "Default test"),
                Arguments.of(0, 10, 0.0, 0, "0 to 10 - 0.0 test"),
                Arguments.of(0, 10, 0.1, 1, "0 to 10 - 0.1 test"),
                Arguments.of(0, 10, 0.2, 2, "0 to 10 - 0.2 test"),
                Arguments.of(0, 10, 0.3, 3, "0 to 10 - 0.3 test"),
                Arguments.of(0, 10, 0.4, 4, "0 to 10 - 0.4 test"),
                Arguments.of(0, 10, 0.5, 5, "0 to 10 - 0.5 test"),
                Arguments.of(0, 10, 0.6, 6, "0 to 10 - 0.6 test"),
                Arguments.of(0, 10, 0.7, 7, "0 to 10 - 0.7 test"),
                Arguments.of(0, 10, 0.8, 8, "0 to 10 - 0.8 test"),
                Arguments.of(0, 10, 0.9, 9, "0 to 10 - 0.9 test"),
                Arguments.of(0, 10, 1.0, 10, "0 to 10 - 1.0 test"),
                Arguments.of(0, 10, -0.5, -5, "Negative extrapolate test"),
                Arguments.of(0, 10, 2.0, 20, "Positive extrapolate test"),
                Arguments.of(10, 0, 0.0, 10, "Reverse points - beginning"),
                Arguments.of(10, 0, 0.5, 5, "Reverse points - middle"),
                Arguments.of(10, 0, 1.0, 0, "Reverse points - end"));
    }

    protected static Stream<Arguments> testInverseInterpolateData() {
        return Stream.of(
                Arguments.of(0, 10, 0, 0.0, "0 to 10 - 0.0 test"),
                Arguments.of(0, 10, 1, 0.1, "0 to 10 - 1.0 test"),
                Arguments.of(0, 10, 2, 0.2, "0 to 10 - 2.0 test"),
                Arguments.of(0, 10, 3, 0.3, "0 to 10 - 3.0 test"),
                Arguments.of(0, 10, 4, 0.4, "0 to 10 - 4.0 test"),
                Arguments.of(0, 10, 5, 0.5, "0 to 10 - 5.0 test"),
                Arguments.of(0, 10, 6, 0.6, "0 to 10 - 6.0 test"),
                Arguments.of(0, 10, 7, 0.7, "0 to 10 - 7.0 test"),
                Arguments.of(0, 10, 8, 0.8, "0 to 10 - 8.0 test"),
                Arguments.of(0, 10, 9, 0.9, "0 to 10 - 9.0 test"),
                Arguments.of(0, 10, 10, 1.0, "0 to 10 - 10.0 test"),
                Arguments.of(0, 10, -5, -0.5, "Negative extrapolate test"),
                Arguments.of(0, 10, 20, 2.0, "Positive extrapolate test"),
                Arguments.of(10, 0, 10, 0.0, "Reverse points - beginning"),
                Arguments.of(10, 0, 5, 0.5, "Reverse points - middle"),
                Arguments.of(10, 0, 0, 1.0, "Reverse points - end"));
    }

    protected static Stream<Arguments> testComparableData() {
        return Stream.of(
                Arguments.of(0, 0, 0, "Equal test"),
                Arguments.of(1, 0, 1, "Greater than test"),
                Arguments.of(0, 1, -1, "Less than test"));
    }
}
