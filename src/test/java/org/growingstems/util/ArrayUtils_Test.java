/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

import static org.growingstems.util.ArrayUtils.countEqual;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class ArrayUtils_Test {
    // =========================================================
    // Count Equal Tests
    // =========================================================
    private static class CountEqualTp<E> {
        public CountEqualTp(E[] array, E compareTo, int expected) {
            this.array = array;
            this.compareTo = compareTo;
            this.expected = expected;
        }

        public E[] array;
        public E compareTo;
        public int expected;
    }

    public static List<CountEqualTp<Boolean>> provideBools() {
        return List.of(
                new CountEqualTp<Boolean>(new Boolean[] {true, true, false, false, true}, true, 3),
                new CountEqualTp<Boolean>(new Boolean[] {false, false, false, false, false}, true, 0),
                new CountEqualTp<Boolean>(new Boolean[] {false, false, false, false, false}, false, 5),
                new CountEqualTp<Boolean>(new Boolean[] {}, true, 0),
                new CountEqualTp<Boolean>(new Boolean[] {}, false, 0),
                new CountEqualTp<Boolean>(new Boolean[] {true, true, null, false, true}, true, 3));
    }

    public static List<CountEqualTp<Integer>> provideInts() {
        return List.of(
                new CountEqualTp<Integer>(new Integer[] {1, 2, 3, 4, 5}, 2, 1),
                new CountEqualTp<Integer>(new Integer[] {0, 1, 1, 0, 5, 0}, 0, 3),
                new CountEqualTp<Integer>(new Integer[] {10, 9, -10, 9, 8, -8, 8, -4, 2, 1, 2, -4}, 8, 2),
                new CountEqualTp<Integer>(new Integer[] {}, -4, 0),
                new CountEqualTp<Integer>(new Integer[] {1, 2, null, 4, 5}, 2, 1),
                new CountEqualTp<Integer>(new Integer[] {1, 2, 3, 4, 5}, 6, 0));
    }

    public static List<CountEqualTp<String>> provideStrings() {
        return List.of(
                new CountEqualTp<String>(new String[] {"hi", "this", "is", "a", "string"}, "this", 1),
                new CountEqualTp<String>(new String[] {"one", "two", "three", "five", "five"}, "five", 2),
                new CountEqualTp<String>(new String[] {}, "hello", 0),
                new CountEqualTp<String>(new String[] {"one", "two", "five", null, "one"}, "one", 2),
                new CountEqualTp<String>(new String[] {"", "a", "", "", "B"}, "", 3));
    }

    @SuppressWarnings("overrides")
    public static class TestObj {
        public TestObj(int foo, int bar) {
            this.foo = foo;
            this.bar = bar;
        }

        public boolean equals(Object o) {
            if (o instanceof TestObj) {
                TestObj rhs = (TestObj) o;
                return this.foo == rhs.foo && this.bar == rhs.bar;
            }
            return false;
        }

        public int foo;
        public int bar;
    }
    ;

    public static List<CountEqualTp<TestObj>> provideCustomType() {
        return List.of(
                new CountEqualTp<TestObj>(
                        new TestObj[] {new TestObj(1, 1), new TestObj(1, 2), new TestObj(4, 1)},
                        new TestObj(1, 2),
                        1),
                new CountEqualTp<TestObj>(
                        new TestObj[] {new TestObj(2, 3), new TestObj(2, 3), null}, new TestObj(2, 3), 2),
                new CountEqualTp<TestObj>(
                        new TestObj[] {new TestObj(0, 0), new TestObj(1, 1), new TestObj(2, 2)},
                        new TestObj(3, 3),
                        0));
    }

    @ParameterizedTest
    @MethodSource({"provideBools", "provideInts", "provideStrings", "provideCustomType"})
    public void countEqualTest(CountEqualTp<?> testPoint) {
        assertEquals(testPoint.expected, countEqual(testPoint.array, testPoint.compareTo));
    }
    // =========================================================
}
