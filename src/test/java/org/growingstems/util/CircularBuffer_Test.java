/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.Test;

public class CircularBuffer_Test {
    private final CircularBuffer<String> m_circularBuffer = new CircularBuffer<>(5);

    private void assertBufferEquals(String... values) {
        assertTrue(Arrays.equals(m_circularBuffer.toArray(), values));
    }

    @Test
    public void addOne() {
        assertTrue(m_circularBuffer.add("Hello"));
        assertBufferEquals("Hello");
    }

    @Test
    public void addOverflow() {
        assertTrue(m_circularBuffer.add("This"));
        assertBufferEquals("This");
        assertTrue(m_circularBuffer.add("is"));
        assertBufferEquals("This", "is");
        assertTrue(m_circularBuffer.add("a"));
        assertBufferEquals("This", "is", "a");
        assertTrue(m_circularBuffer.add("test"));
        assertBufferEquals("This", "is", "a", "test");
        assertTrue(m_circularBuffer.add("with"));
        assertBufferEquals("This", "is", "a", "test", "with");
        assertTrue(m_circularBuffer.add("overflow"));
        assertBufferEquals("is", "a", "test", "with", "overflow");
    }

    @Test
    public void element() {
        assertThrows(NoSuchElementException.class, () -> m_circularBuffer.element());

        assertTrue(m_circularBuffer.add("This"));
        assertEquals(m_circularBuffer.element(), "This");
        assertBufferEquals("This");
        assertTrue(m_circularBuffer.add("is"));
        assertEquals(m_circularBuffer.element(), "This");
        assertBufferEquals("This", "is");
        assertTrue(m_circularBuffer.add("a"));
        assertEquals(m_circularBuffer.element(), "This");
        assertBufferEquals("This", "is", "a");
        assertTrue(m_circularBuffer.add("test"));
        assertEquals(m_circularBuffer.element(), "This");
        assertBufferEquals("This", "is", "a", "test");
        assertTrue(m_circularBuffer.add("with"));
        assertEquals(m_circularBuffer.element(), "This");
        assertBufferEquals("This", "is", "a", "test", "with");
        assertTrue(m_circularBuffer.add("overflow"));
        assertEquals(m_circularBuffer.element(), "is");
        assertBufferEquals("is", "a", "test", "with", "overflow");
    }

    @Test
    public void offerOne() {
        assertTrue(m_circularBuffer.offer("Hello"));
        assertBufferEquals("Hello");
    }

    @Test
    public void offerOverflow() {
        assertTrue(m_circularBuffer.offer("This"));
        assertBufferEquals("This");
        assertTrue(m_circularBuffer.offer("is"));
        assertBufferEquals("This", "is");
        assertTrue(m_circularBuffer.offer("a"));
        assertBufferEquals("This", "is", "a");
        assertTrue(m_circularBuffer.offer("test"));
        assertBufferEquals("This", "is", "a", "test");
        assertTrue(m_circularBuffer.offer("with"));
        assertBufferEquals("This", "is", "a", "test", "with");
        assertTrue(m_circularBuffer.offer("overflow"));
        assertBufferEquals("is", "a", "test", "with", "overflow");
    }

    @Test
    public void peek() {
        assertNull(m_circularBuffer.peek());

        assertTrue(m_circularBuffer.add("This"));
        assertEquals(m_circularBuffer.peek(), "This");
        assertBufferEquals("This");
        assertTrue(m_circularBuffer.add("is"));
        assertEquals(m_circularBuffer.peek(), "This");
        assertBufferEquals("This", "is");
        assertTrue(m_circularBuffer.add("a"));
        assertEquals(m_circularBuffer.peek(), "This");
        assertBufferEquals("This", "is", "a");
        assertTrue(m_circularBuffer.add("test"));
        assertEquals(m_circularBuffer.peek(), "This");
        assertBufferEquals("This", "is", "a", "test");
        assertTrue(m_circularBuffer.add("with"));
        assertEquals(m_circularBuffer.peek(), "This");
        assertBufferEquals("This", "is", "a", "test", "with");
        assertTrue(m_circularBuffer.add("overflow"));
        assertEquals(m_circularBuffer.peek(), "is");
        assertBufferEquals("is", "a", "test", "with", "overflow");
    }

    @Test
    public void poll() {
        assertNull(m_circularBuffer.poll());

        assertTrue(m_circularBuffer.add("This"));
        assertBufferEquals("This");
        assertEquals(m_circularBuffer.poll(), "This");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("is"));
        assertBufferEquals("is");
        assertEquals(m_circularBuffer.poll(), "is");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("a"));
        assertBufferEquals("a");
        assertEquals(m_circularBuffer.poll(), "a");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("test"));
        assertBufferEquals("test");
        assertEquals(m_circularBuffer.poll(), "test");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("with"));
        assertBufferEquals("with");
        assertEquals(m_circularBuffer.poll(), "with");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("overflow"));
        assertBufferEquals("overflow");
        assertEquals(m_circularBuffer.poll(), "overflow");
        assertBufferEquals();
    }

    @Test
    public void remove() {
        assertThrows(NoSuchElementException.class, () -> m_circularBuffer.remove());

        assertTrue(m_circularBuffer.add("This"));
        assertBufferEquals("This");
        assertEquals(m_circularBuffer.poll(), "This");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("is"));
        assertBufferEquals("is");
        assertEquals(m_circularBuffer.poll(), "is");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("a"));
        assertBufferEquals("a");
        assertEquals(m_circularBuffer.poll(), "a");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("test"));
        assertBufferEquals("test");
        assertEquals(m_circularBuffer.poll(), "test");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("with"));
        assertBufferEquals("with");
        assertEquals(m_circularBuffer.poll(), "with");
        assertBufferEquals();

        assertTrue(m_circularBuffer.add("overflow"));
        assertBufferEquals("overflow");
        assertEquals(m_circularBuffer.poll(), "overflow");
        assertBufferEquals();
    }

    @Test
    public void addAllEmpty() {
        assertFalse(m_circularBuffer.addAll(List.of()));
        assertBufferEquals();
    }

    @Test
    public void addAll() {
        assertTrue(m_circularBuffer.addAll(List.of("This", "is", "a", "test")));
        assertBufferEquals("This", "is", "a", "test");
    }

    @Test
    public void addAllOverflow() {
        assertTrue(m_circularBuffer.addAll(List.of("This", "is", "a", "test")));
        assertBufferEquals("This", "is", "a", "test");

        assertTrue(m_circularBuffer.addAll(List.of("This", "is", "a", "test")));
        assertBufferEquals("test", "This", "is", "a", "test");
    }

    @Test
    public void addAllOverflowImmediate() {
        assertTrue(m_circularBuffer.addAll(List.of("This", "is", "a", "full", "buffer", "already")));
        assertBufferEquals("is", "a", "full", "buffer", "already");
    }

    @Test
    public void clear() {
        m_circularBuffer.clear();
        assertBufferEquals();
        m_circularBuffer.add("Test");
        m_circularBuffer.clear();
        assertBufferEquals();
        m_circularBuffer.addAll(List.of("This", "is", "a", "full", "buffer", "already"));
        m_circularBuffer.clear();
        assertBufferEquals();
    }

    @Test
    public void contains() {
        assertFalse(m_circularBuffer.contains("Test"));
        m_circularBuffer.add("Test");
        assertTrue(m_circularBuffer.contains("Test"));
        m_circularBuffer.clear();
        assertFalse(m_circularBuffer.contains("Test"));
        m_circularBuffer.addAll(List.of("This", "is", "a", "full", "buffer", "already"));
        assertFalse(m_circularBuffer.contains("This"));
        assertTrue(m_circularBuffer.contains("is"));
        assertTrue(m_circularBuffer.contains("a"));
        assertTrue(m_circularBuffer.contains("full"));
        assertTrue(m_circularBuffer.contains("buffer"));
        assertTrue(m_circularBuffer.contains("already"));
        assertFalse(m_circularBuffer.contains("Test"));
    }

    @Test
    public void containsAll() {
        assertFalse(m_circularBuffer.containsAll(List.of("Test")));
        m_circularBuffer.add("Test");
        assertTrue(m_circularBuffer.contains("Test"));
        m_circularBuffer.clear();
        assertFalse(m_circularBuffer.contains("Test"));
        m_circularBuffer.addAll(List.of("This", "is", "a", "full", "buffer", "already"));

        assertFalse(
                m_circularBuffer.containsAll(List.of("This", "is", "a", "full", "buffer", "already")));
        assertTrue(m_circularBuffer.containsAll(List.of("is", "a", "full", "buffer", "already")));
    }

    @Test
    public void offer() {
        assertTrue(m_circularBuffer.offer("Hello"));
        assertBufferEquals("Hello");

        m_circularBuffer.clear();
        m_circularBuffer.addAll(List.of("This", "is", "a", "big"));
        assertTrue(m_circularBuffer.offer("test"));
        assertBufferEquals("This", "is", "a", "big", "test");

        m_circularBuffer.clear();
        m_circularBuffer.addAll(List.of("This", "is", "a", "big", "thing"));
        assertTrue(m_circularBuffer.offer("test"));
        assertBufferEquals("is", "a", "big", "thing", "test");
    }

    @Test
    public void negativeBuffer() {
        assertThrows(NegativeArraySizeException.class, () -> new CircularBuffer<>(-10));
    }
}
