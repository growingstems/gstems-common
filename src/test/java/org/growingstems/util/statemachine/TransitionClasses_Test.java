/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class TransitionClasses_Test {
    boolean m_transition = false;
    int m_num = 0;

    @Test
    public void staticTransition() {
        Transition t = new StaticTransition(new InitState("A", () -> {}), () -> m_transition);

        assertFalse(t.triggered());
        m_transition = true;
        assertTrue(t.triggered());
    }

    @Test
    public void DynamicTransition() {
        Transition t = new DynamicTransition(
                () -> {
                    switch (m_num) {
                        case 0:
                            return new InitState("A", () -> {});
                        case 1:
                            return new InitState("B", () -> {});
                        case 2:
                            return new InitState("C", () -> {});
                        default:
                            return new InitState("D", () -> {});
                    }
                },
                () -> m_transition);

        assertFalse(t.triggered());
        m_transition = true;
        assertTrue(t.triggered());

        // check getState
        assertEquals(t.getTarget().getStateName(), "A");
        m_num = 1;
        assertEquals(t.getTarget().getStateName(), "B");
        m_num = 2;
        assertEquals(t.getTarget().getStateName(), "C");
        m_num = 3;
        assertEquals(t.getTarget().getStateName(), "D");
    }
}
