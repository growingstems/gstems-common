/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class StateMachine_Test {
    public static final double k_eps = 1E-10;

    int m_initCountA = 0;
    int m_execCountA = 0;
    int m_exitCountA = 0;
    int m_initCountB = 0;
    int m_execCountB = 0;
    int m_exitCountB = 0;
    int m_initCountC = 0;
    int m_execCountC = 0;
    int m_exitCountC = 0;
    State m_stateA =
            new InitExecExitState("A", () -> m_initCountA++, () -> m_execCountA++, () -> m_exitCountA++);
    State m_stateB =
            new InitExecExitState("B", () -> m_initCountB++, () -> m_execCountB++, () -> m_exitCountB++);
    State m_stateC =
            new InitExecExitState("C", () -> m_initCountC++, () -> m_execCountC++, () -> m_exitCountC++);
    StateMachine m_sm = new StateMachine(m_stateA);
    int m_transitionNum = 0;

    public void checkValsA(int init, int exec, int exit) {
        assertEquals(init, m_initCountA);
        assertEquals(exec, m_execCountA);
        assertEquals(exit, m_exitCountA);
    }

    public void checkValsB(int init, int exec, int exit) {
        assertEquals(init, m_initCountB);
        assertEquals(exec, m_execCountB);
        assertEquals(exit, m_exitCountB);
    }

    public void checkValsC(int init, int exec, int exit) {
        assertEquals(init, m_initCountC);
        assertEquals(exec, m_execCountC);
        assertEquals(exit, m_exitCountC);
    }

    @Test
    public void step() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));
        m_stateB.addTransition(new StaticTransition(m_stateC, () -> m_transitionNum == 2));

        // No steps, begin at state A
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Init and exec state A
        m_sm.step();
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
        // Exec state A
        m_sm.step();
        checkValsA(1, 2, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
        // Exec state A
        m_sm.step();
        checkValsA(1, 3, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // No change - no step occurred
        m_transitionNum = 1;
        checkValsA(1, 3, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
        // Transition: exit state A, init and exec state B
        m_sm.step();
        checkValsA(1, 3, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);
        // Exec state B
        m_sm.step();
        checkValsA(1, 3, 1);
        checkValsB(1, 2, 0);
        checkValsC(0, 0, 0);
        // Exec state B
        m_sm.step();
        checkValsA(1, 3, 1);
        checkValsB(1, 3, 0);
        checkValsC(0, 0, 0);

        // No change - no step occurred
        m_transitionNum = 2;
        checkValsA(1, 3, 1);
        checkValsB(1, 3, 0);
        checkValsC(0, 0, 0);
        // Transition: exit state B, init and exec state C
        m_sm.step();
        checkValsA(1, 3, 1);
        checkValsB(1, 3, 1);
        checkValsC(1, 1, 0);
        // Exec state C
        m_sm.step();
        checkValsA(1, 3, 1);
        checkValsB(1, 3, 1);
        checkValsC(1, 2, 0);
        // Exec state C
        m_sm.step();
        checkValsA(1, 3, 1);
        checkValsB(1, 3, 1);
        checkValsC(1, 3, 0);

        // No change - no step occurred
        m_transitionNum = 1;
        checkValsA(1, 3, 1);
        checkValsB(1, 3, 1);
        checkValsC(1, 3, 0);
        // No transition - Transition to B was only given to state A
        m_sm.step();
        checkValsA(1, 3, 1);
        checkValsB(1, 3, 1);
        checkValsC(1, 4, 0);
    }

    @Test
    public void getCurrentState() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));
        m_stateB.addTransition(new StaticTransition(m_stateC, () -> m_transitionNum == 2));

        // No steps, begin at state A
        assertEquals(m_stateA, m_sm.getCurrentState());

        // Init and exec state A
        m_sm.step();
        assertEquals(m_stateA, m_sm.getCurrentState());
        // Exec state A
        m_sm.step();
        assertEquals(m_stateA, m_sm.getCurrentState());

        // No change - no step occurred
        m_transitionNum = 1;
        assertEquals(m_stateA, m_sm.getCurrentState());
        // Transition: exit state A, init and exec state B
        m_sm.step();
        assertEquals(m_stateB, m_sm.getCurrentState());
        // Exec state B
        m_sm.step();
        assertEquals(m_stateB, m_sm.getCurrentState());

        // No change - no step occurred
        m_transitionNum = 2;
        assertEquals(m_stateB, m_sm.getCurrentState());
        // Transition: exit state B, init and exec state C
        m_sm.step();
        assertEquals(m_stateC, m_sm.getCurrentState());
        // Exec state C
        m_sm.step();
        assertEquals(m_stateC, m_sm.getCurrentState());
    }

    @Test
    public void fastTransitionStep() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> true));
        m_stateB.addTransition(new StaticTransition(m_stateC, () -> true));

        // No steps, begin at state A
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        /*
         * First step inits and execs state A. A transition immediately occurs,
         * exit state A, init and exec state B.
         */
        m_sm.step();
        assertEquals(m_stateB, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        // Transition: Exit state B, init and exec state C
        m_sm.step();
        assertEquals(m_stateC, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(1, 1, 0);

        // Exec state C
        m_sm.step();
        assertEquals(m_stateC, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(1, 2, 0);
        // Exec state C
        m_sm.step();
        assertEquals(m_stateC, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(1, 3, 0);
        // Exec state C
        m_sm.step();
        assertEquals(m_stateC, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(1, 4, 0);
    }

    @Test
    public void loop() {
        m_stateA.addTransition(new StaticTransition(m_stateA, () -> m_transitionNum == 1));

        // No steps, begin at state A
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Init and exec state A
        m_sm.step();
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // No change - no step occurred
        m_transitionNum = 1;
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Transition from state A to state A. Exit, init, and exec state A.
        m_sm.step();
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(2, 2, 1);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
    }

    @Test
    public void stop() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));

        // No steps, begin at state A
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Init and exec state A
        m_sm.step();
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // No change - no step occurred
        m_transitionNum = 1;
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
        // Transition: exit state A, init and exec state B
        m_sm.step();
        assertEquals(m_stateB, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        // Stop state machine - Exit state B
        m_sm.stop();
        m_transitionNum = 0;
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(0, 0, 0);

        // Stop a stopped state machine - Should do nothing
        m_sm.stop();
        m_transitionNum = 0;
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(0, 0, 0);

        // Step progresses again
        m_sm.step();
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(2, 2, 1);
        checkValsB(1, 1, 1);
        checkValsC(0, 0, 0);
    }

    @Test
    public void pause() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));

        // No steps, begin at state A
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Init and exec state A
        m_sm.step();
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // No change - no step occurred
        m_transitionNum = 1;
        assertEquals(m_stateA, m_sm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
        // Transition: exit state A, init and exec state B
        m_sm.step();
        assertEquals(m_stateB, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        // Pause state machine - Exit state B
        m_sm.pause();
        m_transitionNum = 0;
        assertEquals(m_stateB, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(0, 0, 0);

        // Pause a paused state machine - Should do nothing
        m_sm.pause();
        m_transitionNum = 0;
        assertEquals(m_stateB, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(0, 0, 0);

        // Step progresses again
        m_sm.step();
        assertEquals(m_stateB, m_sm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(2, 2, 1);
        checkValsC(0, 0, 0);
    }

    @Test
    public void getInitialState() {
        assertEquals(m_stateA, m_sm.getInitialState());
    }

    @Test
    public void getTriggeredTransition() {
        Transition trans1 = new StaticTransition(m_stateB, () -> m_transitionNum == 1);
        m_stateA.addTransition(trans1);
        Transition trans2 = new StaticTransition(m_stateC, () -> m_transitionNum == 2);
        m_stateB.addTransition(trans2);

        // No steps, begin at state A
        assertEquals(null, m_sm.getTriggeredTransition());

        // Init and exec state A
        m_sm.step();
        assertEquals(null, m_sm.getTriggeredTransition());
        // Exec state A
        m_sm.step();
        assertEquals(null, m_sm.getTriggeredTransition());

        // No change - no step occurred
        m_transitionNum = 1;
        assertEquals(null, m_sm.getTriggeredTransition());
        // Transition: exit state A, init and exec state B
        m_sm.step();
        assertEquals(trans1, m_sm.getTriggeredTransition());
        // Exec state B
        m_sm.step();
        assertEquals(null, m_sm.getTriggeredTransition());

        // No change - no step occurred
        m_transitionNum = 2;
        assertEquals(null, m_sm.getTriggeredTransition());
        // Transition: exit state B, init and exec state C
        m_sm.step();
        assertEquals(trans2, m_sm.getTriggeredTransition());
        // Exec state C
        m_sm.step();
        assertEquals(null, m_sm.getTriggeredTransition());
    }

    @Test
    public void getAsStateStopOnExit() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));

        State smAsState = m_sm.getAsState("TestName", false);

        assertEquals("TestName.A", smAsState.getStateName());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.init();
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 2, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        m_transitionNum = 1;
        smAsState.exec();
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(1, 2, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 0);
        checkValsC(0, 0, 0);

        smAsState.exit();
        m_transitionNum = 0;
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        smAsState.init();
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        smAsState.exec();
        assertEquals("TestName.A", smAsState.getStateName());
        checkValsA(2, 3, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        m_transitionNum = 1;
        smAsState.exec();
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(2, 3, 2);
        checkValsB(2, 3, 1);
        checkValsC(0, 0, 0);

        smAsState.exit();
        checkValsA(2, 3, 2);
        checkValsB(2, 3, 2);
        checkValsC(0, 0, 0);
    }

    @Test
    public void getAsStatePauseOnExit() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));

        State smAsState = m_sm.getAsState("TestName", true);

        assertEquals("TestName.A", smAsState.getStateName());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.init();
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 2, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        m_transitionNum = 1;
        smAsState.exec();
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(1, 2, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 0);
        checkValsC(0, 0, 0);

        smAsState.exit();
        m_transitionNum = 0;
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        smAsState.init();
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        smAsState.exec();
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(1, 2, 1);
        checkValsB(2, 3, 1);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 2, 1);
        checkValsB(2, 4, 1);
        checkValsC(0, 0, 0);

        smAsState.exit();
        checkValsA(1, 2, 1);
        checkValsB(2, 4, 2);
        checkValsC(0, 0, 0);
    }
}
