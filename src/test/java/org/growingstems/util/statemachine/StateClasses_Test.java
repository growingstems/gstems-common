/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class StateClasses_Test {
    private static class TestState extends StateBase {
        public TestState(String name) {
            super(name);
        }

        @Override
        public void init() {}

        @Override
        public void exec() {}

        @Override
        public void exit() {}

        @Override
        public TestState cloneWithoutTransitions(String name) {
            return new TestState(name);
        }
    }

    private int m_transitionNum = 0;

    @Test
    public void stateBase_getNextState() {
        TestState stateA = new TestState("A");
        TestState stateB = new TestState("B");
        TestState stateC = new TestState("C");

        stateA.addTransition(new StaticTransition(stateB, () -> m_transitionNum == 1));
        stateA.addTransition(new StaticTransition(stateC, () -> m_transitionNum == 2));

        State nextState = stateA.getNextState();
        assertEquals(nextState, null);

        m_transitionNum = 1;
        nextState = stateA.getNextState();
        assertNotEquals(nextState, null);
        assertEquals(nextState.getStateName(), "B");

        m_transitionNum = 2;
        nextState = stateA.getNextState();
        assertNotEquals(nextState, null);
        assertEquals(nextState.getStateName(), "C");
    }

    @Test
    public void stateBase_cloneWithoutTransitions() {
        TestState stateA = new TestState("A");
        TestState stateB = stateA.cloneWithoutTransitions("B");

        assertNotEquals(stateA, stateB);
        assertEquals(stateA.getStateName(), "A");
        assertEquals(stateB.getStateName(), "B");
    }

    @Test
    public void stateBase_getTriggeredTransition() {
        TestState stateA = new TestState("A");
        TestState stateB = new TestState("B");
        TestState stateC = new TestState("C");

        StaticTransition stateBTransition =
                new StaticTransition("Test Transition", stateB, () -> m_transitionNum == 1);
        stateA.addTransition(stateBTransition);
        stateA.addTransition(
                new StaticTransition("Test Fake Transition", stateB, () -> m_transitionNum == 1));
        StaticTransition stateCTransition = new StaticTransition(stateC, () -> m_transitionNum == 2);
        stateA.addTransition(stateCTransition);

        assertEquals(stateA.getTriggeredTransition(), null);

        m_transitionNum = 1;
        assertEquals(stateA.getTriggeredTransition(), stateBTransition);
        assertTrue(stateA.getTriggeredTransition().getName().equals("Test Transition"));

        m_transitionNum = 2;
        assertEquals(stateA.getTriggeredTransition(), stateCTransition);
        assertTrue(stateA.getTriggeredTransition().getName().equals(""));
    }
}
