/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.statemachine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class TimedStateMachine_Test {
    public static final double k_eps = 1E-10;

    int m_initCountA = 0;
    int m_execCountA = 0;
    int m_exitCountA = 0;
    int m_initCountB = 0;
    int m_execCountB = 0;
    int m_exitCountB = 0;
    int m_initCountC = 0;
    int m_execCountC = 0;
    int m_exitCountC = 0;
    State m_stateA =
            new InitExecExitState("A", () -> m_initCountA++, () -> m_execCountA++, () -> m_exitCountA++);
    State m_stateB =
            new InitExecExitState("B", () -> m_initCountB++, () -> m_execCountB++, () -> m_exitCountB++);
    State m_stateC =
            new InitExecExitState("C", () -> m_initCountC++, () -> m_execCountC++, () -> m_exitCountC++);
    TestTimeSource m_timeSource = new TestTimeSource();
    TimedStateMachine m_tsm = new TimedStateMachine(m_stateA, m_timeSource);
    int m_transitionNum = 0;

    public void checkValsA(int init, int exec, int exit) {
        assertEquals(init, m_initCountA);
        assertEquals(exec, m_execCountA);
        assertEquals(exit, m_exitCountA);
    }

    public void checkValsB(int init, int exec, int exit) {
        assertEquals(init, m_initCountB);
        assertEquals(exec, m_execCountB);
        assertEquals(exit, m_exitCountB);
    }

    public void checkValsC(int init, int exec, int exit) {
        assertEquals(init, m_initCountC);
        assertEquals(exec, m_execCountC);
        assertEquals(exit, m_exitCountC);
    }

    @Test
    public void staticTimedTransition() {
        Transition t1 = m_tsm.getTimedTransition(m_stateB, Time.seconds(1.0));
        Transition t2 = m_tsm.getTimedTransition(m_stateC, Time.seconds(1.0));
        m_stateA.addTransition(t1);
        m_stateB.addTransition(t2);

        // Initialize state A
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Step forwards 0.5s
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Step forwards 0.6s to 1.1s, past transition time
        m_timeSource.addTime(Time.seconds(0.6));
        // No transition before step
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        // Step transitioned
        assertEquals(m_stateB, m_tsm.getCurrentState());
        // Initialize state B
        m_tsm.step();
        assertEquals(m_stateB, m_tsm.getCurrentState());

        // Step forwards 0.5s
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(m_stateB, m_tsm.getCurrentState());
        m_tsm.step();
        assertEquals(m_stateB, m_tsm.getCurrentState());

        // Step forwards 0.6s to 1.1s, past transition time
        m_timeSource.addTime(Time.seconds(0.6));
        // No transition before step
        assertEquals(m_stateB, m_tsm.getCurrentState());
        m_tsm.step();
        // Step transitioned
        assertEquals(m_stateC, m_tsm.getCurrentState());
        // Initialize state C
        m_tsm.step();
        assertEquals(m_stateC, m_tsm.getCurrentState());
    }

    @Test
    public void dynamicTimedTransition() {
        Transition t = m_tsm.getTimedTransition(
                () -> {
                    switch (m_transitionNum) {
                        case 0:
                            return m_stateB;
                        default:
                            return m_stateC;
                    }
                },
                Time.seconds(1.0));
        m_stateA.addTransition(t);
        m_stateB.addTransition(new StaticTransition(m_stateA, () -> true));
        m_stateC.addTransition(new StaticTransition(m_stateA, () -> true));

        // Initialize state A
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Step forwards 0.5s
        m_transitionNum = 0;
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Step forwards 0.6s to 1.1s, past transition time
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        // Step transitioned to B
        assertEquals(m_stateB, m_tsm.getCurrentState());
        m_tsm.step();
        // Step transitioned back to A
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Step forwards 0.5s
        m_transitionNum = 1;
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Step forwards 0.6s to 1.1s, past transition time
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        // Step transitioned to C
        assertEquals(m_stateC, m_tsm.getCurrentState());
        m_tsm.step();
        // Step transitioned back to A
        assertEquals(m_stateA, m_tsm.getCurrentState());
    }

    @Test
    public void getTimeSinceTransition() {
        Transition t1 = m_tsm.getTimedTransition(m_stateB, Time.seconds(1.0));
        Transition t2 = m_tsm.getTimedTransition(m_stateC, Time.seconds(1.0));
        m_stateA.addTransition(t1);
        m_stateB.addTransition(t2);

        // Just starting
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // No steps occurred. Timer hasn't started.
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Initialize state A then step forwards 0.6s
        m_tsm.step();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());

        // Step again, still 0.6s
        m_tsm.step();
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());
        // Step forwards 0.5s to 1.1s, past transition time
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(1.1, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());
        m_tsm.step();
        // Transition occurred, timer reset
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateB, m_tsm.getCurrentState());

        // Step forwards 0.6s
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateB, m_tsm.getCurrentState());
        // Step forwards 0.5s to 1.1s, past transition time
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(1.1, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateB, m_tsm.getCurrentState());
        m_tsm.step();
        // Transition occurred, timer reset
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateC, m_tsm.getCurrentState());
    }

    @Test
    public void loopWithTime() {
        m_stateA.addTransition(new StaticTransition(m_stateA, () -> m_transitionNum == 1));

        // No steps, begin at state A
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Init and exec state A
        m_tsm.step();
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(0.5, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // No change - no step occurred
        m_transitionNum = 1;
        assertEquals(m_stateA, m_tsm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Transition from state A to state A. Exit, init, and exec state A.
        m_tsm.step();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        m_timeSource.addTime(Time.seconds(0.5));
        assertEquals(0.5, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateA, m_tsm.getCurrentState());
        checkValsA(2, 2, 1);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
    }

    @Test
    public void stop() {
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // State hasn't initialized - timer hasn't started
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // Start state and step forwards 0.6 seconds
        m_tsm.step();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // Still at 0.6s
        m_tsm.step();
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // Step forwards another 0.6s to 1.2s
        m_tsm.step();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(1.2, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // Still at 1.2s
        m_tsm.step();
        assertEquals(1.2, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // Stop the state machine. Timer is reset and stays reset while stopped
        m_tsm.stop();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // Still stopped, no step
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        // Start state and step forwards 0.6 seconds
        m_tsm.step();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
    }

    @Test
    public void pause() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));

        // No steps, begin at state A
        assertEquals(m_stateA, m_tsm.getCurrentState());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // Init and exec state A
        m_tsm.step();
        assertEquals(m_stateA, m_tsm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        // No change - no step occurred
        m_transitionNum = 1;
        assertEquals(m_stateA, m_tsm.getCurrentState());
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);
        // Transition: exit state A, init and exec state B
        m_tsm.step();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateB, m_tsm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        // Pause state machine - Exit state B
        m_tsm.pause();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        m_transitionNum = 0;
        assertEquals(m_stateB, m_tsm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(0, 0, 0);

        // Pause a paused state machine - Should do nothing
        m_tsm.pause();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        m_transitionNum = 0;
        assertEquals(m_stateB, m_tsm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(1, 1, 1);
        checkValsC(0, 0, 0);

        // Step progresses again
        m_tsm.step();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(1.2, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals(m_stateB, m_tsm.getCurrentState());
        checkValsA(1, 1, 1);
        checkValsB(2, 2, 1);
        checkValsC(0, 0, 0);
    }

    @Test
    public void getAsStateStopOnExit() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));

        State smAsState = m_tsm.getAsState("TestName", false);

        assertEquals("TestName.A", smAsState.getStateName());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        m_timeSource.addTime(Time.seconds(0.4));
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        smAsState.init();
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(1, 2, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        m_transitionNum = 1;
        smAsState.exec();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(1, 2, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        m_timeSource.addTime(Time.seconds(0.6));
        smAsState.exec();
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 0);
        checkValsC(0, 0, 0);

        smAsState.exit();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        m_transitionNum = 0;
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        m_timeSource.addTime(Time.seconds(0.4));
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        smAsState.init();
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        smAsState.exec();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals("TestName.A", smAsState.getStateName());
        checkValsA(2, 3, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        m_transitionNum = 1;
        smAsState.exec();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(2, 3, 2);
        checkValsB(2, 3, 1);
        checkValsC(0, 0, 0);

        smAsState.exit();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(2, 3, 2);
        checkValsB(2, 3, 2);
        checkValsC(0, 0, 0);
    }

    @Test
    public void getAsStateDontStopOnExit() {
        m_stateA.addTransition(new StaticTransition(m_stateB, () -> m_transitionNum == 1));

        State smAsState = m_tsm.getAsState("TestName", true);

        assertEquals("TestName.A", smAsState.getStateName());
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        m_timeSource.addTime(Time.seconds(0.4));
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        smAsState.init();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(0, 0, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(1, 1, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 2, 0);
        checkValsB(0, 0, 0);
        checkValsC(0, 0, 0);

        m_transitionNum = 1;
        smAsState.exec();
        assertEquals(0.0, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(1, 2, 1);
        checkValsB(1, 1, 0);
        checkValsC(0, 0, 0);

        smAsState.exec();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 0);
        checkValsC(0, 0, 0);

        smAsState.exit();
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        m_transitionNum = 0;
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);

        smAsState.init();
        assertEquals(0.6, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(1, 2, 1);
        checkValsB(1, 2, 1);
        checkValsC(0, 0, 0);

        smAsState.exec();
        m_timeSource.addTime(Time.seconds(0.6));
        assertEquals(1.2, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        assertEquals("TestName.B", smAsState.getStateName());
        checkValsA(1, 2, 1);
        checkValsB(2, 3, 1);
        checkValsC(0, 0, 0);

        smAsState.exec();
        checkValsA(1, 2, 1);
        checkValsB(2, 4, 1);
        checkValsC(0, 0, 0);

        smAsState.exit();
        assertEquals(1.2, m_tsm.getTimeSinceTransition().asSeconds(), k_eps);
        checkValsA(1, 2, 1);
        checkValsB(2, 4, 2);
        checkValsC(0, 0, 0);
    }
}
