/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class Pair_Test {

    @Test
    public void testBasic() {
        Pair<Integer, Double> a = new Pair<>(1, 2.0);
        assertEquals(Integer.valueOf(1), a.getX());
        assertEquals(Double.valueOf(2.0), a.getY());
        assertTrue(a.getX() instanceof Integer);
        assertTrue(a.getY() instanceof Double);

        Pair<Double, String> b = new Pair<>(4.3, "test");
        assertEquals(Double.valueOf(4.3), b.getX());
        assertEquals("test", b.getY());
        assertTrue(b.getX() instanceof Double);
        assertTrue(b.getY() instanceof String);
    }

    @Test
    public void testWrite() {
        Pair<Integer, Double> a = new Pair<>(1, 2.0);
        assertEquals(Integer.valueOf(1), a.getX());
        assertEquals(Double.valueOf(2.0), a.getY());
        a.setX(3);
        assertEquals(Integer.valueOf(3), a.getX());
        assertEquals(Double.valueOf(2.0), a.getY());
        a.setY(5.7);
        assertEquals(Integer.valueOf(3), a.getX());
        assertEquals(Double.valueOf(5.7), a.getY());
    }

    @Test
    public void testEquals() {
        Pair<Integer, Double> a = new Pair<>(1, 2.0);
        Pair<Integer, Double> b = new Pair<>(1, 2.0);
        assertEquals(a, b);
        b.setX(3);
        assertNotEquals(a, b);

        Pair<Double, Double> c = new Pair<>(1.0, 2.0);
        assertNotEquals(a, c);
    }
}
