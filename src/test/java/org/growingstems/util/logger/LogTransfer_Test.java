/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.JavaTimeSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Tests {@link LogClient} and {@link LogServer} by moving files using both. */
public class LogTransfer_Test {
    private File fromDir;
    private File toDir;
    private InetAddress loopbackAddress;

    private class WrittenFile {
        private final File fromFile;
        private final File toFile;

        private WrittenFile(String fileName, byte[] byteData)
                throws FileNotFoundException, IOException {
            this(fileName);

            // Create and close a file
            try (var stream = new FileOutputStream(fromFile)) {
                stream.write(byteData);
            }
        }

        private WrittenFile(String fileName) throws FileNotFoundException, IOException {
            fromFile = new File(fromDir, fileName);
            toFile = new File(toDir, fileName);

            // Create and close a file
            new FileOutputStream(fromFile).close();
        }
    }

    @BeforeEach
    public void createPaths(@TempDir File tempDir) {
        fromDir = new File(tempDir, "from");
        assertTrue(fromDir.mkdir());

        toDir = new File(tempDir, "to");
        assertTrue(toDir.mkdir());
    }

    @BeforeEach
    public void getLoopbackAddress() {
        loopbackAddress = InetAddress.getLoopbackAddress();
    }

    @Test
    public void transferEmptyFile() throws IOException, NoSuchAlgorithmException {
        var params = new WrittenFile("Log.txt");

        // Verify both files still exist, and are empty.
        transfer(false);
        assertEquals(0, getBytes(params.toFile).length);
        assertEquals(0, getBytes(params.fromFile).length);
    }

    @Test
    public void transferAndDeleteEmptyFile() throws IOException, NoSuchAlgorithmException {
        var params = new WrittenFile("Log.txt");

        // Verify both files still exist, and are empty.
        transfer(true);
        assertEquals(0, getBytes(params.toFile).length);
        assertDeleted(params.fromFile);
    }

    @Test
    public void transferSingleFile() throws IOException, NoSuchAlgorithmException {
        var byteData = "Hello world!".getBytes();
        var params = new WrittenFile("Log.txt", byteData);

        // Verify both files still exist, and are empty.
        transfer(false);
        assertTrue(Arrays.equals(byteData, getBytes(params.toFile)));
        assertTrue(Arrays.equals(byteData, getBytes(params.fromFile)));
    }

    @Test
    public void transferAndDeleteSingleFile() throws IOException, NoSuchAlgorithmException {
        var byteData = "Hello world!".getBytes();
        var params = new WrittenFile("Log.txt", byteData);

        // Verify both files still exist, and are empty.
        transfer(true);
        assertTrue(Arrays.equals(byteData, getBytes(params.toFile)));
        assertDeleted(params.fromFile);
    }

    @Test
    public void transferTwoFiles() throws IOException, NoSuchAlgorithmException {
        var byteData1 = "Hello world!".getBytes();
        var byteData2 = "Other Data".getBytes();
        var params1 = new WrittenFile("Log1.txt", byteData1);
        var params2 = new WrittenFile("Log2.txt", byteData2);

        // Verify both files still exist, and are empty.
        transfer(false);
        assertTrue(Arrays.equals(byteData1, getBytes(params1.toFile)));
        assertTrue(Arrays.equals(byteData1, getBytes(params1.fromFile)));
        assertTrue(Arrays.equals(byteData2, getBytes(params2.toFile)));
        assertTrue(Arrays.equals(byteData2, getBytes(params2.fromFile)));
    }

    @Test
    public void transferAndDeleteTwoFiles() throws IOException, NoSuchAlgorithmException {
        var byteData1 = "Hello world!".getBytes();
        var byteData2 = "Other Data".getBytes();
        var params1 = new WrittenFile("Log1.txt", byteData1);
        var params2 = new WrittenFile("Log2.txt", byteData2);

        // Verify both files still exist, and are empty.
        transfer(true);
        assertTrue(Arrays.equals(byteData1, getBytes(params1.toFile)));
        assertDeleted(params1.fromFile);
        assertTrue(Arrays.equals(byteData2, getBytes(params2.toFile)));
        assertDeleted(params2.fromFile);
    }

    @Test
    public void transferTwice() throws IOException, NoSuchAlgorithmException {
        var byteData = "Hello world!".getBytes();
        var params = new WrittenFile("Log.txt", byteData);

        var server = new LogServer(fromDir);
        try {
            server.setServerAddress(loopbackAddress);
            server.setPort(0);
            server.start();
            var boundPort = assertStarted(server);

            var client = new LogClient(toDir);
            client.setPort(boundPort);

            // Read once
            client.readLogs(loopbackAddress, false);
            assertTrue(Arrays.equals(byteData, getBytes(params.toFile)));
            assertTrue(Arrays.equals(byteData, getBytes(params.fromFile)));

            // Cleanup
            assertTrue(params.toFile.delete());
            assertDeleted(params.toFile);

            // Read again
            client.readLogs(loopbackAddress, true);
            assertTrue(Arrays.equals(byteData, getBytes(params.toFile)));
            assertDeleted(params.fromFile);
        } finally {
            server.stop();
            assertStopped(server);
        }
    }

    @Test
    public void restartServer() throws IOException, NoSuchAlgorithmException, InterruptedException {
        var byteData = "Hello world!".getBytes();
        var params = new WrittenFile("Log.txt", byteData);

        var server = new LogServer(fromDir);
        try {
            server.setServerAddress(loopbackAddress);
            server.setPort(0);
            server.start();
            var boundPort = assertStarted(server);

            var client = new LogClient(toDir);
            client.setPort(boundPort);

            // Read once
            client.readLogs(loopbackAddress, false);
            assertTrue(Arrays.equals(byteData, getBytes(params.toFile)));
            assertTrue(Arrays.equals(byteData, getBytes(params.fromFile)));

            // Cleanup
            assertTrue(params.toFile.delete());
            assertDeleted(params.toFile);

            // Restart server using another port
            server.stop();
            assertStopped(server);
            server.start();
            boundPort = assertStarted(server);

            // Read again
            client.setPort(boundPort);

            client.readLogs(loopbackAddress, true);
            assertTrue(Arrays.equals(byteData, getBytes(params.toFile)));
            assertDeleted(params.fromFile);
        } finally {
            server.stop();
            assertStopped(server);
        }
    }

    private byte[] getBytes(File file) throws IOException {
        try (var stream = new FileInputStream(file)) {
            return stream.readAllBytes();
        }
    }

    private void transfer(boolean deleteFiles) throws IOException, NoSuchAlgorithmException {
        var server = new LogServer(fromDir);
        try {
            server.setServerAddress(loopbackAddress);
            server.setPort(0);
            server.start();
            var boundPort = assertStarted(server);

            var client = new LogClient(toDir);
            client.setPort(boundPort);
            client.readLogs(loopbackAddress, deleteFiles);
        } finally {
            server.stop();
            assertStopped(server);
        }
    }

    private void assertDeleted(File file) {
        assertWithTimeout(
                () -> !file.exists(), Time.seconds(1.0), "File " + file.toString() + " was never deleted!");
    }

    private int assertStarted(LogServer server) {
        return assertWithTimeout(
                        () -> server.getBoundPort(),
                        Optional::isPresent,
                        Time.seconds(1.0),
                        "Server never finished binding!")
                .get();
    }

    private void assertStopped(LogServer server) {
        assertWithTimeout(() -> !server.isBound(), Time.seconds(1.0), "Server never stopped!");
    }

    private void assertWithTimeout(
            Supplier<Boolean> successCondition, Time timeout, String failMessage) {
        assertWithTimeout(successCondition, b -> b, timeout, failMessage);
    }

    private <T> T assertWithTimeout(
            Supplier<T> conditionSubject,
            Predicate<T> successCondition,
            Time timeout,
            String failMessage) {
        var timer = new JavaTimeSource().createTimer().start();
        while (!timer.hasElapsed(timeout)) {
            var subject = conditionSubject.get();
            if (successCondition.test(subject)) {
                return subject;
            }
        }
        fail(failMessage);
        return null;
    }
}
