/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigInteger;
import org.junit.jupiter.api.Test;

public class Unsigned_Test {
    @Test
    public void unsignedByte() {
        UnsignedByte b = new UnsignedByte((byte) 0);
        assertEquals(b.asByte(), 0);
        assertEquals(b.asUnsigned(), 0);

        b = new UnsignedByte((byte) 100);
        assertEquals(b.asByte(), 100);
        assertEquals(b.asUnsigned(), 100);

        b = new UnsignedByte((byte) 200);
        assertEquals(b.asByte(), -56);
        assertEquals(b.asUnsigned(), 200);

        b = new UnsignedByte((byte) 256);
        assertEquals(b.asByte(), 0);
        assertEquals(b.asUnsigned(), 0);
    }

    @Test
    public void unsignedShort() {
        UnsignedShort s = new UnsignedShort((short) 0);
        assertEquals(s.asShort(), 0);
        assertEquals(s.asUnsigned(), 0);

        s = new UnsignedShort((short) 100);
        assertEquals(s.asShort(), 100);
        assertEquals(s.asUnsigned(), 100);

        s = new UnsignedShort((short) 200);
        assertEquals(s.asShort(), 200);
        assertEquals(s.asUnsigned(), 200);

        s = new UnsignedShort((short) 500);
        assertEquals(s.asShort(), 500);
        assertEquals(s.asUnsigned(), 500);

        s = new UnsignedShort((short) 32700);
        assertEquals(s.asShort(), 32700);
        assertEquals(s.asUnsigned(), 32700);

        s = new UnsignedShort((short) 40000);
        assertEquals(s.asShort(), -25536);
        assertEquals(s.asUnsigned(), 40000);

        s = new UnsignedShort((short) 65535);
        assertEquals(s.asShort(), -1);
        assertEquals(s.asUnsigned(), 65535);

        s = new UnsignedShort((short) 65536);
        assertEquals(s.asShort(), 0);
        assertEquals(s.asUnsigned(), 0);
    }

    @Test
    public void unsignedInt() {
        UnsignedInt i = new UnsignedInt((int) 0l);
        assertEquals(i.asInt(), 0);
        assertEquals(i.asUnsigned(), 0l);

        i = new UnsignedInt((int) 100l);
        assertEquals(i.asInt(), 100);
        assertEquals(i.asUnsigned(), 100l);

        i = new UnsignedInt((int) 500l);
        assertEquals(i.asInt(), 500);
        assertEquals(i.asUnsigned(), 500l);

        i = new UnsignedInt((int) 40000l);
        assertEquals(i.asInt(), 40000);
        assertEquals(i.asUnsigned(), 40000l);

        i = new UnsignedInt((int) 70000l);
        assertEquals(i.asInt(), 70000);
        assertEquals(i.asUnsigned(), 70000l);

        i = new UnsignedInt((int) 2147483647l);
        assertEquals(i.asInt(), 2147483647);
        assertEquals(i.asUnsigned(), 2147483647l);

        i = new UnsignedInt((int) 2147483648l);
        assertEquals(i.asInt(), -2147483648);
        assertEquals(i.asUnsigned(), 2147483648l);

        i = new UnsignedInt((int) 4294967295l);
        assertEquals(i.asInt(), -1);
        assertEquals(i.asUnsigned(), 4294967295l);

        i = new UnsignedInt((int) 4294967296l);
        assertEquals(i.asInt(), 0);
        assertEquals(i.asUnsigned(), 0l);
    }

    @Test
    public void unsignedLong() {
        UnsignedLong l = new UnsignedLong(0l);
        assertEquals(l.asLong(), 0);
        assertEquals(l.asUnsigned(), new BigInteger("0"));

        l = new UnsignedLong(100l);
        assertEquals(l.asLong(), 100);
        assertEquals(l.asUnsigned(), new BigInteger("100"));

        l = new UnsignedLong(500l);
        assertEquals(l.asLong(), 500);
        assertEquals(l.asUnsigned(), new BigInteger("500"));

        l = new UnsignedLong(40000l);
        assertEquals(l.asLong(), 40000);
        assertEquals(l.asUnsigned(), new BigInteger("40000"));

        l = new UnsignedLong(70000l);
        assertEquals(l.asLong(), 70000);
        assertEquals(l.asUnsigned(), new BigInteger("70000"));

        l = new UnsignedLong(2147483647l);
        assertEquals(l.asLong(), 2147483647);
        assertEquals(l.asUnsigned(), new BigInteger("2147483647"));

        l = new UnsignedLong(2147483648l);
        assertEquals(l.asLong(), 2147483648l);
        assertEquals(l.asUnsigned(), new BigInteger("2147483648"));

        l = new UnsignedLong(4294967295l);
        assertEquals(l.asLong(), 4294967295l);
        assertEquals(l.asUnsigned(), new BigInteger("4294967295"));

        l = new UnsignedLong(4294967296l);
        assertEquals(l.asLong(), 4294967296l);
        assertEquals(l.asUnsigned(), new BigInteger("4294967296"));

        l = new UnsignedLong(-1000000000000000000l);
        assertEquals(l.asLong(), -1000000000000000000l);
        assertEquals(l.asUnsigned(), new BigInteger("17446744073709551616"));
    }
}
