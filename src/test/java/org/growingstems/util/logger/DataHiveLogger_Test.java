/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.util.logger;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.PrintStreamTest;
import org.growingstems.test.TestCore;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class DataHiveLogger_Test extends PrintStreamTest {
    protected TestTimeSource timeSource = new TestTimeSource();

    protected static class Foo {
        protected final int m_a;
        protected final double m_b;

        protected Foo() {
            this(0, 0.0);
        }

        protected Foo(int a, double b) {
            m_a = a;
            m_b = b;
        }

        protected int getA() {
            return m_a;
        }

        protected double getB() {
            return m_b;
        }

        @Override
        public boolean equals(Object other) {

            if (this == other) {
                return true;
            }

            if (other == null || getClass() != other.getClass()) {
                return false;
            }

            Foo foo = (Foo) other;
            return this.m_a == foo.m_a && this.m_b == foo.m_b;
        }

        @Override
        public int hashCode() {
            return Objects.hash(m_a, m_b);
        }
    }

    protected static class Bar {
        protected final boolean m_isTrue;
        protected final Foo m_foo;

        protected Bar() {
            this(false, new Foo());
        }

        protected Bar(boolean isTrue, Foo foo) {
            m_isTrue = isTrue;
            m_foo = foo;
        }

        protected boolean isTrue() {
            return m_isTrue;
        }

        protected Foo getFoo() {
            return m_foo;
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }

            if (other == null || getClass() != other.getClass()) {
                return false;
            }

            Bar bar = (Bar) other;
            return this.m_isTrue == bar.m_isTrue && this.m_foo.equals(bar.m_foo);
        }

        @Override
        public int hashCode() {
            return Objects.hash(m_isTrue, m_foo);
        }
    }

    protected enum TestEnum {
        A,
        B,
        C,
        D
    }

    protected class TestClassWithArray {
        protected final Boolean[] bools;
        protected final int nonArrayInt;
        protected final TestEnum[] enums;

        protected TestClassWithArray() {
            this(new Boolean[0], 0, new TestEnum[0]);
        }

        protected TestClassWithArray(Boolean[] bools, int nonArrayInt, TestEnum[] enums) {
            this.bools = bools;
            this.nonArrayInt = nonArrayInt;
            this.enums = enums;
        }

        protected Boolean[] getBools() {
            return bools;
        }

        protected int getNonArrayInt() {
            return nonArrayInt;
        }

        protected TestEnum[] getEnums() {
            return enums;
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }

            if (other == null || getClass() != other.getClass()) {
                return false;
            }

            TestClassWithArray testClass = (TestClassWithArray) other;
            return Arrays.equals(bools, testClass.bools)
                    && Arrays.equals(enums, testClass.enums)
                    && nonArrayInt == testClass.nonArrayInt;
        }

        @Override
        public int hashCode() {
            return Objects.hash(bools, enums, nonArrayInt);
        }
    }

    protected String getString(ByteBuffer buffer) {
        String string = "";
        byte currentChar = buffer.get();
        while (currentChar != '\0') {
            // Force UTF-8 via bit masking
            string = string + (char) (currentChar & 0xFF);
            currentChar = buffer.get();
        }
        return string;
    }

    protected Boolean getBoolean(ByteBuffer buffer) {
        return buffer.get() != 0;
    }

    protected void verifyFileIdentifier(ByteBuffer fileBuffer) {
        assertEquals('D', fileBuffer.get());
        assertEquals('H', fileBuffer.get());
        assertEquals('L', fileBuffer.get());
        assertEquals('L', fileBuffer.get());
        assertEquals('O', fileBuffer.get());
        assertEquals('G', fileBuffer.get());
    }

    protected int getExpectedEmptyLogFileLength(String fileDescription) {
        /*
         *                            File Header: 6 + 2 + 20 + (description length + 1) + 1 + 8 = 53
         * Group Log Entry Type Description Table: 2
         *           Asynchronous Log Entry Table: 2
         *            Synchronous Log Entry Table: 2
         *                                  Total: 44 + description length
         */

        return 44 + fileDescription.length();
    }

    protected void validateEmptyLog(byte[] byteData, String fileDescription) {
        ByteBuffer fileBuffer = ByteBuffer.wrap(byteData);

        assertEquals(getExpectedEmptyLogFileLength(fileDescription), byteData.length);

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0, fileBuffer.get()); // File Options
        assertEquals(0.0, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(0, fileBuffer.getShort()); // Log Entry Type Group Table Size
        // Asynchronous Log Entry Table
        assertEquals(0, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        // Synchronous Log Entry Table
        assertEquals(0, fileBuffer.getShort()); // Synchronous Log Entry Table Size
        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void emptyLogger() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create and Initialize Logger
        String fileDescription = "There's nothing here?";
        new DataHiveLogger.Builder(outStream, timeSource, fileDescription).init();
        assertNoError();

        // Test file
        validateEmptyLog(outStream.toByteArray(), fileDescription);
    }

    @Test
    public void maxTimestamp() {
        assertEquals(DataHiveLogger.getMaxTimestamp(Time.ZERO).asSeconds(), 0.0, 0.0);
        assertEquals(
                DataHiveLogger.getMaxTimestamp(Time.seconds(1.0)).asSeconds(),
                Integer.MAX_VALUE,
                TestCore.k_eps);
        assertEquals(
                DataHiveLogger.getMaxTimestamp(Time.microseconds(1.0)).asMinutes(), 35.7913941, 0.000001);

        assertEquals(DataHiveLogger.getFixedTimestampUnit(Time.ZERO).asSeconds(), 0.0, 0.0);
        assertEquals(
                DataHiveLogger.getFixedTimestampUnit(Time.seconds(Integer.MAX_VALUE)).asSeconds(),
                1.0,
                TestCore.k_eps);
        assertEquals(
                DataHiveLogger.getFixedTimestampUnit(Time.minutes(35.7913941)).asMicroseconds(),
                1.0,
                0.000001);
    }

    @Test
    public void fixedTimestamp() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create and Initialize Logger
        String fileDescription = "Timestamps are ints!";
        Time fixedTimestampUnit = Time.seconds(0.1);
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fixedTimestampUnit, fileDescription);
        assertNoError();

        // Register Log Entries

        // Asynchronous Entries
        Consumer<Double> asyncDouble =
                logBuilder.makeAsyncLogEntry("Async Double", logBuilder.doubleType);
        assertNoError();

        // Synchronous Entries
        Consumer<Long> syncLong = logBuilder.makeSyncLogEntry("Sync Long", logBuilder.longType, 0l);
        assertNoError();

        // Initialize log
        DataHiveLogger logger = logBuilder.init();
        assertNoError();

        // Verify logging functionality
        asyncDouble.accept(5.2);
        syncLong.accept(2l);
        logger.update();
        timeSource.addTime(Time.seconds(1.0));
        asyncDouble.accept(2.1);
        syncLong.accept(24l);
        logger.update();
        timeSource.addTime(Time.seconds(0.2));
        asyncDouble.accept(-0.78);
        syncLong.accept(56l);
        timeSource.addTime(Time.seconds(0.3));
        asyncDouble.accept(0.8888);
        syncLong.accept(874l);
        logger.update();

        // Test file
        ByteBuffer fileBuffer = ByteBuffer.wrap(outStream.toByteArray());

        /*
         *                            File Header: 6 + 2 + 20 + 21 + 1 + 8 = 52
         * Group Log Entry Type Description Table: 2
         *           Asynchronous Log Entry Table: 2 + 13 + 2 = 17
         *            Synchronous Log Entry Table: 2 + 10 + 2 = 14
         *                               Messages: 13 * 7 = 78
         *                                  Total: 182
         */
        assertEquals(182, outStream.size());

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0x2, fileBuffer.get()); // File Options - Fixed Timestamp
        assertEquals(0.1, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(0, fileBuffer.getShort()); // Log Entry Type Group Table Size

        // Asynchronous Log Entry Table
        assertEquals(1, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        assertEquals("Async Double", getString(fileBuffer)); // Async Entry Name #1
        assertEquals(10, fileBuffer.getShort()); // Async Entry's Type ID - Double(10)

        // Synchronous Log Entry Table
        assertEquals(1, fileBuffer.getShort()); // Synchronous Log Entry Table Size
        assertEquals("Sync Long", getString(fileBuffer)); // Sync Entry Name #1
        assertEquals(8, fileBuffer.getShort()); // Sync Entry's Type ID - Signed Long(8)

        // Messages
        // Async "Async double" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(0, fileBuffer.getInt()); // Timestamp
        assertEquals(5.2, fileBuffer.getDouble()); // Value

        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(0, fileBuffer.getInt()); // Timestamp
        assertEquals(2L, fileBuffer.getLong()); // "Sync Long" - Value

        // Async "Async double" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(10, fileBuffer.getInt()); // Timestamp
        assertEquals(2.1, fileBuffer.getDouble()); // Value

        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(10, fileBuffer.getInt()); // Timestamp
        assertEquals(24L, fileBuffer.getLong()); // "Sync Long" - Value

        // Async "Async double" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(12, fileBuffer.getInt()); // Timestamp
        assertEquals(-0.78, fileBuffer.getDouble()); // Value

        // Async "Async double" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(15, fileBuffer.getInt()); // Timestamp
        assertEquals(0.8888, fileBuffer.getDouble()); // Value

        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(15, fileBuffer.getInt()); // Timestamp
        assertEquals(874l, fileBuffer.getLong()); // "Sync Long" - Value

        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void groupTypeUsage() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create Logger
        String fileDescription = "Test DHL File";
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fileDescription);

        // Register custom types
        var fooType = logBuilder
                .<Foo>buildGroupType("Foo")
                .addMember("A", logBuilder.integerType, Foo::getA)
                .addMember("B", logBuilder.doubleType, Foo::getB)
                .register(new Foo());
        assertNoError();
        var barType = logBuilder
                .<Bar>buildGroupType("It's a Bar")
                .addMember("isTrue", logBuilder.booleanType, Bar::isTrue)
                .addMember("Bar's Foo", fooType, Bar::getFoo)
                .register(new Bar());
        assertNoError();

        // Register Log Entries

        // Asynchronous Entries
        Consumer<Double> asyncDouble =
                logBuilder.makeAsyncLogEntry("Async double", logBuilder.doubleType);
        Consumer<Bar> asyncBar = logBuilder.makeAsyncLogEntry("Async bar", barType);
        Consumer<Foo> asyncFoo = logBuilder.makeAsyncLogEntry("Async foo", fooType);
        assertNoError();

        // Synchronous Entries
        Consumer<Bar> syncBar =
                logBuilder.makeSyncLogEntry("Sync bar", barType, new Bar(true, new Foo(0, 0.0)));
        Consumer<Long> syncLong = logBuilder.makeSyncLogEntry("Sync Long", logBuilder.longType, 0l);
        Consumer<Foo> syncFoo = logBuilder.makeSyncLogEntry("Sync Foo", fooType, new Foo(3, 6.4));
        Consumer<String> syncString =
                logBuilder.makeSyncLogEntry("Sync string", logBuilder.stringType, "");
        assertNoError();

        // Initialize log
        DataHiveLogger logger = logBuilder.init();
        assertNoError();

        // Verify logging functionality
        asyncDouble.accept(5.2);
        timeSource.addTime(Time.seconds(1.0));
        assertNoError();
        syncLong.accept(3L);
        assertNoError();
        asyncFoo.accept(new Foo(6, Math.PI));
        assertNoError();
        timeSource.addTime(Time.seconds(1.0));
        syncString.accept("Hello World!");
        syncBar.accept(new Bar(true, new Foo(1, 2.3)));
        assertNoError();
        timeSource.addTime(Time.seconds(1.0));
        asyncBar.accept(new Bar(false, new Foo(3, 2.1)));
        logger.update();
        assertNoError();
        syncFoo.accept(
                null); // Honestly just to now have a warning about syncFoo being unused, but also verifies
        // this doesn't get used anymore.
        assertNoError();

        // Test file
        ByteBuffer fileBuffer = ByteBuffer.wrap(outStream.toByteArray());

        /*
         *                            File Header: 6 + 2 + 20 + 14 + 1 + 8 = 51
         * Group Log Entry Type Description Table: 2 + 4 + 2 + 2 + 1 + 2 + 2 + 1 + 2 +
         *                                         11 + + 2+ 7 + 1 + 2 + 10 + 1 + 2 = 54
         *           Asynchronous Log Entry Table: 2 + 13 + 2 + 10 + 2 + 10 + 2 = 41
         *            Synchronous Log Entry Table: 2 + 9 + 2 + 10 + 2 + 9 + 2 + 12 + 2 = 50
         *                               Messages: (1 + 4 + 8) + (1 + 4 + 4 + 8) +
         *                                         (1 + 4 + 1 + 4 + 8) +
         *                                         (1 + 4 + 1 + 4 + 8 + 8 + 4 + 8 + 13) = 99
         *                                  Total: 295
         */
        assertEquals(295, outStream.size());

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0, fileBuffer.get()); // File Options
        assertEquals(0.0, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(2, fileBuffer.getShort()); // Log Entry Type Group Table Size
        // First Group
        assertEquals("Foo", getString(fileBuffer)); // First Group Label
        assertEquals(2, fileBuffer.getShort()); // Total Types in Group
        assertEquals("A", getString(fileBuffer)); // First Type's Label
        assertEquals(0x0, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(7, fileBuffer.getShort()); // First Type's ID - Signed Int(7)
        assertEquals("B", getString(fileBuffer)); // Second Type's Label
        assertEquals(0x0, fileBuffer.get()); // Second Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(10, fileBuffer.getShort()); // Second Type's ID - Double(10)
        // Second Group
        assertEquals("It's a Bar", getString(fileBuffer)); // First Group Label
        assertEquals(2, fileBuffer.getShort()); // Total Types in Group
        assertEquals("isTrue", getString(fileBuffer)); // First Type's Label
        assertEquals(0x0, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(11, fileBuffer.getShort()); // First Type's ID - Boolean(11)
        assertEquals("Bar's Foo", getString(fileBuffer)); // Second Type's Label
        assertEquals(0x0, fileBuffer.get()); // Second Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(64, fileBuffer.getShort()); // Second Type's ID - Foo Group Entry Type(64)

        // Asynchronous Log Entry Table
        assertEquals(3, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        assertEquals("Async double", getString(fileBuffer)); // Async Entry Name
        assertEquals(10, fileBuffer.getShort()); // Async Entry's Type ID - Double(10)
        assertEquals("Async bar", getString(fileBuffer)); // Async Entry Name
        assertEquals(65, fileBuffer.getShort()); // Async Entry's Type ID - Bar Group Entry Type(65)
        assertEquals("Async foo", getString(fileBuffer)); // Async Entry Name
        assertEquals(64, fileBuffer.getShort()); // Async Entry's Type ID - Foo Group Entry Type(64)

        // Synchronous Log Entry Table
        assertEquals(4, fileBuffer.getShort()); // Synchronous Log Entry Table Size
        assertEquals("Sync bar", getString(fileBuffer)); // Sync Entry Name
        assertEquals(65, fileBuffer.getShort()); // Sync Entry's Type ID - Bar Group Entry Type(65)
        assertEquals("Sync Long", getString(fileBuffer)); // Sync Entry Name
        assertEquals(8, fileBuffer.getShort()); // Sync Entry's Type ID - Signed Long(8)
        assertEquals("Sync Foo", getString(fileBuffer)); // Sync Entry Name
        assertEquals(64, fileBuffer.getShort()); // Sync Entry's Type ID - Foo Group Entry Type(64)
        assertEquals("Sync string", getString(fileBuffer)); // Sync Entry Name
        assertEquals(12, fileBuffer.getShort()); // Sync Entry's Type ID - String(12)

        // Messages
        // Async "Async double" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(5.2, fileBuffer.getDouble()); // Value
        // Async "Async foo" Message
        assertEquals(3, fileBuffer.get()); // Async ID
        assertEquals(1.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(6, fileBuffer.getInt()); // Value
        assertEquals(Math.PI, fileBuffer.getDouble()); // Value
        // Async "Async bar" Message
        assertEquals(2, fileBuffer.get()); // Async ID
        assertEquals(3.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(false, getBoolean(fileBuffer)); // Value
        assertEquals(3, fileBuffer.getInt()); // Value
        assertEquals(2.1, fileBuffer.getDouble()); // Value

        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(3.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(true, getBoolean(fileBuffer)); // "Sync Bar" - Value
        assertEquals(1, fileBuffer.getInt()); // "Sync Bar" - Value
        assertEquals(2.3, fileBuffer.getDouble()); // "Sync Bar" - Value
        assertEquals(3L, fileBuffer.getLong()); // "Sync Long" - Value
        assertEquals(3, fileBuffer.getInt()); // "Sync Foo" - Value
        assertEquals(6.4, fileBuffer.getDouble()); // "Sync Foo" - Value
        assertEquals("Hello World!", getString(fileBuffer)); // "Sync string" - Value

        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void loggingEnums() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create Logger
        String fileDescription = "There's nothing here?";
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fileDescription);

        // Register Log Entries
        var testEnumEntry = logBuilder.registerEnum(TestEnum.class, TestEnum.A);
        Consumer<TestEnum> asyncEnum = logBuilder.makeAsyncLogEntry("Async Enum", testEnumEntry);
        Consumer<TestEnum> syncEnum =
                logBuilder.makeSyncLogEntry("Sync Enum", testEnumEntry, TestEnum.B);

        // Initialize log
        DataHiveLogger logger = logBuilder.init();
        assertNoError();

        // Verify logging functionality
        asyncEnum.accept(TestEnum.A);
        timeSource.addTime(Time.seconds(1.0));
        assertNoError();
        logger.update();
        assertNoError();
        syncEnum.accept(TestEnum.C);
        logger.update();
        assertNoError();
        syncEnum.accept(TestEnum.D);
        logger.update();
        assertNoError();
        timeSource.addTime(Time.seconds(1.0));
        syncEnum.accept(TestEnum.B);
        logger.update();
        syncEnum.accept(TestEnum.D);
        assertNoError();
        timeSource.addTime(Time.seconds(1.0));
        asyncEnum.accept(TestEnum.C); // Async before sync update
        logger.update();
        assertNoError();

        // Test file
        ByteBuffer fileBuffer = ByteBuffer.wrap(outStream.toByteArray());

        /*
         *                            File Header: 6 + 2 + 20 + 22 + 1 + 8 = 53
         * Group Log Entry Type Description Table: 2 + 9 + 2 + 9 + 1 + 2 + 4 + 4*(1 + 2) = 41
         *           Asynchronous Log Entry Table: 2 + 11 + 2 = 15
         *            Synchronous Log Entry Table: 2 + 10 + 2 = 14
         *                               Messages: 2*(1 + 4 + 1) + 5*(1 + 4 + 1) = 42
         *                                  Total: 171
         */
        assertEquals(171, outStream.size());

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0, fileBuffer.get()); // File Options
        assertEquals(0.0, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(1, fileBuffer.getShort()); // Log Entry Type Group Table Size
        assertEquals("TestEnum", getString(fileBuffer)); // First Group Label
        assertEquals(1, fileBuffer.getShort()); // Total Types in Group
        assertEquals("TestEnum", getString(fileBuffer)); // First Type's Label
        assertEquals(0x2, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(1, fileBuffer.getShort()); // First Type's ID
        assertEquals(4, fileBuffer.getInt()); // Total Enum Value/String pairs
        assertEquals(0, fileBuffer.get()); // Enum Value
        assertEquals("A", getString(fileBuffer)); // Enum String
        assertEquals(1, fileBuffer.get()); // Enum Value
        assertEquals("B", getString(fileBuffer)); // Enum String
        assertEquals(2, fileBuffer.get()); // Enum Value
        assertEquals("C", getString(fileBuffer)); // Enum String
        assertEquals(3, fileBuffer.get()); // Enum Value
        assertEquals("D", getString(fileBuffer)); // Enum String

        // Asynchronous Log Entry Table
        assertEquals(1, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        assertEquals("Async Enum", getString(fileBuffer)); // Async Entry Name
        assertEquals(64, fileBuffer.getShort()); // Async Entry's Type ID

        // Synchronous Log Entry Table
        assertEquals(1, fileBuffer.getShort()); // Synchronous Log Entry Table Size
        assertEquals("Sync Enum", getString(fileBuffer)); // Sync Entry Name
        assertEquals(64, fileBuffer.getShort()); // Sync Entry's Type ID

        // Messages
        // Async "Async Enum" Message
        assertEquals(1, fileBuffer.get()); // Async "Async Enum" ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(0, fileBuffer.get()); // Enum Value - A
        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(1.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(1, fileBuffer.get()); // Enum Value - B
        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(1.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(2, fileBuffer.get()); // Enum Value - C
        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(1.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3, fileBuffer.get()); // Enum Value - D
        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(2.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(1, fileBuffer.get()); // Enum Value - B
        // Async "Async Enum" Message
        assertEquals(1, fileBuffer.get()); // Async "Async Enum" ID
        assertEquals(3.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(2, fileBuffer.get()); // Enum Value - C
        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(3.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3, fileBuffer.get()); // Enum Value - D

        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void loggingArrays() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create Logger
        String fileDescription = "Arrays!";
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fileDescription);

        // Register Log Entries
        LogEntryType<TestEnum> testEnumType =
                logBuilder.registerEnum(TestEnum.class, "Test Enum", TestEnum.A);
        LogEntryType<TestEnum[]> testEnumArrayType = logBuilder.toArray(testEnumType, new TestEnum[0]);
        LogEntryType<TestClassWithArray> testClassWithArrayType = logBuilder
                .<TestClassWithArray>buildGroupType("TestClassWithArray")
                .addArrayMember("Bool Array", logBuilder.booleanType, TestClassWithArray::getBools)
                .addMember("Int - not an array", logBuilder.integerType, TestClassWithArray::getNonArrayInt)
                .addArrayMember("Enum Array", testEnumType, TestClassWithArray::getEnums)
                .register(new TestClassWithArray());
        LogEntryType<TestClassWithArray[]> testClassWithArrayArrayType =
                logBuilder.toArray(testClassWithArrayType, new TestClassWithArray[0]);

        Consumer<TestEnum[]> asyncEnumArray =
                logBuilder.makeAsyncLogEntry("Async Enum Array", testEnumArrayType);
        Consumer<TestEnum[]> syncEnumArray =
                logBuilder.makeSyncLogEntry("Sync Enum Array", testEnumArrayType, new TestEnum[0]);
        Consumer<TestClassWithArray> asyncClassWithArray =
                logBuilder.makeAsyncLogEntry("Async Array Class", testClassWithArrayType);
        Consumer<TestClassWithArray[]> syncClassArrayWithArray = logBuilder.makeSyncLogEntry(
                "Sync Array of Array Class", testClassWithArrayArrayType, new TestClassWithArray[0]);

        // Initialize log
        DataHiveLogger logger = logBuilder.init();
        assertNoError();

        // Write messages
        // Example: Logging a List
        List<TestEnum> enumList = Arrays.asList(TestEnum.A, TestEnum.B, TestEnum.C);
        asyncEnumArray.accept(enumList.toArray(TestEnum[]::new));
        // Example: Logging an array
        syncEnumArray.accept(new TestEnum[] {TestEnum.B, TestEnum.C});
        timeSource.addTime(Time.seconds(1.0));
        asyncClassWithArray.accept(
                new TestClassWithArray(new Boolean[] {true, true, false}, 5, new TestEnum[] {TestEnum.A}));
        timeSource.addTime(Time.seconds(1.0));
        syncClassArrayWithArray.accept(new TestClassWithArray[] {
            new TestClassWithArray(new Boolean[] {true, true, false}, 6, new TestEnum[] {TestEnum.A}),
            new TestClassWithArray(new Boolean[] {}, 7, new TestEnum[] {}),
            new TestClassWithArray(new Boolean[] {false}, 8, new TestEnum[] {TestEnum.D, TestEnum.C})
        });
        logger.update();
        assertNoError();

        // Test file
        ByteBuffer fileBuffer = ByteBuffer.wrap(outStream.toByteArray());

        /*
         *                            File Header: 6 + 2 + 20 + 8 + 1 + 8                        = 45
         *              Group Log Entry Type Size: 2                                             = 2
         *                Group Log Entry Type #1: 10 + 2 + 10 + 1 + 2 + 4 + 4*(1 + 2)           = 41
         *                Group Log Entry Type #2: 12 + 2 + 12 + 1 + 2                           = 29
         *                Group Log Entry Type #3: 19 + 2 + 11 + 1 + 2 + 19 + 1 + 2 + 11 + 1 + 2 = 71
         *                Group Log Entry Type #4: 21 + 2 + 21 + 1 + 2                           = 47
         *           Asynchronous Log Entry Table: 2 + 17 + 2 + 18 + 2                           = 41
         *            Synchronous Log Entry Table: 2 + 16 + 2 + 26 + 2                           = 48
         *                        Async Message 1: 1 + 4 + 2 + 3*(1)                             = 10
         *                        Async Message 2: 1 + 4 + 2 + 3*(1) + 4 + 2 + 1*(1)             = 17
         *                    Sync Message Header: 1 + 4                                         = 5
         *                        Sync Enum Array: 2 + 2*(1)                                     = 4
         *              Size TestClassWithArray[]: 2                                             = 2
         *                   TestClassWithArray 1: 2 + 3*(1) + 4 + 2 + 1*(1)                     = 12
         *                   TestClassWithArray 2: 2 + 0*(1) + 4 + 2 + 0*(1)                     = 8
         *                   TestClassWithArray 3: 2 + 1*(1) + 4 + 2 + 2*(1)                     = 11
         *                                  Total: 393
         */
        assertEquals(393, outStream.size());

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0, fileBuffer.get()); // File Options
        assertEquals(0.0, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(4, fileBuffer.getShort()); // Log Entry Type Group Table Size
        // First group type
        assertEquals("Test Enum", getString(fileBuffer)); // First Group Label
        assertEquals(1, fileBuffer.getShort()); // Total Types in Group
        assertEquals("Test Enum", getString(fileBuffer)); // First Type's Label
        assertEquals(0x2, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(1, fileBuffer.getShort()); // First Type's ID
        assertEquals(4, fileBuffer.getInt()); // Total Enum Value/String pairs
        assertEquals(0, fileBuffer.get()); // Enum Value
        assertEquals("A", getString(fileBuffer)); // Enum String
        assertEquals(1, fileBuffer.get()); // Enum Value
        assertEquals("B", getString(fileBuffer)); // Enum String
        assertEquals(2, fileBuffer.get()); // Enum Value
        assertEquals("C", getString(fileBuffer)); // Enum String
        assertEquals(3, fileBuffer.get()); // Enum Value
        assertEquals("D", getString(fileBuffer)); // Enum String
        // Second group type
        assertEquals("Test Enum[]", getString(fileBuffer)); // Third Group Label
        assertEquals(1, fileBuffer.getShort()); // Total Types in Group
        assertEquals("Test Enum[]", getString(fileBuffer)); // First Type's Label
        assertEquals(0x1, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(64, fileBuffer.getShort()); // First Type's ID
        // Third group type
        assertEquals("TestClassWithArray", getString(fileBuffer)); // Second Group Label
        assertEquals(3, fileBuffer.getShort()); // Total Types in Group
        assertEquals("Bool Array", getString(fileBuffer)); // First Type's Label
        assertEquals(0x1, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(11, fileBuffer.getShort()); // First Type's ID
        assertEquals("Int - not an array", getString(fileBuffer)); // Second Type's Label
        assertEquals(0x0, fileBuffer.get()); // Second Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(7, fileBuffer.getShort()); // Second Type's ID
        // The second type is an enum array, but the way it's defined by the DataHiveLogger is an
        // array of an independently defined group type, so the element itself is not considered
        // an enum in this context.
        assertEquals("Enum Array", getString(fileBuffer)); // Third Type's Label
        assertEquals(0x1, fileBuffer.get()); // Third Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(64, fileBuffer.getShort()); // Third Type's ID
        // Fourth group type
        assertEquals("TestClassWithArray[]", getString(fileBuffer)); // Fourth Group Label
        assertEquals(1, fileBuffer.getShort()); // Total Types in Group
        assertEquals("TestClassWithArray[]", getString(fileBuffer)); // First Type's Label
        assertEquals(0x1, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(66, fileBuffer.getShort()); // First Type's ID

        // Asynchronous Log Entry Table
        assertEquals(2, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        assertEquals("Async Enum Array", getString(fileBuffer)); // Async Entry Name #1
        assertEquals(65, fileBuffer.getShort()); // Async Entry's Type ID #2
        assertEquals("Async Array Class", getString(fileBuffer)); // Async Entry Name #1
        assertEquals(66, fileBuffer.getShort()); // Async Entry's Type ID #2

        // Synchronous Log Entry Table
        assertEquals(2, fileBuffer.getShort()); // Synchronous Log Entry Table Size
        assertEquals("Sync Enum Array", getString(fileBuffer)); // Sync Entry Name #1
        assertEquals(65, fileBuffer.getShort()); // Sync Entry's Type ID #2
        assertEquals("Sync Array of Array Class", getString(fileBuffer)); // Sync Entry Name #1
        assertEquals(67, fileBuffer.getShort()); // Sync Entry's Type ID #2

        // Messages
        // Async Enum Array Message
        assertEquals(1, fileBuffer.get()); // Async Enum Array ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3, fileBuffer.getShort()); // Enum Array Size
        assertEquals(0, fileBuffer.get()); // Enum Value - A
        assertEquals(1, fileBuffer.get()); // Enum Value - B
        assertEquals(2, fileBuffer.get()); // Enum Value - C
        // Async Array Class Message
        assertEquals(2, fileBuffer.get()); // Async Array Class ID
        assertEquals(1.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3, fileBuffer.getShort()); // Boolean array size
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertFalse(getBoolean(fileBuffer)); // Boolean - false
        assertEquals(5, fileBuffer.getInt()); // Integer 5
        assertEquals(1, fileBuffer.getShort()); // Enum array size
        assertEquals(0, fileBuffer.get()); // Enum - A
        // Sync message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(2.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(2, fileBuffer.getShort()); // Enum array size
        assertEquals(1, fileBuffer.get()); // Enum - B
        assertEquals(2, fileBuffer.get()); // Enum - C
        assertEquals(3, fileBuffer.getShort()); // TestClassWithArray array size
        // TestClassWithArray 1
        assertEquals(3, fileBuffer.getShort()); // Boolean array size
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertFalse(getBoolean(fileBuffer)); // Boolean - false
        assertEquals(6, fileBuffer.getInt()); // Integer 6
        assertEquals(1, fileBuffer.getShort()); // Enum array size
        assertEquals(0, fileBuffer.get()); // Enum - A
        // TestClassWithArray 2
        assertEquals(0, fileBuffer.getShort()); // Boolean array size
        assertEquals(7, fileBuffer.getInt()); // Integer 7
        assertEquals(0, fileBuffer.getShort()); // Enum array size
        // TestClassWithArray 3
        assertEquals(1, fileBuffer.getShort()); // Boolean array size
        assertFalse(getBoolean(fileBuffer)); // Boolean - false
        assertEquals(8, fileBuffer.getInt()); // Integer 8
        assertEquals(2, fileBuffer.getShort()); // Enum array size
        assertEquals(3, fileBuffer.get()); // Enum - D
        assertEquals(2, fileBuffer.get()); // Enum - C

        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void loggingLists() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create Logger
        String fileDescription = "Lists!";
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fileDescription);

        // Register Log Entries
        LogEntryType<TestEnum> testEnumType =
                logBuilder.registerEnum(TestEnum.class, "Test Enum", TestEnum.A);
        LogEntryType<List<TestEnum>> testEnumArrayType =
                logBuilder.toList(testEnumType, Collections.emptyList());
        LogEntryType<TestClassWithArray> testClassWithArrayType = logBuilder
                .<TestClassWithArray>buildGroupType("TestClassWithArray")
                .addArrayMember("Bool Array", logBuilder.booleanType, TestClassWithArray::getBools)
                .addMember("Int - not an array", logBuilder.integerType, TestClassWithArray::getNonArrayInt)
                .addArrayMember("Enum Array", testEnumType, TestClassWithArray::getEnums)
                .register(new TestClassWithArray());
        LogEntryType<List<TestClassWithArray>> testClassWithArrayArrayType =
                logBuilder.toList(testClassWithArrayType, Collections.emptyList());

        Consumer<List<TestEnum>> asyncEnumArray =
                logBuilder.makeAsyncLogEntry("Async Enum Array", testEnumArrayType);
        Consumer<List<TestEnum>> syncEnumArray = logBuilder.makeSyncLogEntry(
                "Sync Enum Array", testEnumArrayType, Arrays.asList(new TestEnum[0]));
        Consumer<TestClassWithArray> asyncClassWithArray =
                logBuilder.makeAsyncLogEntry("Async Array Class", testClassWithArrayType);
        Consumer<List<TestClassWithArray>> syncClassArrayWithArray = logBuilder.makeSyncLogEntry(
                "Sync Array of Array Class",
                testClassWithArrayArrayType,
                Arrays.asList(new TestClassWithArray[0]));

        // Initialize log
        DataHiveLogger logger = logBuilder.init();
        assertNoError();

        // Write messages
        // Example: Logging a List
        List<TestEnum> enumList = Arrays.asList(TestEnum.A, TestEnum.B, TestEnum.C);
        asyncEnumArray.accept(enumList);
        // Example: Logging an array
        syncEnumArray.accept(List.of(TestEnum.B, TestEnum.C));
        timeSource.addTime(Time.seconds(1.0));
        asyncClassWithArray.accept(
                new TestClassWithArray(new Boolean[] {true, true, false}, 5, new TestEnum[] {TestEnum.A}));
        timeSource.addTime(Time.seconds(1.0));
        syncClassArrayWithArray.accept(Arrays.asList(new TestClassWithArray[] {
            new TestClassWithArray(new Boolean[] {true, true, false}, 6, new TestEnum[] {TestEnum.A}),
            new TestClassWithArray(new Boolean[] {}, 7, new TestEnum[] {}),
            new TestClassWithArray(new Boolean[] {false}, 8, new TestEnum[] {TestEnum.D, TestEnum.C})
        }));
        logger.update();
        assertNoError();

        // Test file
        ByteBuffer fileBuffer = ByteBuffer.wrap(outStream.toByteArray());

        /*
         *                            File Header: 6 + 2 + 20 + 7 + 1 + 8                        = 44
         *              Group Log Entry Type Size: 2                                             = 2
         *                Group Log Entry Type #1: 10 + 2 + 10 + 1 + 2 + 4 + 4*(1 + 2)           = 43
         *                Group Log Entry Type #2: 14 + 2 + 14 + 1 + 2                           = 31
         *                Group Log Entry Type #3: 19 + 2 + 11 + 1 + 2 + 19 + 1 + 2 + 11 + 1 + 2 = 73
         *                Group Log Entry Type #4: 23 + 2 + 23 + 1 + 2                           = 49
         *           Asynchronous Log Entry Table: 2 + 17 + 2 + 18 + 2                           = 41
         *            Synchronous Log Entry Table: 2 + 16 + 2 + 26 + 2                           = 48
         *                        Async Message 1: 1 + 4 + 2 + 3*(1)                             = 10
         *                        Async Message 2: 1 + 4 + 2 + 3*(1) + 4 + 2 + 1*(1)             = 17
         *                    Sync Message Header: 1 + 4                                         = 5
         *                        Sync Enum Array: 2 + 2*(1)                                     = 4
         *              Size TestClassWithArray[]: 2                                             = 2
         *                   TestClassWithArray 1: 2 + 3*(1) + 4 + 2 + 1*(1)                     = 12
         *                   TestClassWithArray 2: 2 + 0*(1) + 4 + 2 + 0*(1)                     = 8
         *                   TestClassWithArray 3: 2 + 1*(1) + 4 + 2 + 2*(1)                     = 11
         *                                  Total: 400
         */
        assertEquals(400, outStream.size());

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0, fileBuffer.get()); // File Options
        assertEquals(0.0, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(4, fileBuffer.getShort()); // Log Entry Type Group Table Size
        // First group type
        assertEquals("Test Enum", getString(fileBuffer)); // First Group Label
        assertEquals(1, fileBuffer.getShort()); // Total Types in Group
        assertEquals("Test Enum", getString(fileBuffer)); // First Type's Label
        assertEquals(0x2, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(1, fileBuffer.getShort()); // First Type's ID
        assertEquals(4, fileBuffer.getInt()); // Total Enum Value/String pairs
        assertEquals(0, fileBuffer.get()); // Enum Value
        assertEquals("A", getString(fileBuffer)); // Enum String
        assertEquals(1, fileBuffer.get()); // Enum Value
        assertEquals("B", getString(fileBuffer)); // Enum String
        assertEquals(2, fileBuffer.get()); // Enum Value
        assertEquals("C", getString(fileBuffer)); // Enum String
        assertEquals(3, fileBuffer.get()); // Enum Value
        assertEquals("D", getString(fileBuffer)); // Enum String
        // Second group type
        assertEquals("Test EnumList", getString(fileBuffer)); // Third Group Label
        assertEquals(1, fileBuffer.getShort()); // Total Types in Group
        assertEquals("Test EnumList", getString(fileBuffer)); // First Type's Label
        assertEquals(0x1, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(64, fileBuffer.getShort()); // First Type's ID
        // Third group type
        assertEquals("TestClassWithArray", getString(fileBuffer)); // Second Group Label
        assertEquals(3, fileBuffer.getShort()); // Total Types in Group
        assertEquals("Bool Array", getString(fileBuffer)); // First Type's Label
        assertEquals(0x1, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(11, fileBuffer.getShort()); // First Type's ID
        assertEquals("Int - not an array", getString(fileBuffer)); // Second Type's Label
        assertEquals(0x0, fileBuffer.get()); // Second Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(7, fileBuffer.getShort()); // Second Type's ID
        // The second type is an enum array, but the way it's defined by the DataHiveLogger is an
        // array of an independently defined group type, so the element itself is not considered
        // an enum in this context.
        assertEquals("Enum Array", getString(fileBuffer)); // Third Type's Label
        assertEquals(0x1, fileBuffer.get()); // Third Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(64, fileBuffer.getShort()); // Third Type's ID
        // Fourth group type
        assertEquals("TestClassWithArrayList", getString(fileBuffer)); // Fourth Group Label
        assertEquals(1, fileBuffer.getShort()); // Total Types in Group
        assertEquals("TestClassWithArrayList", getString(fileBuffer)); // First Type's Label
        assertEquals(0x1, fileBuffer.get()); // First Type's Flags - Bit 1: Enum, Bit 0: Array
        assertEquals(66, fileBuffer.getShort()); // First Type's ID

        // Asynchronous Log Entry Table
        assertEquals(2, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        assertEquals("Async Enum Array", getString(fileBuffer)); // Async Entry Name #1
        assertEquals(65, fileBuffer.getShort()); // Async Entry's Type ID #2
        assertEquals("Async Array Class", getString(fileBuffer)); // Async Entry Name #1
        assertEquals(66, fileBuffer.getShort()); // Async Entry's Type ID #2

        // Synchronous Log Entry Table
        assertEquals(2, fileBuffer.getShort()); // Synchronous Log Entry Table Size
        assertEquals("Sync Enum Array", getString(fileBuffer)); // Sync Entry Name #1
        assertEquals(65, fileBuffer.getShort()); // Sync Entry's Type ID #2
        assertEquals("Sync Array of Array Class", getString(fileBuffer)); // Sync Entry Name #1
        assertEquals(67, fileBuffer.getShort()); // Sync Entry's Type ID #2

        // Messages
        // Async Enum Array Message
        assertEquals(1, fileBuffer.get()); // Async Enum Array ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3, fileBuffer.getShort()); // Enum Array Size
        assertEquals(0, fileBuffer.get()); // Enum Value - A
        assertEquals(1, fileBuffer.get()); // Enum Value - B
        assertEquals(2, fileBuffer.get()); // Enum Value - C
        // Async Array Class Message
        assertEquals(2, fileBuffer.get()); // Async Array Class ID
        assertEquals(1.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3, fileBuffer.getShort()); // Boolean array size
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertFalse(getBoolean(fileBuffer)); // Boolean - false
        assertEquals(5, fileBuffer.getInt()); // Integer 5
        assertEquals(1, fileBuffer.getShort()); // Enum array size
        assertEquals(0, fileBuffer.get()); // Enum - A
        // Sync message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(2.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(2, fileBuffer.getShort()); // Enum array size
        assertEquals(1, fileBuffer.get()); // Enum - B
        assertEquals(2, fileBuffer.get()); // Enum - C
        assertEquals(3, fileBuffer.getShort()); // TestClassWithArray array size
        // TestClassWithArray 1
        assertEquals(3, fileBuffer.getShort()); // Boolean array size
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertTrue(getBoolean(fileBuffer)); // Boolean - true
        assertFalse(getBoolean(fileBuffer)); // Boolean - false
        assertEquals(6, fileBuffer.getInt()); // Integer 6
        assertEquals(1, fileBuffer.getShort()); // Enum array size
        assertEquals(0, fileBuffer.get()); // Enum - A
        // TestClassWithArray 2
        assertEquals(0, fileBuffer.getShort()); // Boolean array size
        assertEquals(7, fileBuffer.getInt()); // Integer 7
        assertEquals(0, fileBuffer.getShort()); // Enum array size
        // TestClassWithArray 3
        assertEquals(1, fileBuffer.getShort()); // Boolean array size
        assertFalse(getBoolean(fileBuffer)); // Boolean - false
        assertEquals(8, fileBuffer.getInt()); // Integer 8
        assertEquals(2, fileBuffer.getShort()); // Enum array size
        assertEquals(3, fileBuffer.get()); // Enum - D
        assertEquals(2, fileBuffer.get()); // Enum - C

        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void noCrashPreInit() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        String fileDescription = "Perfectly fine log";

        // Create Logger
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fileDescription);

        AsyncLogEntry<Double> asyncDouble =
                logBuilder.makeAsyncLogEntry("Async Double", logBuilder.doubleType);
        SyncLogEntry<Integer> syncInt =
                logBuilder.makeSyncLogEntry("Sync Int", logBuilder.integerType, 2);

        assertDoesNotThrow(() -> asyncDouble.accept(1.2)); // Does nothing - Log not initialized
        assertDoesNotThrow(() -> syncInt.accept(42)); // Stores number
        assertDoesNotThrow(syncInt::write); // Does nothing - Log not initialized
        DataHiveLogger logger = logBuilder.init();

        asyncDouble.accept(3.4); // Writes 3.4 successfully
        logger.update(); // Writes synchronous 42 because it was stored successfully.
        assertNoError();

        // Test file
        ByteBuffer fileBuffer = ByteBuffer.wrap(outStream.toByteArray());

        /*
         *                            File Header: 6 + 2 + 20 + 19 + 1 + 8 = 50
         * Group Log Entry Type Description Table: 2 = 2
         *           Asynchronous Log Entry Table: 2 + 13 + 2 = 17
         *            Synchronous Log Entry Table: 2 + 9 + 2 = 13
         *                               Messages: 1*(1 + 4 + 8) + 1*(1 + 4 + 4) = 22
         *                                  Total: 110
         */
        assertEquals(110, outStream.size());

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0, fileBuffer.get()); // File Options
        assertEquals(0.0, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(0, fileBuffer.getShort()); // Log Entry Type Group Table Size

        // Asynchronous Log Entry Table
        assertEquals(1, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        assertEquals("Async Double", getString(fileBuffer)); // Async Entry Name
        assertEquals(10, fileBuffer.getShort()); // Async Entry's Type ID

        // Synchronous Log Entry Table
        assertEquals(1, fileBuffer.getShort()); // Synchronous Log Entry Table Size
        assertEquals("Sync Int", getString(fileBuffer)); // Sync Entry Name
        assertEquals(7, fileBuffer.getShort()); // Sync Entry's Type ID

        // Messages
        // Async "Async Double" Message
        assertEquals(1, fileBuffer.get()); // Async "Async Double" ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3.4, fileBuffer.getDouble()); // Double Value - 3.4
        // Sync Message
        assertEquals(0, fileBuffer.get()); // Sync ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(42, fileBuffer.getInt()); // Int value - 42

        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void noRepeatAsyncEntry() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create Logger
        String fileDescription = "Test DHL File";
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fileDescription);

        // Register Log Entries

        // Wrapped No Repeat Asynchronous Entry
        int wrappedDefaultValue = 2;
        var preWrappedAsyncInt =
                logBuilder.makeAsyncLogEntry("Pre Wrapped No Repeat Async int", logBuilder.integerType);
        Consumer<Integer> wrappedAsync =
                logBuilder.makeNoRepeats(preWrappedAsyncInt, wrappedDefaultValue);
        assertNoError();

        int wrappedNoInitDefaultValue = 5;
        var preWrappedNoInitAsyncInt = logBuilder.makeAsyncLogEntry(
                "Pre Wrapped No Repeat, No Init Async int", logBuilder.integerType);
        Consumer<Integer> wrappedNoInitAsync =
                logBuilder.makeNoRepeats(preWrappedNoInitAsyncInt, wrappedNoInitDefaultValue, false);
        assertNoError();

        // Wrapped No Repeat Asynchronous Entry
        double noRepeatDefaultValue = 3.4;
        Consumer<Double> noRepeatAsync = logBuilder.makeNoRepeatsAsyncLogEntry(
                "No Repeat Async double", logBuilder.doubleType, noRepeatDefaultValue);
        assertNoError();

        double noRepeatNoInitDefaultValue = 1.6;
        Consumer<Double> noRepeatNoInitAsync = logBuilder.makeNoRepeatsAsyncLogEntry(
                "No Repeat, No Init Async double",
                logBuilder.doubleType,
                noRepeatNoInitDefaultValue,
                false);
        assertNoError();

        // Initialize log
        logBuilder.init();
        assertNoError();

        // Verify logging functionality
        wrappedAsync.accept(wrappedDefaultValue);
        timeSource.addTime(Time.seconds(1.0));
        wrappedAsync.accept(3);
        timeSource.addTime(Time.seconds(1.0));
        wrappedAsync.accept(3);
        timeSource.addTime(Time.seconds(1.0));
        wrappedAsync.accept(8);
        timeSource.addTime(Time.seconds(1.0));
        wrappedAsync.accept(8);
        timeSource.addTime(Time.seconds(1.0));
        wrappedAsync.accept(-4);
        timeSource.addTime(Time.seconds(1.0));
        assertNoError();

        wrappedNoInitAsync.accept(wrappedNoInitDefaultValue);
        timeSource.addTime(Time.seconds(1.0));
        wrappedNoInitAsync.accept(12);
        timeSource.addTime(Time.seconds(1.0));
        wrappedNoInitAsync.accept(12);
        timeSource.addTime(Time.seconds(1.0));
        wrappedNoInitAsync.accept(43);
        timeSource.addTime(Time.seconds(1.0));
        wrappedNoInitAsync.accept(43);
        timeSource.addTime(Time.seconds(1.0));
        wrappedNoInitAsync.accept(-24);
        timeSource.addTime(Time.seconds(1.0));
        assertNoError();

        noRepeatAsync.accept(noRepeatDefaultValue);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatAsync.accept(5.2);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatAsync.accept(5.2);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatAsync.accept(6.1);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatAsync.accept(6.1);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatAsync.accept(-3.7);
        timeSource.addTime(Time.seconds(1.0));
        assertNoError();

        noRepeatNoInitAsync.accept(noRepeatNoInitDefaultValue);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatNoInitAsync.accept(6.3);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatNoInitAsync.accept(6.3);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatNoInitAsync.accept(2.4);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatNoInitAsync.accept(2.4);
        timeSource.addTime(Time.seconds(1.0));
        noRepeatNoInitAsync.accept(-5.2);
        timeSource.addTime(Time.seconds(1.0));
        assertNoError();

        // Test file
        ByteBuffer fileBuffer = ByteBuffer.wrap(outStream.toByteArray());

        /*
         *                            File Header: 6 + 2 + 20 + 14 + 1 + 8 = 51
         * Group Log Entry Type Description Table: 2
         *           Asynchronous Log Entry Table: 2 + 32 + 2 + 41 + 2 + 23 +
         *                                         2 + 32 + 2 = 138
         *            Synchronous Log Entry Table: 2
         *                               Messages: (1 + 4 + 4) + (1 + 4 + 8) +
         *                                         (1 + 4 + 8) * 3 + (1 + 4 + 8) * 3 +
         *                                         (1 + 4 + 4) * 3 + (1 + 4 + 4) * 3 = 154
         *                                  Total: 347
         */
        assertEquals(347, outStream.size());

        // File Header
        verifyFileIdentifier(fileBuffer);
        assertEquals(1, fileBuffer.getShort()); // Version
        assertEquals(19, getString(fileBuffer).length()); // Date/Time String
        assertEquals(fileDescription, getString(fileBuffer)); // File Description
        assertEquals(0, fileBuffer.get()); // File Options
        assertEquals(0.0, fileBuffer.getDouble()); // Fixed Timestamp

        // Group Log Entry Type Description Table
        assertEquals(0, fileBuffer.getShort()); // Log Entry Type Group Table Size

        // Asynchronous Log Entry Table
        assertEquals(4, fileBuffer.getShort()); // Asynchronous Log Entry Table Size
        assertEquals("Pre Wrapped No Repeat Async int", getString(fileBuffer)); // Async Entry Name
        assertEquals(7, fileBuffer.getShort()); // Async Entry's Type ID - Signed Int(7)
        assertEquals(
                "Pre Wrapped No Repeat, No Init Async int", getString(fileBuffer)); // Async Entry Name
        assertEquals(7, fileBuffer.getShort()); // Async Entry's Type ID - Signed Int(7)
        assertEquals("No Repeat Async double", getString(fileBuffer)); // Async Entry Name
        assertEquals(10, fileBuffer.getShort()); // Async Entry's Type ID - Double(10)
        assertEquals("No Repeat, No Init Async double", getString(fileBuffer)); // Async Entry Name
        assertEquals(10, fileBuffer.getShort()); // Async Entry's Type ID - Double(10)

        // Synchronous Log Entry Table
        assertEquals(0, fileBuffer.getShort()); // Synchronous Log Entry Table Size

        // Messages

        // Async "Pre Wrapped No Repeat Async int" Message - Init
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(wrappedDefaultValue, fileBuffer.getInt()); // Value
        // Async "No Repeat Async double" Message - Init
        assertEquals(3, fileBuffer.get()); // Async ID
        assertEquals(0.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(noRepeatDefaultValue, fileBuffer.getDouble()); // Value

        // Async "Pre Wrapped No Repeat Async int" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(1.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(3, fileBuffer.getInt()); // Value
        // Async "Pre Wrapped No Repeat Async int" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(3.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(8, fileBuffer.getInt()); // Value
        // Async "Pre Wrapped No Repeat Async int" Message
        assertEquals(1, fileBuffer.get()); // Async ID
        assertEquals(5.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(-4, fileBuffer.getInt()); // Value

        // Async "Pre Wrapped No Repeat, No Init Async int" Message
        assertEquals(2, fileBuffer.get()); // Async ID
        assertEquals(7.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(12, fileBuffer.getInt()); // Value
        // Async "Pre Wrapped No Repeat, No Init Async int" Message
        assertEquals(2, fileBuffer.get()); // Async ID
        assertEquals(9.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(43, fileBuffer.getInt()); // Value
        // Async "Pre Wrapped No Repeat, No Init Async int" Message
        assertEquals(2, fileBuffer.get()); // Async ID
        assertEquals(11.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(-24, fileBuffer.getInt()); // Value

        // Async "No Repeat Async double" Message
        assertEquals(3, fileBuffer.get()); // Async ID
        assertEquals(13.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(5.2, fileBuffer.getDouble()); // Value
        // Async "No Repeat Async double" Message
        assertEquals(3, fileBuffer.get()); // Async ID
        assertEquals(15.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(6.1, fileBuffer.getDouble()); // Value
        // Async "No Repeat Async double" Message
        assertEquals(3, fileBuffer.get()); // Async ID
        assertEquals(17.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(-3.7, fileBuffer.getDouble()); // Value

        // Async "No Repeat Async double" Message
        assertEquals(4, fileBuffer.get()); // Async ID
        assertEquals(19.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(6.3, fileBuffer.getDouble()); // Value
        // Async "No Repeat Async double" Message
        assertEquals(4, fileBuffer.get()); // Async ID
        assertEquals(21.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(2.4, fileBuffer.getDouble()); // Value
        // Async "No Repeat Async double" Message
        assertEquals(4, fileBuffer.get()); // Async ID
        assertEquals(23.0, fileBuffer.getFloat()); // Timestamp
        assertEquals(-5.2, fileBuffer.getDouble()); // Value

        // Is end of file
        assertEquals(false, fileBuffer.hasRemaining());
    }

    @Test
    public void defaultInitialValue() throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Create Logger
        String fileDescription = "Test DHL File";
        DataHiveLogger.Builder logBuilder =
                new DataHiveLogger.Builder(outStream, timeSource, fileDescription);

        // Register custom types
        var fooType = logBuilder
                .<Foo>buildGroupType("Foo")
                .addMember("A", logBuilder.integerType, Foo::getA)
                .addMember("B", logBuilder.doubleType, Foo::getB)
                .register(new Foo());
        assertNoError();

        LogEntryType<TestEnum> testEnumType =
                logBuilder.registerEnum(TestEnum.class, "Test Enum", TestEnum.A);
        assertNoError();

        LogEntryType<Integer[]> arrayType = logBuilder.toArray(logBuilder.integerType, new Integer[0]);
        assertNoError();

        LogEntryType<List<String>> listType =
                logBuilder.toList(logBuilder.stringType, Collections.emptyList());
        assertNoError();

        // Primitives
        SyncLogEntry<UnsignedByte> syncUnsignedByte =
                logBuilder.makeSyncLogEntry("Sync Unsigned Byte", logBuilder.unsignedByteType);
        assertNoError();
        assertEquals(new UnsignedByte((byte) 0), syncUnsignedByte.data);
        syncUnsignedByte.accept(new UnsignedByte((byte) 25));
        assertEquals(new UnsignedByte((byte) 25), syncUnsignedByte.data);

        SyncLogEntry<UnsignedShort> syncUnsignedShort =
                logBuilder.makeSyncLogEntry("Sync Unsigned Short", logBuilder.unsignedShortType);
        assertNoError();
        assertEquals(new UnsignedShort((short) 0), syncUnsignedShort.data);
        syncUnsignedShort.accept(new UnsignedShort((short) 45));
        assertEquals(new UnsignedShort((short) 45), syncUnsignedShort.data);

        SyncLogEntry<UnsignedInt> syncUnsignedInt =
                logBuilder.makeSyncLogEntry("Sync Unsigned Int", logBuilder.unsignedIntType);
        assertNoError();
        assertEquals(new UnsignedInt(0), syncUnsignedInt.data);
        syncUnsignedInt.accept(new UnsignedInt(67));
        assertEquals(new UnsignedInt(67), syncUnsignedInt.data);

        SyncLogEntry<UnsignedLong> syncUnsignedLong =
                logBuilder.makeSyncLogEntry("Sync Unsigned Long", logBuilder.unsignedLongType);
        assertNoError();
        assertEquals(new UnsignedLong(0L), syncUnsignedLong.data);
        syncUnsignedLong.accept(new UnsignedLong(93L));
        assertEquals(new UnsignedLong(93L), syncUnsignedLong.data);

        SyncLogEntry<Byte> syncByte = logBuilder.makeSyncLogEntry("Sync Byte", logBuilder.byteType);
        assertNoError();
        assertEquals((byte) 0, syncByte.data);
        syncByte.accept((byte) 436);
        assertEquals((byte) 436, syncByte.data);

        SyncLogEntry<Short> syncShort = logBuilder.makeSyncLogEntry("Sync Short", logBuilder.shortType);
        assertNoError();
        assertEquals((short) 0, syncShort.data);
        syncShort.accept((short) 15);
        assertEquals((short) 15, syncShort.data);

        SyncLogEntry<Integer> syncInt = logBuilder.makeSyncLogEntry("Sync Int", logBuilder.integerType);
        assertNoError();
        assertEquals(0, syncInt.data);
        syncInt.accept(956);
        assertEquals(956, syncInt.data);

        SyncLogEntry<Long> syncLong = logBuilder.makeSyncLogEntry("Sync Long", logBuilder.longType);
        assertNoError();
        assertEquals(0L, syncLong.data);
        syncLong.accept(527L);
        assertEquals(527L, syncLong.data);

        SyncLogEntry<Float> syncFloat = logBuilder.makeSyncLogEntry("Sync Float", logBuilder.floatType);
        assertNoError();
        assertEquals(0.0f, syncFloat.data);
        syncFloat.accept(0.28f);
        assertEquals(0.28f, syncFloat.data);

        SyncLogEntry<Double> syncDouble =
                logBuilder.makeSyncLogEntry("Sync Double", logBuilder.doubleType);
        assertNoError();
        assertEquals(0.0, syncDouble.data);
        syncDouble.accept(3.14);
        assertEquals(3.14, syncDouble.data);

        SyncLogEntry<Boolean> syncBoolean =
                logBuilder.makeSyncLogEntry("Sync Boolean", logBuilder.booleanType);
        assertNoError();
        assertEquals(false, syncBoolean.data);
        syncBoolean.accept(true);
        assertEquals(true, syncBoolean.data);

        SyncLogEntry<String> syncString =
                logBuilder.makeSyncLogEntry("Sync String", logBuilder.stringType);
        assertNoError();
        assertEquals("", syncString.data);
        syncString.accept("Test this");
        assertEquals("Test this", syncString.data);

        // Synchronous Entries
        SyncLogEntry<Foo> syncFoo = logBuilder.makeSyncLogEntry("Sync Foo", fooType);
        assertNoError();
        assertEquals(new Foo(), syncFoo.data);
        syncFoo.accept(new Foo(27, 34.5));
        assertEquals(new Foo(27, 34.5), syncFoo.data);

        SyncLogEntry<TestEnum> syncTestEnum =
                logBuilder.makeSyncLogEntry("Sync Test Enum", testEnumType);
        assertNoError();
        assertEquals(TestEnum.A, syncTestEnum.data);
        syncTestEnum.accept(TestEnum.D);
        assertEquals(TestEnum.D, syncTestEnum.data);

        SyncLogEntry<Integer[]> syncArray = logBuilder.makeSyncLogEntry("Sync Array", arrayType);
        assertNoError();
        assertArrayEquals(new Integer[0], syncArray.data);
        syncArray.accept(new Integer[] {4, 243, -232});
        assertArrayEquals(new Integer[] {4, 243, -232}, syncArray.data);

        SyncLogEntry<List<String>> syncList = logBuilder.makeSyncLogEntry("Sync List", listType);
        assertNoError();
        assertEquals(Collections.emptyList(), syncList.data);
        syncList.accept(Arrays.asList("Test", "this", "out"));
        assertEquals(Arrays.asList("Test", "this", "out"), syncList.data);
    }
}
