package org.growingstems.util.logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.logger.LogFileBuilder.Settings;
import org.growingstems.util.timer.JavaTimeSource;
import org.growingstems.util.timer.Timer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.io.TempDir;

public class LogFileBuilder_Test extends DataHiveLogger_Test {
    protected static final int k_maxFileAttempts = 3;
    protected static final Settings k_defaultTestSettings = new Settings(
            Optional.of(k_maxFileAttempts),
            Settings.k_defaultPipeSize,
            Settings.k_defaultWriteSleepTime,
            Optional.empty());
    protected static final Time k_writeTimeAllowance = k_defaultTestSettings.writeSleepTime.mul(5.0);

    protected final String k_defaultFileName = "TestLog";
    protected final String k_alternateFileName = "AlternateLog";
    protected final String k_defaultFileDescription = "Test Log File";

    @TempDir
    static Path tempDir;

    protected void emptyTempDir() throws IOException {
        Files.walk(tempDir).filter(p -> !p.equals(tempDir)).map(Path::toFile).forEach(File::delete);
    }

    // Make sure to close the returned builder!
    protected LogFileBuilder makeEmptyFile(String fileName, String fileDescription)
            throws IOException {
        var builder = new LogFileBuilder(
                tempDir.toFile(), fileName, fileDescription, k_defaultTestSettings, new JavaTimeSource());
        assertNoError();

        builder.init();

        var expectedFileSize = getExpectedEmptyLogFileLength(fileDescription);
        Timer t = new JavaTimeSource().createTimer();
        while (!t.hasElapsed(k_writeTimeAllowance)
                && builder.getTotalTransferred() < expectedFileSize) {
            // Wait for the thread to write the data
        }

        return builder;
    }

    @Test
    public void singleEmptyFile() throws IOException {
        emptyTempDir();

        var builder = makeEmptyFile(k_defaultFileName, k_defaultFileDescription);
        builder.close();
        var expectedFile = tempDir.resolve(k_defaultFileName + DataHiveLogger.k_fileExtension);
        assertEquals(expectedFile.getFileName().toString(), builder.getLogFilename());
        validateEmptyLog(Files.readAllBytes(expectedFile), k_defaultFileDescription);
    }

    @Test
    @EnabledOnOs(OS.LINUX) // Renaming while logging is not supported on Windows
    public void rename() throws IOException {
        emptyTempDir();

        var builder = makeEmptyFile(k_defaultFileName, k_defaultFileDescription);
        assertTrue(builder.renameTo(k_alternateFileName));
        builder.close();
        var expectedFile = tempDir.resolve(k_alternateFileName + DataHiveLogger.k_fileExtension);
        assertEquals(expectedFile.getFileName().toString(), builder.getLogFilename());
        validateEmptyLog(Files.readAllBytes(expectedFile), k_defaultFileDescription);
    }

    @Test
    public void autoName() throws IOException {
        emptyTempDir();

        for (int i = 0; i <= k_maxFileAttempts + 1; i++) {
            var builder = makeEmptyFile(k_defaultFileName, k_defaultFileDescription);
            builder.close();
            var expectedFile = tempDir.resolve(k_defaultFileName
                    + switch (i) {
                        case 0 -> "";
                        case k_maxFileAttempts, k_maxFileAttempts + 1 -> "_MAX";
                        default -> "_" + i;
                    }
                    + DataHiveLogger.k_fileExtension);
            assertEquals(expectedFile.getFileName().toString(), builder.getLogFilename());
            validateEmptyLog(Files.readAllBytes(expectedFile), k_defaultFileDescription);
        }
    }

    @Test
    @EnabledOnOs(OS.LINUX) // Renaming while logging is not supported on Windows
    public void renameAndAutoName() throws IOException {
        emptyTempDir();

        for (int i = 0; i <= k_maxFileAttempts + 1; i++) {
            var builder = makeEmptyFile(k_defaultFileName, k_defaultFileDescription);
            assertTrue(builder.renameTo(k_alternateFileName));
            builder.close();
            var expectedFile = tempDir.resolve(k_alternateFileName
                    + switch (i) {
                        case 0 -> "";
                        case k_maxFileAttempts, k_maxFileAttempts + 1 -> "_MAX";
                        default -> "_" + i;
                    }
                    + DataHiveLogger.k_fileExtension);
            assertEquals(expectedFile.getFileName().toString(), builder.getLogFilename());
            validateEmptyLog(Files.readAllBytes(expectedFile), k_defaultFileDescription);
        }
    }
}
