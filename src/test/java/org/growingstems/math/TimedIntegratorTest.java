/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import java.util.List;
import org.growingstems.measurements.Measurements.Time;

public class TimedIntegratorTest {
    protected static class IntegralIteration {
        double input;
        Time time;
        double output;

        IntegralIteration(double input, double time_s, double output) {
            this.input = input;
            this.time = Time.seconds(time_s);
            this.output = output;
        }
    }

    protected List<IntegralIteration> getBasicUsageData() {
        return List.of(
                new IntegralIteration(0.0, 0.0, 0.0),
                new IntegralIteration(0.0, 0.0, 0.0),
                new IntegralIteration(0.0, 0.0, 0.0),
                new IntegralIteration(1.0, 2.0, 1.0),
                new IntegralIteration(0.0, 0.0, 0.0),
                new IntegralIteration(-1.0, -1.0, 0.5),
                new IntegralIteration(0.0, 0.0, 0.0),
                new IntegralIteration(-1.0, 2.0, -1.0),
                new IntegralIteration(0.0, 0.0, 0.0),
                new IntegralIteration(1.0, -1.0, -0.5),
                new IntegralIteration(1.0, 0.0, 0.5),
                new IntegralIteration(2.0, 2.0, 3.5));
    }

    protected List<IntegralIteration> getStartWithNonZeroInputData() {
        return List.of(new IntegralIteration(1.0, 0.0, 0.0), new IntegralIteration(1.0, 1.0, 1.0));
    }

    protected List<IntegralIteration> getResetData() {
        return List.of(
                new IntegralIteration(1.0, 0.0, 0.0),
                new IntegralIteration(1.0, 1.0, 1.0),
                new IntegralIteration(1.0, 2.0, 2.0),
                new IntegralIteration(0.0, 4.0, 3.0));
    }

    protected IntegralIteration getLastResetDatum() {
        return new IntegralIteration(0.0, 4.0, 0.0);
    }
}
