/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class AccumulatorU_Test extends AccumulatorTest {
    private static final double k_tolerance = 1E-10;

    @ParameterizedTest
    @MethodSource({"provideDoubles"})
    public void doubleAccumulationTest(Double[] testValues, double expected) {
        AccumulatorU<Length> accumulator = new AccumulatorU<>(Length.ZERO);
        for (Double i : testValues) {
            accumulator.update(Length.inches(i));
        }
        assertEquals(expected, accumulator.getValue().asInches(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource({"provideDoubles"})
    public void accumulationConstructionTest(Double[] testValues, double expected) {
        AccumulatorU<Angle> accumulator = new AccumulatorU<>(Angle.revolutions(testValues[0]));
        for (Double i : testValues) {
            accumulator.update(Angle.revolutions(i));
        }
        assertEquals(expected + testValues[0], accumulator.getValue().asRevolutions(), k_tolerance);
    }

    @Test
    public void accumulatorManipulationTest() {
        AccumulatorU<Unitless> accumulator = new AccumulatorU<>(Unitless.ZERO);
        assertEquals(1.0, accumulator.update(Unitless.none(1.0)).asNone(), k_tolerance);
        assertEquals(2.0, accumulator.update(Unitless.none(1.0)).asNone(), k_tolerance);
        accumulator.setValue(Unitless.ZERO);
        assertEquals(0.0, accumulator.getValue().asNone(), k_tolerance);
        assertEquals(2.0, accumulator.update(Unitless.none(2.0)).asNone(), k_tolerance);
        assertEquals(5.0, accumulator.update(Unitless.none(3.0)).asNone(), k_tolerance);
        accumulator.setValue(Unitless.none(1.0));
        assertEquals(1.0, accumulator.getValue().asNone(), k_tolerance);

        assertEquals(-4.0, accumulator.update(Unitless.none(-5.0)).asNone(), k_tolerance);
        assertEquals(2.0, accumulator.update(Unitless.none(6.0)).asNone(), k_tolerance);
        accumulator.setValue(Unitless.ZERO);
        assertEquals(1.0, accumulator.update(Unitless.none(1.0)).asNone(), k_tolerance);
        assertEquals(1.0, accumulator.update(Unitless.none(0.0)).asNone(), k_tolerance);
        assertEquals(1.0, accumulator.update(Unitless.none(0.0)).asNone(), k_tolerance);
        accumulator.setValue(Unitless.ZERO);
        assertEquals(0.0, accumulator.getValue().asNone(), k_tolerance);
    }

    @Test
    public void settersTest() {
        AccumulatorU<Unitless> accumulator = new AccumulatorU<>(Unitless.ZERO);
        accumulator.setValue(Unitless.none(10.0));
        assertEquals(10.0, accumulator.getValue().asNone(), k_tolerance);

        accumulator.setLimits(new RangeU<>(Unitless.none(-10.0), Unitless.none(-5.0)));
        assertEquals(-5.0, accumulator.getValue().asNone(), k_tolerance);

        accumulator.setValue(Unitless.none(-20.0));
        assertEquals(-10.0, accumulator.getValue().asNone(), k_tolerance);
    }

    @Test
    public void reset() {
        Velocity start = Velocity.feetPerSecond(50.0);
        AccumulatorU<Velocity> accumulator = new AccumulatorU<Velocity>(start);
        assertTrue(start.eq(accumulator.getValue()));

        accumulator.reset();
        assertTrue(start.eq(accumulator.getValue()));

        Velocity accumulatedValue = accumulator.update(Velocity.feetPerSecond(25.0));
        assertEquals(75.0, accumulatedValue.asFeetPerSecond());

        accumulator.reset();
        assertTrue(start.eq(accumulator.getValue()));
    }
}
