/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class Vector2d_Test extends Vector2Test {
    @ParameterizedTest
    @MethodSource("addTestData")
    public void testAdd(
            double aX,
            double aY,
            double bX,
            double bY,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2d a = new Vector2d(aX, aY);
        Vector2d b = new Vector2d(bX, bY);

        Vector2d result = a.add(b);

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("multiplyTestData")
    public void testMultiply(
            double aX,
            double aY,
            double scaleFactor,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2d a = new Vector2d(aX, aY);

        Vector2d result = a.mul(scaleFactor);

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("divideTestData")
    public void testDivide(
            double aX, double aY, double divisor, double expectedX, double expectedY, String testName) {
        Vector2d a = new Vector2d(aX, aY);

        Vector2d result = a.div(divisor);

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("inverseTestData")
    public void testInverse(
            double aX, double aY, double expectedX, double expectedY, String testName) {
        Vector2d a = new Vector2d(aX, aY);

        Vector2d result = a.neg();

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("magnitudeTestData")
    public void testMagnitude(double aX, double aY, double expectedMagnitude, String testName) {
        Vector2d a = new Vector2d(aX, aY);

        assertEquals(a.getMagnitude(), expectedMagnitude, 1E-9, testName);
    }

    @ParameterizedTest
    @MethodSource("normalizeTestData")
    public void testNormalize(
            double aX, double aY, double expectedX, double expectedY, String testName) {
        Vector2d a = new Vector2d(aX, aY);

        Vector2d result = a.normalize(false);

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @Test
    public void normalizeEmptyGroup() {
        List<Vector2d> vectors = new ArrayList<>();
        vectors = Vector2d.normalizeGroup(vectors, true);
        assertEquals(0, vectors.size());
        vectors = Vector2d.normalizeGroup(vectors, false);
        assertEquals(0, vectors.size());
    }

    @ParameterizedTest
    @MethodSource("normalizeGroupTestData")
    public void testNormalizeGroup(
            double aX,
            double aY,
            double bX,
            double bY,
            double expectedAX,
            double expectedAY,
            double expectedBX,
            double expectedBY,
            String testName) {
        Vector2d a = new Vector2d(aX, aY);
        Vector2d b = new Vector2d(bX, bY);

        ArrayList<Vector2d> list = new ArrayList<Vector2d>();
        list.add(a);
        list.add(b);

        List<Vector2d> result = Vector2d.normalizeGroup(list, false);

        Vector2d resultA = result.get(0);
        Vector2d resultB = result.get(1);

        assertEquals(expectedAX, resultA.getX(), 1E-9, testName + " - A(X)");
        assertEquals(expectedAY, resultA.getY(), 1E-9, testName + " - A(Y)");
        assertEquals(expectedBX, resultB.getX(), 1E-9, testName + " - B(X)");
        assertEquals(expectedBY, resultB.getY(), 1E-9, testName + " - B(Y)");
    }

    @ParameterizedTest
    @MethodSource("rotateTestData")
    public void testRotate(
            double startX,
            double startY,
            double rotation_deg,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2d test = new Vector2d(startX, startY);

        Vector2d result = test.rotate(Angle.degrees(rotation_deg));

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("subtractTestData")
    public void testSubtract(
            double aX,
            double aY,
            double bX,
            double bY,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2d a = new Vector2d(aX, aY);
        Vector2d b = new Vector2d(bX, bY);

        Vector2d result = a.sub(b);

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("rangeToTestData")
    public void rangeToTest(
            double aX, double aY, double bX, double bY, double expectedRange, String testName) {
        Vector2d a = new Vector2d(aX, aY);
        Vector2d b = new Vector2d(bX, bY);

        assertEquals(expectedRange, a.rangeTo(b), 1E-5, testName);
    }

    @ParameterizedTest
    @MethodSource("constructPolarTestData")
    public void constructPolarTest(
            double magnitude, double angle_rad, double expectedX, double expectedY) {
        Vector2d a = new Vector2d(magnitude, Angle.radians(angle_rad));
        assertEquals(expectedX, a.getX(), 0.000001);
        assertEquals(expectedY, a.getY(), 0.000001);
    }

    @ParameterizedTest
    @MethodSource("getMethodTestData")
    public void getMethodTests(double x, double y, double expectedMag, double expectedAngle) {
        Vector2d a = new Vector2d(x, y);
        assertEquals(expectedMag, a.getMagnitude(), 0.000001);
        assertEquals(expectedAngle, a.getAngle().asRadians(), 0.000001);
    }

    @ParameterizedTest
    @MethodSource("normalizeOnlyScaleDownTestData")
    public void testNormalizeOnlyScaleDown(
            double aX, double aY, double expectedX, double expectedY, String testName) {
        Vector2d a = new Vector2d(aX, aY);

        Vector2d result = a.normalize(true);

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("normalizeGroupOnlyScaleDownTestData")
    public void testNormalizeGroupOnlyScaleDown(
            double aX,
            double aY,
            double bX,
            double bY,
            double expectedAX,
            double expectedAY,
            double expectedBX,
            double expectedBY,
            String testName) {
        Vector2d a = new Vector2d(aX, aY);
        Vector2d b = new Vector2d(bX, bY);

        ArrayList<Vector2d> list = new ArrayList<Vector2d>();
        list.add(a);
        list.add(b);

        List<Vector2d> result = Vector2d.normalizeGroup(list, true);

        Vector2d resultA = result.get(0);
        Vector2d resultB = result.get(1);

        assertEquals(expectedAX, resultA.getX(), 1E-9, testName + " - A(X)");
        assertEquals(expectedAY, resultA.getY(), 1E-9, testName + " - A(Y)");
        assertEquals(expectedBX, resultB.getX(), 1E-9, testName + " - B(X)");
        assertEquals(expectedBY, resultB.getY(), 1E-9, testName + " - B(Y)");
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductDoubleTests(double x1, double y1, double x2, double y2, double result) {
        Vector2d a = new Vector2d(x1, y1);
        Vector2d b = new Vector2d(x2, y2);
        assertEquals(result, a.dot(b), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductDoubleTests(
            double x1, double y1, double x2, double y2, double result, String testName) {
        Vector2d a = new Vector2d(x1, y1);
        Vector2d b = new Vector2d(x2, y2);
        assertEquals(result, a.cross(b), 0.0001, testName);
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductUnitTests(double x1, double y1, double x2, double y2, double result) {
        Vector2d a = new Vector2d(x1, y1);
        Vector2dU<Length> b = new Vector2dU<>(Length.inches(x2), Length.inches(y2));
        assertEquals(result, a.dot(b).asInches(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductUnitTests(
            double x1, double y1, double x2, double y2, double result, String testName) {
        Vector2d a = new Vector2d(x1, y1);
        Vector2dU<Length> b = new Vector2dU<>(Length.inches(x2), Length.inches(y2));
        assertEquals(result, a.cross(b).asInches(), 0.0001, testName);
    }

    @Test
    public void forEach() {
        Vector2d a = new Vector2d(6.0, 12.0);

        Vector2d squared = a.forEach(v -> v * v);
        assertEquals(36.0, squared.getX(), 0.000001);
        assertEquals(144.0, squared.getY(), 0.000001);

        Vector2d squaredPlus = a.forEach(squared, (v, s) -> v + s);
        assertEquals(42.0, squaredPlus.getX(), 0.000001);
        assertEquals(156.0, squaredPlus.getY(), 0.000001);

        Vector2dU<Length> squaredInches = squared.forEachU(Length::inches);
        Vector2d squaredPlusInches = a.forEach(squaredInches, (v, s) -> v + s.asInches());
        assertEquals(42.0, squaredPlusInches.getX(), 0.000001);
        assertEquals(156.0, squaredPlusInches.getY(), 0.000001);
    }

    @Test
    public void forEachU() {
        Vector2d a = new Vector2d(6.0, 12.0);

        Vector2dU<Time> squared = a.forEachU(v -> Time.seconds(v * v));
        assertEquals(36.0, squared.getX().asSeconds(), 0.000001);
        assertEquals(144.0, squared.getY().asSeconds(), 0.000001);

        Vector2d squaredUnitless = squared.forEach(Time::asSeconds);
        Vector2dU<Time> squaredPlus = a.forEachU(squaredUnitless, (v, s) -> Time.seconds(v + s));
        assertEquals(42.0, squaredPlus.getX().asSeconds(), 0.000001);
        assertEquals(156.0, squaredPlus.getY().asSeconds(), 0.000001);

        Vector2dU<Time> squaredPlusSeconds =
                a.forEachU(squared, (v, s) -> Time.seconds(v).add(s));
        assertEquals(42.0, squaredPlusSeconds.getX().asSeconds(), 0.000001);
        assertEquals(156.0, squaredPlusSeconds.getY().asSeconds(), 0.000001);
    }
}
