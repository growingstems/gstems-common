/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.growingstems.test.AssertExtensions.assertPercentError;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Voltage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class Vector3d_Test extends Vector3Test {
    @ParameterizedTest
    @MethodSource("rangeToTestData")
    public void rangeToTest(
            double m_x,
            double m_y,
            double m_z,
            double other_x,
            double other_y,
            double other_z,
            double expectedDistance) {
        Vector3d point1 = new Vector3d(m_x, m_y, m_z);
        Vector3d point2 = new Vector3d(other_x, other_y, other_z);
        assertPercentError("Distance", expectedDistance, point1.rangeTo(point2), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("magnitudeTestData")
    public void magnitudeTest(double m_x, double m_y, double m_z, double magnitude) {
        Vector3d point1 = new Vector3d(m_x, m_y, m_z);
        assertPercentError("Magnitude", magnitude, point1.getMagnitude(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("additionTestData")
    public void additionTest(
            double m_x,
            double m_y,
            double m_z,
            double other_x,
            double other_y,
            double other_z,
            double expectedX,
            double expectedY,
            double expectedZ) {
        Vector3d point1 = new Vector3d(m_x, m_y, m_z);
        Vector3d point2 = new Vector3d(other_x, other_y, other_z);
        Vector3d resultVector = point1.add(point2);
        assertPercentError(expectedX, resultVector.getX(), 0.0005);
        assertPercentError(expectedY, resultVector.getY(), 0.0005);
        assertPercentError(expectedZ, resultVector.getZ(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("subtractionTestData")
    public void subtractionTest(
            double m_x,
            double m_y,
            double m_z,
            double other_x,
            double other_y,
            double other_z,
            double expectedX,
            double expectedY,
            double expectedZ) {
        Vector3d point1 = new Vector3d(m_x, m_y, m_z);
        Vector3d point2 = new Vector3d(other_x, other_y, other_z);
        Vector3d resultVector = point1.sub(point2);
        assertPercentError(expectedX, resultVector.getX(), 0.0005);
        assertPercentError(expectedY, resultVector.getY(), 0.0005);
        assertPercentError(expectedZ, resultVector.getZ(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("multiplicationTestData")
    public void multiplicationTest(
            double m_x,
            double m_y,
            double m_z,
            double scalar,
            double scaleX,
            double scaleY,
            double scaleZ) {
        Vector3d vector = new Vector3d(m_x, m_y, m_z);
        Vector3d scaledVector = vector.mul(scalar);
        assertPercentError(scaleX, scaledVector.getX(), 0.0005);
        assertPercentError(scaleY, scaledVector.getY(), 0.0005);
        assertPercentError(scaleZ, scaledVector.getZ(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("cylindricalTestData")
    public void cylindricalTest(
            double radius,
            double theta_deg,
            double height,
            double resultX,
            double resultY,
            double resultZ) {
        Angle thetaU = Angle.degrees(theta_deg);
        Vector3d vector = Vector3d.fromCylindrical(radius, thetaU, height);
        assertEquals(resultX, vector.getX(), 1E-9);
        assertEquals(resultY, vector.getY(), 1E-9);
        assertEquals(resultZ, vector.getZ(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("sphericalTestData")
    public void sphericalTest(
            double radius,
            double theta_deg,
            double phi_deg,
            double resultX,
            double resultY,
            double resultZ) {
        Angle thetaU = Angle.degrees(theta_deg);
        Angle phiU = Angle.degrees(phi_deg);
        Vector3d vector = Vector3d.fromSpherical(radius, thetaU, phiU);
        assertEquals(resultX, vector.getX(), 1E-9);
        assertEquals(resultY, vector.getY(), 1E-9);
        assertEquals(resultZ, vector.getZ(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("azElTestData")
    public void azElTest(
            double radius,
            double azimuth_deg,
            double elevation_deg,
            double resultX,
            double resultY,
            double resultZ) {
        Angle azimuthU = Angle.degrees(azimuth_deg);
        Angle elevationU = Angle.degrees(elevation_deg);
        Vector3d vector = Vector3d.fromAzEl(radius, azimuthU, elevationU);
        assertEquals(resultX, vector.getX(), 1E-9);
        assertEquals(resultY, vector.getY(), 1E-9);
        assertEquals(resultZ, vector.getZ(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("normalizeTestData")
    public void normalizeTest(
            double aX, double aY, double aZ, double expectedX, double expectedY, double expectedZ) {
        Vector3d a = new Vector3d(aX, aY, aZ);

        Vector3d result = a.normalize(false);

        assertEquals(expectedX, result.getX(), 1E-9);
        assertEquals(expectedY, result.getY(), 1E-9);
        assertEquals(expectedZ, result.getZ(), 1E-9);
    }

    @Test
    public void normalizeEmptyGroup() {
        List<Vector3d> vectors = new ArrayList<>();
        vectors = Vector3d.normalizeGroup(vectors, true);
        assertEquals(0, vectors.size());
        vectors = Vector3d.normalizeGroup(vectors, false);
        assertEquals(0, vectors.size());
    }

    @ParameterizedTest
    @MethodSource("normalizeGroupTestData")
    public void normalizeGroupTest(
            double aX,
            double aY,
            double aZ,
            double bX,
            double bY,
            double bZ,
            double expectedAX,
            double expectedAY,
            double expectedAZ,
            double expectedBX,
            double expectedBY,
            double expectedBZ) {
        Vector3d a = new Vector3d(aX, aY, aZ);
        Vector3d b = new Vector3d(bX, bY, bZ);

        ArrayList<Vector3d> list = new ArrayList<>();
        list.add(a);
        list.add(b);

        List<Vector3d> result = Vector3d.normalizeGroup(list, false);

        Vector3d resultA = result.get(0);
        Vector3d resultB = result.get(1);

        assertEquals(expectedAX, resultA.getX(), 1E-9);
        assertEquals(expectedAY, resultA.getY(), 1E-9);
        assertEquals(expectedAZ, resultA.getZ(), 1E-9);
        assertEquals(expectedBX, resultB.getX(), 1E-9);
        assertEquals(expectedBY, resultB.getY(), 1E-9);
        assertEquals(expectedBZ, resultB.getZ(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductDoubleTests(
            double x1, double y1, double z1, double x2, double y2, double z2, double result) {
        Vector3d a = new Vector3d(x1, y1, z1);
        Vector3d b = new Vector3d(x2, y2, z2);
        assertEquals(result, a.dot(b), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductDoubleTests(
            double x1,
            double y1,
            double z1,
            double x2,
            double y2,
            double z2,
            double resultX,
            double resultY,
            double resultZ) {
        Vector3d a = new Vector3d(x1, y1, z1);
        Vector3d b = new Vector3d(x2, y2, z2);
        Vector3d c = a.cross(b);
        assertEquals(resultX, c.getX(), 0.0001);
        assertEquals(resultY, c.getY(), 0.0001);
        assertEquals(resultZ, c.getZ(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductUnitTests(
            double x1, double y1, double z1, double x2, double y2, double z2, double result) {
        Vector3d a = new Vector3d(x1, y1, z1);
        Vector3dU<Frequency> b =
                new Vector3dU<>(Frequency.hertz(x2), Frequency.hertz(y2), Frequency.hertz(z2));
        assertEquals(result, a.dot(b).asHertz(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductUnitTests(
            double x1,
            double y1,
            double z1,
            double x2,
            double y2,
            double z2,
            double resultX,
            double resultY,
            double resultZ) {
        Vector3d a = new Vector3d(x1, y1, z1);
        Vector3dU<Frequency> b =
                new Vector3dU<>(Frequency.hertz(x2), Frequency.hertz(y2), Frequency.hertz(z2));
        Vector3dU<Frequency> c = a.cross(b);
        assertEquals(resultX, c.getX().asHertz(), 0.0001);
        assertEquals(resultY, c.getY().asHertz(), 0.0001);
        assertEquals(resultZ, c.getZ().asHertz(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("angleToTestData")
    public void angleToDoubleTest(
            double x1, double y1, double z1, double x2, double y2, double z2, double result_deg) {
        Vector3d a = new Vector3d(x1, y1, z1);
        Vector3d b = new Vector3d(x2, y2, z2);
        assertEquals(result_deg, a.angleTo(b).asDegrees(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("angleToTestData")
    public void angleToUnitTest(
            double x1, double y1, double z1, double x2, double y2, double z2, double result_deg) {
        Vector3d a = new Vector3d(x1, y1, z1);
        Vector3dU<Voltage> b = new Vector3dU<>(Voltage.volts(x2), Voltage.volts(y2), Voltage.volts(z2));
        assertEquals(result_deg, a.angleTo(b).asDegrees(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("normalizeOnlyScaleDownTestData")
    public void testNormalizeOnlyScaleDown(
            double aX, double aY, double aZ, double expectedX, double expectedY, double expectedZ) {
        Vector3d a = new Vector3d(aX, aY, aZ);

        Vector3d result = a.normalize(true);

        assertEquals(expectedX, result.getX(), 1E-9);
        assertEquals(expectedY, result.getY(), 1E-9);
        assertEquals(expectedZ, result.getZ(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("normalizeGroupOnlyScaleDownTestData")
    public void testNormalizeGroupOnlyScaleDown(
            double aX,
            double aY,
            double aZ,
            double bX,
            double bY,
            double bZ,
            double expectedAX,
            double expectedAY,
            double expectedAZ,
            double expectedBX,
            double expectedBY,
            double expectedBZ) {
        Vector3d a = new Vector3d(aX, aY, aZ);
        Vector3d b = new Vector3d(bX, bY, bZ);

        ArrayList<Vector3d> list = new ArrayList<Vector3d>();
        list.add(a);
        list.add(b);

        List<Vector3d> result = Vector3d.normalizeGroup(list, true);

        Vector3d resultA = result.get(0);
        Vector3d resultB = result.get(1);

        assertEquals(expectedAX, resultA.getX(), 1E-9);
        assertEquals(expectedAY, resultA.getY(), 1E-9);
        assertEquals(expectedAZ, resultA.getZ(), 1E-9);
        assertEquals(expectedBX, resultB.getX(), 1E-9);
        assertEquals(expectedBY, resultB.getY(), 1E-9);
        assertEquals(expectedBZ, resultB.getZ(), 1E-9);
    }

    @Test
    public void forEach() {
        Vector3d a = new Vector3d(6.0, 12.0, -5.0);

        Vector3d squared = a.forEach(v -> v * v);
        assertPercentError(36.0, squared.getX(), 0.0005);
        assertPercentError(144.0, squared.getY(), 0.0005);
        assertPercentError(25.0, squared.getZ(), 0.0005);

        Vector3d squaredPlus = a.forEach(squared, (v, s) -> v + s);
        assertPercentError(42.0, squaredPlus.getX(), 0.0005);
        assertPercentError(156.0, squaredPlus.getY(), 0.0005);
        assertPercentError(20.0, squaredPlus.getZ(), 0.0005);

        Vector3dU<Length> squaredInches = squared.forEachU(Length::inches);
        Vector3d squaredPlusInches = a.forEach(squaredInches, (v, s) -> v + s.asInches());
        assertPercentError(42.0, squaredPlusInches.getX(), 0.0005);
        assertPercentError(156.0, squaredPlusInches.getY(), 0.0005);
        assertPercentError(20.0, squaredPlusInches.getZ(), 0.0005);
    }

    @Test
    public void forEachU() {
        Vector3d a = new Vector3d(6.0, 12.0, -5.0);

        Vector3dU<Time> squared = a.forEachU(v -> Time.seconds(v * v));
        assertPercentError(36.0, squared.getX().asSeconds(), 0.0005);
        assertPercentError(144.0, squared.getY().asSeconds(), 0.0005);

        Vector3d squaredUnitless = squared.forEach(Time::asSeconds);
        Vector3dU<Time> squaredPlus = a.forEachU(squaredUnitless, (v, s) -> Time.seconds(v + s));
        assertPercentError(42.0, squaredPlus.getX().asSeconds(), 0.0005);
        assertPercentError(156.0, squaredPlus.getY().asSeconds(), 0.0005);

        Vector3dU<Time> squaredPlusSeconds =
                a.forEachU(squared, (v, s) -> Time.seconds(v).add(s));
        assertPercentError(42.0, squaredPlusSeconds.getX().asSeconds(), 0.0005);
        assertPercentError(156.0, squaredPlusSeconds.getY().asSeconds(), 0.0005);
    }
}
