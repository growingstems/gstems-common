package org.growingstems.math;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class AccumulatorTest {
    protected static Stream<Arguments> provideDoubles() {
        return Stream.of(
                Arguments.of(new Double[] {1.0, 1.0, 2.0, 3.0}, 7.0),
                Arguments.of(new Double[] {1.0, 0.0, -2.0, 3.0, 0.0, 1.0, 3.5}, 6.5),
                Arguments.of(new Double[] {1.0, 1.0, 0.0, 1.0}, 3.0),
                Arguments.of(new Double[] {-1.0, -1.0, -2.0, -3.0}, -7.0));
    }
}
