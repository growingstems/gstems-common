/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class Accumulator_Test extends AccumulatorTest {
    private static final double k_tolerance = 1E-10;

    @ParameterizedTest
    @MethodSource({"provideDoubles"})
    public void doubleAccumulationTest(Double[] testValues, double expected) {
        Accumulator accumulator = new Accumulator();
        for (Double i : testValues) {
            accumulator.update(i);
        }
        assertEquals(expected, accumulator.getValue(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource({"provideDoubles"})
    public void accumulationConstructionTest(Double[] testValues, double expected) {
        Accumulator accumulator = new Accumulator(testValues[0]);
        for (Double i : testValues) {
            accumulator.update(i);
        }
        assertEquals(expected + testValues[0], accumulator.getValue(), k_tolerance);
    }

    @Test
    public void accumulatorManipulationTest() {
        Accumulator accumulator = new Accumulator();
        assertEquals(1.0, accumulator.update(1.0), k_tolerance);
        assertEquals(2.0, accumulator.update(1.0), k_tolerance);
        accumulator.reset();
        assertEquals(0.0, accumulator.getValue(), k_tolerance);
        assertEquals(2.0, accumulator.update(2.0), k_tolerance);
        assertEquals(5.0, accumulator.update(3.0), k_tolerance);
        accumulator.setValue(1.0);
        assertEquals(1.0, accumulator.getValue(), k_tolerance);

        assertEquals(-4.0, accumulator.update(-5.0), k_tolerance);
        assertEquals(2.0, accumulator.update(6.0), k_tolerance);
        accumulator.reset();
        assertEquals(1.0, accumulator.update(1.0), k_tolerance);
        assertEquals(1.0, accumulator.update(0.0), k_tolerance);
        assertEquals(1.0, accumulator.update(0.0), k_tolerance);
        accumulator.setValue(0.0);
        assertEquals(0.0, accumulator.getValue(), k_tolerance);
    }

    @Test
    public void settersTest() {
        Accumulator accumulator = new Accumulator();
        accumulator.setValue(10.0);
        assertEquals(10.0, accumulator.getValue(), k_tolerance);

        accumulator.setLimits(new Range(-10.0, -5.0));
        assertEquals(-5.0, accumulator.getValue(), k_tolerance);

        accumulator.setValue(-20.0);
        assertEquals(-10.0, accumulator.getValue(), k_tolerance);
    }

    @Test
    public void resetTest() {
        Accumulator accumulator = new Accumulator();
        // Move to -1.0 then reset
        assertEquals(0.0, accumulator.getValue(), k_tolerance);
        assertEquals(-1.0, accumulator.update(-1.0), k_tolerance);
        accumulator.reset();
        assertEquals(0.0, accumulator.getValue(), k_tolerance);

        // Apply limits that don't contain 0.0, move to -6.0, then reset
        accumulator.setLimits(new Range(-10.0, -5.0));
        assertEquals(-5.0, accumulator.getValue(), k_tolerance);
        assertEquals(-6.0, accumulator.update(-1.0), k_tolerance);
        accumulator.reset();
        assertEquals(-5.0, accumulator.getValue(), k_tolerance);
    }
}
