/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Angle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class Pose2d_Test extends PoseTest {
    @ParameterizedTest
    @MethodSource("transformTestData")
    public void testTransform(
            double aX,
            double aY,
            double aRotation_deg,
            double translateX,
            double translateY,
            double rotation_deg,
            double expectedX,
            double expectedY,
            double expectedRotation_deg) {
        Pose2d a = new Pose2d(aX, aY, Angle.degrees(aRotation_deg));
        Vector2d transformVector = new Vector2d(translateX, translateY);

        Pose2d result = a.transform(transformVector, Angle.degrees(rotation_deg));

        assertEquals(expectedX, result.getX(), 1E-6, "X Position:");
        assertEquals(expectedY, result.getY(), 1E-6, "Y Position:");
        assertEquals(
                expectedRotation_deg,
                result.getRotation().difference(Angle.ZERO).asDegrees(),
                1E-6,
                "Rotation:");
    }
}
