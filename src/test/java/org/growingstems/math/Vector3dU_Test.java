/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.growingstems.test.AssertExtensions.assertPercentError;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Amount;
import org.growingstems.measurements.Measurements.Area;
import org.growingstems.measurements.Measurements.Force;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.measurements.Measurements.Voltage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class Vector3dU_Test extends Vector3Test {
    @ParameterizedTest
    @MethodSource("rangeToTestData")
    public void rangeToTest(
            double m_x,
            double m_y,
            double m_z,
            double other_x,
            double other_y,
            double other_z,
            double expectedDistance) {
        Vector3dU<Unitless> point1 =
                new Vector3dU<>(Unitless.none(m_x), Unitless.none(m_y), Unitless.none(m_z));
        Vector3dU<Unitless> point2 =
                new Vector3dU<>(Unitless.none(other_x), Unitless.none(other_y), Unitless.none(other_z));
        assertPercentError("Distance", expectedDistance, point1.rangeTo(point2).asNone(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("magnitudeTestData")
    public void magnitudeTest(double m_x, double m_y, double m_z, double magnitude) {
        Vector3dU<Length> point1 =
                new Vector3dU<>(Length.inches(m_x), Length.inches(m_y), Length.inches(m_z));
        assertPercentError("Magnitude", magnitude, point1.getMagnitude().asInches(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("additionTestData")
    public void additionTest(
            double m_x,
            double m_y,
            double m_z,
            double other_x,
            double other_y,
            double other_z,
            double expectedX,
            double expectedY,
            double expectedZ) {
        Vector3dU<Amount> point1 =
                new Vector3dU<>(Amount.count(m_x), Amount.count(m_y), Amount.count(m_z));
        Vector3dU<Amount> point2 =
                new Vector3dU<>(Amount.count(other_x), Amount.count(other_y), Amount.count(other_z));
        Vector3dU<Amount> resultVector = point1.add(point2);
        assertPercentError(expectedX, resultVector.getX().asCount(), 0.0005);
        assertPercentError(expectedY, resultVector.getY().asCount(), 0.0005);
        assertPercentError(expectedZ, resultVector.getZ().asCount(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("subtractionTestData")
    public void subtractionTest(
            double m_x,
            double m_y,
            double m_z,
            double other_x,
            double other_y,
            double other_z,
            double expectedX,
            double expectedY,
            double expectedZ) {
        Vector3dU<Velocity> point1 = new Vector3dU<>(
                Velocity.feetPerSecond(m_x), Velocity.feetPerSecond(m_y), Velocity.feetPerSecond(m_z));
        Vector3dU<Velocity> point2 = new Vector3dU<>(
                Velocity.feetPerSecond(other_x),
                Velocity.feetPerSecond(other_y),
                Velocity.feetPerSecond(other_z));
        Vector3dU<Velocity> resultVector = point1.sub(point2);
        assertPercentError(expectedX, resultVector.getX().asFeetPerSecond(), 0.0005);
        assertPercentError(expectedY, resultVector.getY().asFeetPerSecond(), 0.0005);
        assertPercentError(expectedZ, resultVector.getZ().asFeetPerSecond(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("multiplicationTestData")
    public void multiplicationTest(
            double m_x,
            double m_y,
            double m_z,
            double scalar,
            double scaleX,
            double scaleY,
            double scaleZ) {
        Vector3dU<Time> vector = new Vector3dU<>(Time.hours(m_x), Time.hours(m_y), Time.hours(m_z));
        Vector3dU<Time> scaledVector = vector.mul(scalar);
        assertPercentError(scaleX, scaledVector.getX().asHours(), 0.0005);
        assertPercentError(scaleY, scaledVector.getY().asHours(), 0.0005);
        assertPercentError(scaleZ, scaledVector.getZ().asHours(), 0.0005);
    }

    @ParameterizedTest
    @MethodSource("cylindricalTestData")
    public void cylindricalTest(
            double radius,
            double theta_deg,
            double height,
            double resultX,
            double resultY,
            double resultZ) {
        Unitless radiusU = Unitless.none(radius);
        Angle thetaU = Angle.degrees(theta_deg);
        Unitless heightU = Unitless.none(height);
        Vector3dU<Unitless> vector = Vector3dU.fromCylindrical(radiusU, thetaU, heightU);
        assertEquals(resultX, vector.getX().asNone(), 1E-9);
        assertEquals(resultY, vector.getY().asNone(), 1E-9);
        assertEquals(resultZ, vector.getZ().asNone(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("sphericalTestData")
    public void sphericalTest(
            double radius,
            double theta_deg,
            double phi_deg,
            double resultX,
            double resultY,
            double resultZ) {
        Time radiusU = Time.seconds(radius);
        Angle thetaU = Angle.degrees(theta_deg);
        Angle phiU = Angle.degrees(phi_deg);
        Vector3dU<Time> vector = Vector3dU.fromSpherical(radiusU, thetaU, phiU);
        assertEquals(resultX, vector.getX().asSeconds(), 1E-9);
        assertEquals(resultY, vector.getY().asSeconds(), 1E-9);
        assertEquals(resultZ, vector.getZ().asSeconds(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("azElTestData")
    public void azElTest(
            double radius,
            double azimuth_deg,
            double elevation_deg,
            double resultX,
            double resultY,
            double resultZ) {
        Frequency radiusU = Frequency.millihertz(radius);
        Angle azimuthU = Angle.degrees(azimuth_deg);
        Angle elevationU = Angle.degrees(elevation_deg);
        Vector3dU<Frequency> vector = Vector3dU.fromAzEl(radiusU, azimuthU, elevationU);
        assertEquals(resultX, vector.getX().asMillihertz(), 1E-9);
        assertEquals(resultY, vector.getY().asMillihertz(), 1E-9);
        assertEquals(resultZ, vector.getZ().asMillihertz(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("normalizeTestData")
    public void normalizeTest(
            double aX, double aY, double aZ, double expectedX, double expectedY, double expectedZ) {
        Vector3dU<Force> a = new Vector3dU<>(
                Force.kilogramsForce(aX), Force.kilogramsForce(aY), Force.kilogramsForce(aZ));

        Vector3d result = a.normalize();

        assertEquals(expectedX, result.getX(), 1E-9);
        assertEquals(expectedY, result.getY(), 1E-9);
        assertEquals(expectedZ, result.getZ(), 1E-9);
    }

    @Test
    public void normalizeEmptyGroup() {
        List<Vector3dU<Time>> vectors = new ArrayList<>();
        List<Vector3d> output = Vector3dU.normalizeGroup(vectors);
        assertEquals(0, output.size());
    }

    @ParameterizedTest
    @MethodSource("normalizeGroupTestData")
    public void normalizeGroupTest(
            double aX,
            double aY,
            double aZ,
            double bX,
            double bY,
            double bZ,
            double expectedAX,
            double expectedAY,
            double expectedAZ,
            double expectedBX,
            double expectedBY,
            double expectedBZ) {
        Vector3dU<Frequency> a =
                new Vector3dU<>(Frequency.gigahertz(aX), Frequency.gigahertz(aY), Frequency.gigahertz(aZ));
        Vector3dU<Frequency> b =
                new Vector3dU<>(Frequency.gigahertz(bX), Frequency.gigahertz(bY), Frequency.gigahertz(bZ));

        ArrayList<Vector3dU<Frequency>> list = new ArrayList<>();
        list.add(a);
        list.add(b);

        List<Vector3d> result = Vector3dU.normalizeGroup(list);

        Vector3d resultA = result.get(0);
        Vector3d resultB = result.get(1);

        assertEquals(expectedAX, resultA.getX(), 1E-9);
        assertEquals(expectedAY, resultA.getY(), 1E-9);
        assertEquals(expectedAZ, resultA.getZ(), 1E-9);
        assertEquals(expectedBX, resultB.getX(), 1E-9);
        assertEquals(expectedBY, resultB.getY(), 1E-9);
        assertEquals(expectedBZ, resultB.getZ(), 1E-9);
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductDoubleTests(
            double x1, double y1, double z1, double x2, double y2, double z2, double result) {
        Vector3dU<Length> a = new Vector3dU<>(Length.inches(x1), Length.inches(y1), Length.inches(z1));
        Vector3d b = new Vector3d(x2, y2, z2);
        assertEquals(result, a.dot(b).asInches(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductDoubleTests(
            double x1,
            double y1,
            double z1,
            double x2,
            double y2,
            double z2,
            double resultX,
            double resultY,
            double resultZ) {
        Vector3dU<Length> a = new Vector3dU<>(Length.inches(x1), Length.inches(y1), Length.inches(z1));
        Vector3d b = new Vector3d(x2, y2, z2);
        Vector3dU<Length> c = a.cross(b);
        assertEquals(resultX, c.getX().asInches(), 0.0001);
        assertEquals(resultY, c.getY().asInches(), 0.0001);
        assertEquals(resultZ, c.getZ().asInches(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductUnitTests(
            double x1, double y1, double z1, double x2, double y2, double z2, double result) {
        Vector3dU<Length> a = new Vector3dU<>(Length.inches(x1), Length.inches(y1), Length.inches(z1));
        Vector3dU<Frequency> b =
                new Vector3dU<>(Frequency.hertz(x2), Frequency.hertz(y2), Frequency.hertz(z2));
        assertEquals(result, a.dot(b, Length::mul).asInchesPerSecond(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductUnitTests(
            double x1,
            double y1,
            double z1,
            double x2,
            double y2,
            double z2,
            double resultX,
            double resultY,
            double resultZ) {
        Vector3dU<Length> a = new Vector3dU<>(Length.inches(x1), Length.inches(y1), Length.inches(z1));
        Vector3dU<Frequency> b =
                new Vector3dU<>(Frequency.hertz(x2), Frequency.hertz(y2), Frequency.hertz(z2));
        Vector3dU<Velocity> c = a.cross(b, Length::mul);
        assertEquals(resultX, c.getX().asInchesPerSecond(), 0.0001);
        assertEquals(resultY, c.getY().asInchesPerSecond(), 0.0001);
        assertEquals(resultZ, c.getZ().asInchesPerSecond(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("angleToTestData")
    public void angleToDoubleTest(
            double x1, double y1, double z1, double x2, double y2, double z2, double result_deg) {
        Vector3dU<Time> a = new Vector3dU<Time>(Time.days(x1), Time.days(y1), Time.days(z1));
        Vector3d b = new Vector3d(x2, y2, z2);
        assertEquals(result_deg, a.angleTo(b).asDegrees(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("angleToTestData")
    public void angleToUnitTest(
            double x1, double y1, double z1, double x2, double y2, double z2, double result_deg) {
        Vector3dU<Time> a = new Vector3dU<Time>(Time.days(x1), Time.days(y1), Time.days(z1));
        Vector3dU<Voltage> b = new Vector3dU<>(Voltage.volts(x2), Voltage.volts(y2), Voltage.volts(z2));
        assertEquals(result_deg, a.angleTo(b).asDegrees(), 0.0001);
    }

    @Test
    public void forEach() {
        Vector3dU<Length> a =
                new Vector3dU<>(Length.inches(6.0), Length.inches(12.0), Length.inches(-5.0));

        Vector3d squared = a.forEach(v -> v.mul(v).asInchesSquared());
        assertEquals(36.0, squared.getX(), 0.000001);
        assertEquals(144.0, squared.getY(), 0.000001);
        assertPercentError(25.0, squared.getZ(), 0.0005);

        Vector3d squaredPlus = a.forEach(squared, (v, s) -> v.asInches() + s);
        assertEquals(42.0, squaredPlus.getX(), 0.000001);
        assertEquals(156.0, squaredPlus.getY(), 0.000001);
        assertPercentError(20.0, squaredPlus.getZ(), 0.0005);

        Vector3dU<Length> squaredInches = squared.forEachU(Length::inches);
        Vector3d squaredPlusInches = a.forEach(squaredInches, (v, s) -> v.add(s).asInches());
        assertEquals(42.0, squaredPlusInches.getX(), 0.000001);
        assertEquals(156.0, squaredPlusInches.getY(), 0.000001);
        assertPercentError(20.0, squaredPlusInches.getZ(), 0.0005);
    }

    @Test
    public void forEachU() {
        Vector3dU<Length> a =
                new Vector3dU<>(Length.inches(6.0), Length.inches(12.0), Length.inches(-5.0));

        Vector3dU<Area> squared = a.forEachU(v -> v.mul(v));
        assertEquals(36.0, squared.getX().asInchesSquared(), 0.000001);
        assertEquals(144.0, squared.getY().asInchesSquared(), 0.000001);
        assertEquals(25.0, squared.getZ().asInchesSquared(), 0.000001);

        Vector3d squaredUnitless = squared.forEach(Area::asInchesSquared);
        Vector3dU<Length> squaredPlus = a.forEachU(squaredUnitless, (v, s) -> v.add(Length.inches(s)));
        assertEquals(42.0, squaredPlus.getX().asInches(), 0.000001);
        assertEquals(156.0, squaredPlus.getY().asInches(), 0.000001);
        assertEquals(20.0, squaredPlus.getZ().asInches(), 0.000001);

        Vector3dU<Unitless> squaredPlusDiv = a.forEachU(squaredPlus, Length::div);
        assertEquals(1.0 / 7.0, squaredPlusDiv.getX().asNone(), 0.000001);
        assertEquals(1.0 / 13.0, squaredPlusDiv.getY().asNone(), 0.000001);
        assertEquals(-0.25, squaredPlusDiv.getZ().asNone(), 0.000001);
    }
}
