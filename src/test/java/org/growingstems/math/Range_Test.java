/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class Range_Test extends RangeTest {
    private static double k_maxError = 1e-6;

    @ParameterizedTest
    @MethodSource("inRangeData")
    public void inRange(double low, double high, double input, boolean expected) {
        Range range = new Range(low, high);
        assertEquals(expected, range.inRange(input));
    }

    @ParameterizedTest
    @MethodSource("coerceValueData")
    public void coerceValue(double low, double high, double input, double expected) {
        Range range = new Range(low, high);
        assertEquals(expected, range.coerceValue(input), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("interpolateExtrapolateData")
    public void interpolateExtrapolate(
            double low, double high, double m, double expectedInt, double expectedEx) {
        Range range = new Range(low, high);
        assertEquals(expectedInt, range.interpolate(m), k_maxError);
        assertEquals(expectedEx, range.extrapolate(m), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("inverseInterpolateExtrapolateData")
    public void inverseInterpolateExtrapolate(
            double low, double high, double value, double expectedInt, double expectedEx) {
        Range range = new Range(low, high);
        assertEquals(expectedInt, range.inverseInterpolate(value), k_maxError);
        assertEquals(expectedEx, range.inverseExtrapolate(value), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("remapData")
    public void remap(
            double startlow,
            double startHigh,
            double endlow,
            double endHigh,
            double value,
            double expectedInt,
            double expectedEx) {
        Range start = new Range(startlow, startHigh);
        Range end = new Range(endlow, endHigh);
        assertEquals(expectedInt, start.remap(value, end, false), k_maxError);
        assertEquals(expectedEx, start.remap(value, end, true), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("remapDataInfiniteInput")
    @Disabled // Does not support infinite values yet. See gstems-common issue #194
    public void remapInfiniteInput(
            double startlow,
            double startHigh,
            double endlow,
            double endHigh,
            double value,
            double expectedInt,
            double expectedEx) {
        remap(startlow, startHigh, endlow, endHigh, value, expectedInt, expectedEx);
    }

    @ParameterizedTest
    @MethodSource("clampOrderedData")
    public void clampOrdered(
            double aLow, double aHigh, double bLow, double bHigh, double expLow, double expHigh) {
        Range range = new Range(aLow, aHigh);

        // Create and clamp to first clamp
        Range clamp = new Range(bLow, bHigh);

        range.clampTo(clamp);
        assertEquals(expLow, range.getLow(), k_maxError);
        assertEquals(expHigh, range.getHigh(), k_maxError);
    }

    @Test
    public void automaticOrdering() {
        Range range = new Range(1.0, 2.0);
        assertEquals(1.0, range.getLow(), k_maxError);
        assertEquals(2.0, range.getHigh(), k_maxError);

        range.setRange(5.0, 4.0);
        assertEquals(4.0, range.getLow(), k_maxError);
        assertEquals(5.0, range.getHigh(), k_maxError);
    }

    @Test
    public void reversedConstructionParams() {
        Range range = new Range(2.0, 1.0);
        assertEquals(1.0, range.getLow(), k_maxError);
        assertEquals(2.0, range.getHigh(), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("getWidthData")
    public void getWidthTest(double low, double high, double expected) {
        Range range = new Range(low, high);
        assertEquals(expected, range.getWidth(), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("getCenterData")
    public void getCenterTest(double low, double high, double expected) {
        Range range = new Range(low, high);
        assertEquals(expected, range.getCenter(), k_maxError);
    }
}
