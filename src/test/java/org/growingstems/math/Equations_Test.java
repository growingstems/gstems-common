/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Equations_Test {
    @ParameterizedTest
    @CsvSource({
        "1.0, 1.5, 50.0",
        "81.0, 49.0, 39.50617284",
        "-155.0, 59.5492, 138.41883870968",
        "0.0, 1.0, Infinity",
        "1.0, 0.0, 100.0",
        "0.0, 0.0, 0.0"
    })
    public void percentErrorTest(double expected, double actual, Double expectedError) {
        assertEquals(expectedError, Equations.percentError(expected, actual), 1e-9);
    }
}
