/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Amount;
import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Area;
import org.growingstems.measurements.Measurements.Current;
import org.growingstems.measurements.Measurements.Force;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Mass;
import org.growingstems.measurements.Measurements.Temperature;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class Vector2dU_Test extends Vector2Test {
    @ParameterizedTest
    @MethodSource("addTestData")
    public void testAdd(
            double aX,
            double aY,
            double bX,
            double bY,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2dU<Unitless> a = new Vector2dU<>(Unitless.none(aX), Unitless.none(aY));
        Vector2dU<Unitless> b = new Vector2dU<>(Unitless.none(bX), Unitless.none(bY));

        Vector2dU<Unitless> result = a.add(b);

        assertEquals(expectedX, result.getX().asNone(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY().asNone(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("multiplyTestData")
    public void testMultiply(
            double aX,
            double aY,
            double scaleFactor,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2dU<Length> l = new Vector2dU<>(Length.inches(aX), Length.inches(aY));

        Vector2dU<Length> result = l.mul(scaleFactor);

        assertEquals(expectedX, result.getX().asInches(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY().asInches(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("divideTestData")
    public void testDivide(
            double aX, double aY, double divisor, double expectedX, double expectedY, String testName) {
        Vector2dU<Length> l = new Vector2dU<>(Length.inches(aX), Length.inches(aY));

        Vector2dU<Length> result = l.div(divisor);

        assertEquals(expectedX, result.getX().asInches(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY().asInches(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("divideTestData")
    public void testDivideUnitless(
            double aX, double aY, double divisor, double expectedX, double expectedY, String testName) {
        Vector2dU<Length> l = new Vector2dU<>(Length.inches(aX), Length.inches(aY));

        Vector2dU<Unitless> result = l.div(Length.inches(divisor));

        assertEquals(expectedX, result.getX().asNone(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY().asNone(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("inverseTestData")
    public void testInverse(
            double aX, double aY, double expectedX, double expectedY, String testName) {
        Vector2dU<Amount> a = new Vector2dU<>(Amount.mole(aX), Amount.mole(aY));

        Vector2dU<Amount> result = a.neg();

        assertEquals(expectedX, result.getX().asMole(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY().asMole(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("magnitudeTestData")
    public void testMagnitude(double aX, double aY, double expectedMagnitude, String testName) {
        Vector2dU<Velocity> a = new Vector2dU<>(Velocity.feetPerSecond(aX), Velocity.feetPerSecond(aY));

        assertEquals(a.getMagnitude().asFeetPerSecond(), expectedMagnitude, 1E-9, testName);
    }

    @ParameterizedTest
    @MethodSource("normalizeTestData")
    public void testNormalize(
            double aX, double aY, double expectedX, double expectedY, String testName) {
        Vector2dU<Force> a = new Vector2dU<>(Force.kilogramsForce(aX), Force.kilogramsForce(aY));

        Vector2d result = a.normalize();

        assertEquals(expectedX, result.getX(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY(), 1E-9, testName + " - Y");
    }

    @Test
    public void normalizeEmptyGroup() {
        List<Vector2dU<Time>> vectors = new ArrayList<>();
        List<Vector2d> output = Vector2dU.normalizeGroup(vectors);
        assertEquals(0, output.size());
    }

    @ParameterizedTest
    @MethodSource("normalizeGroupTestData")
    public void testNormalizeGroup(
            double aX,
            double aY,
            double bX,
            double bY,
            double expectedAX,
            double expectedAY,
            double expectedBX,
            double expectedBY,
            String testName) {
        Vector2dU<Frequency> a = new Vector2dU<>(Frequency.gigahertz(aX), Frequency.gigahertz(aY));
        Vector2dU<Frequency> b = new Vector2dU<>(Frequency.gigahertz(bX), Frequency.gigahertz(bY));

        ArrayList<Vector2dU<Frequency>> list = new ArrayList<>();
        list.add(a);
        list.add(b);

        List<Vector2d> result = Vector2dU.normalizeGroup(list);

        Vector2d resultA = result.get(0);
        Vector2d resultB = result.get(1);

        assertEquals(expectedAX, resultA.getX(), 1E-9, testName + " - A(X)");
        assertEquals(expectedAY, resultA.getY(), 1E-9, testName + " - A(Y)");
        assertEquals(expectedBX, resultB.getX(), 1E-9, testName + " - B(X)");
        assertEquals(expectedBY, resultB.getY(), 1E-9, testName + " - B(Y)");
    }

    @ParameterizedTest
    @MethodSource("rotateTestData")
    public void testRotate(
            double startX,
            double startY,
            double rotation_deg,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2dU<AngularVelocity> test = new Vector2dU<>(
                AngularVelocity.revolutionsPerMinute(startX), AngularVelocity.revolutionsPerMinute(startY));

        Vector2dU<AngularVelocity> result = test.rotate(Angle.degrees(rotation_deg));

        assertEquals(expectedX, result.getX().asRevolutionsPerMinute(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY().asRevolutionsPerMinute(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("subtractTestData")
    public void testSubtract(
            double aX,
            double aY,
            double bX,
            double bY,
            double expectedX,
            double expectedY,
            String testName) {
        Vector2dU<Mass> a = new Vector2dU<>(Mass.grams(aX), Mass.grams(aY));
        Vector2dU<Mass> b = new Vector2dU<>(Mass.grams(bX), Mass.grams(bY));

        Vector2dU<Mass> result = a.sub(b);

        assertEquals(expectedX, result.getX().asGrams(), 1E-9, testName + " - X");
        assertEquals(expectedY, result.getY().asGrams(), 1E-9, testName + " - Y");
    }

    @ParameterizedTest
    @MethodSource("rangeToTestData")
    public void rangeToTest(
            double aX, double aY, double bX, double bY, double expectedRange, String testName) {
        Vector2dU<Current> a = new Vector2dU<>(Current.milliamps(aX), Current.milliamps(aY));
        Vector2dU<Current> b = new Vector2dU<>(Current.milliamps(bX), Current.milliamps(bY));

        assertEquals(expectedRange, a.rangeTo(b).asMilliamps(), 1E-5, testName);
    }

    @ParameterizedTest
    @MethodSource("constructPolarTestData")
    public void constructPolarTest(
            double magnitude, double angle_rad, double expectedX, double expectedY) {
        Vector2dU<Temperature> a =
                Vector2dU.fromPolar(Temperature.kelvin(magnitude), Angle.radians(angle_rad));
        assertEquals(expectedX, a.getX().asKelvin(), 0.000001);
        assertEquals(expectedY, a.getY().asKelvin(), 0.000001);
    }

    @ParameterizedTest
    @MethodSource("getMethodTestData")
    public void getMethodTests(double x, double y, double expectedMag, double expectedAngle_rad) {
        Vector2dU<Angle> a = new Vector2dU<>(Angle.revolutions(x), Angle.revolutions(y));
        assertEquals(expectedMag, a.getMagnitude().asRevolutions(), 0.000001);
        assertEquals(expectedAngle_rad, a.getAngle().asRadians(), 0.000001);
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductDoubleTests(double x1, double y1, double x2, double y2, double result) {
        Vector2dU<Length> a = new Vector2dU<>(Length.inches(x1), Length.inches(y1));
        Vector2d b = new Vector2d(x2, y2);
        assertEquals(result, a.dot(b).asInches(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductDoubleTests(
            double x1, double y1, double x2, double y2, double result, String testName) {
        Vector2dU<Length> a = new Vector2dU<>(Length.inches(x1), Length.inches(y1));
        Vector2d b = new Vector2d(x2, y2);
        assertEquals(result, a.cross(b).asInches(), 0.0001, testName);
    }

    @ParameterizedTest
    @MethodSource("dotProductTestData")
    public void dotProductUnitTests(double x1, double y1, double x2, double y2, double result) {
        Vector2dU<Length> a = new Vector2dU<>(Length.inches(x1), Length.inches(y1));
        Vector2dU<Frequency> b = new Vector2dU<>(Frequency.hertz(x2), Frequency.hertz(y2));
        assertEquals(result, a.dot(b, Length::mul).asInchesPerSecond(), 0.0001);
    }

    @ParameterizedTest
    @MethodSource("crossProductTestData")
    public void crossProductUnitTests(
            double x1, double y1, double x2, double y2, double result, String testName) {
        Vector2dU<Length> a = new Vector2dU<>(Length.inches(x1), Length.inches(y1));
        Vector2dU<Frequency> b = new Vector2dU<>(Frequency.hertz(x2), Frequency.hertz(y2));
        assertEquals(result, a.cross(b, Length::mul).asInchesPerSecond(), 0.0001, testName);
    }

    @Test
    public void forEach() {
        Vector2dU<Length> a = new Vector2dU<>(Length.inches(6.0), Length.inches(12.0));

        Vector2d squared = a.forEach(v -> v.mul(v).asInchesSquared());
        assertEquals(36.0, squared.getX(), 0.000001);
        assertEquals(144.0, squared.getY(), 0.000001);

        Vector2d squaredPlus = a.forEach(squared, (v, s) -> v.asInches() + s);
        assertEquals(42.0, squaredPlus.getX(), 0.000001);
        assertEquals(156.0, squaredPlus.getY(), 0.000001);

        Vector2dU<Length> squaredInches = squared.forEachU(Length::inches);
        Vector2d squaredPlusInches = a.forEach(squaredInches, (v, s) -> v.add(s).asInches());
        assertEquals(42.0, squaredPlusInches.getX(), 0.000001);
        assertEquals(156.0, squaredPlusInches.getY(), 0.000001);
    }

    @Test
    public void forEachU() {
        Vector2dU<Length> a = new Vector2dU<>(Length.inches(6.0), Length.inches(12.0));

        Vector2dU<Area> squared = a.forEachU(v -> v.mul(v));
        assertEquals(36.0, squared.getX().asInchesSquared(), 0.000001);
        assertEquals(144.0, squared.getY().asInchesSquared(), 0.000001);

        Vector2d squaredUnitless = squared.forEach(Area::asInchesSquared);
        Vector2dU<Length> squaredPlus = a.forEachU(squaredUnitless, (v, s) -> v.add(Length.inches(s)));
        assertEquals(42.0, squaredPlus.getX().asInches(), 0.000001);
        assertEquals(156.0, squaredPlus.getY().asInches(), 0.000001);

        Vector2dU<Unitless> squaredPlusDiv = a.forEachU(squaredPlus, Length::div);
        assertEquals(1.0 / 7.0, squaredPlusDiv.getX().asNone(), 0.000001);
        assertEquals(1.0 / 13.0, squaredPlusDiv.getY().asNone(), 0.000001);
    }
}
