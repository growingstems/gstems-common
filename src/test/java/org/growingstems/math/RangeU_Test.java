/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.Force;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Temperature;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class RangeU_Test extends RangeTest {
    private double k_maxError = 1e-6;

    @ParameterizedTest
    @MethodSource("inRangeData")
    public void inRange(double low, double high, double input, boolean output) {
        RangeU<Angle> range = new RangeU<>(Angle.degrees(low), Angle.degrees(high));
        assertEquals(output, range.inRange(Angle.degrees(input)));
    }

    @ParameterizedTest
    @MethodSource("coerceValueData")
    public void coerceValue(double low, double high, double input, double output) {
        RangeU<Length> range = new RangeU<>(Length.inches(low), Length.inches(high));
        assertEquals(output, range.coerceValue(Length.inches(input)).asInches(), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("interpolateExtrapolateData")
    public void interpolateExtrapolate(
            double low, double high, double m, double expectedInt, double expectedEx) {
        RangeU<Length> range = new RangeU<>(Length.inches(low), Length.inches(high));
        assertEquals(expectedInt, range.interpolate(m).asInches(), k_maxError);
        assertEquals(expectedEx, range.extrapolate(m).asInches(), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("inverseInterpolateExtrapolateData")
    public void inverseInterpolateExtrapolate(
            double low, double high, double value, double expectedInt, double expectedEx) {
        RangeU<Length> range = new RangeU<>(Length.inches(low), Length.inches(high));
        assertEquals(expectedInt, range.inverseInterpolate(Length.inches(value)), k_maxError);
        assertEquals(expectedEx, range.inverseExtrapolate(Length.inches(value)), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("remapData")
    public void remap(
            double startlow,
            double startHigh,
            double endlow,
            double endHigh,
            double value,
            double expectedInt,
            double expectedEx) {
        RangeU<Length> start = new RangeU<>(Length.inches(startlow), Length.inches(startHigh));
        RangeU<Temperature> end = new RangeU<>(Temperature.kelvin(endlow), Temperature.kelvin(endHigh));
        assertEquals(expectedInt, start.remap(Length.inches(value), end, false).asKelvin(), k_maxError);
        assertEquals(expectedEx, start.remap(Length.inches(value), end, true).asKelvin(), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("remapDataInfiniteInput")
    @Disabled // Does not support infinite values yet. See gstems-common issue #194
    public void remapInfiniteInput(
            double startlow,
            double startHigh,
            double endlow,
            double endHigh,
            double value,
            double expectedInt,
            double expectedEx) {
        remap(startlow, startHigh, endlow, endHigh, value, expectedInt, expectedEx);
    }

    @ParameterizedTest
    @MethodSource("clampOrderedData")
    public void clampOrdered(
            double aLow, double aHigh, double bLow, double bHigh, double expLow, double expHigh) {
        RangeU<Velocity> range =
                new RangeU<>(Velocity.milesPerHour(aLow), Velocity.milesPerHour(aHigh));

        // Create and clamp to first clamp
        RangeU<Velocity> clamp =
                new RangeU<>(Velocity.milesPerHour(bLow), Velocity.milesPerHour(bHigh));

        range.clampTo(clamp);
        assertEquals(expLow, range.getLow().asMilesPerHour(), k_maxError);
        assertEquals(expHigh, range.getHigh().asMilesPerHour(), k_maxError);
    }

    @Test
    public void automaticOrdering() {
        RangeU<Force> range = new RangeU<>(Force.newtons(1.0), Force.newtons(2.0));
        assertEquals(1.0, range.getLow().asNewtons(), k_maxError);
        assertEquals(2.0, range.getHigh().asNewtons(), k_maxError);

        range.setRange(Force.newtons(5.0), Force.newtons(4.0));
        assertEquals(4.0, range.getLow().asNewtons(), k_maxError);
        assertEquals(5.0, range.getHigh().asNewtons(), k_maxError);
    }

    @Test
    public void reversedConstructionParams() {
        RangeU<Acceleration> range = new RangeU<>(
                Acceleration.feetPerSecondSquared(2.0), Acceleration.feetPerSecondSquared(1.0));
        assertEquals(1.0, range.getLow().asFeetPerSecondSquared(), k_maxError);
        assertEquals(2.0, range.getHigh().asFeetPerSecondSquared(), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("getWidthData")
    public void getWidthTest(double low, double high, double expected) {
        RangeU<Temperature> range = new RangeU<>(Temperature.kelvin(low), Temperature.kelvin(high));
        assertEquals(expected, range.getWidth().asKelvin(), k_maxError);
    }

    @ParameterizedTest
    @MethodSource("getCenterData")
    public void getCenterTest(double low, double high, double expected) {
        RangeU<Temperature> range = new RangeU<>(Temperature.kelvin(low), Temperature.kelvin(high));
        assertEquals(expected, range.getCenter().asKelvin(), k_maxError);
    }
}
