package org.growingstems.math;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class Vector2Test {
    protected static Stream<Arguments> addTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, "Ax = 1.0"),
                Arguments.of(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, "Ay = 1.0"),
                Arguments.of(0.0, 0.0, 1.0, 0.0, 1.0, 0.0, "Bx = 1.0"),
                Arguments.of(0.0, 0.0, 0.0, 1.0, 0.0, 1.0, "By = 1.0"),
                Arguments.of(-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, "Ax = -1.0"),
                Arguments.of(0.0, -1.0, 0.0, 0.0, 0.0, -1.0, "Ay = -1.0"),
                Arguments.of(0.213, 0.781, 0.645, 0.426, 0.858, 1.207, "Random Numbers 1"),
                Arguments.of(0.17, -0.386, 0.135, 0.623, 0.305, 0.237, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, 20.4, -85.5, -8.1, -27.5, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, -57.4, 25.4, 24.1, 72.699999999999, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, -457.4, 356.7, 1883.83, 391.3, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, 2980.3, 4007.3, 3023.8, -20560.1, "Random Numbers 6"));
    }

    protected static Stream<Arguments> multiplyTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, "Default test"),
                Arguments.of(0.0, 0.0, 10.0, 0.0, 0.0, "Scale (0.0 0.0) by 10.0"),
                Arguments.of(1.0, 0.0, 10.0, 10.0, 0.0, "Scale (1.0 0.0) by 10.0"),
                Arguments.of(0.0, 1.0, 10.0, 0.0, 10.0, "Scale (0.0 1.0) by 10.0"),
                Arguments.of(1.0, 1.0, 10.0, 10.0, 10.0, "Scale (1.0 1.0) by 10.0"),
                Arguments.of(1.0, 1.0, -10.0, -10.0, -10.0, "Scale (1.0 1.0) by -10.0"),
                Arguments.of(0.213, 0.781, 23.5, 5.0055, 18.3535, "Random Numbers 1"),
                Arguments.of(0.17, -0.386, 0.564, 0.0958799999, -0.2177039999, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, 20.4, -581.4, 1183.19999999999, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, -57.4, -4678.0999999999, -2715.02, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, -457.4, -1070878.602, -15826.039999999, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, 2980.3, 129643.05, -73218222.22000001, "Random Numbers 6"));
    }

    protected static Stream<Arguments> divideTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, Double.NaN, Double.NaN, "NaN test"),
                Arguments.of(0.0, 0.0, 0.1, 0.0, 0.0, "Divide (0.0 0.0) by 0.1"),
                Arguments.of(1.0, 0.0, 0.1, 10.0, 0.0, "Divide (1.0 0.0) by 0.1"),
                Arguments.of(0.0, 1.0, 0.1, 0.0, 10.0, "Divide (0.0 1.0) by 0.1"),
                Arguments.of(1.0, 1.0, 0.1, 10.0, 10.0, "Divide (1.0 1.0) by 0.1"),
                Arguments.of(1.0, 1.0, -0.1, -10.0, -10.0, "Divide (1.0 1.0) by -0.1"),
                Arguments.of(
                        0.213, 0.781, 23.5, 0.00906382978723404, 0.0332340425531915, "Random Numbers 1"),
                Arguments.of(
                        0.17, -0.386, 0.564, 0.301418439716312, -0.684397163120567, "Random Numbers 2"),
                Arguments.of(-28.5, 58, 20.4, -1.39705882352941, 2.84313725490196, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, -57.4, -1.4198606271777, -0.82404181184669, "Random Numbers 4"),
                Arguments.of(
                        2341.23, 34.6, -457.4, -5.11856143419327, -0.0756449497157849, "Random Numbers 5"),
                Arguments.of(
                        43.5, -24567.4, 2980.3, 0.0145958460557662, -8.24326410092944, "Random Numbers 6"));
    }

    protected static Stream<Arguments> inverseTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, -1.0, 0.0, "Ax = 1.0"),
                Arguments.of(0.0, 1.0, 0.0, -1.0, "Ay = 1.0"),
                Arguments.of(-1.0, 0.0, 1.0, 0.0, "Ax = -1.0"),
                Arguments.of(0.0, -1.0, 0.0, 1.0, "Ay = -1.0"),
                Arguments.of(2.0, 2.0, -2.0, -2.0, "(2.0 2.0)"),
                Arguments.of(0.213, 0.781, -0.213, -0.781, "Random Numbers 1"),
                Arguments.of(0.17, -0.386, -0.17, 0.386, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, 28.5, -58.0, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, -81.5, -47.3, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, -2341.23, -34.6, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, -43.5, 24567.4, "Random Numbers 6"));
    }

    protected static Stream<Arguments> magnitudeTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, 1.0, "Ax = 1.0"),
                Arguments.of(0.0, 1.0, 1.0, "Ay = 1.0"),
                Arguments.of(-1.0, 0.0, 1.0, " Ax = -1.0"),
                Arguments.of(0.0, -1.0, 1.0, "Ay = -1.0"),
                Arguments.of(1.0, 1.0, 1.414213562, "(1.0 1.0)"),
                Arguments.of(0.213, 0.781, 0.8095245518, "Random Numbers 1"),
                Arguments.of(0.17, -0.386, 0.4217771923, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, 64.62391198310421, " Random Numbers 3"),
                Arguments.of(81.5, 47.3, 94.2313111444386, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, 2341.4856550703016, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, 24567.438511371107, "Random Numbers 6"));
    }

    protected static Stream<Arguments> normalizeTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, Double.NaN, Double.NaN, "Default test"),
                Arguments.of(1.0, 0.0, 1.0, 0.0, "Ax = 1.0"),
                Arguments.of(0.0, 1.0, 0.0, 1.0, "Ay = 1.0"),
                Arguments.of(0.01, 0.0, 1.0, 0.0, "Ax = 0.01"),
                Arguments.of(0.0, 0.01, 0.0, 1.0, "Ay = 0.01"),
                Arguments.of(-1.0, -1.0, -0.707106781, -0.707106781, "(-1.0 -1.0)"),
                Arguments.of(0.213, 0.781, 0.263117405, 0.964763821, "Random Numbers 1"),
                Arguments.of(0.17, -0.386, 0.403056407, -0.915175137, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, -0.44101322754108835, 0.8975006034169517, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, 0.8648929852528111, 0.5019562969626744, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, 0.9998908150174877, 0.014776942974250747, "Random Numbers 5"),
                Arguments.of(
                        43.5, -24567.4, 0.0017706363640583003, -0.9999984324222044, "Random Numbers 6"));
    }

    protected static Stream<Arguments> normalizeGroupTestData() {
        return Stream.of(
                Arguments.of(
                        0.0, 0.0, 0.0, 0.0, Double.NaN, Double.NaN, Double.NaN, Double.NaN, "Default test"),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, "Test 1"),
                Arguments.of(1.0, 0.0, 0.25, 0.3, 1.0, 0.0, 0.25, 0.3, "Test 2"),
                Arguments.of(0.5, 0.0, 0.25, 0.0, 1.0, 0.0, 0.5, 0.0, "Test 3"),
                Arguments.of(2.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.5, "Test 4"),
                Arguments.of(
                        0.213,
                        0.781,
                        0.741,
                        0.275,
                        0.26311740579,
                        0.96476382123,
                        0.9153521018,
                        0.33970557085,
                        "Random Numbers 1"),
                Arguments.of(
                        0.17,
                        0.86,
                        0.305,
                        0.126,
                        0.193921957083,
                        0.98101695936,
                        0.347918805356,
                        0.14373039172,
                        "Random Numbers 2"),
                Arguments.of(
                        -28.5,
                        58.0,
                        90.6,
                        68.4,
                        -0.25105582004,
                        0.51092061623,
                        0.79809323845,
                        0.60253396810,
                        " Random Numbers 3"),
                Arguments.of(
                        81.5,
                        47.3,
                        14.8,
                        94.2,
                        0.85469596792,
                        0.496038273409,
                        0.15520859294,
                        0.98788171998,
                        "Random Numbers 4"),
                Arguments.of(
                        2341.23,
                        34.6,
                        1.23,
                        5.62,
                        0.99989081501,
                        0.0147769429742,
                        0.00052530751035,
                        0.0024001855351,
                        "Random Numbers 5"),
                Arguments.of(
                        43.5,
                        -24567.4,
                        567.2,
                        2319.2,
                        0.00177063636405,
                        -0.99999843242,
                        0.023087470015,
                        0.094401375989,
                        "Random Numbers 6"));
    }

    protected static Stream<Arguments> rotateTestData() {
        return Stream.of(
                Arguments.of(1.0, 0.0, 0.0, 1.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, 90.0, 0.0, 1.0, "+90 Degrees test"),
                Arguments.of(1.0, 0.0, -90.0, 0.0, -1.0, "-90 Degrees test"),
                Arguments.of(1.0, 0.0, 180.0, -1.0, 0.0, "+180 Degrees test"),
                Arguments.of(1.0, 0.0, -180.0, -1.0, 0.0, "-180 Degrees test"),
                Arguments.of(1.0, 0.0, 10.0, 0.984807753, 0.1736481777, "10 Degrees test"),
                Arguments.of(1.0, 0.0, -10.0, 0.984807753, -0.1736481777, "-10 Degrees test"),
                Arguments.of(1.0, 0.0, 30.0, 0.8660254038, 0.5, "30 Degrees test"),
                Arguments.of(1.0, 0.0, -30.0, 0.8660254038, -0.5, "-30 Degrees test"),
                Arguments.of(1.0, 0.0, 45.0, 0.7071067812, 0.7071067812, "45 Degrees test"),
                Arguments.of(1.0, 0.0, -45.0, 0.7071067812, -0.7071067812, "-45 Degrees test"),
                Arguments.of(1.0, 0.0, 70.0, 0.3420201433, 0.9396926208, "70 Degrees test"),
                Arguments.of(1.0, 0.0, -70.0, 0.3420201433, -0.9396926208, "-70 Degrees test"),
                Arguments.of(1.0, 0.0, 120.0, -0.5, 0.8660254038, "120 Degrees test"),
                Arguments.of(1.0, 0.0, -120.0, -0.5, -0.8660254038, "-120 Degrees test"),
                Arguments.of(0.213, 0.781, 843.2, -0.770143895, -0.249416078, "Random Numbers 1"),
                Arguments.of(0.17, 0.864, 0.357, 0.164613301, 0.865042461, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, 27.5, -52.0612283132, 38.28679284713, " Random Numbers 3"),
                Arguments.of(81.5, 47.3, -58.3, 83.0693050381, -44.486296322, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, -678.5, 1730.550948514, 1577.259803423, "Random Numbers 5"),
                Arguments.of(
                        43.5, -24567.4, -5823.2, -21908.89966322, -11115.716376236, "Random Numbers 6"));
    }

    protected static Stream<Arguments> subtractTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, "Ax = 1.0"),
                Arguments.of(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, "Ay = 1.0"),
                Arguments.of(0.0, 0.0, 1.0, 0.0, -1.0, 0.0, "Bx = 1.0"),
                Arguments.of(0.0, 0.0, 0.0, 1.0, 0.0, -1.0, "By = 1.0"),
                Arguments.of(-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, " Ax = -1.0"),
                Arguments.of(0.0, -1.0, 0.0, 0.0, 0.0, -1.0, "Ay = -1.0"),
                Arguments.of(0.213, 0.781, 0.645, 0.426, -0.432, 0.355, "Random Numbers 1"),
                Arguments.of(0.17, -0.386, 0.135, 0.623, 0.035, -1.009, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, -20.4, 85.5, -8.1, -27.5, " Random Numbers 3"),
                Arguments.of(81.5, 47.3, 57.4, -25.4, 24.1, 72.7, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, 457.4, -356.7, 1883.83, 391.3, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, -2980.3, -4007.3, 3023.8, -20560.1, "Random Numbers 6"));
    }

    protected static Stream<Arguments> rangeToTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 1.0, "Ax = 1.0"),
                Arguments.of(0.0, 1.0, 0.0, 0.0, 1.0, "Ay = 1.0"),
                Arguments.of(0.0, 0.0, 1.0, 0.0, 1.0, "Bx = 1.0"),
                Arguments.of(0.0, 0.0, 0.0, 1.0, 1.0, "By = 1.0"),
                Arguments.of(-1.0, 0.0, 0.0, 0.0, 1.0, "Ax = -1.0"),
                Arguments.of(0.0, -1.0, 0.0, 0.0, 1.0, "Ay = -1.0"),
                Arguments.of(0.213, 0.781, 0.125, -0.2358, 1.0206009, "Random Numbers 1"),
                Arguments.of(0.17, 0.86, -0.362, -0.267, 1.246255, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, -20.4, 85.5, 28.66810074, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, 57.4, -25.4, 76.59046938, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, 457.4, -356.7, 1924.040322, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, -2980.3, -4007.3, 20781.26749, "Random Numbers 6"));
    }

    protected static Stream<Arguments> constructPolarTestData() {
        return Stream.of(
                Arguments.of(1.04719755, 3.28986812, -1.03570698, -0.15470538),
                Arguments.of(5.96, 18.7238922, 5.9130036, -0.74698607),
                Arguments.of(7.98, 25.0699093756, 7.96425329, -0.50106834));
    }

    protected static Stream<Arguments> getMethodTestData() {
        return Stream.of(
                Arguments.of(12, 33, 35.11409973, 1.222025323),
                Arguments.of(4, 3, 5, 0.6435011088),
                Arguments.of(2, 2, 2.828427125, 0.7853981634),
                Arguments.of(1, 1, 1.414213562373, 0.785398163397),
                Arguments.of(-1, 1, 1.414213562373, 2.35619449019),
                Arguments.of(-1, -1, 1.414213562373, -2.35619449),
                Arguments.of(1, -1, 1.414213562373, -0.7853981634));
    }

    protected static Stream<Arguments> normalizeOnlyScaleDownTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, 1.0, 0.0, "Ax = 1.0"),
                Arguments.of(0.0, 1.0, 0.0, 1.0, "Ay = 1.0"),
                Arguments.of(0.01, 0.0, 0.01, 0.0, "Ax = 0.01"),
                Arguments.of(0.0, 0.01, 0.0, 0.01, "Ay = 0.01"),
                Arguments.of(-1.0, -1.0, -0.707106781, -0.707106781, "(-1.0 -1.0)"),
                Arguments.of(0.213, 0.781, 0.213, 0.781, "Random Numbers 1"),
                Arguments.of(0.17, -0.386, 0.17, -0.386, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, -0.44101322754108835, 0.8975006034169517, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, 0.8648929852528111, 0.5019562969626744, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, 0.9998908150174877, 0.014776942974250747, "Random Numbers 5"),
                Arguments.of(
                        43.5, -24567.4, 0.0017706363640583003, -0.9999984324222044, "Random Numbers 6"));
    }

    protected static Stream<Arguments> normalizeGroupOnlyScaleDownTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "Default test"),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, "Test 1"),
                Arguments.of(1.0, 0.0, 0.25, 0.3, 1.0, 0.0, 0.25, 0.3, "Test 2"),
                Arguments.of(0.5, 0.0, 0.25, 0.0, 0.5, 0.0, 0.25, 0.0, "Test 3"),
                Arguments.of(2.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.5, "Test 4"),
                Arguments.of(0.213, 0.781, 0.741, 0.275, 0.213, 0.781, 0.741, 0.275, "Random Numbers 1"),
                Arguments.of(0.17, 0.86, 0.305, 0.126, 0.17, 0.86, 0.305, 0.126, "Random Numbers 2"),
                Arguments.of(
                        -28.5,
                        58.0,
                        90.6,
                        68.4,
                        -0.25105582004,
                        0.51092061623,
                        0.79809323845,
                        0.60253396810,
                        " Random Numbers 3"),
                Arguments.of(
                        81.5,
                        47.3,
                        14.8,
                        94.2,
                        0.85469596792,
                        0.496038273409,
                        0.15520859294,
                        0.98788171998,
                        "Random Numbers 4"),
                Arguments.of(
                        2341.23,
                        34.6,
                        1.23,
                        5.62,
                        0.99989081501,
                        0.0147769429742,
                        0.00052530751035,
                        0.0024001855351,
                        "Random Numbers 5"),
                Arguments.of(
                        43.5,
                        -24567.4,
                        567.2,
                        2319.2,
                        0.00177063636405,
                        -0.99999843242,
                        0.023087470015,
                        0.094401375989,
                        "Random Numbers 6"));
    }

    protected static Stream<Arguments> dotProductTestData() {
        return Stream.of(
                Arguments.of(1.0, 0.0, 0.0, 1.0, 0.0, "Basic Test 1"),
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, "Basic Test 2"),
                Arguments.of(1.0, 0.0, 1.0, 0.0, 1.0, "Basic Test 3"),
                Arguments.of(1.0, 1.0, 1.0, 1.0, 2.0, "Basic Test 4"),
                Arguments.of(-1.0, 1.0, 1.0, -1.0, -2.0, "Basic Test 5"),
                Arguments.of(2.0, 1.0, 2.0, 1.0, 5.0, "Basic Test 6"),
                Arguments.of(1.0, 0.0, 0.0, 1.0, 0.0, "Basic Test 7"),
                Arguments.of(-6.0, 8.0, 5.0, 12.0, 66.0, "Basic Test 8"),
                Arguments.of(0.213, 0.781, 74.1, 27.5, 37.260799999, "Random Numbers 1"),
                Arguments.of(0.317, -0.486, 0.305, 0.126, 0.0354489999, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, 90.6, 68.4, 1385.1, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, 14.8, 94.2, 5661.86, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, 1.23, 5.62, 3074.1648999999, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, 567.2, 2319.2, -56952040.879999995, "Random Numbers 6"),
                Arguments.of(0.61, 2.066, 2.77, 1.46, 4.70606, "Random Numbers 7"));
    }

    protected static Stream<Arguments> crossProductTestData() {
        return Stream.of(
                Arguments.of(1.0, 0.0, 0.0, 1.0, 1.0, "Basic Test 1"),
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, "Basic Test 2"),
                Arguments.of(1.0, 1.0, 1.0, 1.0, 0.0, "Basic Test 3"),
                Arguments.of(1.0, 0.0, -1.0, 0.0, 0.0, "Basic Test 4"),
                Arguments.of(1.0, 0.0, 1.0, 1.0, 1.0, "Basic Test 5"),
                Arguments.of(1.0, 1.0, 1.0, 0.0, -1.0, "Basic Test 6"),
                Arguments.of(-1.0, 1.0, 1.0, 0.0, -1.0, "Basic Test 7"),
                Arguments.of(2.0, 0.0, 0.0, 2.0, 4.0, "Basic Test 8"),
                Arguments.of(0.5, 0.5, 0.0, 1.0, 0.5, "Basic Test 9"),
                Arguments.of(0.213, 0.781, 74.1, 27.5, -52.014599999, "Random Numbers 1"),
                Arguments.of(0.317, -0.486, 0.305, 0.126, 0.188172, "Random Numbers 2"),
                Arguments.of(-28.5, 58.0, 90.6, 68.4, -7204.1999999999, "Random Numbers 3"),
                Arguments.of(81.5, 47.3, 14.8, 94.2, 6977.26, "Random Numbers 4"),
                Arguments.of(2341.23, 34.6, 1.23, 5.62, 13115.1546, "Random Numbers 5"),
                Arguments.of(43.5, -24567.4, 567.2, 2319.2, 14035514.48, "Random Numbers 6"),
                Arguments.of(0.61, 2.066, 2.77, 1.46, -4.83222, "Random Numbers 7"));
    }
}
