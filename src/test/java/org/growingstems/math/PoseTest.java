package org.growingstems.math;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class PoseTest {
    protected static Stream<Arguments> transformTestData() {
        return Stream.of(
                Arguments.of(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 0.0, 90.0, 1.0, 0.0, 90.0),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 0.0, -90.0, 1.0, 0.0, -90.0),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 0.0, 45.0, 1.0, 0.0, 45.0),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 0.0, -45.0, 1.0, 0.0, -45.0),
                Arguments.of(1.0, 0.0, 0.0, 0.0, 0.0, 360.0, 1.0, 0.0, 0.0),
                Arguments.of(1.0, 0.0, 90.0, 0.0, 0.0, -180.0, 1.0, 0.0, -90.0),
                Arguments.of(1.0, 0.0, -90.0, 0.0, 0.0, 180.0, 1.0, 0.0, 90.0),
                Arguments.of(1.0, 0.0, 90.0, 0.0, 0.0, 180.0, 1.0, 0.0, -90.0),
                Arguments.of(1.0, 0.0, -90.0, 0.0, 0.0, -180.0, 1.0, 0.0, 90.0),
                Arguments.of(1.0, 0.0, -90.0, 0.0, 0.0, -90.0, 1.0, 0.0, 180.0),
                Arguments.of(1.0, 0.0, 90.0, 0.0, 0.0, 90.0, 1.0, 0.0, 180.0),
                Arguments.of(1.0, 0.0, -66.787, 0.0, 0.0, 155.667, 1.0, 0.0, 88.88),
                Arguments.of(1.0, 0.0, 66.787, 0.0, 0.0, -155.667, 1.0, 0.0, -88.88),
                Arguments.of(1.0, 0.0, -66.787, 0.0, 0.0, -155.667, 1.0, 0.0, 137.546));
    }
}
