/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class RateCalculator_Test {
    private TestTimeSource m_timeSource = new TestTimeSource(Time.seconds(1.0));
    private RateCalculator rateCalculator = new RateCalculator(m_timeSource);
    private static final double k_eps = 1E-10;

    @Test
    public void noTime() {
        Frequency rate = rateCalculator.update(-1.0);
        assertEquals(0.0, rate.asHertz(), k_eps);

        m_timeSource.addTime(Time.seconds(1.0));
        rate = rateCalculator.update(1.0);
        assertEquals(2.0, rate.asHertz(), k_eps);

        rate = rateCalculator.update(2.0); // increasing with no time
        assertEquals(2.0, rate.asHertz(), k_eps);

        m_timeSource.addTime(Time.seconds(1.0));
        rate = rateCalculator.update(1.0); // decreasing with time change
        assertEquals(-1.0, rate.asHertz(), k_eps);

        rate = rateCalculator.update(0.0); // decreasing with no time
        assertEquals(-1.0, rate.asHertz(), k_eps);

        rate = rateCalculator.update(0.0); // No change, no time
        assertEquals(-1.0, rate.asHertz(), k_eps);

        m_timeSource.addTime(Time.seconds(1.0));
        rate = rateCalculator.update(10.0); // increasing with time change
        assertEquals(10.0, rate.asHertz(), k_eps);
    }

    @Test
    public void test() {
        Frequency rate = rateCalculator.update(2.0);
        assertEquals(0.0, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(4.0));
        rate = rateCalculator.update(3.0);
        assertEquals(1.0 / 3.0, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(5.5));
        rate = rateCalculator.update(3.0);
        assertEquals(0.0 / 1.5, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(8.0));
        rate = rateCalculator.update(4.0);
        assertEquals(1.0 / 2.5, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(9.0));
        rate = rateCalculator.update(5.0);
        assertEquals(1.0 / 1.0, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(9.5));
        rate = rateCalculator.update(5.5);
        assertEquals(0.5 / 0.5, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(13.0));
        rate = rateCalculator.update(6.0);
        assertEquals(0.5 / 3.5, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(17.5));
        rate = rateCalculator.update(7.5);
        assertEquals(1.5 / 4.5, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(21.0));
        rate = rateCalculator.update(9.0);
        assertEquals(1.5 / 3.5, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(29.0));
        rate = rateCalculator.update(13.0);
        assertEquals(4.0 / 8.0, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(33.0));
        rate = rateCalculator.update(19.5);
        assertEquals(6.5 / 4.0, rate.asHertz(), k_eps);

        m_timeSource.setTime(Time.seconds(38.0));
        rate = rateCalculator.update(24.0);
        assertEquals(4.5 / 5.0, rate.asHertz(), k_eps);
    }
}
