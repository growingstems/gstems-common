/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class TimedIntegratorU_Test extends TimedIntegratorTest {
    public TestTimeSource m_timeSource = new TestTimeSource();
    public TimedIntegratorU<Velocity, Length> m_integrator =
            new TimedIntegratorU<>(m_timeSource, Length.ZERO);
    public static final double k_tolerance = 1E-10;

    private void verifyIteration(IntegralIteration iteration) {
        m_timeSource.setTime(iteration.time);
        assertEquals(
                iteration.output,
                m_integrator.update(Velocity.inchesPerSecond(iteration.input)).asInches(),
                k_tolerance);
        assertEquals(iteration.output, m_integrator.getAccumulatedValue().asInches(), k_tolerance);
    }

    @Test
    public void basicUsage() {
        getBasicUsageData().forEach(this::verifyIteration);
    }

    @Test
    public void startWithNonZeroInput() {
        getStartWithNonZeroInputData().forEach(this::verifyIteration);
    }

    @Test
    public void reset() {
        getResetData().forEach(this::verifyIteration);
        m_integrator.reset();
        verifyIteration(getLastResetDatum());
    }
}
