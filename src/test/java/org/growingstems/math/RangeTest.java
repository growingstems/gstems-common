package org.growingstems.math;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class RangeTest {
    // short-hand infinity values for formatting purposes
    private static final double pInf = Double.POSITIVE_INFINITY;
    private static final double nInf = Double.NEGATIVE_INFINITY;

    protected static Stream<Arguments> inRangeData() {
        return Stream.of(
                // Basic tests
                Arguments.of(1.0, 5.0, 4.0, true),
                Arguments.of(1.0, 5.0, 2.5, true),

                // Checking edge cases
                Arguments.of(1.0, 5.0, 0.0, false),
                Arguments.of(1.0, 5.0, 55.0, false),
                Arguments.of(1.0, 5.0, -1.0, false),

                // Checking when a=b
                Arguments.of(1.0, 5.0, 5.0, true),
                Arguments.of(1.0, 5.0, 1.0, true),
                Arguments.of(-5.0, -1.0, 4.0, false),
                Arguments.of(-5.0, -1.0, -4.0, true),
                Arguments.of(-5.0, -1.0, -5.0, true),
                Arguments.of(-5.0, -1.0, 0.0, false),
                Arguments.of(-5.0, -1.0, -6.0, false),
                Arguments.of(1.0, 1.0, 1.0, true),
                Arguments.of(1.0, 1.0, 0.0, false),

                // Checking infinite values
                Arguments.of(nInf, 1.0, nInf, true),
                Arguments.of(nInf, 1.0, 0.0, true),
                Arguments.of(nInf, 1.0, 2.0, false),
                Arguments.of(nInf, 1.0, pInf, false),
                Arguments.of(1.0, pInf, nInf, false),
                Arguments.of(1.0, pInf, 0.0, false),
                Arguments.of(1.0, pInf, 2.0, true),
                Arguments.of(1.0, pInf, pInf, true),
                Arguments.of(nInf, pInf, nInf, true),
                Arguments.of(nInf, pInf, 0.0, true),
                Arguments.of(nInf, pInf, 2.0, true),
                Arguments.of(nInf, pInf, pInf, true));
    }

    protected static Stream<Arguments> coerceValueData() {
        return Stream.of(
                Arguments.of(1.0, 5.0, 4.0, 4.0),
                Arguments.of(1.0, 5.0, 6.0, 5.0),
                Arguments.of(1.0, 5.0, 1.0, 1.0),
                Arguments.of(1.0, 5.0, 5.0, 5.0),
                Arguments.of(-5.0, -1.0, -4.0, -4.0),
                Arguments.of(-5.0, -1.0, -6.0, -5.0),
                Arguments.of(-5.0, -1.0, -1.0, -1.0),
                Arguments.of(-5.0, -1.0, -5.0, -5.0),
                Arguments.of(1.0, 1.0, 1.0, 1.0),
                Arguments.of(1.0, 1.0, 5.0, 1.0),
                Arguments.of(1.0, 1.0, 0.0, 1.0),
                Arguments.of(nInf, 1.0, 0.0, 0.0),
                Arguments.of(nInf, 1.0, 2.0, 1.0),
                Arguments.of(nInf, 1.0, pInf, 1.0),
                Arguments.of(nInf, 1.0, nInf, nInf),
                Arguments.of(1.0, pInf, 0.0, 1.0),
                Arguments.of(1.0, pInf, 2.0, 2.0),
                Arguments.of(1.0, pInf, nInf, 1.0),
                Arguments.of(1.0, pInf, pInf, pInf),
                Arguments.of(nInf, pInf, nInf, nInf),
                Arguments.of(nInf, pInf, 0.0, 0.0),
                Arguments.of(nInf, pInf, pInf, pInf));
    }

    protected static Stream<Arguments> interpolateExtrapolateData() {
        return Stream.of(
                Arguments.of(0.0, 10.0, 0.0, 0.0, 0.0),
                Arguments.of(0.0, 10.0, 1.0, 10.0, 10.0),
                Arguments.of(0.0, 10.0, 0.2, 2.0, 2.0),
                Arguments.of(0.0, 10.0, 0.5, 5.0, 5.0),
                Arguments.of(0.0, 10.0, 0.7, 7.0, 7.0),
                Arguments.of(0.0, 10.0, -1.0, 0.0, -10.0),
                Arguments.of(0.0, 10.0, 2.0, 10.0, 20.0),
                Arguments.of(10.0, 0.0, 0.5, 5.0, 5.0),
                Arguments.of(-10.0, 0.0, 0.5, -5.0, -5.0),
                Arguments.of(0.0, -10.0, 0.5, -5.0, -5.0),
                Arguments.of(-2.0, 3.0, 0.5, 0.5, 0.5),
                Arguments.of(nInf, 3.0, 0.0, nInf, nInf),
                Arguments.of(nInf, 3.0, -1.0, nInf, nInf),
                Arguments.of(nInf, 3.0, 0.9, nInf, nInf),
                Arguments.of(nInf, 3.0, 1.0, 3.0, 3.0),
                Arguments.of(nInf, 3.0, 2.0, 3.0, pInf),
                Arguments.of(3.0, pInf, 1.0, pInf, pInf),
                Arguments.of(3.0, pInf, 2.0, pInf, pInf),
                Arguments.of(3.0, pInf, 0.1, pInf, pInf),
                Arguments.of(3.0, pInf, 0.0, 3.0, 3.0),
                Arguments.of(3.0, pInf, -1.0, 3.0, nInf),
                Arguments.of(nInf, pInf, 0.0, nInf, nInf),
                Arguments.of(nInf, pInf, 1.0, pInf, pInf),
                Arguments.of(nInf, pInf, 0.1, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, -1.0, nInf, nInf),
                Arguments.of(nInf, pInf, 2.0, pInf, pInf));
    }

    protected static Stream<Arguments> inverseInterpolateExtrapolateData() {
        return Stream.of(
                Arguments.of(0.0, 10.0, 0.0, 0.0, 0.0),
                Arguments.of(0.0, 10.0, 10.0, 1.0, 1.0),
                Arguments.of(0.0, 10.0, 2.0, 0.2, 0.2),
                Arguments.of(0.0, 10.0, 5.0, 0.5, 0.5),
                Arguments.of(0.0, 10.0, 7.0, 0.7, 0.7),
                Arguments.of(0.0, 10.0, -10.0, 0.0, -1.0),
                Arguments.of(0.0, 10.0, 20.0, 1.0, 2.0),
                Arguments.of(10.0, 0.0, 5.0, 0.5, 0.5),
                Arguments.of(-10.0, 0.0, -5.0, 0.5, 0.5),
                Arguments.of(0.0, -10.0, -5.0, 0.5, 0.5),
                Arguments.of(-2.0, 3.0, 0.5, 0.5, 0.5),
                Arguments.of(0.0, 10.0, nInf, 0.0, nInf),
                Arguments.of(0.0, 10.0, pInf, 1.0, pInf),
                Arguments.of(nInf, 3.0, nInf, 0.0, 0.0),
                Arguments.of(nInf, 3.0, 3.0, 1.0, 1.0),
                Arguments.of(nInf, 3.0, 5.0, 1.0, 1.0),
                Arguments.of(nInf, 3.0, 1.0, 1.0, 1.0),
                Arguments.of(nInf, 3.0, pInf, 1.0, Double.NaN),
                Arguments.of(3.0, pInf, pInf, 1.0, 1.0),
                Arguments.of(3.0, pInf, 3.0, 0.0, 0.0),
                Arguments.of(3.0, pInf, 5.0, 0.0, 0.0),
                Arguments.of(3.0, pInf, 1.0, 0.0, 0.0),
                Arguments.of(3.0, pInf, nInf, 0.0, Double.NaN),
                Arguments.of(nInf, pInf, nInf, 0.0, 0.0),
                Arguments.of(nInf, pInf, pInf, 1.0, 1.0),
                Arguments.of(nInf, pInf, 0.0, Double.NaN, Double.NaN));
    }

    protected static Stream<Arguments> remapData() {
        return Stream.of(
                // Finite ranges
                Arguments.of(0.0, 10.0, 100.0, 200.0, 0.0, 100.0, 100.0),
                Arguments.of(0.0, 10.0, 100.0, 200.0, 2.0, 120.0, 120.0),
                Arguments.of(0.0, 10.0, 100.0, 200.0, 7.0, 170.0, 170.0),
                Arguments.of(0.0, 10.0, 100.0, 200.0, 10.0, 200.0, 200.0),
                Arguments.of(0.0, 10.0, 100.0, 200.0, -2.0, 100.0, 80.0),
                Arguments.of(0.0, 10.0, 100.0, 200.0, 20.0, 200.0, 300.0),
                // Semi-finite output range
                Arguments.of(0.0, 10.0, 100.0, pInf, 0.0, 100.0, 100.0),
                Arguments.of(0.0, 10.0, 100.0, pInf, 1.0, pInf, pInf),
                Arguments.of(0.0, 10.0, 100.0, pInf, 10.0, pInf, pInf),
                Arguments.of(0.0, 10.0, 100.0, pInf, 11.0, pInf, pInf),
                Arguments.of(0.0, 10.0, 100.0, pInf, -1.0, 100.0, nInf),
                Arguments.of(0.0, 10.0, nInf, 200.0, 0.0, nInf, nInf),
                Arguments.of(0.0, 10.0, nInf, 200.0, 1.0, nInf, nInf),
                Arguments.of(0.0, 10.0, nInf, 200.0, 10.0, 200.0, 200.0),
                Arguments.of(0.0, 10.0, nInf, 200.0, 11.0, 200.0, pInf),
                Arguments.of(0.0, 10.0, nInf, 200.0, -1.0, nInf, nInf),
                // Infinite output range
                Arguments.of(0.0, 10.0, nInf, pInf, 0.0, nInf, nInf),
                Arguments.of(0.0, 10.0, nInf, pInf, 1.0, Double.NaN, Double.NaN),
                Arguments.of(0.0, 10.0, nInf, pInf, 10.0, pInf, pInf),
                Arguments.of(0.0, 10.0, nInf, pInf, 11.0, pInf, pInf),
                Arguments.of(0.0, 10.0, nInf, pInf, -1.0, nInf, nInf),
                // Semi-finite input range
                Arguments.of(0.0, pInf, 100.0, 200.0, 0.0, 100.0, 100.0),
                Arguments.of(0.0, pInf, 100.0, 200.0, 1.0, 100.0, 100.0),
                Arguments.of(0.0, pInf, 100.0, 200.0, -1.0, 100.0, 100.0),
                Arguments.of(nInf, 0.0, 100.0, 200.0, 0.0, 200.0, 200.0),
                Arguments.of(nInf, 0.0, 100.0, 200.0, 1.0, 200.0, 200.0),
                Arguments.of(nInf, 0.0, 100.0, 200.0, -1.0, 200.0, 200.0),
                // Infinite input range
                Arguments.of(nInf, pInf, 100.0, 200.0, 0.0, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, 100.0, 200.0, 1.0, Double.NaN, Double.NaN),
                // Semi-finite input range, semi-finite output range
                Arguments.of(0.0, pInf, 100.0, pInf, 0.0, 100.0, 100.0),
                Arguments.of(0.0, pInf, 100.0, pInf, 1.0, 100.0, 100.0),
                Arguments.of(0.0, pInf, 100.0, pInf, -1.0, 100.0, 100.0),
                Arguments.of(nInf, 0.0, 100.0, pInf, 0.0, pInf, pInf),
                Arguments.of(nInf, 0.0, 100.0, pInf, 1.0, pInf, pInf),
                Arguments.of(nInf, 0.0, 100.0, pInf, -1.0, pInf, pInf), // Maybe nan?
                // Likely to calculate NaN but I think pInf is right?
                Arguments.of(0.0, pInf, nInf, 200.0, 0.0, nInf, nInf),
                Arguments.of(0.0, pInf, nInf, 200.0, 1.0, nInf, nInf), // Maybe nan?
                Arguments.of(0.0, pInf, nInf, 200.0, -1.0, nInf, nInf),
                // Likely to calculate NaN but I think pInf is right?
                Arguments.of(nInf, 0.0, nInf, 200.0, 0.0, 200.0, 200.0),
                Arguments.of(nInf, 0.0, nInf, 200.0, 1.0, 200.0, 200.0),
                Arguments.of(nInf, 0.0, nInf, 200.0, -1.0, 200.0, 200.0),
                // Semi-finite input range, infinite output range
                Arguments.of(0.0, pInf, nInf, pInf, 0.0, nInf, nInf),
                Arguments.of(0.0, pInf, nInf, pInf, 1.0, nInf, nInf),
                Arguments.of(0.0, pInf, nInf, pInf, -1.0, nInf, nInf),
                Arguments.of(nInf, 0.0, nInf, pInf, 0.0, pInf, pInf),
                Arguments.of(nInf, 0.0, nInf, pInf, 1.0, pInf, pInf),
                Arguments.of(nInf, 0.0, nInf, pInf, -1.0, pInf, pInf),
                // Infinite input range, semi-finite output range
                Arguments.of(nInf, pInf, 100.0, pInf, 0.0, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, 100.0, pInf, 1.0, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, nInf, 200.0, 0.0, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, nInf, 200.0, 1.0, Double.NaN, Double.NaN),
                // Infinite input range, infinite output range
                Arguments.of(nInf, pInf, nInf, pInf, 0.0, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, nInf, pInf, 1.0, Double.NaN, Double.NaN));
    }

    // It's possible that these values may need to be changed once support for infinite inputs is
    // added. These values are here to help start the process.
    protected static Stream<Arguments> remapDataInfiniteInput() {
        return Stream.of(
                Arguments.of(0.0, 10.0, 100.0, 200.0, nInf, 100.0, nInf),
                Arguments.of(0.0, 10.0, 100.0, 200.0, pInf, 200.0, pInf),
                Arguments.of(0.0, 10.0, 100.0, pInf, pInf, pInf, pInf),
                Arguments.of(0.0, 10.0, 100.0, pInf, nInf, 100.0, nInf),
                Arguments.of(0.0, 10.0, nInf, 200.0, pInf, 200.0, pInf),
                Arguments.of(0.0, 10.0, nInf, 200.0, nInf, nInf, nInf),
                Arguments.of(0.0, pInf, 100.0, 200.0, nInf, Double.NaN, Double.NaN),
                Arguments.of(0.0, pInf, 100.0, 200.0, pInf, 200.0, 200.0),
                Arguments.of(nInf, 0.0, 100.0, 200.0, nInf, 100.0, 100.0),
                Arguments.of(nInf, 0.0, 100.0, 200.0, pInf, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, 100.0, 200.0, pInf, 200.0, 200.0),
                Arguments.of(nInf, pInf, 100.0, 200.0, nInf, 100.0, 100.0),
                Arguments.of(0.0, pInf, 100.0, pInf, nInf, Double.NaN, Double.NaN),
                Arguments.of(0.0, pInf, 100.0, pInf, pInf, pInf, pInf),
                Arguments.of(nInf, 0.0, 100.0, pInf, nInf, 100.0, 100.0),
                Arguments.of(nInf, 0.0, 100.0, pInf, pInf, pInf, pInf),
                Arguments.of(0.0, pInf, nInf, 200.0, nInf, nInf, nInf),
                Arguments.of(0.0, pInf, nInf, 200.0, pInf, 200.0, 200.0),
                Arguments.of(nInf, 0.0, nInf, 200.0, nInf, nInf, nInf),
                Arguments.of(nInf, 0.0, nInf, 200.0, pInf, Double.NaN, Double.NaN),
                Arguments.of(0.0, pInf, nInf, pInf, nInf, Double.NaN, Double.NaN),
                Arguments.of(0.0, pInf, nInf, pInf, pInf, pInf, pInf),
                Arguments.of(nInf, 0.0, nInf, pInf, nInf, nInf, nInf),
                Arguments.of(nInf, 0.0, nInf, pInf, pInf, Double.NaN, Double.NaN),
                Arguments.of(nInf, pInf, 100.0, pInf, pInf, pInf, pInf),
                Arguments.of(nInf, pInf, 100.0, pInf, nInf, 100.0, 100.0),
                Arguments.of(nInf, pInf, nInf, 200.0, pInf, 200.0, 200.0),
                Arguments.of(nInf, pInf, nInf, 200.0, nInf, nInf, nInf),
                Arguments.of(nInf, pInf, nInf, pInf, pInf, pInf, pInf),
                Arguments.of(nInf, pInf, nInf, pInf, nInf, nInf, nInf));
    }

    protected static Stream<Arguments> clampOrderedData() {
        return Stream.of(
                Arguments.of(2.0, 3.0, 1.0, 4.0, 2.0, 3.0),
                Arguments.of(1.0, 4.0, 2.0, 3.0, 2.0, 3.0),
                Arguments.of(1.0, 3.0, 2.0, 4.0, 2.0, 3.0),
                Arguments.of(2.0, 4.0, 1.0, 3.0, 2.0, 3.0),
                // both limits negative
                Arguments.of(-3.0, -2.0, -4.0, -1.0, -3.0, -2.0),
                Arguments.of(-4.0, -1.0, -3.0, -2.0, -3.0, -2.0),
                Arguments.of(-4.0, -2.0, -3.0, -1.0, -3.0, -2.0),
                Arguments.of(-3.0, -1.0, -4.0, -2.0, -3.0, -2.0),
                // low negative high positive
                Arguments.of(-1.0, 1.0, -2.0, 2.0, -1.0, 1.0),
                Arguments.of(-2.0, 2.0, -1.0, 1.0, -1.0, 1.0),
                Arguments.of(-2.0, 1.0, -1.0, 2.0, -1.0, 1.0),
                Arguments.of(-1.0, 2.0, -2.0, 1.0, -1.0, 1.0),
                // random generated number tests
                Arguments.of(-5.84966, -2.21588, -6.67582, 1.40667, -5.84966, -2.21588),
                Arguments.of(-7.1318, 0.443425, -7.44879, -4.55824, -7.1318, -4.55824),
                Arguments.of(1.68109, 5.15653, -2.70994, 7.2746, 1.68109, 5.15653),
                Arguments.of(0.263446, 5.9378, -8.64942, -8.05266, 0.263446, -8.05266),
                Arguments.of(-5.94296, -1.3932, -4.30567, -1.66571, -4.30567, -1.66571),
                Arguments.of(-7.73674, 8.54118, -7.69598, -6.03995, -7.69598, -6.03995),
                Arguments.of(-8.2559, -2.57025, -1.59916, 9.6038, -1.59916, -2.57025),
                Arguments.of(-9.51955, -6.78346, -1.85239, 1.97387, -1.85239, -6.78346),
                Arguments.of(1.63289, 4.90309, -6.81162, 4.89161, 1.63289, 4.89161),
                Arguments.of(-9.4295, 5.08241, -0.355911, 1.86237, -0.355911, 1.86237),
                // infinite values
                Arguments.of(nInf, pInf, -1.0, 1.0, -1.0, 1.0),
                Arguments.of(nInf, 0.0, -1.0, 1.0, -1.0, 0.0),
                Arguments.of(0.0, pInf, -1.0, 1.0, 0.0, 1.0),
                Arguments.of(-1.0, 1.0, nInf, pInf, -1.0, 1.0),
                Arguments.of(-1.0, 1.0, nInf, 0.0, -1.0, 0.0),
                Arguments.of(-1.0, 1.0, 0.0, pInf, 0.0, 1.0),
                Arguments.of(nInf, pInf, nInf, pInf, nInf, pInf),
                Arguments.of(nInf, 1.0, nInf, 0.0, nInf, 0.0),
                Arguments.of(0.0, pInf, 1.0, pInf, 1.0, pInf),
                Arguments.of(nInf, pInf, nInf, 1.0, nInf, 1.0),
                Arguments.of(1.0, pInf, nInf, pInf, 1.0, pInf));
    }

    protected static Stream<Arguments> getWidthData() {
        return Stream.of(
                Arguments.of(2.0, 5.0, 3.0),
                Arguments.of(-1.0, 10.0, 11.0),
                Arguments.of(5.0, 2.0, 3.0),
                Arguments.of(3.5, 7.5, 4.0),
                Arguments.of(3.0, 3.0, 0.0),
                Arguments.of(2.0, -5.0, 7.0),
                Arguments.of(-7.0, -2.0, 5.0),
                Arguments.of(nInf, -2.0, pInf),
                Arguments.of(-7.0, pInf, pInf),
                Arguments.of(nInf, pInf, pInf));
    }

    protected static Stream<Arguments> getCenterData() {
        return Stream.of(
                Arguments.of(2.0, 5.0, 3.5),
                Arguments.of(-1.0, 10.0, 4.5),
                Arguments.of(5.0, 2.0, 3.5),
                Arguments.of(3.5, 7.5, 5.5),
                Arguments.of(3.0, 3.0, 3.0),
                Arguments.of(2.0, -5.0, -1.5),
                Arguments.of(0.0, pInf, pInf),
                Arguments.of(nInf, 0.0, nInf),
                Arguments.of(nInf, pInf, Double.NaN));
    }
}
