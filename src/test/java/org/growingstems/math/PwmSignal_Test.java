/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class PwmSignal_Test {
    @ParameterizedTest
    @CsvSource({"20.0, 0.25", "20.0, 0.0", "10.0, 1.0"})
    public void periodDutyCycle(double period, double dutyCycle) {
        PwmSignal pwmSignal = PwmSignal.fromDutyCycleAndPeriod(dutyCycle, period);
        double observedDutyCycle = pwmSignal.getDutyCycle();
        double observedPeriod = pwmSignal.getPeriod();
        assertEquals(dutyCycle, observedDutyCycle, 1.0e-9);
        assertEquals(period, observedPeriod, 1.0e-9);
    }

    @ParameterizedTest
    @CsvSource({"20.0, 0.25, 5.0, 15.0", "20.0, 0.0, 0.0, 20.0", "10.0, 1.0, 10.0, 0.0"})
    public void periodDutyCycleToHighLow(
            double period, double dutyCycle, double expectedHigh, double expectedLow) {
        PwmSignal pwmSignal = PwmSignal.fromDutyCycleAndPeriod(dutyCycle, period);
        double observedHigh = pwmSignal.getHigh();
        double observedLow = pwmSignal.getLow();
        assertEquals(expectedHigh, observedHigh, 1.0e-9);
        assertEquals(expectedLow, observedLow, 1.0e-9);
    }
}
