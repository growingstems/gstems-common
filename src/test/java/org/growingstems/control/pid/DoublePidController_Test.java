/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.pid;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class DoublePidController_Test {
    public TestTimeSource m_timeSource = new TestTimeSource();
    public DoublePidController m_controller;
    public static final double k_tolerance = 1E-10;

    @Test
    public void zero() {
        PidController<Double> pid =
                new DoublePidController(0.0, Frequency.ZERO, Time.ZERO, m_timeSource);
        assertEquals(0.0, pid.update(0.0), 0.0);
        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(0.0, pid.update(0.0), 0.0);
        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(0.0, pid.update(0.0), 0.0);
    }

    @ParameterizedTest
    @CsvSource({
        "0.5, 0.4, -0.2",
        "2.0, 6.0, -12.0",
        "1.1, 0.4, -0.44",
        "-0.5, 0.4, 0.2",
        "-2.0, 6.0, 12.0",
        "-1.1, 0.4, 0.44",
        "0.0, 0.0, 0.0"
    })
    public void proportionalTest(double distance, double pGain, double finalValue) {
        m_controller = new DoublePidController(pGain, Frequency.ZERO, Time.ZERO, m_timeSource);

        double result = m_controller.update(1.0);

        // Pvalue is tested here to ensure that the proportional math behaves correctly inside the PID
        // controller.
        // Given a position of 1, the proportional value should return the negative of the p gain since
        // the setpoint is 0.0
        assertEquals(-pGain, result, k_tolerance);

        m_timeSource.addTime(Time.seconds(1.0));
        result = m_controller.update(distance);
        assertEquals(finalValue, result, k_tolerance);
    }

    @ParameterizedTest
    @CsvSource({
        "1.0, 3.0, 2.0, 1.0, -4.0",
        "4.0, 1.0, 4.0, 0.5, -5.0",
        "-1.0, -3.0, 2.0, 1.0, 4.0",
        "-4.0, -1.0, 4.0, 0.5, 5.0"
    })
    public void integralTest(
            double firstDistance,
            double secondDistance,
            double time_s,
            double ivalue,
            double finalValue) {
        m_controller = new DoublePidController(0.0, Frequency.hertz(ivalue), Time.ZERO, m_timeSource);

        double result = m_controller.update(firstDistance);

        assertEquals(0.0, result, k_tolerance);

        m_timeSource.setTime(Time.seconds(time_s));
        result = m_controller.update(secondDistance);

        assertEquals(finalValue, result, k_tolerance);

        m_controller.resetIntegral();
        m_timeSource.addTime(Time.seconds(1.0));

        result = m_controller.update(secondDistance);
        assertEquals(0.0, result, k_tolerance);

        m_timeSource.addTime(Time.seconds(1));

        result = m_controller.update(secondDistance);

        assertEquals(-ivalue * secondDistance, result, k_tolerance);

        m_timeSource.addTime(Time.seconds(time_s));

        result = m_controller.update(firstDistance);

        assertEquals(finalValue - ivalue * secondDistance, result, k_tolerance);
    }

    @ParameterizedTest
    @CsvSource({
        "0.1, 0.1, 1.0, -1.0",
        "2.0, 1.0, 0.5, -1.0",
        "5.5, 1.1, 2.0, -10.0",
        "-0.1, 0.1, 1.0, 1.0",
        "-2.0, 1.0, 0.5, 1.0",
        "-5.5, 1.1, 2.0, 10.0"
    })
    public void derivativeTest(double distance, double time_s, double dvalue, double finalValue) {
        m_controller = new DoublePidController(0.0, Frequency.ZERO, Time.seconds(dvalue), m_timeSource);
        double result = m_controller.update(0.0);
        assertEquals(0.0, result, k_tolerance);

        m_timeSource.setTime(Time.seconds(time_s));
        result = m_controller.update(distance);

        assertEquals(finalValue, result, k_tolerance);

        m_timeSource.addTime(Time.seconds(1));
        result = m_controller.update(distance + 2);

        assertEquals(-2 * dvalue, result, k_tolerance);
    }

    @Test
    public void PDTest() {
        m_controller = new DoublePidController(1.0, Frequency.ZERO, Time.seconds(1.0), m_timeSource);
        m_timeSource.setTime(Time.ZERO);
        double result = m_controller.update(0.0);

        assertEquals(0.0, result, k_tolerance);

        m_timeSource.setTime(Time.seconds(1.0));
        result = m_controller.update(1.0);

        assertEquals(-2.0, result, k_tolerance);
    }

    @Test
    public void PITest() {
        m_controller = new DoublePidController(1.0, Frequency.hertz(1.0), Time.ZERO, m_timeSource);
        m_timeSource.setTime(Time.ZERO);
        double result = m_controller.update(0.0);

        assertEquals(0.0, result, k_tolerance);

        m_timeSource.setTime(Time.seconds(1.0));
        result = m_controller.update(1.0);

        assertEquals(-1.5, result, k_tolerance);
    }

    @Test
    public void IDTest() {
        m_controller =
                new DoublePidController(0.0, Frequency.hertz(1.0), Time.seconds(1.0), m_timeSource);
        m_timeSource.setTime(Time.ZERO);
        double result = m_controller.update(0.0);

        assertEquals(0.0, result, k_tolerance);

        m_timeSource.setTime(Time.seconds(1.0));
        result = m_controller.update(1.0);

        assertEquals(-1.5, result, k_tolerance);
    }

    @Test
    public void PIDTest() {
        m_controller =
                new DoublePidController(0.5, Frequency.hertz(1.0), Time.seconds(1.0), m_timeSource);
        m_timeSource.setTime(Time.ZERO);
        double result = m_controller.update(0.0);

        assertEquals(0.0, result, k_tolerance);

        m_timeSource.setTime(Time.seconds(1.0));
        result = m_controller.update(1.0);

        assertEquals(-2.0, result, k_tolerance);
    }
}
