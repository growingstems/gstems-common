/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.pid;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.test.AssertExtensions;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class UnitPidController_Test {
    private static final double k_allowablePercentError = 0.001;
    private TestTimeSource m_timeSource = new TestTimeSource();

    @Test
    public void zero() {
        PidController<Length> pid = new UnitPidController<Length>(
                0.0, Frequency.ZERO, Time.ZERO, m_timeSource, Length.ZERO, Length::asInches);
        assertEquals(0.0, pid.update(Length.ZERO), 0.0);
        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(0.0, pid.update(Length.ZERO), 0.0);
        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(0.0, pid.update(Length.ZERO), 0.0);
    }

    @Test
    public void normalUsage() {
        PidController<Velocity> pid = new UnitPidController<Velocity>(
                1.0,
                Frequency.hertz(2.0),
                Time.seconds(3.0),
                m_timeSource,
                Velocity.ZERO,
                Velocity::asFeetPerSecond);
        pid.setSetpoint(Velocity.feetPerSecond(1.0));

        // Error: 0.25 mph. I and D are 0.0 because it's the first frame
        double pVal = 0.25;
        double iVal = 0.0;
        double dVal = 0.0;
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Velocity.inchesPerSecond(9.0)), k_allowablePercentError);

        m_timeSource.addTime(Time.seconds(2.0));
        pVal = 0.45; // 0.45 mph * 1.0 gain
        iVal += 1.4; // (0.45 mph + 0.25 mph)/2 * 2.0 seconds * 2.0 gain
        dVal = 0.3; // (0.75 mph - 0.55 mph) / 2.0 seconds * 3.0 gain
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Velocity.feetPerSecond(0.55)), k_allowablePercentError);

        pid.setSetpoint(Velocity.inchesPerSecond(3.0));
        pid.setGains(4.0, Frequency.hertz(10.0), Time.seconds(30.0));
        m_timeSource.addTime(Time.seconds(2.0));
        // Error: setpoint - input = pi/2 - pi = -pi/2 radians = -0.25 mph
        pVal = -1.0; // -0.25 mph * 4.0 gain
        iVal += -1.6; // (-0.25 mph * 10.0 gain + 0.45 mph * 2.0 gain)/2 * 2.0 seconds
        dVal = 0.75; // (0.55 mph - 0.5 mph) / 2.0 seconds * 30.0 gain
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Velocity.feetPerSecond(0.5)), k_allowablePercentError);
    }
}
