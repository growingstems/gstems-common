/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.pid;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.AssertExtensions;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class AnglePidController_Test {
    private static final double k_allowablePercentError = 0.001;
    private TestTimeSource m_timeSource = new TestTimeSource();

    @Test
    public void zero() {
        PidController<Angle> pid =
                new AnglePidController(0.0, Frequency.ZERO, Time.ZERO, Angle.Unit.RADIANS, m_timeSource);
        assertEquals(0.0, pid.update(Angle.ZERO), 0.0);
        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(0.0, pid.update(Angle.ZERO), 0.0);
        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(0.0, pid.update(Angle.ZERO), 0.0);
    }

    @Test
    public void normalUsage() {
        PidController<Angle> pid = new AnglePidController(
                1.0, Frequency.hertz(2.0), Time.seconds(3.0), Angle.Unit.REVOLUTIONS, m_timeSource);
        pid.setSetpoint(Angle.TWO_PI);

        // Error: 0.25 revolutions. I and D are 0.0 because it's the first frame
        double pVal = 0.25;
        double iVal = 0.0;
        double dVal = 0.0;
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Angle.revolutions(0.75)), k_allowablePercentError);

        m_timeSource.addTime(Time.seconds(2.0));
        pVal = 0.45; // 0.45 revolutions * 1.0 gain
        iVal += 1.4; // (0.45 revolutions + 0.25 revolutions)/2 * 2.0 seconds * 2.0 gain
        dVal = 0.3; // (0.75 revolutions - 0.55 revolutions) / 2.0 seconds * 3.0 gain
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Angle.revolutions(0.55)), k_allowablePercentError);

        pid.setSetpoint(Angle.PI_BY_TWO);
        pid.setGains(4.0, Frequency.hertz(10.0), Time.seconds(30.0));
        m_timeSource.addTime(Time.seconds(2.0));
        // Error: setpoint - input = pi/2 - pi = -pi/2 radians = -0.25 revolutions
        pVal = -1.0; // -0.25 revolutions * 4.0 gain
        iVal += -1.6; // (-0.25 revolutions * 10.0 gain + 0.45 revolutions * 2.0 gain)/2 * 2.0 seconds
        dVal = 0.75; // (0.55 revolutions - 0.5 revolutions) / 2.0 seconds * 30.0 gain
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Angle.revolutions(0.5)), k_allowablePercentError);
    }

    @Test
    public void differencePid() {
        PidController<Angle> pid = new AnglePidController(
                1.0, Frequency.hertz(2.0), Time.seconds(3.0), Angle.Unit.REVOLUTIONS, m_timeSource);
        pid.setSetpoint(Angle.TWO_PI);

        // Error: 0.25 revolutions. I and D are 0.0 because it's the first frame
        double pVal = 0.25;
        double iVal = 0.0;
        double dVal = 0.0;
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Angle.revolutions(10.75)), k_allowablePercentError);

        m_timeSource.addTime(Time.seconds(2.0));
        pVal = 0.45; // 0.45 revolutions * 1.0 gain
        iVal += 1.4; // (0.45 revolutions + 0.25 revolutions)/2 * 2.0 seconds * 2.0 gain
        // 20.55 revolutions shifted to the scope of 10.75 revolutions is 10.55 revolutions
        dVal = 0.3; // (10.75 revolutions - 10.55 revolutions) / 2.0 seconds * 3.0 gain
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Angle.revolutions(20.55)), k_allowablePercentError);

        pid.setSetpoint(Angle.PI_BY_TWO);
        pid.setGains(4.0, Frequency.hertz(10.0), Time.seconds(30.0));
        m_timeSource.addTime(Time.seconds(2.0));
        // Error: setpoint - input = pi/2 - pi = -pi/2 radians = -0.25 revolutions
        pVal = -1.0; // -0.25 revolutions * 4.0 gain
        iVal += -1.6; // (-0.25 revolutions * 10.0 gain + 0.45 revolutions * 2.0 gain)/2 * 2.0 seconds
        // -2.5 revolutions shifted to the scope of 10.55 revolutions is 10.5 revolutions
        dVal = 0.75; // (10.55 revolutions - 10.5 revolutions) / 2.0 seconds * 30.0 gain
        AssertExtensions.assertPercentError(
                pVal + iVal + dVal, pid.update(Angle.revolutions(-2.5)), k_allowablePercentError);
    }
}
