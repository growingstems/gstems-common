/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import static org.growingstems.test.AssertExtensions.assertPercentError;

import org.growingstems.math.Range;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class SymmetricDeadzone_Test {
    private double allowableError = 0.005;

    @ParameterizedTest
    @CsvSource({
        "-1.0, 1.0, 0.0, 1.0",
        "0.0, 2.0, 1.0, 1.0",
        "4.0, 79.0, 41.5, 37.5",
        "-4.1383, 13.38134, 4.62152, 8.75982"
    })
    public void rangeConstructor(
            double low, double high, double expectedCenter, double expectedRadius) {
        Range r = new Range(low, high);
        SymmetricDeadzone d = new SymmetricDeadzone(r, 0.5);
        assertPercentError(expectedCenter, d.getCenterValue(), allowableError);
        assertPercentError(d.getCenterValue(), d.getDeadOutput(), allowableError);
        assertPercentError(expectedRadius, d.getRadius(), allowableError);
    }

    @ParameterizedTest
    @CsvSource({
        "-1.0, -1.0",
        "-0.9, -0.8",
        "-0.8, -0.6",
        "-0.7, -0.4",
        "-0.6, -0.2",
        "-0.5, 0.0",
        "-0.4, 0.0",
        "-0.3, 0.0",
        "-0.2, 0.0",
        "-0.1, 0.0",
        "0.0, 0.0",
        "0.1, 0.0",
        "0.2, 0.0",
        "0.3, 0.0",
        "0.4, 0.0",
        "0.5, 0.0",
        "0.6, 0.2",
        "0.7, 0.4",
        "0.8, 0.6",
        "0.9, 0.8",
        "1.0, 1.0"
    })
    public void simpleDeadzoneTest(double inValue, double expectedOut) {
        SymmetricDeadzone d = new SymmetricDeadzone(1.0, 0.5);
        assertPercentError(expectedOut, d.update(inValue), allowableError);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0",
        "0.1, 0.2",
        "0.2, 0.4",
        "0.3, 0.6",
        "0.4, 0.8",
        "0.5, 1.0",
        "0.6, 1.0",
        "0.7, 1.0",
        "0.8, 1.0",
        "0.9, 1.0",
        "1.0, 1.0",
        "1.1, 1.0",
        "1.2, 1.0",
        "1.3, 1.0",
        "1.4, 1.0",
        "1.5, 1.0",
        "1.6, 1.2",
        "1.7, 1.4",
        "1.8, 1.6",
        "1.9, 1.8",
        "2.0, 2.0"
    })
    public void shiftedDeadzoneTest(double inValue, double expectedOut) {
        SymmetricDeadzone d = new SymmetricDeadzone(1.0, 1.0, 0.5);
        assertPercentError(expectedOut, d.update(inValue), allowableError);
    }
}
