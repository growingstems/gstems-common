/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.math.Range;
import org.growingstems.sensor.SensorCore.Correlation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class SoftStop_Test {
    SoftStop m_ss;

    @BeforeEach
    public void setUp() {
        m_ss = new SoftStop(new Range(-20.0, 20.0), new Range(-10.0, 10.0), -0.25, 0.25);
    }

    // constructor tests
    @Test
    public void limitsOnlyContructorTest() {
        SoftStop ss = new SoftStop(new Range(-20.0, 20.0));

        Range limits = ss.getLimits();
        assertEquals(limits.getHigh(), 20.0, 1E-6);
        assertEquals(limits.getLow(), -20.0, 1E-6);

        Range nearLimits = ss.getNearLimits();
        assertEquals(nearLimits.getHigh(), 16.0, 1E-6);
        assertEquals(nearLimits.getLow(), -16.0, 1E-6);

        assertEquals(ss.getHighNearSpeed(), 0.40, 1E-6);
        assertEquals(ss.getLowNearSpeed(), -0.40, 1E-6);

        Correlation correlation = ss.getCorrelation();
        assertEquals(correlation, Correlation.POSITIVE_INCREASING);
    }

    @Test
    public void bothLimitsConstructorTest() {
        SoftStop ss = new SoftStop(new Range(-20.0, 20.0), new Range(-10.0, 10.0));

        Range limits = ss.getLimits();
        assertEquals(limits.getHigh(), 20.0, 1E-6);
        assertEquals(limits.getLow(), -20.0, 1E-6);

        Range nearLimits = ss.getNearLimits();
        assertEquals(nearLimits.getHigh(), 10.0, 1E-6);
        assertEquals(nearLimits.getLow(), -10.0, 1E-6);

        assertEquals(ss.getHighNearSpeed(), 0.4, 1E-6);
        assertEquals(ss.getLowNearSpeed(), -0.4, 1E-6);

        Correlation correlation = ss.getCorrelation();
        assertEquals(correlation, Correlation.POSITIVE_INCREASING);
    }

    @Test
    public void bothLimitsSpeedConstructorTest() {
        SoftStop ss = new SoftStop(new Range(-20.0, 20.0), new Range(-10.0, 10.0), -0.25, 0.25);

        Range limits = ss.getLimits();
        assertEquals(limits.getHigh(), 20.0, 1E-6);
        assertEquals(limits.getLow(), -20.0, 1E-6);

        Range nearLimits = ss.getNearLimits();
        assertEquals(nearLimits.getHigh(), 10.0, 1E-6);
        assertEquals(nearLimits.getLow(), -10.0, 1E-6);

        assertEquals(ss.getHighNearSpeed(), 0.25, 1E-6);
        assertEquals(ss.getLowNearSpeed(), -0.25, 1E-6);

        Correlation correlation = ss.getCorrelation();
        assertEquals(correlation, Correlation.POSITIVE_INCREASING);
    }

    @Test
    public void fullConstructorTest() {
        SoftStop ss = new SoftStop(
                new Range(-20.0, 20.0),
                new Range(-10.0, 10.0),
                -0.25,
                0.25,
                Correlation.NEGATIVE_INCREASING);

        Range limits = ss.getLimits();
        assertEquals(limits.getHigh(), 20.0, 1E-6);
        assertEquals(limits.getLow(), -20.0, 1E-6);

        Range nearLimits = ss.getNearLimits();
        assertEquals(nearLimits.getHigh(), 10.0, 1E-6);
        assertEquals(nearLimits.getLow(), -10.0, 1E-6);

        assertEquals(ss.getHighNearSpeed(), 0.25, 1E-6);
        assertEquals(ss.getLowNearSpeed(), -0.25, 1E-6);

        Correlation correlation = ss.getCorrelation();
        assertEquals(correlation, Correlation.NEGATIVE_INCREASING);
    }

    // setter tests
    @ParameterizedTest
    @CsvSource({ // valid inputs
        "10.0, 20.0, 10.0, 20.0", // +, +
        "-10.0, 10.0, -10.0, 10.0", // +, -
        "-20.0, -10.0, -20.0, -10.0", // -, -
        // reversed inputs
        "20.0, 10.0, 10.0, 20.0", // +, +
        "10.0, -10.0, -10.0, 10.0", // +, -
        "-10.0, -20.0, -20.0, -10.0" // -, -
    })
    public void setLimitsTest(double lo, double hi, double expectedLo, double expectedHi) {
        m_ss.setLimits(new Range(lo, hi));

        Range limits = m_ss.getLimits();
        assertEquals(limits.getLow(), expectedLo, 1E-10);
        assertEquals(limits.getHigh(), expectedHi, 1E-10);
    }

    @ParameterizedTest
    @CsvSource({ // valid inputs
        "-20.0, 20.0, 5.0, 7.5, 5.0, 7.5", // +, +
        "-20.0, 20.0, -7.5, -5.0, -7.5, -5.0", // -, -
        "-20.0, 20.0, -5.0, 5.0, -5.0, 5.0", // -, +
        // invalid inputs (hi and lo switched)
        "-20.0, 20.0, 7.5, 5.0, 5.0, 7.5", // +, +
        "-20.0, 20.0, -5.0, -7.5, -7.5, -5.0", // -, -
        "-20.0, 20.0, 5.0, -5.0, -5.0, 5.0", // -, +
        // invalid inputs (larger than regular limits)
        "10.0, 20.0, 5.0, 25.0, 10.0, 20.0", // +, + (both)
        "10.0, 20.0, 11.0, 25.0, 11.0, 20.0", // +, + (just hi)
        "10.0, 20.0, 5.0, 19.0, 10.0, 19.0", // +, + (just lo)
        "-20.0, -10.0, -25.0, -5.0, -20.0, -10.0", // -, - (both)
        "-20.0, -10.0, -19.0, -9.0, -19.0, -10.0", // -, - (just hi)
        "-20.0, -10.0, -25.0, -11.0, -20.0, -11.0", // -, - (just lo)
        "-20.0, 20.0, -25.0, 25.0, -20.0, 20.0", // -, + (both)
        "-20.0, 20.0, -10.0, 25.0, -10.0, 20.0", // -, + (just hi)
        "-20.0, 20.0, -25.0, 10.0, -20.0, 10.0", // -, + (just lo)
        // invalid inputs (switched and larger than regular inputs})
        "10.0, 20.0, 25.0, 5.0, 10.0, 20.0", // +, + (both)
        "10.0, 20.0, 25.0, 11.0, 11.0, 20.0", // +, + (just hi)
        "10.0, 20.0, 19.0, 5.0, 10.0, 19.0", // +, + (just lo)
        "-20.0, -10.0, -5.0, -25.0, -20.0, -10.0", // -, - (both)
        "-20.0, -10.0, -9.0, -19.0, -19.0, -10.0", // -, - (just hi)
        "-20.0, -10.0, -11.0, -25.0, -20.0, -11.0", // -, - (just lo)
        "-20.0, 20.0, 25.0, -25.0, -20.0, 20.0", // -, + (both)
        "-20.0, 20.0, 25.0, -10.0, -10.0, 20.0", // -, + (just hi)
        "-20.0, 20.0, 10.0, -25.0, -20.0, 10.0" // -, + (just lo)
    })
    public void setNearLimitsTest(
            double loLim,
            double hiLim,
            double loNear,
            double hiNear,
            double expectedLo,
            double expectedHi) {
        // make soft stop with limits described for this test
        SoftStop ss = new SoftStop(new Range(loLim, hiLim));

        // set near limits
        ss.setNearLimits(new Range(loNear, hiNear));

        // ensure near limits were set correctly
        Range nearLimits = ss.getNearLimits();
        assertEquals(nearLimits.getLow(), expectedLo, 1E-10);
        assertEquals(nearLimits.getHigh(), expectedHi, 1E-10);
    }

    @ParameterizedTest
    @CsvSource({ // valid inputs
        "0.2, 0.5, 0.2, 0.5", // +, +
        "-0.5, -0.2, -0.5, -0.2", // -, -
        "-0.5, 0.5, -0.5, 0.5", // -, +
        // reversed inputs
        "0.5, 0.2, 0.5, 0.2", // +, +
        "-0.2, -0.5, -0.2, -0.5", // -, -
        "0.5, -0.5, 0.5, -0.5", // -, +
    })
    public void setNearSpeedsTest(double lo, double hi, double expectedLo, double expectedHi) {
        m_ss.setNearSpeeds(lo, hi);

        assertEquals(m_ss.getLowNearSpeed(), expectedLo, 1E-10);
        assertEquals(m_ss.getHighNearSpeed(), expectedHi, 1E-10);
    }

    // interaction function tests
    @ParameterizedTest
    @CsvSource({ // positive motor input, positive increasing correlation
        "0.9, 5.0, POSITIVE_INCREASING, 0.9", // at no limit
        "0.9, 10.0, POSITIVE_INCREASING, 0.25", // at near hi limit
        "0.9, 15.0, POSITIVE_INCREASING, 0.25", // between near hi limit and hi limit
        "0.9, 20.0, POSITIVE_INCREASING, 0.0", // at hi limit
        "0.9, 25.0, POSITIVE_INCREASING, 0.0", // above hi limit
        "0.9, -5.0, POSITIVE_INCREASING, 0.9", // at no limit
        "0.9, -10.0, POSITIVE_INCREASING, 0.9", // at near lo limit
        "0.9, -15.0, POSITIVE_INCREASING, 0.9", // between near lo limit and lo limit
        "0.9, -20.0, POSITIVE_INCREASING, 0.9", // at lo limit
        "0.9, -25.0, POSITIVE_INCREASING, 0.9", // below lo limit
        // positive motor input, negative increasing correlation
        "0.9, 5.0, NEGATIVE_INCREASING, 0.9", // at no limit
        "0.9, 10.0, NEGATIVE_INCREASING, 0.9", // at near hi limit
        "0.9, 15.0, NEGATIVE_INCREASING, 0.9", // between near hi limit and hi limit
        "0.9, 20.0, NEGATIVE_INCREASING, 0.9", // at hi limit
        "0.9, 25.0, NEGATIVE_INCREASING, 0.9", // above hi limit
        "0.9, -5.0, NEGATIVE_INCREASING, 0.9", // at no limit
        "0.9, -10.0, NEGATIVE_INCREASING, 0.25", // at near lo limit
        "0.9, -15.0, NEGATIVE_INCREASING, 0.25", // between near lo limit and lo limit
        "0.9, -20.0, NEGATIVE_INCREASING, 0.0", // at lo limit
        "0.9, -25.0, NEGATIVE_INCREASING, 0.0", // below lo limit
        // negative motor input, negative increasing correlation
        "-0.9, 5.0, POSITIVE_INCREASING, -0.9", // at no limit
        "-0.9, 10.0, POSITIVE_INCREASING, -0.9", // at near hi limit
        "-0.9, 15.0, POSITIVE_INCREASING, -0.9", // between near hi limit and hi limit
        "-0.9, 20.0, POSITIVE_INCREASING, -0.9", // at hi limit
        "-0.9, 25.0, POSITIVE_INCREASING, -0.9", // above hi limit
        "-0.9, -5.0, POSITIVE_INCREASING, -0.9", // at no limit
        "-0.9, -10.0, POSITIVE_INCREASING, -0.25", // at near lo limit
        "-0.9, -15.0, POSITIVE_INCREASING, -0.25", // between near lo limit and lo limit
        "-0.9, -20.0, POSITIVE_INCREASING, 0.0", // at lo limit
        "-0.9, -25.0, POSITIVE_INCREASING, 0.0", // below lo limit
        // negative motor input, negative increasing correlation
        "-0.9, 5.0, NEGATIVE_INCREASING, -0.9", // at no limit
        "-0.9, 10.0, NEGATIVE_INCREASING, -0.25", // at near hi limit
        "-0.9, 15.0, NEGATIVE_INCREASING, -0.25", // between near hi limit and hi limit
        "-0.9, 20.0, NEGATIVE_INCREASING, 0.0", // at hi limit
        "-0.9, 25.0, NEGATIVE_INCREASING, 0.0", // above hi limit
        "-0.9, -5.0, NEGATIVE_INCREASING, -0.9", // at no limit
        "-0.9, -10.0, NEGATIVE_INCREASING, -0.9", // at near lo limit
        "-0.9, -15.0, NEGATIVE_INCREASING, -0.9", // between near lo limit and lo limit
        "-0.9, -20.0, NEGATIVE_INCREASING, -0.9", // at lo limit
        "-0.9, -25.0, NEGATIVE_INCREASING, -0.9", // below lo limit
        // edge case: motor input slower than near speed is allowed
        "0.1, 15.0, POSITIVE_INCREASING, 0.1", // positive corelation, hi near limit
        "-0.1, -15.0, POSITIVE_INCREASING, -0.1", // positive corelation, lo near limit
        "-0.1, 15.0, NEGATIVE_INCREASING, -0.1", // negative corelation, hi near limit
        "0.1, -15.0, NEGATIVE_INCREASING, 0.1", // negative corelation, lo near limit
    })
    public void functionalityTest(
            double powerIn, double senseIn, Correlation correlation, double powerOut) {
        m_ss.setCorrelation(correlation);

        // make the near speeds match the correlation being used for the test
        if (correlation == Correlation.NEGATIVE_INCREASING) {
            m_ss.setNearSpeeds(m_ss.getHighNearSpeed(), m_ss.getLowNearSpeed());
        }

        // ensure works as expected
        assertEquals(m_ss.getPowerOut(powerIn, senseIn), powerOut, 1E-10);

        // disable and ensure bypasses all softstops
        m_ss.disable();
        assertEquals(m_ss.getPowerOut(powerIn, senseIn), powerIn, 1E-10);

        // ensure re-enable works
        m_ss.enable();
        assertEquals(m_ss.getPowerOut(powerIn, senseIn), powerOut, 1E-10);
    }

    // introspection function tests
    @ParameterizedTest
    @CsvSource({
        "-25.0, true, true, false, false",
        "-20.001, true, true, false, false",
        "-20.0, true, true, false, false", // lo limit
        "-19.99,f alse, true, false, false",
        "-15.0, false, true, false, false",
        "-10.001, false, true, false, false",
        "-10.0, false, true, false, false", // lo near
        "-9.99, false, false, false, false",
        "-5.0, false, false, false, false",
        "0.0, false, false, false, false",
        "5.0, false, false, false, false",
        "9.99, false, false, false, false",
        "10.0, false, false, true, false", // hi near
        "10.001, false, false, true, false",
        "15.0, false, false, true, false",
        "19.99, false, false, true, false",
        "20.0, false, false, true, true", // hi limit
        "20.001, false, false, true, true",
        "25.0, false, false, true, true"
    })
    public void atLimitFunctionTests(
            double d, boolean atLow, boolean atLowNear, boolean atHighNear, boolean atHigh) {
        assertEquals(m_ss.atLowLimit(d), atLow);
        assertEquals(m_ss.atLowNearLimit(d), atLowNear);
        assertEquals(m_ss.atHighNearLimit(d), atHighNear);
        assertEquals(m_ss.atHighLimit(d), atHigh);
    }
}
