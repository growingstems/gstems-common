/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control.actuators;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.math.RangeU;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.test.TestCore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DummyActuator_Test {
    // Full Constructor
    private static final Length k_positiveNonZeroUnit = Length.meters(0.1);
    private static final Length k_unitPerSensorUnit = Length.meters(0.1);
    private static final Length k_startingPositionOffset = Length.meters(0.1);
    private static final RangeU<Length> k_positionRange =
            new RangeU<>(Length.meters(-0.5), Length.meters(0.5));
    private static final RangeU<Velocity> k_velocityRange =
            new RangeU<>(Velocity.metersPerSecond(-0.5), Velocity.metersPerSecond(0.5));

    private DummyActuator<Length, Velocity> m_simpleDummy;
    private DummyActuator<Length, Velocity> m_dummy;

    @BeforeEach
    public void resetTest() {
        m_simpleDummy = new DummyActuator<>(k_positiveNonZeroUnit);
        m_dummy = new DummyActuator<>(
                k_unitPerSensorUnit, k_startingPositionOffset, k_positionRange, k_velocityRange);
    }

    @Test
    public void outputVoltageTest() {
        var testVoltage = Voltage.ZERO;
        assertEquals(
                testVoltage.asVolts(), m_dummy.getCurrentlySetOutputVoltage().asVolts(), TestCore.k_eps);

        // Set Voltage
        testVoltage = Voltage.volts(5.0);
        m_dummy.setOutputVoltage(testVoltage);
        assertEquals(
                testVoltage.asVolts(), m_dummy.getCurrentlySetOutputVoltage().asVolts(), TestCore.k_eps);
    }

    @Test
    public void positionTest() {
        // Minimal Actuator
        // Init Values Test
        var testLength = Length.ZERO;
        assertEquals(
                Length.ZERO.asMeters(), m_simpleDummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_simpleDummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(
                Length.ZERO.asMeters(), m_simpleDummy.getPositionError().asMeters(), TestCore.k_eps);
        assertEquals(testLength.asMeters(), m_simpleDummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(
                Length.ZERO.asMeters(),
                m_simpleDummy.getPositionalRange().getLow().asMeters(),
                TestCore.k_eps);
        assertEquals(
                Length.ZERO.asMeters(),
                m_simpleDummy.getPositionalRange().getHigh().asMeters(),
                TestCore.k_eps);

        // Constructed/Default Dummy Position Unit Range
        testLength = Length.meters(2.0);
        m_simpleDummy.setPosition(testLength);
        assertEquals(
                Length.ZERO.asMeters(), m_simpleDummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_simpleDummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(
                Length.ZERO.asMeters(), m_simpleDummy.getPositionError().asMeters(), TestCore.k_eps);

        testLength = Length.meters(2.0).neg();
        m_simpleDummy.setPosition(testLength);
        assertEquals(
                Length.ZERO.asMeters(), m_simpleDummy.getGoalPosition().asMeters(), TestCore.k_eps);

        // Full Actuator
        // Init Values Test
        testLength = Length.ZERO;
        assertEquals(testLength.asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPositionError().asMeters(), TestCore.k_eps);
        assertEquals(
                k_positionRange.getLow().asMeters(),
                m_dummy.getPositionalRange().getLow().asMeters(),
                TestCore.k_eps);
        assertEquals(
                k_positionRange.getHigh().asMeters(),
                m_dummy.getPositionalRange().getHigh().asMeters(),
                TestCore.k_eps);

        // Constructed/Default Dummy Position Unit Range
        testLength = Length.meters(2.0);
        m_dummy.setPosition(testLength);
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPositionError().asMeters(), TestCore.k_eps);

        testLength = Length.meters(2.0).neg();
        m_dummy.setPosition(testLength);
        assertEquals(
                k_positionRange.getLow().asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
    }

    @Test
    public void velocityTest() {
        // Minimal Actuator
        // Init Values Test
        var testVelocity = Velocity.ZERO;
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getVelocityRange().getLow().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getVelocityRange().getHigh().asMetersPerSecond(),
                TestCore.k_eps);

        // Constructed/Default Dummy Velocity Unit Range
        testVelocity = Velocity.metersPerSecond(0.5);
        m_simpleDummy.setVelocity(testVelocity);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);

        testVelocity = Velocity.metersPerSecond(0.5).neg();
        m_simpleDummy.setVelocity(testVelocity);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_simpleDummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);

        // Full Actuator
        // Init Values Test
        testVelocity = Velocity.ZERO;
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                testVelocity.asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                k_velocityRange.getLow().asMetersPerSecond(),
                m_dummy.getVelocityRange().getLow().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                k_velocityRange.getHigh().asMetersPerSecond(),
                m_dummy.getVelocityRange().getHigh().asMetersPerSecond(),
                TestCore.k_eps);

        // Constructed/Default Dummy Velocity Unit Range
        testVelocity = Velocity.metersPerSecond(2.0);
        m_dummy.setVelocity(testVelocity);
        assertEquals(
                k_velocityRange.getHigh().asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                k_velocityRange.getHigh().asMetersPerSecond(),
                m_dummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);

        testVelocity = Velocity.metersPerSecond(2.0).neg();
        m_dummy.setVelocity(testVelocity);
        assertEquals(
                k_velocityRange.getLow().asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
    }

    @Test
    public void stopTest() {
        var testVoltage = Voltage.volts(5.0);
        m_dummy.setOutputVoltage(testVoltage);
        assertEquals(
                testVoltage.asVolts(), m_dummy.getCurrentlySetOutputVoltage().asVolts(), TestCore.k_eps);
        m_dummy.stop();
        assertEquals(
                Voltage.ZERO.asVolts(), m_dummy.getCurrentlySetOutputVoltage().asVolts(), TestCore.k_eps);

        m_dummy.setPosition(k_positionRange.getHigh());
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPositionError().asMeters(), TestCore.k_eps);
        m_dummy.stop();
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPositionError().asMeters(), TestCore.k_eps);

        m_dummy.setVelocity(k_velocityRange.getHigh());
        assertEquals(
                k_velocityRange.getHigh().asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                k_velocityRange.getHigh().asMetersPerSecond(),
                m_dummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);
        m_dummy.stop();
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);
    }

    @Test
    public void disableEnableTest() {
        m_dummy.disable();

        m_dummy.setOutputVoltage(Voltage.volts(5.0));
        assertEquals(
                Voltage.ZERO.asVolts(), m_dummy.getCurrentlySetOutputVoltage().asVolts(), TestCore.k_eps);
        m_dummy.setOutputVoltage(Voltage.volts(5.0).neg());
        assertEquals(
                Voltage.ZERO.asVolts(), m_dummy.getCurrentlySetOutputVoltage().asVolts(), TestCore.k_eps);

        m_dummy.setPosition(Length.meters(2.0));
        assertEquals(Length.ZERO.asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPositionError().asMeters(), TestCore.k_eps);

        m_dummy.setVelocity(Velocity.metersPerSecond(2.0));
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);

        m_dummy.enable();

        var testVoltage = Voltage.volts(5.0);
        m_dummy.setOutputVoltage(testVoltage);
        assertEquals(
                testVoltage.asVolts(), m_dummy.getCurrentlySetOutputVoltage().asVolts(), TestCore.k_eps);
        m_dummy.setOutputVoltage(testVoltage.neg());
        assertEquals(
                testVoltage.neg().asVolts(),
                m_dummy.getCurrentlySetOutputVoltage().asVolts(),
                TestCore.k_eps);

        m_dummy.setPosition(k_positionRange.getHigh());
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getGoalPosition().asMeters(), TestCore.k_eps);
        assertEquals(
                k_positionRange.getHigh().asMeters(), m_dummy.getPosition().asMeters(), TestCore.k_eps);
        assertEquals(Length.ZERO.asMeters(), m_dummy.getPositionError().asMeters(), TestCore.k_eps);

        m_dummy.setVelocity(k_velocityRange.getHigh());
        assertEquals(
                k_velocityRange.getHigh().asMetersPerSecond(),
                m_dummy.getGoalVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                k_velocityRange.getHigh().asMetersPerSecond(),
                m_dummy.getVelocity().asMetersPerSecond(),
                TestCore.k_eps);
        assertEquals(
                Velocity.ZERO.asMetersPerSecond(),
                m_dummy.getVelocityError().asMetersPerSecond(),
                TestCore.k_eps);
    }
}
