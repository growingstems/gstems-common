/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.TestCore;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class MomentumKiller_Test {
    public TestTimeSource m_timeSource = new TestTimeSource();
    public MomentumKiller m_mk;
    public boolean m_start = false;
    public boolean m_stop = false;
    public static final Time k_epsTime = Time.seconds(TestCore.k_eps);

    // setter tests
    @ParameterizedTest
    // valid inputs
    @CsvSource({
        "3.0, 3.0",
        "1.5, 1.5",
        "0.5, 0.5",
        "0.1, 0.1",
        "0.1, 3.0",
        "3.0, 0.1",
        "12.341, 32.01",
    })
    public void normalOPTest(double reverseTime_s, double stopTime_s) {
        // setup variables
        Time reverseTime = Time.seconds(reverseTime_s);
        Time stopTime = Time.seconds(stopTime_s);
        final double reversePower = 0.8;
        final double powerIn = -0.4;
        m_mk = new MomentumKiller(
                reversePower, reverseTime, () -> m_start, stopTime, () -> m_stop, m_timeSource);

        // ensure power coming out works fine when not started
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(2.5));
        m_start = true;
        assertEquals(reversePower, m_mk.update(powerIn), TestCore.k_eps);
        m_start = false;
        assertEquals(reversePower, m_mk.update(powerIn), TestCore.k_eps);

        m_timeSource.addTime(reverseTime.sub(k_epsTime));
        assertEquals(reversePower, m_mk.update(powerIn), TestCore.k_eps);
        m_timeSource.addTime(k_epsTime.mul(2.0));
        assertEquals(0.0, m_mk.update(powerIn), TestCore.k_eps);

        m_timeSource.addTime(stopTime.sub(k_epsTime));
        assertEquals(0.0, m_mk.update(powerIn), TestCore.k_eps);
        m_timeSource.addTime(k_epsTime.mul(2.0));
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps);
    }

    @Test
    public void stopTest() {
        final double reversePower = 0.8;
        final double powerIn = -0.4;
        final Time reverseTime = Time.seconds(10.0);
        final Time stopTime = Time.seconds(10.0);
        m_mk = new MomentumKiller(
                reversePower, reverseTime, () -> m_start, stopTime, () -> m_stop, m_timeSource);

        // stop in middle of reverse state
        m_timeSource.setTime(Time.seconds(2.5));
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps);
        m_start = true;
        assertEquals(reversePower, m_mk.update(powerIn), TestCore.k_eps); // trigger transition
        m_start = false;
        assertEquals(
                reversePower, m_mk.update(powerIn), TestCore.k_eps); // now actually in reverse state
        m_stop = true;
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps); // trigger stop
        m_stop = false;
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps); // back in idle state

        // stop in middle of stop state
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps);
        m_start = true;
        assertEquals(reversePower, m_mk.update(powerIn), TestCore.k_eps); // trigger transition
        m_start = false;
        assertEquals(
                reversePower, m_mk.update(powerIn), TestCore.k_eps); // now actually in reverse state
        m_timeSource.addTime(reverseTime.add(k_epsTime));
        assertEquals(0.0, m_mk.update(powerIn), TestCore.k_eps); // now actually in stop state
        m_stop = true;
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps); // trigger stop transition
        m_stop = false;
        assertEquals(powerIn, m_mk.update(powerIn), TestCore.k_eps); // back in idle state
    }
}
