/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.control;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TakeBackHalf_Test {
    @Test
    public void tbhReducedConstructorTest() {
        TakeBackHalf tbh = new TakeBackHalf(1000);
        assertEquals(0.7, tbh.update(300.0), 0.00001);
        assertEquals(1.0, tbh.update(550.0), 0.00001);
        assertEquals(0.25, tbh.update(1500.0), 0.00001);
        assertEquals(0.25, tbh.update(1000.0), 0.00001);
    }

    @Test
    public void tbhFullConstructorTest() {
        TakeBackHalf tbh = new TakeBackHalf(5, 0.2, 0.0, 1.0);
        assertEquals(0.7, tbh.update(1.5), 0.00001);
        assertEquals(1.0, tbh.update(2.75), 0.00001);
        assertEquals(0.25, tbh.update(7.5), 0.00001);
        assertEquals(0.25, tbh.update(5.0), 0.00001);
    }
}
