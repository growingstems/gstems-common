/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.geometry;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;
import org.growingstems.measurements.Angle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Cone2d_Test {

    @ParameterizedTest
    @CsvSource({
        "80.0, 4.0, 0.0, 0.0, 0.0, 2.0, 2.0, false", // Testing FOV
        "80.0, 4.0, 0.0, 0.0, 0.0, 2.0, 1.5, true", // Testing FOV
        "100.0, 4.0, 0.0, 0.0, 0.0, 2.0, 2.0, true", // Testing FOV
        "100.0, 4.0, 0.0, 0.0, 0.0, 2.0, 1.5, true", // Testing FOV
        "180.0, 4.0, 0.0, 0.0, 0.0, 3.0, 0.0, true", // Testing Radius
        "180.0, 2.0, 0.0, 0.0, 0.0, 3.0, 0.0, false", // Testing Radius
        "20.0, 4.0, 45.0, 0.0, 0.0, 2.0, 0.0, false", // Testing Test Location X
        "20.0, 4.0, 45.0, 0.0, 0.0, 5.0, 0.0, false", // Testing Test Location X
        "20.0, 4.0, 90.0, 0.0, 0.0, 0.0, 2.0, true", // Testing Test Location Y
        "20.0, 4.0, 90.0, 0.0, 0.0, 0.0, 5.0, false", // Testing Test Location Y
        "1.0, 100.0, 0.0, 0.0, 0.0, 2.5, 0.0, true", // Testing X Position
        "1.0, 100.0, 0.0, 5.0, 0.0, 2.5, 0.0, false", // Testing X Position
        "20.0, 4.0, 0.0, 0.0, 3.0, 3.0, 3.0, true", // Testing Y Position
        "20.0, 4.0, 0.0, 0.0, 0.0, 3.0, 3.0, false" // Testing Y Position
    })
    public void testValues(
            double fov_deg,
            double radius,
            double coneAngle_deg,
            double coneLocX,
            double coneLocY,
            double testPointX,
            double testPointY,
            boolean expectedResult) {
        Cone2d testCone = new Cone2d(
                Angle.degrees(fov_deg),
                radius,
                new Pose2d(new Vector2d(coneLocX, coneLocY), Angle.degrees(coneAngle_deg)));

        Vector2d testPoint = new Vector2d(testPointX, testPointY);

        // Adjusting cone parameter.
        assertEquals(expectedResult, testCone.isInCone(testPoint));
    }
}
