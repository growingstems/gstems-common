/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.sensor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class LinearCal_Test {
    private static final double k_allowableError = 1.0e-10;

    @ParameterizedTest
    @CsvSource({
        "1.0, 2.0, 2.0, 6.0, 6.0",
        "2.0, 2.0, 4.0, 3.0, 6.0",
        "3.0, 2.0, 6.0, -3.0, -9.0",
        "0.5, 2.0, 1.0, 6.0, 3.0",
        "0.0, 2.0, 0.0, 3.0, 0.0",
        "-2.0, 2.0, -4.0, -3.0, 6.0"
    })
    public void scaleTest(double scaleFactor, double inA, double outA, double inB, double outB) {
        LinearCal linearCal = new LinearCal(scaleFactor);
        assertEquals(outA, linearCal.update(inA), k_allowableError);
        assertEquals(outB, linearCal.update(inB), k_allowableError);
    }
}
