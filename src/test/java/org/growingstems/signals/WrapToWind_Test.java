/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.growingstems.math.Range;
import org.junit.jupiter.api.Test;

public class WrapToWind_Test {
    private static final double k_tolerance = 0.01;

    @Test
    public void noWrap() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 360.0));
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
        assertEquals(100.0, testWrap.update(100.0), k_tolerance);
        assertEquals(200.0, testWrap.update(200.0), k_tolerance);
        assertEquals(300.0, testWrap.update(300.0), k_tolerance);
        assertEquals(250.0, testWrap.update(250.0), k_tolerance);
        assertEquals(130.0, testWrap.update(130.0), k_tolerance);
        assertEquals(50.0, testWrap.update(50.0), k_tolerance);
        assertEquals(2.0, testWrap.update(2.0), k_tolerance);
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
    }

    @Test
    public void OneWrapUp() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 360.0));
        assertEquals(120.0, testWrap.update(120.0), k_tolerance);
        assertEquals(240.0, testWrap.update(240.0), k_tolerance);
        assertEquals(360.0, testWrap.update(0.0), k_tolerance);
        assertEquals(410.0, testWrap.update(50.0), k_tolerance);
    }

    @Test
    public void OneWrapDown() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 360.0));
        assertEquals(100.0, testWrap.update(100.0), k_tolerance);
        assertEquals(20.0, testWrap.update(20.0), k_tolerance);
        assertEquals(-60.0, testWrap.update(300.0), k_tolerance);
    }

    @Test
    public void TwoWrapsUp() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 360.0));
        assertEquals(120.0, testWrap.update(120.0), k_tolerance);
        assertEquals(240.0, testWrap.update(240.0), k_tolerance);
        assertEquals(360.0, testWrap.update(0.0), k_tolerance);
        assertEquals(480.0, testWrap.update(120.0), k_tolerance);
        assertEquals(600.0, testWrap.update(240.0), k_tolerance);
        assertEquals(720.0, testWrap.update(0.0), k_tolerance);
        assertEquals(840.0, testWrap.update(120.0), k_tolerance);
    }

    @Test
    public void TwoWrapsDown() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 360.0));
        assertEquals(120.0, testWrap.update(120.0), k_tolerance);
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
        assertEquals(-120.0, testWrap.update(240.0), k_tolerance);
        assertEquals(-240.0, testWrap.update(120.0), k_tolerance);
        assertEquals(-360.0, testWrap.update(0.0), k_tolerance);
        assertEquals(-480.0, testWrap.update(240.0), k_tolerance);
        assertEquals(-600.0, testWrap.update(120.0), k_tolerance);
    }

    @Test
    public void WrapUpWrapDown() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 360.0));
        assertEquals(120.0, testWrap.update(120.0), k_tolerance);
        assertEquals(240.0, testWrap.update(240.0), k_tolerance);
        assertEquals(360.0, testWrap.update(0.0), k_tolerance);
        assertEquals(480.0, testWrap.update(120.0), k_tolerance);
        assertEquals(360.0, testWrap.update(0.0), k_tolerance);
        assertEquals(240.0, testWrap.update(240.0), k_tolerance);
    }

    @Test
    public void WrapDownWrapUp() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 360.0));
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
        assertEquals(-120.0, testWrap.update(240.0), k_tolerance);
        assertEquals(-240.0, testWrap.update(120.0), k_tolerance);
        assertEquals(-120.0, testWrap.update(240.0), k_tolerance);
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
        assertEquals(120.0, testWrap.update(120.0), k_tolerance);
    }

    @Test
    public void WrapBoundsNegaNega() {
        WrapToWind testWrap = new WrapToWind(new Range(-360.0, -180.0));
        assertEquals(-270.0, testWrap.update(-270.0), k_tolerance);
        assertEquals(-200.0, testWrap.update(-200.0), k_tolerance);
        assertEquals(-150.0, testWrap.update(-330.0), k_tolerance);
        assertEquals(-200.0, testWrap.update(-200.0), k_tolerance);
        assertEquals(-270.0, testWrap.update(-270.0), k_tolerance);
        assertEquals(-330.0, testWrap.update(-330.0), k_tolerance);
        assertEquals(-390.0, testWrap.update(-210.0), k_tolerance);
    }

    @Test
    public void WrapBoundsNegaPosi() {
        WrapToWind testWrap = new WrapToWind(new Range(-180.0, 180.0));
        assertEquals(120.0, testWrap.update(120.0), k_tolerance);
        assertEquals(240.0, testWrap.update(-120.0), k_tolerance);
        assertEquals(120.0, testWrap.update(120.0), k_tolerance);
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
        assertEquals(-120.0, testWrap.update(-120.0), k_tolerance);
        assertEquals(-240.0, testWrap.update(120.0), k_tolerance);
    }

    @Test
    public void WrapDiffValues() {
        WrapToWind testWrap = new WrapToWind(new Range(-50.0, 50.0));
        assertEquals(25.0, testWrap.update(25.0), k_tolerance);
        assertEquals(55.0, testWrap.update(-45.0), k_tolerance);
        assertEquals(65.0, testWrap.update(-35.0), k_tolerance);
        assertEquals(45.0, testWrap.update(45.0), k_tolerance);
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
        assertEquals(-30.0, testWrap.update(-30.0), k_tolerance);
        assertEquals(-60.0, testWrap.update(40.0), k_tolerance);
    }

    @Test
    public void WrapRoundAndRound() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 50.0));
        assertEquals(20.0, testWrap.update(20.0), k_tolerance);
        assertEquals(40.0, testWrap.update(40.0), k_tolerance);
        assertEquals(60.0, testWrap.update(10.0), k_tolerance);
        assertEquals(80.0, testWrap.update(30.0), k_tolerance);
        assertEquals(100.0, testWrap.update(50.0), k_tolerance);
        assertEquals(120.0, testWrap.update(20.0), k_tolerance);
        assertEquals(140.0, testWrap.update(40.0), k_tolerance);
        assertEquals(160.0, testWrap.update(10.0), k_tolerance);
        assertEquals(180.0, testWrap.update(30.0), k_tolerance);
        assertEquals(200.0, testWrap.update(50.0), k_tolerance);
    }

    @Test
    public void WrapNewConstruct() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 50.0), 30.0);
        assertEquals(40.0, testWrap.update(40.0), k_tolerance);
        assertEquals(20.0, testWrap.update(20.0), k_tolerance);
        assertEquals(0.0, testWrap.update(0.0), k_tolerance);
        assertEquals(24.0, testWrap.update(24.0), k_tolerance);
        assertEquals(45.0, testWrap.update(45.0), k_tolerance);
        assertEquals(60.0, testWrap.update(10.0), k_tolerance);
    }

    @Test
    public void SignalDoesNotContainZero() {
        WrapToWind testWrap = new WrapToWind(new Range(100.0, 200.0));
        assertEquals(190.0, testWrap.update(190.0), k_tolerance);
        assertEquals(210.0, testWrap.update(110.0), k_tolerance);
        assertEquals(250.0, testWrap.update(150.0), k_tolerance);
        assertEquals(290.0, testWrap.update(190.0), k_tolerance);
        assertEquals(330.0, testWrap.update(130.0), k_tolerance);
        assertEquals(295.0, testWrap.update(195.0), k_tolerance);
    }

    @Test
    public void WrapFirstUpdate() {
        WrapToWind testWrap = new WrapToWind(new Range(100.0, 200.0), 190.0);
        assertEquals(210.0, testWrap.update(110.0), k_tolerance);
        assertEquals(250.0, testWrap.update(150.0), k_tolerance);
        assertEquals(290.0, testWrap.update(190.0), k_tolerance);
        assertEquals(330.0, testWrap.update(130.0), k_tolerance);
        assertEquals(295.0, testWrap.update(195.0), k_tolerance);
    }

    @Test
    public void UnalignedRange() {
        WrapToWind testWrap = new WrapToWind(new Range(50.0, 150.0));
        assertEquals(110.0, testWrap.update(110.0), k_tolerance);
        assertEquals(140.0, testWrap.update(140.0), k_tolerance);
        assertEquals(150.0, testWrap.update(50.0), k_tolerance);
        assertEquals(150.0, testWrap.update(150.0), k_tolerance);
        assertEquals(160.0, testWrap.update(60.0), k_tolerance);
    }

    @Test
    public void ExceptionWhenOutOfRange() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 100.0));
        assertEquals(90.0, testWrap.update(90.0), k_tolerance);
        try {
            testWrap.update(110.0);
            fail("IllegalArgumentException was not thrown.");
        } catch (IllegalArgumentException e) {
            // Do nothing - test passed!
        }
    }

    @Test
    public void ResetOutOfRangeThrows() {
        WrapToWind testWrap = new WrapToWind(new Range(0.0, 100.0));
        assertEquals(90.0, testWrap.update(90.0), k_tolerance);
        try {
            testWrap.reset(110.0);
            fail("IllegalArgumentException was not thrown.");
        } catch (IllegalArgumentException e) {
            // Do nothing - test passed!
        }
    }
}
