package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.growingstems.signals.api.SignalModifier;
import org.junit.jupiter.api.Test;

public class Splitter_Test {
    @Test
    public void emptySplitter() {
        SignalModifier<Integer, Stream<Integer>> splitter = new Splitter<Integer, Integer>(List.of());
        List<Integer> out = splitter.update(5).collect(Collectors.toList());
        assertEquals(0, out.size());
    }

    @Test
    public void singleSplitter() {
        SignalModifier<Integer, Stream<Integer>> splitter =
                new Splitter<Integer, Integer>(List.of(v -> v));
        List<Integer> out = splitter.update(5).collect(Collectors.toList());
        assertEquals(1, out.size());
        assertTrue(out.stream().allMatch(i -> i == 5));
    }

    @Test
    public void manySplitter() {
        SignalModifier<Integer, Stream<Integer>> splitter =
                new Splitter<Integer, Integer>(List.of(v -> v, v -> v, v -> v, v -> v));
        List<Integer> out = splitter.update(5).collect(Collectors.toList());
        assertEquals(4, out.size());
        assertTrue(out.stream().allMatch(i -> i == 5));
    }

    @Test
    public void splitterOrder() {
        SignalModifier<Integer, Stream<Integer>> splitter =
                new Splitter<Integer, Integer>(List.of(v -> v, v -> 2 * v, v -> v * v, v -> v / 2));
        List<Integer> out = splitter.update(5).collect(Collectors.toList());
        assertTrue(out.equals(List.of(5, 10, 25, 2)));
    }

    @Test
    public void addTest() {
        SignalModifier<Integer, Stream<Integer>> splitter =
                new Splitter<Integer, Integer>(List.of(v -> v, v -> 2 * v, v -> v * v, v -> v / 2));
        SignalModifier<Integer, Integer> adder = splitter.append(s -> s.reduce(0, Integer::sum));
        assertEquals(42, adder.update(5));
    }
}
