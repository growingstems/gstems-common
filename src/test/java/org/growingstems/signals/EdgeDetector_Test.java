/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.logic.LogicCore.Edge;
import org.junit.jupiter.api.Test;

public class EdgeDetector_Test {
    EdgeDetector m_ed;

    // constructor tests
    @Test
    public void defaultConstruction() {
        m_ed = new EdgeDetector(Edge.RISING);

        assertEquals(Edge.RISING, m_ed.getTrackedEdge());
    }

    @Test
    public void parameterConstructor() {
        m_ed = new EdgeDetector(Edge.FALLING);

        assertEquals(Edge.FALLING, m_ed.getTrackedEdge());
    }

    // basic functionality tests
    @Test
    public void risingEdge() {
        m_ed = new EdgeDetector(Edge.RISING);

        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(false)); // no edge
        assertTrue(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(true)); // no edge
        assertFalse(m_ed.update(false)); // falling edge
        assertTrue(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(false)); // falling edge

        m_ed = new EdgeDetector(Edge.RISING, null);

        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(false)); // no edge
        assertTrue(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(true)); // no edge
        assertFalse(m_ed.update(false)); // falling edge
        assertTrue(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(false)); // falling edge
    }

    @Test
    public void fallingEdge() {
        m_ed = new EdgeDetector(Edge.FALLING);

        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(true)); // no edge
        assertTrue(m_ed.update(false)); // falling edge
        assertFalse(m_ed.update(true)); // rising edge
        assertTrue(m_ed.update(false)); // falling edge

        m_ed = new EdgeDetector(Edge.FALLING, null);

        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(true)); // no edge
        assertTrue(m_ed.update(false)); // falling edge
        assertFalse(m_ed.update(true)); // rising edge
        assertTrue(m_ed.update(false)); // falling edge
    }

    @Test
    public void anyEdge() {
        m_ed = new EdgeDetector(Edge.ANY);

        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(false)); // no edge
        assertTrue(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(true)); // no edge
        assertTrue(m_ed.update(false)); // falling edge
        assertTrue(m_ed.update(true)); // rising edge
        assertTrue(m_ed.update(false)); // falling edge

        m_ed = new EdgeDetector(Edge.ANY, null);

        assertFalse(m_ed.update(false)); // no edge
        assertFalse(m_ed.update(false)); // no edge
        assertTrue(m_ed.update(true)); // rising edge
        assertFalse(m_ed.update(true)); // no edge
        assertTrue(m_ed.update(false)); // falling edge
        assertTrue(m_ed.update(true)); // rising edge
        assertTrue(m_ed.update(false)); // falling edge
    }

    // edge case tests
    @Test
    public void neverTrueFirstCall() {
        EdgeDetector redTrue = new EdgeDetector(Edge.RISING);
        assertFalse(redTrue.update(true));

        EdgeDetector redFalse = new EdgeDetector(Edge.RISING);
        assertFalse(redFalse.update(false));

        EdgeDetector fedTrue = new EdgeDetector(Edge.FALLING);
        assertFalse(fedTrue.update(true));

        EdgeDetector fedFalse = new EdgeDetector(Edge.FALLING);
        assertFalse(fedFalse.update(false));

        EdgeDetector aedTrue = new EdgeDetector(Edge.ANY);
        assertFalse(aedTrue.update(true));

        EdgeDetector aedFalse = new EdgeDetector(Edge.ANY);
        assertFalse(aedFalse.update(false));
    }

    @Test
    public void neverTrueAfterReset() {
        EdgeDetector risingEdge = new EdgeDetector(Edge.RISING);
        assertFalse(risingEdge.update(false));
        risingEdge.reset();
        assertFalse(risingEdge.update(true));
        assertFalse(risingEdge.update(false));
        assertTrue(risingEdge.update(true));

        EdgeDetector fallingEdge = new EdgeDetector(Edge.FALLING);
        assertFalse(fallingEdge.update(true));
        fallingEdge.reset();
        assertFalse(fallingEdge.update(false));
        assertFalse(fallingEdge.update(true));
        assertTrue(fallingEdge.update(false));

        EdgeDetector anyEdge = new EdgeDetector(Edge.ANY);
        assertFalse(anyEdge.update(false));
        anyEdge.reset();
        assertFalse(anyEdge.update(true));
        assertTrue(anyEdge.update(false));
        assertTrue(anyEdge.update(true));
        anyEdge.reset();
        assertFalse(anyEdge.update(false));
        assertTrue(anyEdge.update(true));
    }
}
