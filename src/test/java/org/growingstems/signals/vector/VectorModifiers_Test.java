/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals.vector;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.math.Vector2d;
import org.growingstems.measurements.Angle;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.test.TestCore;
import org.junit.jupiter.api.Test;

public class VectorModifiers_Test {
    private static SignalModifier<Double, Double> k_doubleAddOne = in -> in + 1.0;
    private static SignalModifier<Double, Double> k_doubleTimesThree = in -> in * 3.0;
    private static SignalModifier<Angle, Angle> k_angleAddNinety = in -> in.add(Angle.degrees(90.0));

    @Test
    public void xyModifierTest() {
        var xy = VectorModifiers.xyModifier(k_doubleAddOne, k_doubleTimesThree);

        assertTrue(new Vector2d(0.0, 3.0).rangeTo(xy.update(new Vector2d(-1.0, 1.0))) < TestCore.k_eps);
    }

    @Test
    public void xModifierTest() {
        var x = VectorModifiers.xModifier(k_doubleAddOne);

        assertTrue(new Vector2d(0.0, 1.0).rangeTo(x.update(new Vector2d(-1.0, 1.0))) < TestCore.k_eps);
    }

    @Test
    public void yModifierTest() {
        var y = VectorModifiers.yModifier(k_doubleAddOne);

        assertTrue(new Vector2d(-1.0, 2.0).rangeTo(y.update(new Vector2d(-1.0, 1.0))) < TestCore.k_eps);
    }

    @Test
    public void magAngleModifierTest() {
        var magAngle = VectorModifiers.magAngleModifier(k_doubleAddOne, k_angleAddNinety);

        assertTrue(
                new Vector2d(0.0, -3.0).rangeTo(magAngle.update(new Vector2d(-2.0, 0.0))) < TestCore.k_eps);
    }

    @Test
    public void magModifierTest() {
        var mag = VectorModifiers.magModifier(k_doubleAddOne);

        assertTrue(
                new Vector2d(-3.0, 0.0).rangeTo(mag.update(new Vector2d(-2.0, 0.0))) < TestCore.k_eps);
    }

    @Test
    public void angleModifierTest() {
        var angle = VectorModifiers.angleModifier(k_angleAddNinety);

        assertTrue(
                new Vector2d(0.0, -2.0).rangeTo(angle.update(new Vector2d(-2.0, 0.0))) < TestCore.k_eps);
    }
}
