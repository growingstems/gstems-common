/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals.vector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.math.Vector2d;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.TestCore;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class VectorRateLimiter_Test {
    private TestTimeSource m_timeSource = new TestTimeSource();

    @Test
    public void simpleTest() {
        VectorRateLimiter rateLimiter = new VectorRateLimiter(m_timeSource, Frequency.hertz(0.25));

        assertEquals(new Vector2d(), rateLimiter.update(new Vector2d(0.0, 0.0)));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(
                new Vector2d(0.0, 0.25).equals(rateLimiter.update(new Vector2d(0.0, 0.3)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(
                new Vector2d(0.0, 0.3).equals(rateLimiter.update(new Vector2d(0.0, 0.3)), TestCore.k_eps));
    }

    @Test
    public void resetTest() {
        VectorRateLimiter rateLimiter = new VectorRateLimiter(m_timeSource, Frequency.hertz(0.25));

        assertEquals(new Vector2d(), rateLimiter.update(new Vector2d(0.0, 0.0)));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(0.0, 0.3)
                .equals(rateLimiter.reset().update(new Vector2d(0.0, 0.3)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(
                new Vector2d(0.0, 0.3).equals(rateLimiter.update(new Vector2d(0.0, 0.3)), TestCore.k_eps));
    }

    @Test
    public void peekTest() {
        VectorRateLimiter rateLimiter = new VectorRateLimiter(m_timeSource, Frequency.hertz(0.25));

        assertFalse(rateLimiter.peek().isPresent());

        assertEquals(new Vector2d(), rateLimiter.update(new Vector2d(0.0, 0.0)));
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(new Vector2d(), rateLimiter.peek().get());
    }

    @Test
    public void sidewaysTest() {
        VectorRateLimiter rateLimiter = new VectorRateLimiter(m_timeSource, Frequency.hertz(0.25));

        assertEquals(new Vector2d(0.5, 0.5), rateLimiter.update(new Vector2d(0.5, 0.5)));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(0.25, 0.5)
                .equals(rateLimiter.update(new Vector2d(-0.6, 0.5)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(
                new Vector2d(0.0, 0.5).equals(rateLimiter.update(new Vector2d(-0.6, 0.5)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.25, 0.5)
                .equals(rateLimiter.update(new Vector2d(-0.6, 0.5)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.5, 0.5)
                .equals(rateLimiter.update(new Vector2d(-0.6, 0.5)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.6, 0.5)
                .equals(rateLimiter.update(new Vector2d(-0.6, 0.5)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.6, 0.5)
                .equals(rateLimiter.update(new Vector2d(-0.6, 0.5)), TestCore.k_eps));
    }

    @Test
    public void diagonalTest() {
        VectorRateLimiter rateLimiter = new VectorRateLimiter(m_timeSource, Frequency.hertz(0.25));

        assertEquals(new Vector2d(0.4, 0.6), rateLimiter.update(new Vector2d(0.4, 0.6)));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(0.204782797, 0.443826238)
                .equals(rateLimiter.update(new Vector2d(-0.6, -0.2)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(0.009565595, 0.287652476)
                .equals(rateLimiter.update(new Vector2d(-0.6, -0.2)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.185651607, 0.131478714)
                .equals(rateLimiter.update(new Vector2d(-0.6, -0.2)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.380868809, -0.024695047)
                .equals(rateLimiter.update(new Vector2d(-0.6, -0.2)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.576086011, -0.180868809)
                .equals(rateLimiter.update(new Vector2d(-0.6, -0.2)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.6, -0.2)
                .equals(rateLimiter.update(new Vector2d(-0.6, -0.2)), TestCore.k_eps));

        m_timeSource.addTime(Time.seconds(1.0));
        assertTrue(new Vector2d(-0.6, -0.2)
                .equals(rateLimiter.update(new Vector2d(-0.6, -0.2)), TestCore.k_eps));
    }

    @Test
    public void initialValueTest() {
        VectorRateLimiter rateLimiter =
                new VectorRateLimiter(m_timeSource, Frequency.hertz(0.25), new Vector2d(0.4, 0.6));
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(new Vector2d(0.4, 0.6), rateLimiter.peek().get());
    }

    @Test
    public void forceUpdateTest() {
        VectorRateLimiter rateLimiter =
                new VectorRateLimiter(m_timeSource, Frequency.hertz(0.25), new Vector2d(0.4, 0.6));
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(new Vector2d(0.4, 0.6), rateLimiter.peek().get());

        rateLimiter.forceUpdate(new Vector2d(-0.6, -0.2));
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(new Vector2d(-0.6, -0.2), rateLimiter.peek().get());
    }
}
