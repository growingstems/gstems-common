/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals.vector;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.math.Vector2dU;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.test.TestCore;
import org.junit.jupiter.api.Test;

public class VectorUModifiers_Test {
    private static SignalModifier<Unitless, Velocity> k_translationModifier =
            in -> in.mul(Velocity.feetPerSecond(12.0));
    private static SignalModifier<Length, Length> k_doubleAddOne = in -> in.add(Length.inches(1.0));
    private static SignalModifier<Length, Length> k_doubleTimesThree = in -> in.mul(3.0);
    private static SignalModifier<Angle, Angle> k_angleNoChange = in -> in;
    private static SignalModifier<Angle, Angle> k_angleAddNinety = in -> in.add(Angle.degrees(90.0));

    @Test
    public void xyModifierTest() {
        var xy = VectorUModifiers.xyModifier(k_doubleAddOne, k_doubleTimesThree);

        assertTrue(new Vector2dU<>(Length.inches(0.0), Length.inches(3.0))
                .rangeTo(xy.update(new Vector2dU<>(Length.inches(-1.0), Length.inches(1.0))))
                .lt(Length.inches(TestCore.k_eps)));
    }

    @Test
    public void xyModifierDifferentUnitTest() {
        var xy = VectorUModifiers.xyModifier(k_translationModifier, k_translationModifier);

        assertTrue(new Vector2dU<>(Velocity.feetPerSecond(6.0), Velocity.feetPerSecond(6.0))
                .rangeTo(xy.update(new Vector2dU<>(Unitless.none(0.5), Unitless.none(0.5))))
                .lt(Velocity.inchesPerSecond(TestCore.k_eps)));
    }

    @Test
    public void xModifierTest() {
        var x = VectorUModifiers.xModifier(k_doubleAddOne);

        assertTrue(new Vector2dU<>(Length.inches(0.0), Length.inches(1.0))
                .rangeTo(x.update(new Vector2dU<>(Length.inches(-1.0), Length.inches(1.0))))
                .lt(Length.inches(TestCore.k_eps)));
    }

    @Test
    public void yModifierTest() {
        var y = VectorUModifiers.yModifier(k_doubleAddOne);

        assertTrue(new Vector2dU<>(Length.inches(-1.0), Length.inches(2.0))
                .rangeTo(y.update(new Vector2dU<>(Length.inches(-1.0), Length.inches(1.0))))
                .lt(Length.inches(TestCore.k_eps)));
    }

    @Test
    public void magAngleModifierTest() {
        var magAngle = VectorUModifiers.magAngleModifier(k_doubleAddOne, k_angleAddNinety);

        assertTrue(new Vector2dU<>(Length.inches(0.0), Length.inches(-3.0))
                .rangeTo(magAngle.update(new Vector2dU<>(Length.inches(-2.0), Length.inches(0.0))))
                .lt(Length.inches(TestCore.k_eps)));
    }

    @Test
    public void magAngleModifierDifferentUnitTest() {
        var magAngle = VectorUModifiers.magAngleModifier(k_translationModifier, k_angleNoChange);

        assertTrue(new Vector2dU<>(Velocity.feetPerSecond(6.0), Velocity.feetPerSecond(6.0))
                .rangeTo(magAngle.update(new Vector2dU<>(Unitless.none(0.5), Unitless.none(0.5))))
                .lt(Velocity.inchesPerSecond(TestCore.k_eps)));
    }

    @Test
    public void magModifierTest() {
        var mag = VectorUModifiers.magModifier(k_doubleAddOne);

        assertTrue(new Vector2dU<>(Length.inches(-3.0), Length.inches(0.0))
                .rangeTo(mag.update(new Vector2dU<>(Length.inches(-2.0), Length.inches(0.0))))
                .lt(Length.inches(TestCore.k_eps)));
    }

    @Test
    public void angleModifierTest() {
        SignalModifier<Vector2dU<Length>, Vector2dU<Length>> angle =
                VectorUModifiers.angleModifier(k_angleAddNinety);

        assertTrue(new Vector2dU<>(Length.inches(0.0), Length.inches(-2.0))
                .rangeTo(angle.update(new Vector2dU<>(Length.inches(-2.0), Length.inches(0.0))))
                .lt(Length.inches(TestCore.k_eps)));
    }
}
