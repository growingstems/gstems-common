/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.growingstems.measurements.Angle;
import org.junit.jupiter.api.Test;

public class Memory_Test {

    public static class DoubleTest {
        public double val;
    }

    public static class IntegerTest {
        public int val;
    }

    @Test
    public void angleMemory() {
        Angle testAngle = Angle.degrees(20);
        Memory<Angle> savedAngle = new Memory<Angle>();
        savedAngle.update(testAngle);
        assertEquals(testAngle, savedAngle.getLastValue());
        testAngle = testAngle.add(Angle.degrees(15));
        assertNotEquals(testAngle, savedAngle.getLastValue());
    }

    @Test
    public void mutableTest() {
        DoubleTest testVal = new DoubleTest();
        testVal.val = 12.0;
        Memory<DoubleTest> savedVal = new Memory<DoubleTest>();
        savedVal.update(testVal);
        assertEquals(testVal, savedVal.getLastValue());
        testVal.val = 17.0;
        assertEquals(testVal, savedVal.getLastValue());
    }

    @Test
    public void constructorTest() {
        // Empty Constructor
        Memory<IntegerTest> emptyConstructor = new Memory<IntegerTest>();
        assertEquals(null, emptyConstructor.getLastValue());

        // Initialized Constructor
        IntegerTest testVal = new IntegerTest();
        testVal.val = 12;
        Memory<IntegerTest> initConstructor = new Memory<IntegerTest>(testVal);
        assertEquals(testVal, initConstructor.getLastValue());
    }
}
