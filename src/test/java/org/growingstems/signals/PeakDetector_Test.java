/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.signals.PeakDetector.PeakType;
import org.growingstems.signals.api.SignalModifier;
import org.junit.jupiter.api.Test;

public class PeakDetector_Test {

    public void filler(
            double[] inputs, boolean[] anyOutputs, boolean[] maxOutputs, boolean[] minOutputs) {
        SignalModifier<Double, Boolean> peakDetectorAny = new PeakDetector(PeakType.BOTH);
        SignalModifier<Double, Boolean> peakDetectorMax = new PeakDetector(PeakType.MAXIMUM);
        SignalModifier<Double, Boolean> peakDetectorMin = new PeakDetector(PeakType.MINIMUM);

        for (int i = 0; i < inputs.length; i++) {
            assertEquals(anyOutputs[i], peakDetectorAny.update(inputs[i]), String.valueOf(i));
            assertEquals(maxOutputs[i], peakDetectorMax.update(inputs[i]), String.valueOf(i));
            assertEquals(minOutputs[i], peakDetectorMin.update(inputs[i]), String.valueOf(i));
        }
    }

    @Test
    public void simpleMaxTest() {
        double[] inputs = new double[] {1.0, 2.0, 1.0};
        boolean[] anyOutput = new boolean[] {false, false, true};
        boolean[] maxOutput = anyOutput;
        boolean[] minOutput = new boolean[] {false, false, false};
        filler(inputs, anyOutput, maxOutput, minOutput);
    }

    @Test
    public void simpleMinTest() {
        double[] inputs = new double[] {2.0, 1.0, 2.0};
        boolean[] anyOutput = new boolean[] {false, false, true};
        boolean[] maxOutput = new boolean[] {false, false, false};
        boolean[] minOutput = anyOutput;
        filler(inputs, anyOutput, maxOutput, minOutput);
    }

    @Test
    public void equalValueRisingRisingTest() {
        // constructor
        double[] inputs = new double[] {1.0, 2.0, 3.0, 3.0, 3.0, 3.0, 4.0};
        boolean[] anyOutput = new boolean[] {false, false, false, false, false, false, false};
        boolean[] maxOutput = anyOutput;
        boolean[] minOutput = anyOutput;
        filler(inputs, anyOutput, maxOutput, minOutput);
    }

    @Test
    public void equalValueRisingFallingTest() {
        double[] inputs = new double[] {-1.0, 0.0, 2.0, 3.0, 3.0, 3.0, -2.0};
        boolean[] anyOutput = new boolean[] {false, false, false, false, false, false, true};
        boolean[] minOutput = new boolean[] {false, false, false, false, false, false, false};
        boolean[] maxOutput = anyOutput;
        filler(inputs, anyOutput, maxOutput, minOutput);
    }

    @Test
    public void equalValueFallingFallingTest() {
        double[] inputs = new double[] {-1.0, -2.0, -3.0, -4.5, -4.5, -4.5, -6.0};
        boolean[] anyOutput = new boolean[] {false, false, false, false, false, false, false};
        boolean[] maxOutput = anyOutput;
        boolean[] minOutput = anyOutput;
        filler(inputs, anyOutput, maxOutput, minOutput);
    }

    @Test
    public void equalValueFallingRisingTest() {
        double[] inputs = new double[] {1.0, -2.0, -5.0, -5.0, -5.0, 5.0, 6.0};
        boolean[] anyOutput = new boolean[] {false, false, false, false, false, true, false};
        boolean[] minOutput = anyOutput;
        boolean[] maxOutput = new boolean[] {false, false, false, false, false, false, false};
        filler(inputs, anyOutput, maxOutput, minOutput);
    }
}
