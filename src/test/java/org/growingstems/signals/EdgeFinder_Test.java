/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.logic.LogicCore.Edge;
import org.junit.jupiter.api.Test;

public class EdgeFinder_Test {
    @Test
    public void defaultConstructionTestKeptLow() {
        EdgeFinder ed;

        // Kept LOW
        ed = new EdgeFinder();
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));

        // Kept LOW
        ed = new EdgeFinder(null);
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));
    }

    @Test
    public void defaultConstructionTestKeptHigh() {
        EdgeFinder ed;

        // Kept HIGH
        ed = new EdgeFinder();
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));

        // Kept HIGH
        ed = new EdgeFinder(null);
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));
    }

    @Test
    public void defaultConstructionTestLowToHigh() {
        EdgeFinder ed;

        // LOW to HIGH
        ed = new EdgeFinder();
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.RISING, ed.update(true));

        // LOW to HIGH
        ed = new EdgeFinder(null);
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.RISING, ed.update(true));
    }

    @Test
    public void defaultConstructionTestHighToLow() {
        EdgeFinder ed;

        // HIGH to LOW
        ed = new EdgeFinder();
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.FALLING, ed.update(false));

        // HIGH to LOW
        ed = new EdgeFinder(null);
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.FALLING, ed.update(false));
    }

    @Test
    public void startingLevelConstructorTest() {
        EdgeFinder ed;

        // ------------------------------------
        // Starting Level LOW Constructor
        // ------------------------------------
        // Kept LOW
        ed = new EdgeFinder(false);
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));

        // Kept HIGH
        ed = new EdgeFinder(false);
        assertEquals(Edge.RISING, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));

        // LOW to HIGH
        ed = new EdgeFinder(false);
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.RISING, ed.update(true));

        // HIGH to LOW
        ed = new EdgeFinder(false);
        assertEquals(Edge.RISING, ed.update(true));
        assertEquals(Edge.FALLING, ed.update(false));

        // -------------------------------------
        // Starting Level HIGH Constructor
        // -------------------------------------
        // Kept LOW
        ed = new EdgeFinder(true);
        assertEquals(Edge.FALLING, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));

        // Kept HIGH
        ed = new EdgeFinder(true);
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));

        // LOW to HIGH
        ed = new EdgeFinder(true);
        assertEquals(Edge.FALLING, ed.update(false));
        assertEquals(Edge.RISING, ed.update(true));

        // HIGH to LOW
        ed = new EdgeFinder(true);
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.FALLING, ed.update(false));
    }

    @Test
    public void updateTest() {
        EdgeFinder ed = new EdgeFinder();

        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.RISING, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.FALLING, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.NONE, ed.update(false));
        assertEquals(Edge.RISING, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));
        assertEquals(Edge.NONE, ed.update(true));
    }

    @Test
    public void resetTest() {
        EdgeFinder ed = new EdgeFinder();

        // -------------------------
        // Default Constructor
        // -------------------------
        ed = new EdgeFinder();

        // Last Signal LOW - Next Signal LOW
        ed.update(false);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(false));

        // Last Signal LOW - Next Signal HIGH
        ed.update(false);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(true));

        // Last Signal HIGH - Next Signal LOW
        ed.update(true);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(false));

        // Last Signal HIGH - Next Signal HIGH
        ed.update(true);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(true));

        // ------------------------------------
        // Starting Level LOW Constructor
        // ------------------------------------
        ed = new EdgeFinder(false);

        // Last Signal LOW - Next Signal LOW
        ed.update(false);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(false));

        // Last Signal LOW - Next Signal HIGH
        ed.update(false);
        ed.reset();
        assertEquals(Edge.RISING, ed.update(true));

        // Last Signal HIGH - Next Signal LOW
        ed.update(true);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(false));

        // Last Signal HIGH - Next Signal HIGH
        ed.update(true);
        ed.reset();
        assertEquals(Edge.RISING, ed.update(true));

        // -------------------------------------
        // Starting Level HIGH Constructor
        // -------------------------------------
        ed = new EdgeFinder(true);

        // Last Signal LOW - Next Signal LOW
        ed.update(false);
        ed.reset();
        assertEquals(Edge.FALLING, ed.update(false));

        // Last Signal LOW - Next Signal HIGH
        ed.update(false);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(true));

        // Last Signal HIGH - Next Signal LOW
        ed.update(true);
        ed.reset();
        assertEquals(Edge.FALLING, ed.update(false));

        // Last Signal HIGH - Next Signal HIGH
        ed.update(true);
        ed.reset();
        assertEquals(Edge.NONE, ed.update(true));
    }
}
