/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class Schmitt_Test {

    @Test
    public void schmittTestReducedConstructor() {
        SchmittTrigger st = new SchmittTrigger(1.5, 3.5);
        assertFalse(st.update(2.0));
        assertTrue(st.update(4.0));
        assertTrue(st.update(3.0));
        assertFalse(st.update(1.0));
        assertFalse(st.update(2.0));
    }

    @Test
    public void schmittTestFullConstructor() {
        SchmittTrigger st = new SchmittTrigger(1.5, 3.5, true);
        assertTrue(st.update(2.345));
        assertFalse(st.update(1.0));
        assertFalse(st.update(2.0));
        assertTrue(st.update(4.0));
        assertTrue(st.update(3.0));
    }
}
