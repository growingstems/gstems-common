/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.PrintStreamTest;
import org.growingstems.test.TestCore;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class Stable_Test extends PrintStreamTest {
    TestTimeSource m_timeSource = new TestTimeSource();
    Stable m_s = null;

    @Test
    public void stableTrue() {
        m_s = new Stable(true, Time.seconds(0.1), m_timeSource);
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(true));
        m_timeSource.setTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(true));
        assertNoError();
    }

    @Test
    public void stableFalse() {
        m_s = new Stable(false, Time.seconds(0.1), m_timeSource);
        assertTrue(m_s.update(false));
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(false));
        m_timeSource.setTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(false));
        assertNoError();
    }

    @Test
    public void bounceyFalse() {
        m_s = new Stable(false, Time.seconds(500), m_timeSource);
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(300));
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(false)); // Timer was reset
        m_timeSource.addTime(Time.seconds(300));
        assertTrue(m_s.update(false)); // Timer was reset, so this should still be true
        m_timeSource.addTime(Time.seconds(100));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(300));
        assertFalse(m_s.update(false));
        m_timeSource.addTime(Time.seconds(300));
        assertFalse(m_s.update(false));
        assertTrue(m_s.update(true));
        assertNoError();
    }

    @Test
    public void bounceyTrue() {
        m_s = new Stable(true, Time.seconds(500), m_timeSource);
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(300));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(true)); // Timer was reset
        m_timeSource.addTime(Time.seconds(300));
        assertFalse(m_s.update(true)); // Timer was reset, so this should still be false
        m_timeSource.addTime(Time.seconds(100));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(300));
        assertTrue(m_s.update(true));
        m_timeSource.addTime(Time.seconds(300));
        assertTrue(m_s.update(true));
        assertFalse(m_s.update(false));
        assertNoError();
    }

    @Test
    public void startingLogicLevel() {
        // Null starting logic level
        m_s = new Stable(true, Time.seconds(0.1), m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));

        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(false, Time.seconds(0.1), m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(false));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));

        // false starting logic level
        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(true, Time.seconds(0.1), false, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));

        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(false, Time.seconds(0.1), false, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));

        // true starting logic level
        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(true, Time.seconds(0.1), true, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));

        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(false, Time.seconds(0.1), true, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(false));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        assertNoError();
    }

    @Test
    public void reset() {
        // Null starting logic level
        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(true, Time.seconds(0.1), m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        // Reset and repeat the same test criteria as construction.
        m_s.reset();
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));

        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(false, Time.seconds(0.1), m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(false));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        // Reset and repeat the same test criteria as construction.
        m_s.reset();
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(false));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));

        // false starting logic level
        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(true, Time.seconds(0.1), false, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        // Reset and repeat the same test criteria as construction.
        m_s.reset();
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));

        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(false, Time.seconds(0.1), false, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        // Reset and repeat the same test criteria as construction.
        m_s.reset();
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));

        // true starting logic level
        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(true, Time.seconds(0.1), true, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        // Reset and repeat the same test criteria as construction.
        m_s.reset();
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));

        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(false, Time.seconds(0.1), true, m_timeSource);
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(false));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        // Reset and repeat the same test criteria as construction.
        m_s.reset();
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(false));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(1));
        assertFalse(m_s.update(false));
        assertNoError();
    }

    @Test
    public void zeroEvalPeriod() {
        m_s = new Stable(true, Time.seconds(0.1), m_timeSource);
        assertFalse(m_s.update(true));
        assertFalse(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));

        m_timeSource.setTime(Time.ZERO);
        m_s = new Stable(true, Time.seconds(0.0), m_timeSource);
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(true));
        m_timeSource.addTime(Time.seconds(1));
        assertTrue(m_s.update(true));
        assertNoError();
    }

    @Test
    public void setEvalPeriod() {
        // Constructor
        Time setTime = Time.seconds(1.0);
        m_s = new Stable(true, setTime, m_timeSource);
        assertEquals(m_s.getEvalPeriod().getBaseValue(), setTime.getBaseValue(), TestCore.k_eps);

        setTime = Time.seconds(0.0);
        m_s = new Stable(true, setTime, m_timeSource);
        assertEquals(m_s.getEvalPeriod().getBaseValue(), setTime.getBaseValue(), TestCore.k_eps);
        assertNoError();

        setTime = Time.seconds(-1.0);
        m_s = new Stable(true, setTime, m_timeSource);
        assertErrorOccurred();
        assertEquals(m_s.getEvalPeriod().getBaseValue(), Time.ZERO.getBaseValue(), TestCore.k_eps);

        // Calling setEvalPeriod
        setTime = Time.seconds(1.0);
        m_s = new Stable(true, Time.seconds(100.0), m_timeSource);
        m_s.setEvalPeriod(setTime);
        assertEquals(m_s.getEvalPeriod().getBaseValue(), setTime.getBaseValue(), TestCore.k_eps);

        setTime = Time.seconds(0.0);
        m_s = new Stable(true, Time.seconds(100.0), m_timeSource);
        m_s.setEvalPeriod(setTime);
        assertEquals(m_s.getEvalPeriod().getBaseValue(), setTime.getBaseValue(), TestCore.k_eps);
        assertNoError();

        setTime = Time.seconds(-1.0);
        m_s = new Stable(true, Time.seconds(100.0), m_timeSource);
        m_s.setEvalPeriod(setTime);
        assertErrorOccurred();
        assertEquals(m_s.getEvalPeriod().getBaseValue(), Time.ZERO.getBaseValue(), TestCore.k_eps);
    }

    @Test
    public void randomTest() {
        m_s = new Stable(false, Time.seconds(0.5), false, m_timeSource);
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(false));
        assertTrue(m_s.update(true));
        m_timeSource.addTime(Time.seconds(0.3));
        assertTrue(m_s.update(true));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(0.3));
        assertTrue(m_s.update(false));
        m_timeSource.addTime(Time.seconds(0.3));
        assertFalse(m_s.update(false));
        assertNoError();
    }

    @Test
    public void timeZeroTestTrue() {
        m_s = new Stable(true, Time.ZERO, m_timeSource);
        assertTrue(m_s.update(true));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(false));
        assertTrue(m_s.update(true));
    }

    @Test
    public void timeZeroTestFalse() {
        m_s = new Stable(false, Time.ZERO, m_timeSource);
        assertTrue(m_s.update(true));
        assertFalse(m_s.update(false));
        assertFalse(m_s.update(false));
        assertTrue(m_s.update(true));
    }
}
