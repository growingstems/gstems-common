/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

public class StreamFilter_Test {
    private static final Object nullObj = null;

    @Test
    public void emptyStream() {
        Stream<Object> stream = Stream.of();
        Predicate<Object> predicate = in -> {
            nullObj.toString(); // Throws an exception if the predicate gets used
            return true;
        };

        StreamFilter<Object> filter = new StreamFilter<>(predicate);
        Stream<Object> out = filter.update(stream);
        assertEquals(0l, out.count());
    }

    @Test
    public void evenPredicate() {
        Stream<Integer> stream = Stream.of(1, 2, 3, 6, 7, 8);
        Predicate<Integer> predicate = integer -> integer % 2 == 0;
        StreamFilter<Integer> filter = new StreamFilter<>(predicate);
        Stream<Integer> out = filter.update(stream);
        List<Integer> list = out.collect(Collectors.toList());
        assertEquals(3, list.size()); // Verify exactly 3 elements
        assertEquals(
                new HashSet<>(List.of(2, 6, 8)),
                new HashSet<>(list)); // Verify the 3 elements are the unique values of 2, 6, and 8
    }
}
