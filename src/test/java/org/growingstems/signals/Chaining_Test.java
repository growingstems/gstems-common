/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Consumer;
import java.util.function.Supplier;
import org.growingstems.signals.api.SignalModifier;
import org.junit.jupiter.api.Test;

public class Chaining_Test {
    private static final double k_tolerance = 1e-12;

    private int lastInt = 0;

    @Test
    public void chainingBoolToInt() {
        SignalModifier<Boolean, Boolean> notGate = (in) -> !in;
        assertEquals(false, notGate.update(true));

        SignalModifier<Boolean, Integer> boolInteger = (in) -> in ? 1 : 0;
        SignalModifier<Boolean, Integer> notBoolToInt = notGate.append(boolInteger);

        int notTrue = notBoolToInt.update(true);
        assertEquals(0, notTrue);
    }

    @Test
    public void chainingIntToInt() {
        SignalModifier<Integer, Integer> negateValue = (in) -> -in;
        SignalModifier<Integer, Integer> divideByThree = (in) -> in / 3;
        SignalModifier<Integer, Integer> negateOverThree = negateValue.append(divideByThree);
        int negativeNineOverThree = negateOverThree.update(9);
        assertEquals(-3, negativeNineOverThree);
    }

    @Test
    public void chainingBoolIntStringDouble() {
        SignalModifier<Boolean, Integer> boolToInt = (in) -> in ? 10184 : 284;
        SignalModifier<Integer, String> intToString = (in) -> in.toString();
        SignalModifier<String, Double> stringToDouble = (in) -> Double.parseDouble(in + in + "0.5");
        SignalModifier<Boolean, Double> boolToDoubleFinal =
                boolToInt.append(intToString).append(stringToDouble);
        double outputTrue = boolToDoubleFinal.update(true);
        double outputFalse = boolToDoubleFinal.update(false);
        assertEquals(10184101840.5, outputTrue, k_tolerance);
        assertEquals(2842840.5, outputFalse, k_tolerance);
    }

    private void acceptInt(Integer number) {
        lastInt = number;
    }

    @Test
    public void handle() {
        SignalModifier<Boolean, Integer> handleTest = (in) -> in ? 1 : 0;
        Consumer<Integer> consume = this::acceptInt;
        Consumer<Boolean> returnedConsume = handleTest.handle(consume);

        lastInt = 0;
        returnedConsume.accept(true);
        assertEquals(1, lastInt);
        returnedConsume.accept(false);
        assertEquals(0, lastInt);
    }

    @Test
    public void provide() {
        SignalModifier<Boolean, Integer> supplierTest = (in) -> in ? 1 : 0;
        Supplier<Boolean> supply = () -> {
            return true;
        };
        int supplierOutput = supplierTest.provide(supply).get();

        assertEquals(1, supplierOutput);
    }

    @Test
    public void runnable() {
        SignalModifier<Boolean, Integer> runnableTest = (in) -> in ? 1 : 0;
        Supplier<Boolean> supply = () -> {
            return true;
        };
        Consumer<Integer> consumer = (in) -> {
            acceptInt(in);
        };

        runnableTest.makeRunnable(supply, consumer).run();

        assertEquals(1, lastInt);
        lastInt = 0;
    }
}
