package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

public class Reducer_Test {
    private static final Object nullObj = null;

    @Test
    public void emptySetDoesntReduce() {
        Stream<String> stream = Stream.of();
        Reducer<String> reducer = new Reducer<>(null, (a, b) -> nullObj.toString());
        assertNull(reducer.update(stream));
    }

    @Test
    public void emptySetUsesIdentity() {
        Stream<String> stream = Stream.of();
        Reducer<String> reducer = new Reducer<>("Identity", (a, b) -> a + ", " + b);
        assertEquals("Identity", reducer.update(stream));
    }

    @Test
    public void singleSet() {
        Stream<String> stream = Stream.of("1");
        Reducer<String> reducer = new Reducer<>("Identity", (a, b) -> a + ", " + b);
        assertEquals("Identity, 1", reducer.update(stream));
    }

    @Test
    public void multiSet() {
        Stream<String> stream = Stream.of("1", "2", "3");
        Reducer<String> reducer = new Reducer<>("Identity", (a, b) -> a + ", " + b);
        assertEquals("Identity, 1, 2, 3", reducer.update(stream));
    }

    @Test
    public void product() {
        Stream<Integer> stream = Stream.of(1, 2, 3);
        Reducer<Integer> reducer = new Reducer<>(1, (a, b) -> a * b);
        assertEquals(6, reducer.update(stream));
    }
}
