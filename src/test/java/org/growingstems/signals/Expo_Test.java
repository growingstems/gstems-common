package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.test.TestCore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Expo_Test {
    @Test
    public void neutralAdjustment() {
        var expo = new Expo(0.0);

        double inc = 0.1;
        double start = -1.0;
        double stop = 1.0;

        double input = start;

        while (input < stop) {
            assertEquals(input, expo.update(input), 1.0e-6);
            input += inc;
        }
    }

    @ParameterizedTest
    @CsvSource({
        "-1.0, 1.0, 1.0",
        "-1.0, 0.8, 0.992",
        "-1.0, 0.6, 0.936",
        "-1.0, 0.4, 0.784",
        "-1.0, 0.2, 0.488",
        "-1.0, 0.0, 0.0",
        // ---------------
        "-0.5, 1.0, 1.0",
        "-0.5, 0.8, 0.896",
        "-0.5, 0.6, 0.768",
        "-0.5, 0.4, 0.592",
        "-0.5, 0.2, 0.344",
        "-0.5, 0.0, 0.0",
        // ---------------
        "0.0, 1.0, 1.0",
        "0.0, 0.8, 0.8",
        "0.0, 0.6, 0.6",
        "0.0, 0.4, 0.4",
        "0.0, 0.2, 0.2",
        "0.0, 0.0, 0.0",
        // ---------------
        "0.5, 1.0, 1.0",
        "0.5, 0.8, 0.656",
        "0.5, 0.6, 0.408",
        "0.5, 0.4, 0.232",
        "0.5, 0.2, 0.104",
        "0.5, 0.0, 0.0",
        // ---------------
        "1.0, 1.0, 1.0",
        "1.0, 0.8, 0.512",
        "1.0, 0.6, 0.216",
        "1.0, 0.4, 0.064",
        "1.0, 0.2, 0.008",
        "1.0, 0.0, 0.0",
    })
    public void testInputOutput(double adjustment, double input, double output) {
        var expo = new Expo(adjustment);
        assertEquals(output, expo.update(input), TestCore.k_eps);
        assertEquals(-output, expo.update(-input), TestCore.k_eps);
    }

    @Test
    public void adjustmentRange() {
        var expo = new Expo(0.0);
        assertEquals(0.0, expo.getCoefficient(), 1.0e-6);

        expo = new Expo(0.5);
        assertEquals(0.5, expo.getCoefficient(), 1.0e-6);

        expo = new Expo(-0.5);
        assertEquals(-0.5, expo.getCoefficient(), 1.0e-6);

        // Positive out of range
        expo.setCoefficient(2.0);
        assertEquals(1.0, expo.getCoefficient(), 1.0e-6);

        // Negative out of range
        expo.setCoefficient(-2.0);
        assertEquals(-1.0, expo.getCoefficient(), 1.0e-6);
    }
}
