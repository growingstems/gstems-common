/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Test;

public class Adder_Test {
    private static double k_tolerance = 1e-9;

    private Adder<Double> dAdder = Adder.ofDouble();
    private Adder<Float> fAdder = Adder.ofFloat();
    private Adder<Integer> iAdder = Adder.ofInt();
    private Adder<Long> lAdder = Adder.ofLong();
    private Adder<Velocity> vAdder = Adder.ofUnit(Velocity.ZERO);
    private Adder<Foo> fooAdder = new Adder<>(Foo.ZERO, Foo::add);

    static enum Foo {
        ZERO(0),
        ONE(1),
        TWO(2),
        INVALID(-1);

        final int m_v;

        Foo(int v) {
            m_v = v;
        }

        Foo add(Foo other) {
            if (this == ZERO) {
                return other;
            }
            if (other == ZERO) {
                return this;
            }
            if (this == ONE && other == ONE) {
                return TWO;
            }
            return INVALID;
        }
    }

    @Test
    public void noElements() {
        assertEquals(0.0, dAdder.update(Stream.of()), 0.0);
        assertEquals(0.f, fAdder.update(Stream.of()), 0.f);
        assertEquals(0, iAdder.update(Stream.of()));
        assertEquals(0, lAdder.update(Stream.of()));
        assertTrue(vAdder.update(Stream.of()).eq(Velocity.ZERO));
        assertEquals(Foo.ZERO, fooAdder.update(Stream.of()));
    }

    @Test
    public void oneElement() {
        assertEquals(10.0, dAdder.update(Stream.of(10.0)), 0.0);
        assertEquals(10.f, fAdder.update(Stream.of(10.f)), 0.f);
        assertEquals(10, iAdder.update(Stream.of(10)));
        assertEquals(10l, lAdder.update(Stream.of(10l)));
        assertEquals(
                120.0,
                vAdder.update(Stream.of(Velocity.feetPerSecond(10.0))).asInchesPerSecond(),
                k_tolerance);
        assertEquals(Foo.TWO, fooAdder.update(Stream.of(Foo.TWO)));
    }

    @Test
    public void multipleElements() {
        assertEquals(25.0, dAdder.update(Stream.of(10.0, 20.0, -5.0)), k_tolerance);
        assertEquals(25.f, fAdder.update(Stream.of(10.f, 20.f, -5.f)), k_tolerance);
        assertEquals(25, iAdder.update(Stream.of(10, 20, -5)));
        assertEquals(25l, lAdder.update(Stream.of(10l, 20l, -5l)));
        assertEquals(
                300.0,
                vAdder.update(Stream.of(10.0, 20.0, -5.0).map(Velocity::feetPerSecond)).asInchesPerSecond(),
                k_tolerance);
        assertEquals(Foo.TWO, fooAdder.update(Stream.of(Foo.ONE, Foo.ZERO, Foo.ONE)));
        assertEquals(Foo.INVALID, fooAdder.update(Stream.of(Foo.ONE, Foo.ZERO, Foo.ONE, Foo.TWO)));
    }
}
