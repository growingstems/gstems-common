/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.test.PrintStreamTest;
import org.junit.jupiter.api.Test;

public class Latch_Test extends PrintStreamTest {
    Latch m_lch;

    @Test
    public void defaultConstructorTest() {
        m_lch = new Latch(false);
        assertFalse(m_lch.update(false));
        assertTrue(m_lch.update(true));
        assertTrue(m_lch.update(false));
        assertFalse(m_lch.update(true));
        assertFalse(m_lch.update(true));
        assertFalse(m_lch.update(false));
    }

    @Test
    public void fullConstructorTest() {
        m_lch = new Latch(Edge.FALLING, true);
        assertTrue(m_lch.update(false));
        assertTrue(m_lch.update(true));
        assertFalse(m_lch.update(false));
        assertFalse(m_lch.update(true));
        assertFalse(m_lch.update(true));
        assertTrue(m_lch.update(false));
    }

    @Test
    public void resetTest() {
        m_lch = new Latch(false);
        assertFalse(m_lch.update(false));
        assertTrue(m_lch.update(true));
        assertTrue(m_lch.update(true));
        m_lch.reset(false);
        assertFalse(m_lch.update(true));
    }

    @Test
    public void invalidEdgesTest() {
        m_lch = new Latch(Edge.ANY, false);
        assertErrorOccurred();
        assertEquals(m_lch.getEdge(), Edge.RISING);

        m_lch = new Latch(Edge.NONE, false);
        assertErrorOccurred();
        assertEquals(m_lch.getEdge(), Edge.RISING);
    }
}
