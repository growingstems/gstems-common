/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class SignalFilter_Test {
    private static class Foo {}

    private boolean condition = false;

    @Test
    public void miscFilter() {
        Foo a = new Foo();
        Foo b = new Foo();
        Foo c = new Foo();
        SignalFilter<Foo> filter = new SignalFilter<>(unused -> condition);
        assertNull(filter.update(a));
        assertNull(filter.update(b));
        assertNull(filter.update(c));

        condition = true;
        assertEquals(a, filter.update(a));
        assertEquals(b, filter.update(b));
        assertEquals(c, filter.update(c));

        condition = false;
        assertEquals(c, filter.update(a));
        assertEquals(c, filter.update(b));
        assertEquals(c, filter.update(c));
    }

    @Test
    public void noFilter() {
        SignalFilter<Integer> filter = SignalFilter.noFilter();
        assertEquals(2, filter.update(2));
        assertEquals(-2, filter.update(-2));
        assertEquals(Integer.MAX_VALUE, filter.update(Integer.MAX_VALUE));
        assertEquals(Integer.MIN_VALUE, filter.update(Integer.MIN_VALUE));
    }

    @Test
    public void filterDoubles_none() {
        SignalFilter<Double> filter = SignalFilter.filterDoubles(false, false);
        assertEquals(0.0, filter.update(0.0));
        assertEquals(Double.POSITIVE_INFINITY, filter.update(Double.POSITIVE_INFINITY));
        assertEquals(Double.NEGATIVE_INFINITY, filter.update(Double.NEGATIVE_INFINITY));
        assertTrue(Double.isNaN(filter.update(Double.NaN)));
    }

    @Test
    public void filterDoubles_nan() {
        SignalFilter<Double> filter = SignalFilter.filterDoubles(true, false);
        assertNull(filter.update(Double.NaN));
        assertEquals(0.0, filter.update(0.0));
        assertEquals(Double.POSITIVE_INFINITY, filter.update(Double.POSITIVE_INFINITY));
        assertEquals(Double.NEGATIVE_INFINITY, filter.update(Double.NEGATIVE_INFINITY));
        assertEquals(Double.NEGATIVE_INFINITY, filter.update(Double.NaN));
    }

    @Test
    public void filterDoubles_inf() {
        SignalFilter<Double> filter = SignalFilter.filterDoubles(false, true);
        assertNull(filter.update(Double.POSITIVE_INFINITY));
        assertNull(filter.update(Double.NEGATIVE_INFINITY));
        assertEquals(0.0, filter.update(0.0));
        assertEquals(0.0, filter.update(Double.POSITIVE_INFINITY));
        assertEquals(0.0, filter.update(Double.NEGATIVE_INFINITY));
        assertTrue(Double.isNaN(filter.update(Double.NaN)));
    }

    @Test
    public void filterDoubles_nanInf() {
        SignalFilter<Double> filter = SignalFilter.filterDoubles(true, true);
        assertNull(filter.update(Double.NaN));
        assertNull(filter.update(Double.POSITIVE_INFINITY));
        assertNull(filter.update(Double.NEGATIVE_INFINITY));
        assertEquals(0.0, filter.update(0.0));
        assertEquals(0.0, filter.update(Double.POSITIVE_INFINITY));
        assertEquals(0.0, filter.update(Double.NEGATIVE_INFINITY));
        assertEquals(0.0, filter.update(Double.NaN));
    }
}
