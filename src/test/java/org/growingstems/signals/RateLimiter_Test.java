/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.signals.RateLimiter.LimitingMode;
import org.growingstems.test.TestCore;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class RateLimiter_Test {
    private TestTimeSource m_timeSource = new TestTimeSource();

    @Test
    public void allLimitedTest() {
        RateLimiter rateLimiter =
                new RateLimiter(m_timeSource, Frequency.hertz(0.25), LimitingMode.ALL_MAGNITUDE);

        Double power = rateLimiter.update(0.1);
        assertEquals(0.1, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(1.0));
        power = rateLimiter.update(0.4);
        assertEquals(0.35, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(2.0));
        power = rateLimiter.update(-0.1);
        assertEquals(0.1, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(3.0));
        power = rateLimiter.update(-0.2);
        assertEquals(-0.15, power, TestCore.k_eps);
    }

    @Test
    public void decreasingLimitedTest() {
        RateLimiter rateLimiter =
                new RateLimiter(m_timeSource, Frequency.hertz(0.25), LimitingMode.DECREASING_MAGNITUDE);

        Double power = rateLimiter.update(0.1);
        assertEquals(0.1, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(1.0));
        power = rateLimiter.update(-0.3);
        assertEquals(-0.15, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(2.0));
        power = rateLimiter.update(0.4);
        assertEquals(0.4, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(3.0));
        power = rateLimiter.update(0.1);
        assertEquals(0.15, power, TestCore.k_eps);
    }

    @Test
    public void increasingLimitedTest() {
        RateLimiter rateLimiter =
                new RateLimiter(m_timeSource, Frequency.hertz(0.25), LimitingMode.INCREASING_MAGNITUDE);

        Double power = rateLimiter.update(0.25);
        assertEquals(0.25, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(1.0));
        power = rateLimiter.update(0.8);
        assertEquals(0.5, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(2.0));
        power = rateLimiter.update(0.2);
        assertEquals(0.2, power, TestCore.k_eps);

        m_timeSource.setTime(Time.seconds(3.0));
        power = rateLimiter.update(1.0);
        assertEquals(0.45, power, TestCore.k_eps);
    }

    @Test
    public void resetTest() {
        RateLimiter rateLimiter =
                new RateLimiter(m_timeSource, Frequency.hertz(0.25), LimitingMode.ALL_MAGNITUDE);

        assertEquals(0.25, rateLimiter.update(0.25), TestCore.k_eps);

        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(0.5, rateLimiter.update(1.0), TestCore.k_eps);

        m_timeSource.addTime(Time.seconds(1.0));
        assertEquals(1.0, rateLimiter.reset().update(1.0), TestCore.k_eps);
        assertEquals(1.0, rateLimiter.update(10.0), TestCore.k_eps);
    }

    @Test
    public void peekTest() {
        RateLimiter rateLimiter =
                new RateLimiter(m_timeSource, Frequency.hertz(0.25), LimitingMode.ALL_MAGNITUDE);
        assertFalse(rateLimiter.peek().isPresent());

        rateLimiter.update(0.25);
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(0.25, rateLimiter.peek().get(), TestCore.k_eps);

        m_timeSource.addTime(Time.seconds(1.0));
        rateLimiter.update(1.0);
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(0.5, rateLimiter.peek().get(), TestCore.k_eps);
    }

    @Test
    public void initialValueTest() {
        RateLimiter rateLimiter =
                new RateLimiter(m_timeSource, Frequency.hertz(0.25), LimitingMode.ALL_MAGNITUDE, 2.9);
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(2.9, rateLimiter.peek().get(), TestCore.k_eps);
    }

    @Test
    public void forceUpdateTest() {
        RateLimiter rateLimiter =
                new RateLimiter(m_timeSource, Frequency.hertz(0.25), LimitingMode.ALL_MAGNITUDE, 2.9);
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(2.9, rateLimiter.peek().get(), TestCore.k_eps);

        rateLimiter.forceUpdate(5.6);
        assertTrue(rateLimiter.peek().isPresent());
        assertEquals(5.6, rateLimiter.peek().get(), TestCore.k_eps);
    }
}
