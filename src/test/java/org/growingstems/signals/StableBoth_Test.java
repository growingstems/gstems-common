/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class StableBoth_Test {
    TestTimeSource m_ts = new TestTimeSource();
    private StableBoth m_sb;

    @Test
    public void testFalse() {
        m_sb = new StableBoth(Time.seconds(0.5), Time.seconds(0.2), false, m_ts);
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertTrue(m_sb.update(true));

        // Verify that the timers reset
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true)); // Should trigger reset of timer
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(false));

        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(false)); // Should trigger reset of timer
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
    }

    @Test
    public void testTrue() {
        m_sb = new StableBoth(Time.seconds(0.5), Time.seconds(0.2), true, m_ts);
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertTrue(m_sb.update(true));

        // Verify that the timers reset
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true)); // Should trigger reset of timer
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(false));

        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(false)); // Should trigger reset of timer
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.15));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
    }

    @Test
    public void resetFalse() {
        m_sb = new StableBoth(Time.seconds(0.5), Time.seconds(0.2), false, m_ts);
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_sb.reset();
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
    }

    @Test
    public void resetTrue() {
        m_sb = new StableBoth(Time.seconds(0.5), Time.seconds(0.2), true, m_ts);
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(true));
        m_sb.reset();
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertTrue(m_sb.update(false));
        m_ts.addTime(Time.seconds(0.3));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(true));
    }

    @Test
    public void startingLogicLevel() {
        m_sb = new StableBoth(Time.seconds(0.5), Time.seconds(0.2), true, m_ts);
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true));

        // Set false
        m_sb.setStartingLogicLevel(false);
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true));
        m_sb.reset();
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));

        // Set true
        m_sb.setStartingLogicLevel(true);
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        m_sb.reset();
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true));
    }

    @Test
    public void timeZeroTestFalse() {
        m_sb = new StableBoth(Time.ZERO, Time.seconds(0.1), false, m_ts);
        assertFalse(m_sb.update(true));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
        m_ts.setTime(Time.seconds(1));
        assertTrue(m_sb.update(true));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(true));
    }

    @Test
    public void timeZeroTestTrue() {
        m_sb = new StableBoth(Time.seconds(0.1), Time.ZERO, true, m_ts);
        assertTrue(m_sb.update(false));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
        m_ts.setTime(Time.seconds(1));
        assertFalse(m_sb.update(false));
        assertTrue(m_sb.update(true));
        assertTrue(m_sb.update(false));
    }

    @Test
    public void timeBothZeroTest() {
        m_sb = new StableBoth(Time.ZERO, Time.ZERO, true, m_ts);
        assertTrue(m_sb.update(true));
        assertFalse(m_sb.update(false));
        assertFalse(m_sb.update(false));
        assertTrue(m_sb.update(true));
    }
}
