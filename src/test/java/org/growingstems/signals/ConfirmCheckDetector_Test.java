/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.signals;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.measurements.Measurements.Time;
import org.growingstems.signals.ConfirmCheckDetector.ConfirmState;
import org.growingstems.test.TestTimeSource;
import org.junit.jupiter.api.Test;

public class ConfirmCheckDetector_Test {

    TestTimeSource timeSource = new TestTimeSource();
    ConfirmCheckDetector m_checkDector =
            new ConfirmCheckDetector(Time.seconds(1.0), Time.seconds(1.0), Time.seconds(1.0), timeSource);

    @Test
    public void confirmCheckDetectorTest() {
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false)); // Is at idle
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Idle to trigger

        // Always false the first time - does not record time
        timeSource.addTime(Time.seconds(0.999));
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Is at trigger
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Is at trigger
        timeSource.addTime(Time.seconds(0.002));
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(true)); // Trigger to ConfBegin
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(false)); // ConfBegin to ConfRelease
        timeSource.addTime(Time.seconds(0.5));
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(false)); // Is at ConfRelease
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(true)); // ConfRelease to ConfPress
        timeSource.addTime(Time.seconds(0.999));
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(true)); // Is at ConfPress
        timeSource.addTime(Time.seconds(0.002));
        assertEquals(ConfirmState.CONFIRMED, m_checkDector.update(true)); // ConfPress to Confirmed
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false));
    }

    @Test
    public void confirmCheckDetectorCancelTest() {
        // Cancel output at trigger
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false)); // Is at idle
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Idle to trigger
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false)); // To cancel, then to idle

        // Cancel output at ConfRelease
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false)); // Is at idle
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Idle to trigger
        timeSource.addTime(Time.seconds(0.999));
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Is at trigger
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Is at trigger
        timeSource.addTime(Time.seconds(0.002));
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(true)); // Trigger to ConfBegin
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(false)); // ConfBegin to ConfRelease
        timeSource.addTime(Time.seconds(1.001));
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false)); // ConfRelease to Cancel

        // Cancel output at ConfPress
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false)); // Is at idle
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Idle to trigger
        timeSource.addTime(Time.seconds(0.999));
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Is at trigger
        assertEquals(ConfirmState.IDLE, m_checkDector.update(true)); // Is at trigger
        timeSource.addTime(Time.seconds(0.002));
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(true)); // Trigger to ConfBegin
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(false)); // ConfBegin to ConfRelease
        timeSource.addTime(Time.seconds(0.5));
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(false)); // Is at ConfRelease
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(true)); // ConfRelease to ConfPress
        timeSource.addTime(Time.seconds(0.999));
        assertEquals(ConfirmState.CONFIRMING, m_checkDector.update(true)); // Is at ConfPress
        assertEquals(ConfirmState.IDLE, m_checkDector.update(false)); // ConfPress to Cancel
    }
}
