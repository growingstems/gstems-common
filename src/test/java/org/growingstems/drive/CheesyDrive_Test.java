/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.drive;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.growingstems.drive.CheesyDrive.GearState;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CheesyDrive_Test {

    @ParameterizedTest
    @CsvSource({
        "0.6, 0.6, 1.0, 0",
        "2, 1, 1, 0",
        "-1, 1, 0, -1.0",
        "0.5, -0.8, -0.3, 1",
        "0, 0.4, 0.4, -0.4",
        "0, 0, 0, 0",
        "1, 0, 1, 1",
        "1, 1, 1, 0",
        "0, 1, 1, -1"
    })
    public void cheesyTestDefaultQuick(
            double wheel, double throttle, double expectedL, double expectedR) {
        CheesyDrive cDrive = new CheesyDrive();
        double actualL = cDrive.cheesyDrive(wheel, throttle, GearState.HIGH, true).getX();
        double actualR = cDrive.cheesyDrive(wheel, throttle, GearState.HIGH, true).getY();
        assertEquals(expectedL, actualL, 0.0000001);
        assertEquals(expectedR, actualR, 0.0000001);
    }

    @ParameterizedTest
    @CsvSource({"0, 0, 0, 0", "1, 0, 0, 0", "1, 1, 1, -0.4", "0, 1, 1, -1"})
    public void cheesyTestDefaultSpeed(
            double wheel, double throttle, double expectedL, double expectedR) {
        CheesyDrive cDrive = new CheesyDrive();
        double actualL = cDrive.cheesyDrive(wheel, throttle, GearState.HIGH, false).getX();
        double actualR = cDrive.cheesyDrive(wheel, throttle, GearState.HIGH, false).getY();
        assertEquals(expectedL, actualL, 0.0000001);
        assertEquals(expectedR, actualR, 0.0000001);
    }

    @ParameterizedTest
    @CsvSource({
        "1, 1, HIGH, 1, 1, 1, 0",
        "2, 4, HIGH, 0.4, 0.7, 1, 0.1",
        "2, 4, LOW, 0.4, 0.7, 1, 0.9",
        "3, 7, LOW, -.3, -0.9, -1, -1",
        "0.6, 0.2, HIGH, -0.6, 0.5, 0.14, -0.86"
    })
    public void cheesyTestModifiedQuick(
            double quickHigh,
            double quickLow,
            GearState gear,
            double wheel,
            double throttle,
            double expectedL,
            double expectedR) {
        CheesyDrive cDrive = new CheesyDrive(1, 1, 1, 1, quickLow, quickHigh);
        double actualL = cDrive.cheesyDrive(wheel, throttle, gear, true).getX();
        double actualR = cDrive.cheesyDrive(wheel, throttle, gear, true).getY();
        assertEquals(expectedL, actualL, 0.0000001);
        assertEquals(expectedR, actualR, 0.0000001);
    }

    @ParameterizedTest
    @CsvSource({
        "3, 2, 1, 0, HIGH, 1, 1, 1, 1",
        "0.3, 0, 4, 0.6, LOW, 0.4, 0.6, 1, -0.1296",
        "-0.5, -0.63, 2, 0, HIGH, -0.5, -0.6, -0.7734, 0.4266",
        "1, 1, 1, 1, HIGH, 2, 1, 1, 0",
        "1, 1, 1, 1, LOW, -1, -2, 0, 1",
        "1, 1, 1, 1, HIGH, 0.5, 0.5, 0.75, -0.25",
        "1, 1, 1, 1, LOW, -0.4, 0.75, 0.45, -1",
        "1, 1, 1, 1, LOW, 0.2, -0.7, -0.84, 0.56"
    })
    public void cheesyTestModifiedSpeed(
            double speedHighMax,
            double speedHigh,
            double speedLowMax,
            double speedLow,
            GearState gear,
            double wheel,
            double throttle,
            double expectedL,
            double expectedR) {
        CheesyDrive cDrive = new CheesyDrive(speedLowMax, speedLow, speedHighMax, speedHigh, 1, 1);
        double actualL = cDrive.cheesyDrive(wheel, throttle, gear, false).getX();
        double actualR = cDrive.cheesyDrive(wheel, throttle, gear, false).getY();
        assertEquals(expectedL, actualL, 0.0000001);
        assertEquals(expectedR, actualR, 0.0000001);
    }
}
