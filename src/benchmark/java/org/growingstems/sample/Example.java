package org.growingstems.sample;

import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

/**
 * This is a simple sample of how to use JDH to generate benchmarks. for more information on how to
 * use JDH, please read the following: http://tutorials.jenkov.com/java-performance/jmh.html
 * https://www.baeldung.com/java-microbenchmark-harness https://github.com/openjdk/jmh
 */
@Fork(value = 1) // times to run the benchmark (can also be applied to individual benchmarks)
public class Example {
    @Benchmark
    @Warmup(iterations = 1, time = 100, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 1, time = 100, timeUnit = TimeUnit.MILLISECONDS)
    public void testMethod() {
        /*
         * This method shows various ways to confiugre how long your benchmark takes to run
         * Each benchmark has a set of warmup iterations (I expect to simulate having the JVM
         * been running for a bit before your code gets hit instead of just cold staring your code)
         * and a set of 'Measurement iterations', each of which can be tuned.
         * iterations will tell JMH how many times to run that type of iteration (warmup/measurement)
         * time will tell JMH how long the iteration should run for
         * timeUnit tells JMH what unit your input to 'time' is in.
         * The defaults cause benchmarks to run for quite a while, but if we don't run them
         * automatically and only when we need to test performance sensitive changes the defaults
         * should be fine.
         */
    }

    /*
     * This is one way to do 2 things, store state for different benchmarks, and help prevent
     * JVM optimization from messing with the benchmark results
     *
     * This object can be passed into benchmarks and utilized inside of the benchmark. This
     * has the benefit of any code that happens inside of the state class not affecting the
     * results of the benchmark, since this code will happen before the benchmark starts. This
     * is the prefered way to handle any form of initialization that needs to happen before
     * the code to be measured can run.
     *
     * This is also a method to get around the JVM constant folding optimization
     * Basic gist is that any constants your benchmark will need should be stored in a state
     * object and passed in instead of written in the benchmark itself.
     */
    @State(Scope.Thread)
    public static class MyState {
        public int a = 1;
        public int b = 2;
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @BenchmarkMode(Mode.All)
    public void exampleBench(MyState state, Blackhole bh) {
        int sum = state.a + state.b;
        int prod = state.a * state.b;

        /*
         * This is another way to help avoid JVM optimization messing up results, specifically
         * this is a way to thwart the dead code optimization.
         * If the JVM can detect that results of code aren't used then it will remove the code
         * to make things faster. This means if the benchmark was written as above only, the JVM
         * would detect that 'sum' and 'prod' are never used and simply not run that code.
         * Luckily JMH has a method to avoid this. Any variable that is necessary to the benchmark
         * but isn't used by anything should be passed into the Blackhole object.
         */
        bh.consume(sum);
        bh.consume(prod);
    }
}
