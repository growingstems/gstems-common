image: gradle:jdk17

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

stages:
  - build
  - test
  - package
  - deploy

build:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: build
  script:
    - |
      if [ -n $CI_COMMIT_TAG ]; then
        sed -i "s/version = 'in-dev'/version = \'$CI_COMMIT_TAG\'/" ./build.gradle
      fi
    - ./gradlew --build-cache assemble
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle

test:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: test
  script:
    - |
      if [ -n $CI_COMMIT_TAG ]; then
        sed -i "s/version = 'in-dev'/version = \'$CI_COMMIT_TAG\'/" ./build.gradle
      fi
    - ./gradlew test
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle
  artifacts:
    when: always
    reports:
      junit: build/test-results/test/**/TEST-*.xml

spotlessCheck:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: test
  script: ./gradlew spotlessCheck
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle

docs:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: package
  script:
    - |
      if [ -n $CI_COMMIT_TAG ]; then
        sed -i "s/version = 'in-dev'/version = \'$CI_COMMIT_TAG\'/" ./build.gradle
      fi
    - ./gradlew javadoc
  cache:
    key: "$CI_COMMIT_REF_NAME"
    paths:
      - build
      - .gradle

deploy:
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradle
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - chmod 600 "$SSH_PRIVATE_KEY"
    - cp -p "$SSH_PRIVATE_KEY" ~/.ssh/id_rsa
    - cp "$SSH_KNOWN_HOSTS" ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  stage: deploy
  script:
    - mkdir -p ./build/maven/org/growingstems/gstems-common
    - 'scp -p maven@growingstems.org:$MAVEN_SERVER_PATH/org/growingstems/gstems-common/maven-metadata.* ./build/maven/org/growingstems/gstems-common/ || true'
    - sed -i "s/version = 'in-dev'/version = \'$CI_COMMIT_TAG\'/" ./build.gradle
    - ./gradlew publish
    - 'scp -rp ./build/maven/org/growingstems/gstems-common/* maven@growingstems.org:$MAVEN_SERVER_PATH/org/growingstems/gstems-common'
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle
  after_script:
    - rm -rf ~/.ssh

pages:
  rules:
    - if: $CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^[0-9\.]+$/
  stage: deploy
  script: cp -r build/docs/javadoc public
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle
  artifacts:
    paths:
      - public
